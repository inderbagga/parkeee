package com.parkeee.android;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.slidingmenu.lib.SlidingMenu;

/**
 * Created by user on 30-11-2016.
 */

public class ActivityStaticPages extends Activity implements View.OnClickListener {

    public static SlidingMenu menu;
    Button btnVisitWebsite;
    ImageView img_back, img_logo, img_menu;
    TextView txt_website, txt_content;
    String page = "";
    Intent Intnt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parkeee_org);
        Initialize();
    }

    private void Initialize() {
        MenuFragment.SameActivity="ActivityStaticPage";
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);


        img_menu = (ImageView) findViewById(R.id.img_menu_static_pages);
        img_menu.setOnClickListener(this);

        btnVisitWebsite = (Button) findViewById(R.id.btnVisitWebsite);
        btnVisitWebsite.setOnClickListener(this);
        img_back = (ImageView) findViewById(R.id.img_back_staticpage);
        img_back.setOnClickListener(this);
        img_logo = (ImageView) findViewById(R.id.img_logo_static_page);
        img_logo.setOnClickListener(this);
        txt_website = (TextView) findViewById(R.id.txt_website_static_page);
        txt_content = (TextView) findViewById(R.id.txt_content_static_page);

        Intnt = getIntent();
        if (Intnt != null) {
            if (!Intnt.getBooleanExtra("is_sidemenu", false)) {
                img_back.setVisibility(View.VISIBLE);
                img_menu.setVisibility(View.GONE);

                page = Intnt.getStringExtra("PageName");
                if (page.equals("Parkeee.org")) {
                    txt_website.setText("www.parkeee.org");
                    img_logo.setImageResource(R.drawable.logo_org);
                    txt_content.setText("Parkeee.Org is the private foundation arm of Parkeee. Parkeee is proud to donate 10% of its revenue to help the environment, and to provide educational and technical support in this endeavor.\n" +
                            "Started in July 2016, Parkeee.Org has pledged to plant a minimum of 10,000 trees in its first year. If you wish to volunteer or want us to plant a tree in your backyard or neighborhood, please visit the website and complete the forms.");
                } else if (page.equals("Alsets")) {
                    txt_website.setText("www.alsets.com");
                    img_logo.setImageResource(R.drawable.logo_alsets);
                    txt_content.setText("Parkeee is proud to announce our association with AlSets Recruitment Solutions. AlSets provides a management and recruitment platform that works with small and large companies to find you the right job in the right environment. AlSets can also help you locate the best Internship and Volunteer programs. Visit the Alsets website to learn more.");
                } else if (page.equals("Swatico")) {
                    txt_website.setText("www.swatico.com");
                    img_logo.setImageResource(R.drawable.logo_swatico);
                    txt_content.setText("Swatico IT is the developer of Parkeee. Swatico\\'s team has worked tirelessly to provide the best car care solutions to the marketplace. It\\'s experienced team of award winning developers is now excited to work with you. Swatico is capable of turning your idea into reality. Visit the Swatico website today to learn more.");
                }
            } else {
                img_back.setVisibility(View.GONE);
                img_menu.setVisibility(View.VISIBLE);
                if(Intnt.getIntExtra("child_pos",0)==0){
                    page="Parkeee.org";
                    txt_website.setText("www.parkeee.org");
                    img_logo.setImageResource(R.drawable.logo_org);
                    txt_content.setText("Parkeee.Org is the private foundation arm of Parkeee. Parkeee is proud to donate 10% of its revenue to help the environment, and to provide educational and technical support in this endeavor.\n" +
                            "Started in July 2016, Parkeee.Org has pledged to plant a minimum of 10,000 trees in its first year. If you wish to volunteer or want us to plant a tree in your backyard or neighborhood, please visit the website and complete the forms.");
                }else if(Intnt.getIntExtra("child_pos",0)==1){
                    page="Alsets";
                    txt_website.setText("www.alsets.com");
                    img_logo.setImageResource(R.drawable.logo_alsets);
                    txt_content.setText("Parkeee is proud to announce our association with AlSets Recruitment Solutions. AlSets provides a management and recruitment platform that works with small and large companies to find you the right job in the right environment. AlSets can also help you locate the best Internship and Volunteer programs. Visit the Alsets website to learn more.");
                }else if(Intnt.getIntExtra("child_pos",0)==2){
                    page="Swatico";
                    txt_website.setText("www.swatico.com");
                    img_logo.setImageResource(R.drawable.logo_swatico);
                    txt_content.setText("Swatico IT is the developer of Parkeee. Swatico\\'s team has worked tirelessly to provide the best car care solutions to the marketplace. It\\'s experienced team of award winning developers is now excited to work with you. Swatico is capable of turning your idea into reality. Visit the Swatico website today to learn more.");
                }
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnVisitWebsite:
                if (page.equals("Parkeee.org")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.parkeee.org")));
                } else if (page.equals("Alsets")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.alsets.com")));
                } else if (page.equals("Swatico")) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.swatico.com")));
                }
                break;
            case R.id.img_back_staticpage:
                this.finish();
                break;

            case R.id.img_menu_static_pages:
                menu.toggle();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        if (Intnt != null) {
            if (!Intnt.getBooleanExtra("is_sidemenu", false)) {
                super.onBackPressed();
                this.finish();
            }
        }
    }
}