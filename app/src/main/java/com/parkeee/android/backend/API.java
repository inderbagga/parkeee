package com.parkeee.android.backend;

/**
 * Created by inderbagga on 10/12/16.
 */

public class API {

    private static String BASE_URL=Config.getBaseUrl();

    public static String pInsertUpdateINSByPUID() {

        return BASE_URL+"WebService/InsuranceService.svc/InsertUpdateINSByPUID";
    }

    public static String pViolationInsertUpdate() {

        return BASE_URL+"WebService/InsuranceService.svc/ViolationInsertUpdate";
    }

    public static String gDeleteINSByServerIDs(String strServerIDs) {

        return BASE_URL+"WebService/InsuranceService.svc/DeleteINSByServerIDs/"+strServerIDs;
    }

    public static String pUserBillingInsertUpdate() {

        return BASE_URL+"WebService/ParkeeeUser.svc/UserBillingInsertUpdate";
    }

    public static String  gDeleteUserBillByIDs(String strBillIDs) {

        return BASE_URL+"WebService/ParkeeeUser.svc/DeleteUserBillByIDs/"+strBillIDs;
    }

    public static String  gGetStatesForViolations() {

        return BASE_URL+"WebService/InsuranceService.svc/GetStatesForViolations";
    }

    public static String  gDeleteVLByServerIDs(String strPUID,String strServerIDs) {

        return BASE_URL+"WebService/InsuranceService.svc/DeleteVLByServerIDs/"+strServerIDs+"/"+strPUID;
    }

    public static String fInsertUpdateRegisterPUID() {
        return BASE_URL+"WebService/ParkeeeUser.svc/NewUser ";
    }

    public static String fInsertUpdateRegisterReferralPUID() {
        return BASE_URL+"WebService/ParkeeeUser.svc/SaveUserReferral";
    }

    public static String fInsertUpdateSaveInboxMessage() {
        return BASE_URL+"WebService/ParkeeeUser.svc/UserInboxInsert";
    }

    public static String fOTPsent() {
        return BASE_URL+"WebService/ParkeeeUser.svc/SendValidateOTPonMobile/";
    }

    public static String fMobileVerification() {
        return BASE_URL+"WebService/ParkeeeUser.svc/VerifyUserMobileNumber/";
    }

    public static String fUpdateInsertUpdateRegisterPUID() {
        return BASE_URL+"WebService/ParkeeeUser.svc/UpdateUser ";
    }

    public static String fUploadProfilePicture() {
        return BASE_URL+"WebService/ParkeeeUser.svc/UploadProfilePic";
    }

    public static String fCheckEmailVerifiedOrNot() {
        return BASE_URL+"WebService/ParkeeeUser.svc/CheckEmailVerifiedOrNot/";
    }

    public static String fResendEmailVerificationLink() {
        return BASE_URL+"WebService/ParkeeeUser.svc/ResendEmailVerificationLink/";
    }
    public static String fChangePassword() {
        return BASE_URL+"WebService/ParkeeeUser.svc/ChangePassword";
    }

    public static String fAddNewCarPUID() {
        return BASE_URL+"WebService/ParkeeeUser.svc/RegNewCar";
    }
    public static String fUpdateCarPUID() {
        return BASE_URL+"WebService/ParkeeeUser.svc/UpdateCar";
    }

    public static String fDeleteCar() {
        return BASE_URL+"WebService/ParkeeeUser.svc/DeleteCar";
    }

    public static String fValidateUser() {
        return BASE_URL+"WebService/ParkeeeUser.svc/LoginUser";
    }
    public static String fLoginNewDevice() {
        return BASE_URL+"WebService/ParkeeeUser.svc/LoginNewDevice";
    }

    public static String fForgotPassword() {
        return BASE_URL+"WebService/ParkeeeUser.svc/ForgotPassword/";
    }

    public static String fGSManagerLogin() {
        return BASE_URL+"WebService/GasStationService.svc/GSManagerLogin";
    }

    public static String fGSPriceUpdate() {
        return BASE_URL+"WebService/GasStationService.svc/GSPriceUpdate";
    }

    public static String fSearchGasStationPlaces() {
        return "https://maps.googleapis.com/maps/api/place/autocomplete/json?sensor=false&key=AIzaSyCwlCbRk9LG7TqyNUITHvIT_Bh7a_MlvBo&components=country:usa&input=";
    }

    public static String fgetLocationLatLong() {
        return "http://maps.google.com/maps/api/geocode/json?sensor=false&address=";
    }

    public static String fGetGasStationUrl() {
        return  BASE_URL+"WebService/GasStationService.svc/GetGasStations/";
    }

    public static String fGetGasStationDetailById() {
        return  BASE_URL+"WebService/GasStationService.svc/GetGasStationDetailById/";
    }
    public static String fSaveGSRatingByUser() {
        return  BASE_URL+"WebService/GasStationService.svc/SaveGSRatingByUser";
    }

    public static String fSaveGasStationReviewReply() {
        return  BASE_URL+"WebService/GasStationService.svc/SaveGasStationReviewReply";
    }

    public static String fSaveDealLikeDislike() {
        return  BASE_URL+"WebService/ParkeeeUser.svc/SaveDealLikeDislike";
    }

    public static String fAddGSInFav() {
        return  BASE_URL+"WebService/ParkeeeUser.svc/AddGSInFav";
    }

    public static String fRemoveGSFromFav() {
        return  BASE_URL+"WebService/ParkeeeUser.svc/RemoveGSFromFav";
    }

    public static String fGetReviewRepliesByReviewId() {
        return BASE_URL+"WebService/GasStationService.svc/GetReviewRepliesByReviewId/";
    }

    public static String fGetUserFavGasStations() {
        return BASE_URL+"WebService/GasStationService.svc/GetUserFavGasStations/";
    }

    public static String fSendBulkSMS() {
        return BASE_URL+"WebService/ParkeeeUser.svc/SendBulkSMS";
    }

    public static String fGetReviewsByGSID() {
        return BASE_URL+"WebService/GasStationService.svc/GetReviewsByGSID/";
    }




}
