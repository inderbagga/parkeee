package com.parkeee.android.backend;

import android.app.Activity;
import android.app.Application;
import android.support.v4.app.Fragment;
import android.util.Log;

import com.parkeee.android.model.params.ViolationInsertUpdate;
import com.parkeee.android.model.tables.AddNewCarTb;
import com.parkeee.android.model.tables.GSFavouriteTb;
import com.parkeee.android.model.tables.InsertUpdateCache;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.model.tables.States;
import com.parkeee.android.model.tables.UserBillingInsertUpdateCache;
import com.parkeee.android.model.tables.ViolationInsertUpdateCache;

import io.realm.Realm;
import io.realm.RealmResults;

public class RealmController {

    private static RealmController instance;
    private final Realm realm;

    public RealmController(Application application) {
        realm = Realm.getDefaultInstance();
    }

    public static RealmController with(Fragment fragment) {

        if (instance == null) {
            instance = new RealmController(fragment.getActivity().getApplication());
        }
        return instance;
    }

    public static RealmController with(Activity activity) {

        if (instance == null) {
            instance = new RealmController(activity.getApplication());
        }
        return instance;
    }

    public static RealmController with(Application application) {

        if (instance == null) {
            instance = new RealmController(application);
        }
        return instance;
    }

    public static RealmController getInstance() {

        return instance;
    }

    public Realm getRealm() {

        return realm;
    }

    public void clearInsuranceReminders() {

        realm.beginTransaction();
        realm.where(InsertUpdateCache.class).findAll().clear();
        realm.commitTransaction();
    }

    public boolean hasViolationsReminder() {
        return realm.where(ViolationInsertUpdateCache.class).findAll().size()>0?true:false;
    }

    public boolean hasStates() {
        return realm.where(States.class).findAll().size()>0?true:false;
    }

    public boolean hasInsuranceReminders() {
        return realm.where(InsertUpdateCache.class).findAll().size()>0?true:false;
    }

    public boolean hasOtherBills() {
        return realm.where(UserBillingInsertUpdateCache.class).findAll().size()>0?true:false;
    }

    //check if duplicate record already present
    public boolean containsPolicy(String strPolicyNumber) {

        return realm.where(InsertUpdateCache.class).contains("insurance_id",strPolicyNumber).findAll().size()>0?true:false;
    }

    public boolean containsViolation(String strViolationNumber) {

        return realm.where(ViolationInsertUpdateCache.class).contains("violation_number",strViolationNumber).findAll().size()>0?true:false;
    }

    public InsertUpdateCache getInsuranceReminder(String id) {

        return realm.where(InsertUpdateCache.class).equalTo("insurance_id", id).findFirst();
    }

    public UserBillingInsertUpdateCache getOtherBill(long id) {

        return realm.where(UserBillingInsertUpdateCache.class).equalTo("timeStamp", id).findFirst();
    }

    public RealmResults<InsertUpdateCache> getInsuranceReminders() {
        return realm.where(InsertUpdateCache.class).equalTo("isINSDeleted","0").findAll();
    }

    public RealmResults<UserBillingInsertUpdateCache> getOtherBills() {
        return realm.where(UserBillingInsertUpdateCache.class).equalTo("isINSDeleted","0").findAll();
    }

    public RealmResults<States> getStates() {
        return realm.where(States.class).findAll();
    }

    public RealmResults<ViolationInsertUpdateCache> getViolationsReminder() {
        return realm.where(ViolationInsertUpdateCache.class).equalTo("isINSDeleted","0").findAll();
    }

    public void removeInsuranceReminder(long strTempId) {

        RealmResults<InsertUpdateCache> result=realm.where(InsertUpdateCache.class).equalTo("timeStamp",strTempId).findAll();

        realm.beginTransaction();
        result.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void removeOtherBill(long strTempId) {

        RealmResults<UserBillingInsertUpdateCache> result=realm.where(UserBillingInsertUpdateCache.class).equalTo("timeStamp",strTempId).findAll();

        realm.beginTransaction();
        result.deleteAllFromRealm();
        realm.commitTransaction();
    }

    public void removeViolationReminder(long strTempId) {

        RealmResults<ViolationInsertUpdateCache> result=realm.where(ViolationInsertUpdateCache.class).equalTo("timeStamp",strTempId).findAll();

        realm.beginTransaction();
        result.deleteAllFromRealm();
        realm.commitTransaction();
    }




    public void clearRegisterInformation() {
        realm.beginTransaction();
        realm.where(InsertUpdateRegister.class).findAll().clear();
        realm.commitTransaction();
    }

    public void clearAddNewCarTb() {
        realm.beginTransaction();
        realm.where(AddNewCarTb.class).findAll().clear();
        realm.commitTransaction();
    }

    public InsertUpdateRegister getRegisterInformation() {

        return realm.where(InsertUpdateRegister.class).findFirst();
    }


    public RealmResults<AddNewCarTb> getAddedCarsInformation() {
        return realm.where(AddNewCarTb.class).findAll();
    }


    public boolean hasRegisterInformation() {
        return realm.where(InsertUpdateRegister.class).findAll().size()>0?true:false;
    }

    public AddNewCarTb getCarItem(String id) {
        return realm.where(AddNewCarTb.class).equalTo("id", id).findFirst();
    }


    public void removeCarItems(String Id) {
        AddNewCarTb result=realm.where(AddNewCarTb.class).equalTo("id",Id).findFirst();
        realm.beginTransaction();
        result.deleteFromRealm();
        realm.commitTransaction();
        Log.e("result size ",""+"Deleted");

    }

    public void removeFavStations(String Id) {
        GSFavouriteTb result=realm.where(GSFavouriteTb.class).equalTo("gsID",Id).findFirst();
        realm.beginTransaction();
        result.deleteFromRealm();
        realm.commitTransaction();
        Log.e("result size ",""+"Deleted");
    }

    public GSFavouriteTb getGasStationFav(String id) {
        return realm.where(GSFavouriteTb.class).equalTo("gsID", id).findFirst();
    }

    public RealmResults<GSFavouriteTb> getFavGasStation() {
        return realm.where(GSFavouriteTb.class).findAll();
    }

    public void clearGSFavouriteTb() {
        realm.beginTransaction();
        realm.where(GSFavouriteTb.class).findAll().deleteAllFromRealm();
        realm.commitTransaction();
    }
}
