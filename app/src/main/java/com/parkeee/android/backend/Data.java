package com.parkeee.android.backend;

/**
 * Created by inderbagga on 05/12/16.
 */

public class Data {

    public static String[] strArrayCompanies={"21st Century", "AAA", "Access", "Alliance", "Allstate", "American Family", "Anchor", "Auto-Owner Insurance", "Dairyland", "Erie", "Esurance", "Farmers", "Foremost", "GEICO", "Hartford", "Infinity", "Kemper", "Liberty Mutual", "Mercury", "Metlife", "National General", "Nationwide", "Progessive", "State Farm", "Travelers", "USAA", "Other"};
    public static String[] strArrayCategories={"Rent", "Medical", "Education", "Daycare", "Loan", "Utility", "Other"};
    public static String[] strArrayPremiumModes={"Yearly", "Half-yearly", "Quarterly", "Monthly"};
    public static String[] strArrayViolationTypes={"Parking Ticket", "Red Light Violation", "Bus Lane Violation", "Speed Camera Violation"};
    public static String[] strArrayReminderTypes={"Weekly", "Monthly", "Quarterly", "Half-yearly", "Yearly"};
    public static String[] strArrayDayNames={"Sunday","Monday", "Tuesday", "Wednesday", "Thursday", "Friday","Saturday"};
    public static String[] strArrayMonthNames={"January", "February", "March", "April", "May", "June", "July", "August", "September", "October", "November", "December"};
}
