package com.parkeee.android.backend;

/**
 * Created by inderbagga on 10/12/16.
 */

public enum RequestSource {

    InsertUpdateINSByPUID,
    DeleteINSByServerIDs,
    UserBillingInsertUpdate,
    DeleteUserBillByIDs,
    ViolationInsertUpdate,
    DeleteVLByServerIDs,
    GetStatesForViolations,

    InsertUpdateRegisterPUID,
    InsertReferralRegister,
    SaveInboxMessage,
    SaveInboxMessageRef,
    MobileVerification,
    OTPSent,
    UpdateInsertUpdateRegisterPUID,
    CheckEmailVerified,
    ResendEmailVerification,
    ChangePassword,
    UplaodProfilePic,
    AddNewCarPUID,
    UpdateCar,
    DeleteCar,
    ValidateUser,
    LoginNewDevice,
    ForgotPassword,
    GSManagerLogin,
    GSPriceUpdate,
    SearchGasStationPlaces,
    LocationLatLong,
    GetGasStations,
    fGetGasStationDetailById,
    SaveGSRatingByUser,
    SaveGasStationReviewReply,
    SaveDealLikeDislike,
    GSFavourite,
    GSFavouriteRemove,
    GetReviewRepliesByReviewId,
    GetUserFavGasStations,
    SendBulkSMS,
    GetReviewsByGSID
}
