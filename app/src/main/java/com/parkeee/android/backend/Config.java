package com.parkeee.android.backend;

/**
 * Created by inderbagga on 08/12/16.
 */

public class Config {

    private static String BASE_URL="http://owner.parkeee.net/";
    private static String IMAGE_BASE_URL="http://owner.parkeee.net/owner/";

    public static String getBaseUrl() {
        return BASE_URL;
    }

    public static String getImageBaseUrl() {
        return IMAGE_BASE_URL;
    }

    public static long TAP_DELAY=1000;
    public static String DATE_FORMAT="MMM dd, yyyy";
    public static String VL_DATE_FORMAT="MM-dd-yyyy";


}
