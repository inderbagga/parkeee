package com.parkeee.android.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.parkeee.android.R;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.utils.Utils;

/**
 * Created by user on 02-12-2016.
 */

public class ActivityReferralInbox extends Activity implements View.OnClickListener {

    TextView txt_founder,txt_referral_code_inbox,txt_username_ref_inbox;
    ImageView img_back;
    InsertUpdateRegister oinsertupdateregister;
    Context mContext;
    Button btn_invite_friends;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_referral);
        Initialize();
    }

    private void Initialize() {
        mContext=this;
        oinsertupdateregister = RealmController.with(this).getRegisterInformation();
        txt_referral_code_inbox=(TextView)findViewById(R.id.txt_referral_code_inbox);
        txt_referral_code_inbox.setText(oinsertupdateregister.getReferal_code());
        txt_founder=(TextView)findViewById(R.id.txt_founder);
        txt_founder.setText("Founder & CEO");
        img_back=(ImageView)findViewById(R.id.img_back_inbox_ref);
        img_back.setOnClickListener(this);

        txt_username_ref_inbox=(TextView)findViewById(R.id.txt_username_ref_inbox);
        txt_username_ref_inbox.setText(Utils.getUserName(mContext));

        btn_invite_friends=(Button)findViewById(R.id.btn_invite_friends);
        btn_invite_friends.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String shareBody = getResources().getString(R.string.referal_message) + " Download Parkeee app and use Referral code: " + oinsertupdateregister.getReferal_code() + " \n" + Utils.URL_APPDOWNLOAD;
                Intent sharingIntent = new Intent(Intent.ACTION_SEND);
                sharingIntent.setType("text/plain");
                sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Shared via Parkeeee");
                sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody);
                startActivity(Intent.createChooser(sharingIntent, "Invite Friends"));
            }
        });
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.img_back_inbox_ref:
                this.finish();
                break;
            default:
                break;
        }
    }
}
