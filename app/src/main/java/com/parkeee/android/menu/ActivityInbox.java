package com.parkeee.android.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parkeee.android.MenuFragment;
import com.parkeee.android.R;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.slidingmenu.lib.SlidingMenu;


/**
 * Created by user on 02-12-2016.
 */

public class ActivityInbox extends Activity implements View.OnClickListener {
    ImageView img_menu;
    public static SlidingMenu menu;
    LinearLayout ll_referral,ll_welcome;
    TextView txt_referral_code,txt_welcome_date,txt_referral_date;
    String Referral_Code,Referral_Date;
    InsertUpdateRegister oinsertupdateregister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox);
        Initialize();
    }

    private void Initialize() {
        img_menu = (ImageView) findViewById(R.id.img_menu_inbox);
        img_menu.setOnClickListener(this);

        txt_referral_code=(TextView)findViewById(R.id.txt_referral_code);
        txt_welcome_date=(TextView)findViewById(R.id.txt_welcome_date);
        txt_referral_date=(TextView)findViewById(R.id.txt_referral_date);

        oinsertupdateregister = RealmController.with(this).getRegisterInformation();
        Referral_Code=oinsertupdateregister.getReferal_code();
        Referral_Date=oinsertupdateregister.getReferral_date();

        txt_referral_code.setText(Referral_Code);
        txt_welcome_date.setText(Referral_Date);
        txt_referral_date.setText(Referral_Date);

        MenuFragment.SameActivity="Inbox";
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);

        ll_referral=(LinearLayout)findViewById(R.id.ll_referral);
        ll_welcome=(LinearLayout)findViewById(R.id.ll_welcome);

        ll_referral.setOnClickListener(this);
        ll_welcome.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.img_menu_inbox:
                menu.toggle();
                break;

            case R.id.ll_referral:
                startActivity(new Intent(ActivityInbox.this,ActivityReferralInbox.class));
                break;
            case R.id.ll_welcome:
                startActivity(new Intent(ActivityInbox.this,ActivityWelcomeInbox.class));
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }
}
