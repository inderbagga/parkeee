package com.parkeee.android.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.parkeee.android.R;
import com.parkeee.android.WebViewActivity;
import com.parkeee.android.utils.Utils;

/**
 * Created by user on 05-12-2016.
 */

public class ActivityAboutUs extends Activity implements View.OnClickListener {

    ImageView img_back_aboutus;
    TextView txt_readmore_aboutus;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about_us);
        InitializeInterface();
    }

    private void InitializeInterface() {
        img_back_aboutus=(ImageView)findViewById(R.id.img_back_aboutus);
        img_back_aboutus.setOnClickListener(this);

        txt_readmore_aboutus=(TextView)findViewById(R.id.txt_readmore_aboutus);

        SpannableString ss = new SpannableString(Html.fromHtml("You can read more about Terms &amp; Conditions, EULA and Privacy Policy"));
        Log.e("Length", "-----" + ss.length());
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                newActivity(Utils.URL_TC, "Terms & Conditions");

            }
        };
        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                newActivity(Utils.URL_EULA, "EULA");
            }
        };
        ClickableSpan span3 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                newActivity(Utils.URL_PP, "Privacy Policy");
            }
        };
        ss.setSpan(span1, 23, 42, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, 43, 48, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span3, 52, 67, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        txt_readmore_aboutus.setText(ss);
        txt_readmore_aboutus.setMovementMethod(LinkMovementMethod.getInstance());
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.img_back_aboutus:
                this.finish();
                break;
        }
    }

    void newActivity(String url, String title) {
        Intent IWV= new Intent(this, WebViewActivity.class);
        IWV.putExtra("url",url);
        IWV.putExtra("title",title);
        startActivity(IWV);
    }
}
