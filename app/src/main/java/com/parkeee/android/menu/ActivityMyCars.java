package com.parkeee.android.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parkeee.android.MenuFragment;
import com.parkeee.android.R;
import com.parkeee.android.adapter.MyCarsAdapter;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.model.tables.AddNewCarTb;
import com.slidingmenu.lib.SlidingMenu;

import io.realm.RealmResults;


/**
 * Created by user on 03-12-2016.
 */

public class ActivityMyCars extends Activity implements View.OnClickListener {

    ImageView img_menu;
    public static SlidingMenu menu;
    Button btn_add_car;
    RealmResults<AddNewCarTb> getAddedCarsInformation;
    ListView lv_MyCars;
    TextView txt_mycars_noresult;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_cars);
        Initialize();
    }

    private void Initialize() {
        txt_mycars_noresult=(TextView)findViewById(R.id.txt_mycars_noresult);

        lv_MyCars=(ListView)findViewById(R.id.lv_MyCars);
        getAddedCarsInformation = RealmController.with(this).getAddedCarsInformation();
        if(getAddedCarsInformation.size()>0){
            lv_MyCars.setAdapter(new MyCarsAdapter(getAddedCarsInformation,this));
            txt_mycars_noresult.setVisibility(View.GONE);
        }else{
            txt_mycars_noresult.setVisibility(View.VISIBLE);
        }

        img_menu = (ImageView) findViewById(R.id.img_menu_Mycar);
        img_menu.setOnClickListener(this);

        MenuFragment.SameActivity="MyCars";
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);
        btn_add_car=(Button)findViewById(R.id.btn_add_car);
        btn_add_car.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_add_car:
                Intent intnt=new Intent(ActivityMyCars.this,ActivityAddCar.class);
                intnt.putExtra("is_Edit",false);
                startActivity(intnt);
                break;

            case R.id.img_menu_Mycar:
                menu.toggle();

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }
}
