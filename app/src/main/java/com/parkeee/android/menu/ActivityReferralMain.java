package com.parkeee.android.menu;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.text.Html;

import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parkeee.android.MenuFragment;
import com.parkeee.android.R;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.slidingmenu.lib.SlidingMenu;

/**
 * Created by user on 03-12-2016.
 */

public class ActivityReferralMain extends Activity implements View.OnClickListener{
    ImageView img_menu;
    public static SlidingMenu menu;
    Button btnShare_referral;
    EditText et_referral_content;
    TextView txt_referral_content;
    InsertUpdateRegister oinsertupdateregister;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_referral_main);
        Initialize();
    }

    private void Initialize() {
        oinsertupdateregister = RealmController.with(this).getRegisterInformation();
        img_menu = (ImageView) findViewById(R.id.img_menu_ref_main);
        img_menu.setOnClickListener(this);

        MenuFragment.SameActivity="ReferralMain";
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);

        btnShare_referral=(Button)findViewById(R.id.btnShare_referral);
        btnShare_referral.setOnClickListener(this);

        et_referral_content=(EditText)findViewById(R.id.et_referral_content);
        txt_referral_content=(TextView)findViewById(R.id.txt_referral_content);

        txt_referral_content.setText(Html.fromHtml(txt_referral_content.getText().toString().replaceAll("MJ5691", "<font color='#f47d31'>" + oinsertupdateregister.getReferal_code() + "</font>")));
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_menu_ref_main:
                menu.toggle();
                break;

            case R.id.btnShare_referral:
                Intent intnt= new Intent(ActivityReferralMain.this,Search.class);
                intnt.putExtra("Body",et_referral_content.getText().toString().trim());
                startActivity(intnt);

                break;
        }
    }
    @Override
    public void onBackPressed() {
    }
}
