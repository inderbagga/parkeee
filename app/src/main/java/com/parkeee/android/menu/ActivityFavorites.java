package com.parkeee.android.menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parkeee.android.ActivityVerification;
import com.parkeee.android.MenuFragment;
import com.parkeee.android.R;
import com.parkeee.android.adapter.FavGasStationAdapter;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.tables.GSFavouriteTb;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Utils;
import com.slidingmenu.lib.SlidingMenu;

import org.json.JSONArray;
import org.json.JSONException;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by user on 02-12-2016.
 */

public class ActivityFavorites extends Activity implements View.OnClickListener, OnResultReceived {

    ImageView img_menu;
    public static SlidingMenu menu;
    TextView txt_fav_noresult;
    ListView lv_favorite_gas_stations;
    Button btn_sync_fav;
    String ServerResult;
    OnResultReceived mOnResultReceived;
    ProgressDialog mProgressDialog;
    Context mContext;

    private Realm realm;

    RealmResults<GSFavouriteTb> getFavGasStation;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_favorites);
        Initialize();
    }

    private void Initialize() {
        mContext = this;
        mOnResultReceived = this;

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        img_menu = (ImageView) findViewById(R.id.img_menu_fav);
        img_menu.setOnClickListener(this);

        MenuFragment.SameActivity = "Favorites";
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);

        btn_sync_fav = (Button) findViewById(R.id.btn_sync_favourites);
        btn_sync_fav.setOnClickListener(this);

        txt_fav_noresult = (TextView) findViewById(R.id.txt_fav_noresult);
        lv_favorite_gas_stations = (ListView) findViewById(R.id.lv_favorite_gas_stations);

        getFavGasStation = RealmController.with(this).getFavGasStation();
        if (getFavGasStation.size() > 0) {
            lv_favorite_gas_stations.setAdapter(new FavGasStationAdapter(getFavGasStation, this));
            txt_fav_noresult.setVisibility(View.GONE);
        } else {
            txt_fav_noresult.setVisibility(View.VISIBLE);
        }

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_menu_fav:
                menu.toggle();
                break;

            case R.id.btn_sync_favourites:
                String Url = API.fGetUserFavGasStations();
                Url = Url.concat(Utils.getPuid(mContext));
                mProgressDialog.show();
                GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
                oInsertUpdateApi.setRequestSource(RequestSource.GetUserFavGasStations);
                oInsertUpdateApi.executeGetRequest(Url);
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }

    @Override
    protected void onResume() {
        super.onResume();
        getFavGasStation = RealmController.with(this).getFavGasStation();
        if (getFavGasStation.size() > 0) {
            lv_favorite_gas_stations.setAdapter(new FavGasStationAdapter(getFavGasStation, this));
            txt_fav_noresult.setVisibility(View.GONE);
        } else {
            lv_favorite_gas_stations.setAdapter(new FavGasStationAdapter(getFavGasStation, this));
            txt_fav_noresult.setVisibility(View.VISIBLE);
        }
    }

    @Override
    public void dispatchString(RequestSource from, final String what) {
        mProgressDialog.dismiss();
        if (from.toString().equalsIgnoreCase("GetUserFavGasStations")) {

            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            RealmController oRealmController= new RealmController((Application) mContext.getApplicationContext());
                            oRealmController.clearGSFavouriteTb();
                            try {
                                JSONArray ja_fav= new JSONArray(what);
                                for(int m=0;m<ja_fav.length();m++){
                                    GSFavouriteTb oGSFavouriteTb = new GSFavouriteTb();
                                    oGSFavouriteTb.setBrandName(ja_fav.getJSONObject(m).getString("BrandName"));
                                    oGSFavouriteTb.setGsID(ja_fav.getJSONObject(m).getString("GSId"));
                                    oGSFavouriteTb.setLat(ja_fav.getJSONObject(m).getString("Latitude"));
                                    oGSFavouriteTb.setLng(ja_fav.getJSONObject(m).getString("Longitude"));
                                    oGSFavouriteTb.setStreet(ja_fav.getJSONObject(m).getString("Street"));
                                    oGSFavouriteTb.setCity(ja_fav.getJSONObject(m).getString("City"));
                                    oGSFavouriteTb.setZip(ja_fav.getJSONObject(m).getString("Zip"));
                                    oGSFavouriteTb.setPricePer(ja_fav.getJSONObject(m).getString("PricePerGallonInDoller"));
                                    oGSFavouriteTb.setCoffeeDeal(ja_fav.getJSONObject(m).getString("CoffeeDeal"));
                                    oGSFavouriteTb.setLastUpdateOn(ja_fav.getJSONObject(m).getString("LastUpdateOn"));
                                    oGSFavouriteTb.setPkg(ja_fav.getJSONObject(m).getString("PackageName"));
                                    oGSFavouriteTb.setFtid("1");
                                    // Persist your data easily
                                    realm.beginTransaction();
                                    realm.copyToRealm(oGSFavouriteTb);
                                    realm.commitTransaction();
                                }

                                getFavGasStation = RealmController.with((Application)mContext.getApplicationContext()).getFavGasStation();
                                if (getFavGasStation.size() > 0) {
                                    lv_favorite_gas_stations.setAdapter(new FavGasStationAdapter(getFavGasStation, (Application) mContext.getApplicationContext()));
                                    txt_fav_noresult.setVisibility(View.GONE);
                                } else {
                                    lv_favorite_gas_stations.setAdapter(new FavGasStationAdapter(getFavGasStation, (Application) mContext.getApplicationContext()));
                                    txt_fav_noresult.setVisibility(View.VISIBLE);
                                }

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });

                    break;
            }
        }
    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityFavorites.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }
}
