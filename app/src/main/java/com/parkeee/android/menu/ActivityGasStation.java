package com.parkeee.android.menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import com.parkeee.android.MenuFragment;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RequestSource;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.model.params.GSManagerLogin;
import com.parkeee.android.processor.PostApiClient;
import com.slidingmenu.lib.SlidingMenu;

import org.json.JSONArray;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by user on 06-12-2016.
 */

public class ActivityGasStation extends Activity implements View.OnClickListener,OnResultReceived {

    public static SlidingMenu menu;
    ImageView img_menu;
    Button btnGo_Gas_Station;
    EditText et_gas_station_id, et_gas_station_security_pin;
    String ServerResult;
    Context mContext;
    OnResultReceived mOnResultReceived;
    Gson oGson;
    ProgressDialog mProgressDialog;

    Dialog mdialog;
    Button btn_positive_custom_dailog;
    TextView txt_custom_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gas_station);
        InitializeInterface();
    }

    private void InitializeInterface() {
        mContext=this;
        mOnResultReceived=this;
        oGson=new Gson();

        mdialog = new  Dialog(ActivityGasStation.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });


        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        img_menu = (ImageView) findViewById(R.id.img_menu_gas_station);
        img_menu.setOnClickListener(this);

        MenuFragment.SameActivity = "GasStation";
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);

        btnGo_Gas_Station = (Button) findViewById(R.id.btnGo_Gas_Station);
        btnGo_Gas_Station.setOnClickListener(this);

        et_gas_station_id = (EditText) findViewById(R.id.et_gas_station_id);
        et_gas_station_security_pin = (EditText) findViewById(R.id.et_gas_station_security_pin);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_menu_gas_station:
                menu.toggle();
                break;

            case R.id.btnGo_Gas_Station:

                if (et_gas_station_id.getText().toString().trim().equals("")) {

                    ErrorMessage("Gas Station ID field can't be blank.");
                } else if (et_gas_station_security_pin.getText().toString().trim().equals("")) {

                    ErrorMessage("Security PIN can't be blank.");
                } else {

                    GSManagerLogin oGSManagerLogin = new GSManagerLogin();
                    oGSManagerLogin.setGSId(et_gas_station_id.getText().toString().trim());
                    oGSManagerLogin.setPIN(et_gas_station_security_pin.getText().toString().trim());

                    String jsonString=oGson.toJson(oGSManagerLogin,GSManagerLogin.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi=new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.GSManagerLogin);
                    oInsertUpdateApi.executePostRequest(API.fGSManagerLogin(),jsonString);

                }
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }

    private void ErrorMessage(String content) {
        txt_custom_dialog.setText(content);
        mdialog.show();
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        mProgressDialog.dismiss();
        //ServerResult = what.replace("\"", "");
        ServerResult = what;
        ServerResult = ServerResult.replace("\"{", "{");
        ServerResult = ServerResult.replace("}\"", "}");
        ServerResult = ServerResult.replace("\\", "");

        try {
            JSONObject obj = new JSONObject(ServerResult);
            ServerResult = obj.getString("result");

            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                    displayAlert("Invalid Gas Station ID or Security PIN!.", false);
                    break;
                case "-1":
                    displayAlert("Your Gas Station member account is blocked.", false);
                    break;
                default:
                    JSONArray jarray=obj.getJSONArray("Prices");
                    Intent intnt= new Intent(ActivityGasStation.this, ActivityUpdateGasPrice.class);
                    intnt.putExtra("json", jarray.toString());
                    startActivity(intnt);
                    break;
            }
        }catch (Exception e){
        }
    }
    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityGasStation.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {

                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }
}
