package com.parkeee.android.menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.model.params.InsertUpdateRegisterPUID;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.PostApiClient;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;

/**
 * Created by user on 02-12-2016.
 */

public class ActivityPersonalInfo extends Activity implements View.OnClickListener,OnResultReceived {
    Spinner sp_country;
    ImageView img_back;
    EditText et_phone, et_FirstName_update, et_LastName_update, etNickName,et_addressline_1, et_addressline_2, et_city, et_state, et_zipcode_update;
    String a,ServerResult;
    int keyDel;
    Button btnUpdate_PersonalInfo;
    private Realm realm;
    InsertUpdateRegister oinsertupdateregister;
    OnResultReceived mOnResultReceived;
    Context mContext;
    Gson oGson;
    ProgressDialog mProgressDialog;

    Dialog mdialog;
    Button btn_positive_custom_dailog;
    TextView txt_custom_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_personal_information);
        Initialize();
    }

    private void Initialize() {
        oGson=new Gson();
        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        mOnResultReceived=this;
        mContext=ActivityPersonalInfo.this;

        mdialog = new  Dialog(ActivityPersonalInfo.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        sp_country = (Spinner) findViewById(R.id.sp_country);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrCountry));
        sp_country.setAdapter(adapter);

        img_back = (ImageView) findViewById(R.id.img_back_personal_info);
        img_back.setOnClickListener(this);

        et_FirstName_update=(EditText)findViewById(R.id.et_FirstName_update);
        et_LastName_update=(EditText)findViewById(R.id.et_LastName_update);
        etNickName =(EditText)findViewById(R.id.etNickName);

        et_addressline_1=(EditText)findViewById(R.id.et_addressline_1);
        et_addressline_2=(EditText)findViewById(R.id.et_addressline_2);
        et_city=(EditText)findViewById(R.id.et_city);
        et_state=(EditText)findViewById(R.id.et_state);
        et_zipcode_update=(EditText)findViewById(R.id.et_zipcode_update);

        btnUpdate_PersonalInfo=(Button)findViewById(R.id.btnUpdate_PersonalInfo);
        btnUpdate_PersonalInfo.setOnClickListener(this);

        et_phone = (EditText) findViewById(R.id.et_PhoneNumber_update);
        /*et_phone.addTextChangedListener(new TextWatcher() {
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                boolean flag = true;
                String eachBlock[] = et_phone.getText().toString().split("-");
                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 3) {
                        Log.v("11111111111111111111", "cc" + flag + eachBlock[i].length());
                    }
                }
                if (flag) {
                    et_phone.setOnKeyListener(new View.OnKeyListener() {

                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });
                    if (keyDel == 0) {

                        if (((et_phone.getText().length() + 1) % 4) == 0) {
                            if (et_phone.getText().toString().split("-").length <= 2) {
                                et_phone.setText(et_phone.getText() + "-");
                                et_phone.setSelection(et_phone.getText().length());
                            }
                        }
                        a = et_phone.getText().toString();
                    } else {
                        a = et_phone.getText().toString();
                        keyDel = 0;
                    }
                } else {
                    et_phone.setText(a);
                }
            }
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {}
            public void afterTextChanged(Editable s) {}
        });*/

         oinsertupdateregister = RealmController.with(this).getRegisterInformation();
         if(oinsertupdateregister!=null){
             et_FirstName_update.setText(oinsertupdateregister.getFirstname());
             et_LastName_update.setText(oinsertupdateregister.getLastname());
             String phone=oinsertupdateregister.getMobile();
             phone=phone.replace("+1-","");

             et_phone.setText(phone);

            // et_phone.setText(FormatStringAsPhoneNumber(phone));


             etNickName.setText(oinsertupdateregister.getNick_name());
             et_addressline_1.setText(oinsertupdateregister.getAddress());
             et_addressline_2.setText(oinsertupdateregister.getAddress1());
             et_city.setText(oinsertupdateregister.getPuCity());
             et_state.setText(oinsertupdateregister.getState());
             et_zipcode_update.setText(oinsertupdateregister.getPostal_code());
         }
    }

    public static String FormatStringAsPhoneNumber(String input) {
        String output;
        switch (input.length()) {
            case 7:
                output = String.format("%s-%s", input.substring(0,3), input.substring(3,7));
                break;
            case 10:
                output = String.format("%s-%s-%s", input.substring(0,3), input.substring(3,6), input.substring(6,10));
                break;
            case 11:
                output = String.format("%s%s-%s-%s", input.substring(0,1) ,input.substring(1,4), input.substring(4,7), input.substring(7,11));
                break;
            case 12:
                output = String.format("%s%s-%s-%s", input.substring(0,2) ,input.substring(2,5), input.substring(5,8), input.substring(8,12));
                break;
            default:
                return null;
        }
        return output;
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_personal_info:
                this.finish();
                break;
            case R.id.btnUpdate_PersonalInfo:
                int phone_number_length=et_phone.length();
                phone_number_length=phone_number_length-2;
                int zip_code_length=et_zipcode_update.length();

                if(et_FirstName_update.getText().toString().trim().equals("")){

                    ErrorMessage("First name field can't be blank.");
                }else if(et_LastName_update.getText().toString().trim().equals("")){

                    ErrorMessage("Last name field can't be blank.");
                }else if(et_phone.getText().toString().trim().equals("")){

                    ErrorMessage("Mobile field can't be blank.");
                }else if(phone_number_length<10){

                    ErrorMessage("Mobile number must be between 10-13 character.");
                }else if(et_addressline_1.getText().toString().trim().equals("")){

                    ErrorMessage("Address field can't be blank.");
                }else if(et_city.getText().toString().trim().equals("")){

                    ErrorMessage("City field can't be blank.");
                }else if(et_state.getText().toString().trim().equals("")){

                    ErrorMessage("State field can't be blank.");
                }else if(et_zipcode_update.getText().toString().trim().equals("")){

                    ErrorMessage("Zip code field can't be blank.");
                }else if(et_zipcode_update.length()<5){

                    ErrorMessage("Zip code must be between 5-6 character.");
                }else{
                    String PhoneNumber=et_phone.getText().toString().trim();
                    PhoneNumber=PhoneNumber.replace("-","");

                    InsertUpdateRegisterPUID oInsertUpdateRegister=new InsertUpdateRegisterPUID();
                    oInsertUpdateRegister.setFirstname(et_FirstName_update.getText().toString().trim());
                    oInsertUpdateRegister.setLastname(et_LastName_update.getText().toString().trim());
                    oInsertUpdateRegister.setNick_name(etNickName.getText().toString().trim());
                    oInsertUpdateRegister.setMobile("+1-"+PhoneNumber);
                    oInsertUpdateRegister.setAddress(et_addressline_1.getText().toString().trim());
                    oInsertUpdateRegister.setAddress1(et_addressline_2.getText().toString().trim());
                    oInsertUpdateRegister.setPuCity(et_city.getText().toString().trim());
                    oInsertUpdateRegister.setPuCountry(sp_country.getSelectedItem().toString());
                    oInsertUpdateRegister.setState(et_state.getText().toString());
                    oInsertUpdateRegister.setPostal_code(et_zipcode_update.getText().toString().trim());
                    oInsertUpdateRegister.setDevice_type("Android");
                    oInsertUpdateRegister.setDob(oinsertupdateregister.getDob());
                    oInsertUpdateRegister.setEmail(oinsertupdateregister.getEmail());
                    oInsertUpdateRegister.setPassword(oinsertupdateregister.getPassword());
                    oInsertUpdateRegister.setPuid(oinsertupdateregister.getPuid());
                    oInsertUpdateRegister.setReg_date("");
                    oInsertUpdateRegister.setReferal_code(oinsertupdateregister.getReferal_code());
                    oInsertUpdateRegister.setReward_points("0");

                    String jsonString=oGson.toJson(oInsertUpdateRegister,InsertUpdateRegisterPUID.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi=new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.UpdateInsertUpdateRegisterPUID);
                    oInsertUpdateApi.executePostRequest(API.fUpdateInsertUpdateRegisterPUID(),jsonString);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        mProgressDialog.dismiss();
        ServerResult = what.replace("\"", "");
        switch (ServerResult) {
            case "-2":
            case "-3":
                displayAlert("app error", false);
                break;
            case "0":
            case "-1":
                displayAlert("Sorry! The registration process failed due to some technical error. Please try after some time.", false);
                break;
            default:
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        String PhoneNumber=et_phone.getText().toString().trim();
                        PhoneNumber=PhoneNumber.replace("-","");
                        realm.beginTransaction();
                        oinsertupdateregister.setFirstname(et_FirstName_update.getText().toString().trim());
                        oinsertupdateregister.setLastname(et_LastName_update.getText().toString().trim());
                        oinsertupdateregister.setNick_name(etNickName.getText().toString().trim());
                        oinsertupdateregister.setMobile("+1-"+PhoneNumber);
                        oinsertupdateregister.setAddress(et_addressline_1.getText().toString().trim());
                        oinsertupdateregister.setAddress1(et_addressline_2.getText().toString().trim());
                        oinsertupdateregister.setPuCity(et_city.getText().toString().trim());
                        oinsertupdateregister.setPuCountry(sp_country.getSelectedItem().toString());
                        oinsertupdateregister.setState(et_state.getText().toString());
                        oinsertupdateregister.setPostal_code(et_zipcode_update.getText().toString().trim());
                        oinsertupdateregister.setDevice_type("Android");
                        realm.commitTransaction();
                    }
                });
                displayAlert("Personal information updated successfully", true);
                break;
        }

    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityPersonalInfo.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                    startActivity(new Intent(ActivityPersonalInfo.this,ActivityMyProfile.class));
                                    ActivityPersonalInfo.this.finish();
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }

    private void ErrorMessage(String content) {
        txt_custom_dialog.setText(content);
        mdialog.show();
    }
}
