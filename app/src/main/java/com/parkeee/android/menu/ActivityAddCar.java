package com.parkeee.android.menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import com.google.gson.Gson;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.AddNewCarPUID;
import com.parkeee.android.model.tables.AddNewCarTb;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;

/**
 * Created by user on 03-12-2016.
 */

public class ActivityAddCar extends Activity implements View.OnClickListener, OnResultReceived {

    Button btnAddCar_Save;
    ImageView img_back_add_car,img_minus_reg_exp,img_plus_reg_exp,img_minus_inspection_date,img_plus_inspection_date;
    TextView txt_reg_exp_date,txt_inspection_date,txt_selected_inspection_date,txt_selected_reg_exp_date;
    FrameLayout fl_reg_exp_date,fl_inspection_date;
    Spinner sp_car_brand,sp_car_color,sp_car_type;
    int reg_exp_date=1,inspection_date=1;
    DatePickerDialog dpd_date;
    boolean Is_Inspection;
    EditText et_license_plate_number,et_car_reg_no;

    Context mContext;
    OnResultReceived mOnResultReceived;
    Gson oGson;
    ProgressDialog mProgressDialog;
    InsertUpdateRegister oinsertupdateregister;
    String ServerResult;
    private Realm realm;
    boolean Is_Update;
    AddNewCarTb oaddnewcartb;

    Dialog mdialog;
    Button btn_positive_custom_dailog;
    TextView txt_custom_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_car);
        InitializeInterface();
    }

    private void InitializeInterface() {
        mContext=ActivityAddCar.this;
        Intent intnt= getIntent();
        oinsertupdateregister = RealmController.with(this).getRegisterInformation();

        oGson=new Gson();
        mOnResultReceived=this;

        mdialog = new  Dialog(ActivityAddCar.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        //get realm instance
        this.realm = RealmController.with(this).getRealm();


        sp_car_brand=(Spinner)findViewById(R.id.sp_car_brand);
        sp_car_color=(Spinner)findViewById(R.id.sp_car_color);
        sp_car_type=(Spinner)findViewById(R.id.sp_car_type);

        img_minus_reg_exp=(ImageView)findViewById(R.id.img_minus_reg_exp);
        img_plus_reg_exp=(ImageView)findViewById(R.id.img_plus_reg_exp);
        img_minus_inspection_date=(ImageView)findViewById(R.id.img_minus_inspection_date);
        img_plus_inspection_date=(ImageView)findViewById(R.id.img_plus_inspection_date);
        img_back_add_car=(ImageView)findViewById(R.id.img_back_add_car);
        img_back_add_car.setOnClickListener(this);

        img_minus_reg_exp.setOnClickListener(this);
        img_plus_reg_exp.setOnClickListener(this);
        img_minus_inspection_date.setOnClickListener(this);
        img_plus_inspection_date.setOnClickListener(this);

        txt_reg_exp_date=(TextView)findViewById(R.id.txt_reg_exp);
        txt_inspection_date=(TextView)findViewById(R.id.txt_inspection_date);

        fl_inspection_date=(FrameLayout)findViewById(R.id.fl_inspection_date);
        fl_reg_exp_date=(FrameLayout)findViewById(R.id.fl_reg_exp_date);

        fl_inspection_date.setOnClickListener(this);
        fl_reg_exp_date.setOnClickListener(this);

        txt_selected_inspection_date=(TextView)findViewById(R.id.txt_selected_inspection_date);
        txt_selected_reg_exp_date=(TextView)findViewById(R.id.txt_selected_reg_exp_date);

        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item,getResources().getStringArray(R.array.arrCarBrand));
        sp_car_brand.setAdapter(adapter);

        ArrayAdapter<String> adapter2 = new ArrayAdapter<String>(this, R.layout.spinner_item,getResources().getStringArray(R.array.arrCarColor));
        sp_car_color.setAdapter(adapter2);

        ArrayAdapter<String> adapter3 = new ArrayAdapter<String>(this, R.layout.spinner_item,getResources().getStringArray(R.array.arrCarType));
        sp_car_type.setAdapter(adapter3);

        final Calendar calendar = Calendar.getInstance();
        calendar.add(Calendar.DATE, 2);
        dpd_date = new DatePickerDialog(this, new DateListener(),
                calendar.get(Calendar.YEAR), calendar.get(Calendar.MONTH),
                calendar.get(Calendar.DAY_OF_MONTH));
        dpd_date.getDatePicker().setMinDate(calendar.getTimeInMillis() - 1000);

        btnAddCar_Save=(Button)findViewById(R.id.btnAddCar_Save);
        btnAddCar_Save.setOnClickListener(this);

        et_license_plate_number=(EditText)findViewById(R.id.et_license_plate_number);
        et_license_plate_number.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                et_license_plate_number.setCursorVisible(true);
            }
        });
        et_car_reg_no=(EditText)findViewById(R.id.et_car_reg_no);

        if(intnt.getBooleanExtra("is_Edit",false)){
            Is_Update=true;
            String Id=intnt.getExtras().getString("id");
            oaddnewcartb= RealmController.with(this).getCarItem(Id);
            if(oaddnewcartb!=null){
                btnAddCar_Save.setText("UPDATE");
                sp_car_brand.setSelection(adapter.getPosition(oaddnewcartb.getBrand()));
                sp_car_type.setSelection(adapter3.getPosition(oaddnewcartb.getType()));
                sp_car_color.setSelection(adapter2.getPosition(oaddnewcartb.getColor()));
                et_license_plate_number.setText(oaddnewcartb.getPlateNumber());
                et_car_reg_no.setText(oaddnewcartb.getRegNumber());
                txt_selected_inspection_date.setText(oaddnewcartb.getInspectionDate());
                txt_selected_reg_exp_date.setText(oaddnewcartb.getRegistrationDate());
                txt_inspection_date.setText(oaddnewcartb.getInspectionReminderBefore());
                txt_reg_exp_date.setText(oaddnewcartb.getRegistrationReminderBefore());
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.img_minus_reg_exp:
                if(reg_exp_date<=1){
                }else{
                    reg_exp_date=reg_exp_date-1;
                    txt_reg_exp_date.setText(String.valueOf(reg_exp_date));
                }
                break;
            case R.id.img_plus_reg_exp:
                reg_exp_date=reg_exp_date+1;
                txt_reg_exp_date.setText(String.valueOf(reg_exp_date));
                break;
            case R.id.img_minus_inspection_date:
                if(inspection_date<=1){
                }else{
                    inspection_date=inspection_date-1;
                    txt_inspection_date.setText(String.valueOf(inspection_date));
                }
                break;
            case R.id.img_plus_inspection_date:
                inspection_date=inspection_date+1;
                txt_inspection_date.setText(String.valueOf(inspection_date));
                break;
            case R.id.fl_inspection_date:
                Is_Inspection=true;
                dpd_date.show();
                break;
            case R.id.fl_reg_exp_date:
                Is_Inspection=false;
                dpd_date.show();
                break;
            case R.id.img_back_add_car:
                this.finish();
                break;
            case R.id.btnAddCar_Save:

                if(sp_car_brand.getSelectedItem().equals("Select Brand")){

                    ErrorMessage("Please select the Car Brand.");
                }else if(sp_car_color.getSelectedItem().equals("Select Car Color")){

                    ErrorMessage("Please select the Car Color.");
                }else if(et_license_plate_number.getText().toString().trim().equals("")){

                    ErrorMessage("License Plate Number field can't be blank.");
                }else if(sp_car_type.getSelectedItem().equals("Select Car Type")){

                    ErrorMessage("Please select the Car Type.");
                }else{

                    AddNewCarPUID oAddNewCarpuid = new AddNewCarPUID();
                    oAddNewCarpuid.setBrand(sp_car_brand.getSelectedItem().toString());
                    oAddNewCarpuid.setCar_type(sp_car_type.getSelectedItem().toString());
                    oAddNewCarpuid.setCarregno(et_car_reg_no.getText().toString().trim());
                    oAddNewCarpuid.setColor(sp_car_color.getSelectedItem().toString());
                    if(!txt_selected_reg_exp_date.getText().toString().trim().equals("Registration Expiry Date")){
                        oAddNewCarpuid.setExpiry_date(Utils.ServerDateFormat(txt_selected_reg_exp_date.getText().toString().trim()));
                    }
                    if(!txt_selected_inspection_date.getText().toString().trim().equals("Inspection Date")){
                        oAddNewCarpuid.setInspection_date(Utils.ServerDateFormat(txt_selected_inspection_date.getText().toString().trim()));
                    }

                    if(Is_Update){
                        oAddNewCarpuid.setLicence_plate_number(oaddnewcartb.getId());
                    }else{
                        oAddNewCarpuid.setLicence_plate_number(et_license_plate_number.getText().toString().trim());
                    }
                    oAddNewCarpuid.setModel(sp_car_brand.getSelectedItem().toString());
                    oAddNewCarpuid.setNew_licence_plate_number(et_license_plate_number.getText().toString().trim());
                    oAddNewCarpuid.setPuid(oinsertupdateregister.getPuid());
                    String jsonString=oGson.toJson(oAddNewCarpuid,AddNewCarPUID.class).toString();
                    mProgressDialog.show();
                    //Post oAddNewCarpuid Api
                    PostApiClient oInsertUpdateApi=new PostApiClient(mOnResultReceived);
                    if(Is_Update){
                        oInsertUpdateApi.setRequestSource(RequestSource.UpdateCar);
                        oInsertUpdateApi.executePostRequest(API.fUpdateCarPUID(),jsonString);
                    }else{
                        oInsertUpdateApi.setRequestSource(RequestSource.AddNewCarPUID);
                        oInsertUpdateApi.executePostRequest(API.fAddNewCarPUID(),jsonString);
                    }
                }
                break;
            default:
                break;
        }
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        if(from.toString().equalsIgnoreCase("AddNewCarPUID")){
            mProgressDialog.dismiss();
            ServerResult=what.replace("\"","");
            switch(ServerResult){
                case "-2":
                case "-3":
                    displayAlert("app error",false);
                    break;
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR,false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                                AddNewCarTb oAddNewCarTb= new AddNewCarTb();
                                oAddNewCarTb.setBrand(sp_car_brand.getSelectedItem().toString());
                                oAddNewCarTb.setType(sp_car_type.getSelectedItem().toString());
                                oAddNewCarTb.setRegNumber(et_car_reg_no.getText().toString().trim());
                                oAddNewCarTb.setColor(sp_car_color.getSelectedItem().toString());
                                oAddNewCarTb.setRegistrationDate(txt_selected_reg_exp_date.getText().toString().trim());
                                oAddNewCarTb.setInspectionDate(txt_selected_inspection_date.getText().toString().trim());
                                oAddNewCarTb.setPlateNumber(et_license_plate_number.getText().toString().trim());
                                oAddNewCarTb.setIsCarDeleted("0");
                                oAddNewCarTb.setId(et_license_plate_number.getText().toString().trim());
                                oAddNewCarTb.setIsUploaded("1");
                                oAddNewCarTb.setInspectionReminderBefore(txt_inspection_date.getText().toString().trim());
                                oAddNewCarTb.setRegistrationReminderBefore(txt_reg_exp_date.getText().toString().trim());
                                // Persist your data easily
                                realm.beginTransaction();
                                realm.copyToRealm(oAddNewCarTb);
                                realm.commitTransaction();
                                displayAlert("Car Added Successfully",true);
                        }
                    });
                    break;
            }
        }else if(from.toString().equalsIgnoreCase("UpdateCar")){
            mProgressDialog.dismiss();
            ServerResult=what.replace("\"","");
            switch(ServerResult){
                case "-2":
                case "-3":
                    displayAlert("app error",false);
                    break;
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR,false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            realm.beginTransaction();
                            oaddnewcartb.setBrand(sp_car_brand.getSelectedItem().toString());
                            oaddnewcartb.setType(sp_car_type.getSelectedItem().toString());
                            oaddnewcartb.setRegNumber(et_car_reg_no.getText().toString().trim());
                            oaddnewcartb.setColor(sp_car_color.getSelectedItem().toString());
                            oaddnewcartb.setRegistrationDate(txt_selected_reg_exp_date.getText().toString().trim());
                            oaddnewcartb.setInspectionDate(txt_selected_inspection_date.getText().toString().trim());
                            oaddnewcartb.setPlateNumber(et_license_plate_number.getText().toString().trim());
                            oaddnewcartb.setIsCarDeleted("0");
                            oaddnewcartb.setId(et_license_plate_number.getText().toString().trim());
                            oaddnewcartb.setIsUploaded("1");
                            oaddnewcartb.setInspectionReminderBefore(txt_inspection_date.getText().toString().trim());
                            oaddnewcartb.setRegistrationReminderBefore(txt_reg_exp_date.getText().toString().trim());
                            realm.commitTransaction();
                            displayAlert("Edit successfully", true);
                        }
                    });
                    break;
            }
        }
    }


    private void displayAlert(final String strMessage,final boolean is_successfully) {
        ActivityAddCar.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog=new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name)+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if(is_successfully){
                                    startActivity(new Intent(ActivityAddCar.this,ActivityMyCars.class));
                                    ActivityAddCar.this.finish();
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }

    class DateListener implements DatePickerDialog.OnDateSetListener {
        @Override
        public void onDateSet(DatePicker arg0, int y, int m, int d) {
            Calendar c = Calendar.getInstance();
            c.set(y, m, d);
            String strM = new SimpleDateFormat("MMM dd, yyyy").format(c
                    .getTime());
            if(Is_Inspection){
                txt_selected_inspection_date.setText(strM);
            }else{
                txt_selected_reg_exp_date.setText(strM);
            }
        }
    }

    private void ErrorMessage(String content) {
        txt_custom_dialog.setText(content);
        mdialog.show();
    }
}
