package com.parkeee.android.menu;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.graphics.Color;
import android.graphics.drawable.GradientDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.Editable;
import android.text.TextWatcher;

import com.google.gson.Gson;
import com.parkeee.android.Manifest;
import com.parkeee.android.R;
import com.parkeee.android.adapter.FavGasStationAdapter;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.InsertUpdateRegisterPUID;
import com.parkeee.android.model.params.ShareReferralCode;
import com.parkeee.android.model.tables.GSFavouriteTb;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;

import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.Random;

/**
 * Created by user on 05-12-2016.
 */

public class Search extends Activity implements View.OnClickListener,OnResultReceived {

    LinearLayout ll_search_contacts,ll_search_contacts_edittext;
    TextView txt_icon_clear_search;
    EditText et_search_contacts;
    ArrayList<String> Name= new ArrayList<String>();
    ArrayList<String> Phone= new ArrayList<String>();
    ArrayList<String> SearchName= new ArrayList<String>();
    ArrayList<String> SearchPhone= new ArrayList<String>();
    ArrayList<Integer> Custom_Color= new ArrayList<Integer>();
    ListView lv_contacts;
    final Random mRandom = new Random(System.currentTimeMillis());

    int textlength;
    ArrayList<Integer> posArray= new ArrayList<Integer>();

    ArrayList<String>selected_contacts= new ArrayList<String>();

    Button btn_cancel_search,btn_done_search;
    int MY_PERMISSIONS_REQUEST_READ_CONTACTS=1;
    String Selected_User_Conatct_No="",Body="",ServerResult;

    Context mContext;
    OnResultReceived mOnResultReceived;
    Gson oGson;
    ProgressDialog mProgressDialog;

    Dialog mdialog;
    Button btn_positive_custom_dailog;
    TextView txt_custom_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_contact);
        InitializeInterface();
    }

    private void InitializeInterface() {
        readContacts();

        oGson = new Gson();
        mOnResultReceived = this;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        mdialog = new  Dialog(Search.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });


        Intent intnt= getIntent();
        if(intnt!=null){
            Body=intnt.getStringExtra("Body");
        }


        ll_search_contacts=(LinearLayout)findViewById(R.id.ll_search_contacts);
        ll_search_contacts.setOnClickListener(this);
        ll_search_contacts_edittext=(LinearLayout)findViewById(R.id.ll_search_contacts_edittext);
        ll_search_contacts_edittext.setOnClickListener(this);

        txt_icon_clear_search=(TextView)findViewById(R.id.txt_icon_clear_search);
        txt_icon_clear_search.setOnClickListener(this);

        btn_cancel_search=(Button)findViewById(R.id.btn_cancel_search);
        btn_cancel_search.setOnClickListener(this);

        btn_done_search=(Button)findViewById(R.id.btn_done_search);
        btn_done_search.setOnClickListener(this);

        et_search_contacts=(EditText)findViewById(R.id.et_search_contacts);
        et_search_contacts.setFocusableInTouchMode(true);
        et_search_contacts.addTextChangedListener(new TextWatcher() {

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {
                textlength = et_search_contacts.getText().length();
                SearchName.clear();
                SearchPhone.clear();

                for (int i = 0; i < Name.size(); i++) {
                    if (textlength <= Name.get(i).length()) {
                        if (et_search_contacts
                                .getText()
                                .toString()
                                .equalsIgnoreCase(
                                        (String) Name.get(i).subSequence(0,textlength))) {
                            SearchName.add(Name.get(i));
                        }
                    }
                }
                for(int m=0;m<SearchName.size();m++){
                    String obj=SearchName.get(m);
                    int indx=Name.indexOf(obj);
                    String objct2=Phone.get(indx);
                    SearchPhone.add(objct2);
                }
                lv_contacts.setAdapter(new dataListAdapter(SearchName,SearchPhone,Custom_Color));
            }

            @Override
            public void beforeTextChanged(CharSequence s, int start, int count,
                                          int after) {
            }

            @Override
            public void afterTextChanged(Editable s) {
            }
        });

        lv_contacts=(ListView)findViewById(R.id.lv_contacts);

        lv_contacts.setAdapter(new dataListAdapter(Name,Phone,Custom_Color));
        mContext=Search.this;
    }

    private void ErrorMessage(String content) {
        txt_custom_dialog.setText(content);
        mdialog.show();
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.ll_search_contacts:
                ll_search_contacts.setVisibility(View.GONE);
                ll_search_contacts_edittext.setVisibility(View.VISIBLE);
                InputMethodManager inputMgr = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                inputMgr.toggleSoftInput(0, 0);
                inputMgr.showSoftInput(et_search_contacts,InputMethodManager.SHOW_IMPLICIT);
                break;

            case R.id.txt_icon_clear_search:
                et_search_contacts.setText("");
                InputMethodManager imm = (InputMethodManager)getSystemService(Context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(et_search_contacts.getWindowToken(), 0);
                ll_search_contacts_edittext.setVisibility(View.GONE);
                ll_search_contacts.setVisibility(View.VISIBLE);
                break;

            case R.id.btn_done_search:
                if(selected_contacts.size()>0){
                    for(int s=0;s<selected_contacts.size();s++){
                        Selected_User_Conatct_No=Selected_User_Conatct_No+selected_contacts.get(s)+",";
                        Selected_User_Conatct_No=Selected_User_Conatct_No.replace(" ","");
                    }
                    if(Selected_User_Conatct_No.endsWith(","))
                    {
                        Selected_User_Conatct_No = Selected_User_Conatct_No.substring(0,Selected_User_Conatct_No.length() - 1);
                    }

                    ShareReferralCode oShareReferralCode= new ShareReferralCode();
                    oShareReferralCode.setBody(Body);
                    oShareReferralCode.setMsisdn(Selected_User_Conatct_No);

                    String jsonString = oGson.toJson(oShareReferralCode, ShareReferralCode.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.SendBulkSMS);
                    oInsertUpdateApi.executePostRequest(API.fSendBulkSMS(), jsonString);
                }else{
                    ErrorMessage("No contact is selected to share referral");
                }


                break;

            case R.id.btn_cancel_search:
                this.finish();
                break;
        }
    }

    public void readContacts() {
        Uri uri = ContactsContract.CommonDataKinds.Phone.CONTENT_URI;
        String[] projection    = new String[] {ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME,
                ContactsContract.CommonDataKinds.Phone.NUMBER};
        Cursor people = getContentResolver().query(uri, projection, null, null, null);
        int indexName = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.DISPLAY_NAME);
        int indexNumber = people.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER);

        people.moveToFirst();
        if( people != null && people.moveToFirst() ){
            do {
                String name   = people.getString(indexName);
                String number = people.getString(indexNumber);
                Phone.add(number);
                Name.add(name);
                Custom_Color.add(generateRandomColor());
            } while (people.moveToNext());
        }
    }

    public int generateRandomColor() {
        // This is the base color which will be mixed with the generated one
        final int baseColor = Color.WHITE;

        final int baseRed = Color.red(baseColor);
        final int baseGreen = Color.green(baseColor);
        final int baseBlue = Color.blue(baseColor);

        final int red = (baseRed + mRandom.nextInt(256)) / 2;
        final int green = (baseGreen + mRandom.nextInt(256)) / 2;
        final int blue = (baseBlue + mRandom.nextInt(256)) / 2;

        return Color.rgb(red, green, blue);
    }

    @Override
    public void dispatchString(RequestSource from, String what) {
        ServerResult=what;
        mProgressDialog.dismiss();
        if (from.toString().equalsIgnoreCase("SendBulkSMS")) {

            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    if(selected_contacts.size()>1){
                        displayAlert(selected_contacts.size()+" persons invited successfully.", true);
                    }else{
                        displayAlert(selected_contacts.size()+" person invited successfully.", true);
                    }

                    break;
            }
        }

    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        Search.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }

    class dataListAdapter extends BaseAdapter {
        ArrayList<String> name;
        ArrayList<String> phone;
        ArrayList<Integer> random_color;
        public dataListAdapter(ArrayList<String> Name,ArrayList<String> Phone,ArrayList<Integer> Random_color) {
            name = Name;
            phone=Phone;
            random_color=Random_color;
        }

        @Override
        public int getCount() {
            return name.size();
        }
        @Override
        public Object getItem(int position) {
            return position;
        }
        @Override
        public long getItemId(int position) {
            return position;
        }

        @SuppressLint({ "ViewHolder", "InflateParams" }) @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            final ImageView img_checked_contacts;
            LinearLayout ll_lv_item_search;
            TextView txt_user_name,txt_user_phone,txt_user_profile_label;
            LayoutInflater inflater = getLayoutInflater();
            convertView = inflater.inflate(R.layout.lv_item_contacts, null);
            txt_user_profile_label=(TextView)convertView.findViewById(R.id.txt_user_profile_label);
            img_checked_contacts=(ImageView)convertView.findViewById(R.id.img_checked_contacts);
            txt_user_name=(TextView)convertView.findViewById(R.id.txt_user_name);
            txt_user_phone=(TextView)convertView.findViewById(R.id.txt_user_phone);
            ll_lv_item_search=(LinearLayout)convertView.findViewById(R.id.ll_lv_item_search);
            ll_lv_item_search.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if(posArray.contains(position)){
                        posArray.remove(posArray.indexOf(position));
                        img_checked_contacts.setVisibility(View.GONE);
                        selected_contacts.remove(phone.get(position));
                    }else{
                        selected_contacts.add(phone.get(position));
                        posArray.add(position);
                        img_checked_contacts.setVisibility(View.VISIBLE);
                    }
                }
            });
            if(posArray.contains(position)){
                img_checked_contacts.setVisibility(View.VISIBLE);
            }


            txt_user_name.setText(name.get(position).toString());
            txt_user_phone.setText(phone.get(position).toString());
            GradientDrawable gd = new GradientDrawable();
            gd.setColor(random_color.get(position));
            gd.setCornerRadius(50);

            if(name.get(position).toString().contains(" ")){
                String obj2=String.valueOf(name.get(position).toString().charAt(0))  ;
                String obj=name.get(position).toString().substring(name.get(position).toString().lastIndexOf(" ") + 1);
                String obj3=String.valueOf(obj.charAt(0));
                txt_user_profile_label.setText(obj2+obj3);
            }else{
                String obj2=String.valueOf(name.get(position).toString().charAt(0))  ;
                txt_user_profile_label.setText(String.valueOf(name.get(position).toString().charAt(0)));
            }
            txt_user_profile_label.setBackgroundDrawable(gd);
            return (convertView);
        }
    }
}
