package com.parkeee.android.menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.text.Editable;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.TextWatcher;

import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RequestSource;

import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.model.params.GSFuelPrice;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by user on 06-12-2016.
 */

public class ActivityUpdateGasPrice extends Activity implements View.OnClickListener,OnResultReceived {

    ImageView img_regular_gas, img_midgrade, img_premium, img_diesel, img_back;
    EditText et_regular_gas, et_Midgrade, et_Premium, et_diesel;
    Context mContext;
    TextView txt_regular, txt_midgrade, txt_premium, txt_diesel;
    Button btnUpdate_Gas_Station;
    String IsRegularGasAvailable="false",IsMidgradeAvailable="false",IsPremiumAvailable="false",IsDieselFuelAvailable="false";


    String ServerResult;
    OnResultReceived mOnResultReceived;
    Gson oGson;
    ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_updatefuel_price);
        InitializeInterface();
    }

    private void InitializeInterface() {
        mContext = this;
        mOnResultReceived=this;
        oGson=new Gson();

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        img_regular_gas = (ImageView) findViewById(R.id.img_regular_gas);
        img_regular_gas.setTag("0");
        img_midgrade = (ImageView) findViewById(R.id.img_midgrade);
        img_midgrade.setTag("0");
        img_premium = (ImageView) findViewById(R.id.img_premium);
        img_premium.setTag("0");
        img_diesel = (ImageView) findViewById(R.id.img_diesel);
        img_diesel.setTag("0");
        img_back = (ImageView) findViewById(R.id.img_back_updatefuel_price);
        img_back.setOnClickListener(this);

        btnUpdate_Gas_Station=(Button)findViewById(R.id.btnUpdate_Gas_Station);
        btnUpdate_Gas_Station.setOnClickListener(this);

        img_regular_gas.setOnClickListener(this);
        img_midgrade.setOnClickListener(this);
        img_premium.setOnClickListener(this);
        img_diesel.setOnClickListener(this);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);

        et_regular_gas = (EditText) findViewById(R.id.et_regular_gas);
        et_regular_gas.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String obj1 = et_regular_gas.getText().toString().trim();
                    obj1 = obj1.replace("$", "");
                    et_regular_gas.setText(obj1);
                } else {
                    String obj = et_regular_gas.getText().toString().trim();
                    obj = "$" + obj;
                    if (obj.equals("$")) {
                        obj = obj + "0.0";
                    }
                    et_regular_gas.setText(obj);
                }
            }
        });
        et_regular_gas.post(new Runnable() {
            @Override
            public void run() {
                et_regular_gas.setSelection(et_regular_gas.getText().toString().length());
            }
        });
        et_regular_gas.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(5, 4)});
        et_regular_gas.addTextChangedListener(mDateEntryWatcher);

        et_Midgrade = (EditText) findViewById(R.id.et_Midgrade);
        et_Midgrade.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String obj1 = et_Midgrade.getText().toString().trim();
                    obj1 = obj1.replace("$", "");
                    et_Midgrade.setText(obj1);
                } else {
                    String obj = et_Midgrade.getText().toString().trim();
                    obj = "$" + obj;
                    if (obj.equals("$")) {
                        obj = obj + "0.0";
                    }
                    et_Midgrade.setText(obj);
                }
            }
        });
        et_Midgrade.post(new Runnable() {
            @Override
            public void run() {
                et_Midgrade.setSelection(et_Midgrade.getText().toString().length());
            }
        });
        et_Midgrade.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(5, 4)});
        et_Midgrade.addTextChangedListener(mDateEntryWatcher_midgrade);

        et_Premium = (EditText) findViewById(R.id.et_Premium);
        et_Premium.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String obj1 = et_Premium.getText().toString().trim();
                    obj1 = obj1.replace("$", "");
                    et_Premium.setText(obj1);
                } else {
                    String obj = et_Premium.getText().toString().trim();
                    obj = "$" + obj;
                    if (obj.equals("$")) {
                        obj = obj + "0.0";
                    }
                    et_Premium.setText(obj);
                }
            }
        });
        et_Premium.post(new Runnable() {
            @Override
            public void run() {
                et_Premium.setSelection(et_Premium.getText().toString().length());
            }
        });
        et_Premium.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(5, 4)});
        et_Premium.addTextChangedListener(mDateEntryWatcher_premium);


        et_diesel = (EditText) findViewById(R.id.et_diesel);
        et_diesel.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View v, boolean hasFocus) {
                if (hasFocus) {
                    String obj1 = et_diesel.getText().toString().trim();
                    obj1 = obj1.replace("$", "");
                    et_diesel.setText(obj1);
                } else {
                    String obj = et_diesel.getText().toString().trim();
                    obj = "$" + obj;
                    if (obj.equals("$")) {
                        obj = obj + "0.0";
                    }
                    et_diesel.setText(obj);
                }
            }
        });
        et_diesel.post(new Runnable() {
            @Override
            public void run() {
                et_diesel.setSelection(et_diesel.getText().toString().length());
            }
        });
        et_diesel.setFilters(new InputFilter[]{new DecimalDigitsInputFilter(5, 4)});
        et_diesel.addTextChangedListener(mDateEntryWatcher_diesel);

        txt_regular= (TextView)findViewById(R.id.txt_regular_price) ;
        txt_midgrade= (TextView)findViewById(R.id.txt_midgrade_price) ;
        txt_premium= (TextView)findViewById(R.id.txt_premium_price) ;
        txt_diesel= (TextView)findViewById(R.id.txt_diesel_price) ;


        Intent intnt = getIntent();
        if (intnt != null) {
            try {
                JSONArray oJarray = new JSONArray(intnt.getStringExtra("json"));
                et_regular_gas.setText("$" + oJarray.getJSONObject(0).getString("PricePerGallonInDoller"));
                et_Midgrade.setText("$" + oJarray.getJSONObject(1).getString("PricePerGallonInDoller"));
                et_Premium.setText("$" + oJarray.getJSONObject(2).getString("PricePerGallonInDoller"));
                et_diesel.setText("$" + oJarray.getJSONObject(3).getString("PricePerGallonInDoller"));

                IsRegularGasAvailable=oJarray.getJSONObject(0).getString("IsGasAvailable");
                IsMidgradeAvailable=oJarray.getJSONObject(1).getString("IsGasAvailable");
                IsPremiumAvailable=oJarray.getJSONObject(2).getString("IsGasAvailable");
                IsDieselFuelAvailable=oJarray.getJSONObject(3).getString("IsGasAvailable");

                if(IsRegularGasAvailable.equals("True")){
                    IsRegularGasAvailable= IsRegularGasAvailable.replace("True","true");
                }else{
                    IsRegularGasAvailable= IsRegularGasAvailable.replace("False","false");
                }
                if(IsMidgradeAvailable.equals("True")){
                    IsMidgradeAvailable= IsMidgradeAvailable.replace("True","true");
                }else{
                    IsMidgradeAvailable=IsMidgradeAvailable.replace("False","false");
                }

                if(IsPremiumAvailable.equals("True")){
                    IsPremiumAvailable= IsPremiumAvailable.replace("True","true");
                }else{
                    IsPremiumAvailable= IsPremiumAvailable.replace("False","false");
                }

                if(IsDieselFuelAvailable.equals("True")){
                    IsDieselFuelAvailable=  IsDieselFuelAvailable.replace("True","true");
                }else{
                    IsDieselFuelAvailable=IsDieselFuelAvailable.replace("False","false");

                }

                if (oJarray.getJSONObject(0).getString("IsGasAvailable").equals("False")) {
                    et_regular_gas.setTextColor(getResources().getColor( R.color.view_bg));
                    img_regular_gas.setImageResource(R.drawable.grey_uncheck_update_price);
                    txt_regular.setTextColor(getResources().getColor( R.color.view_bg));

                    et_regular_gas.setFocusable(false);
                    et_regular_gas.setFocusableInTouchMode(false);
                    et_regular_gas.setClickable(false);

                }
                if (oJarray.getJSONObject(1).getString("IsGasAvailable").equals("False")) {
                    et_Midgrade.setTextColor(getResources().getColor( R.color.view_bg));
                    img_midgrade.setImageResource(R.drawable.grey_uncheck_update_price);
                    txt_midgrade.setTextColor(getResources().getColor( R.color.view_bg));

                    et_Midgrade.setFocusable(false);
                    et_Midgrade.setFocusableInTouchMode(false);
                    et_Midgrade.setClickable(false);

                }
                if (oJarray.getJSONObject(2).getString("IsGasAvailable").equals("False")) {
                    et_Premium.setTextColor(getResources().getColor( R.color.view_bg));
                    img_premium.setImageResource(R.drawable.grey_uncheck_update_price);
                    txt_premium.setTextColor(getResources().getColor( R.color.view_bg));

                    et_Premium.setFocusable(false);
                    et_Premium.setFocusableInTouchMode(false);
                    et_Premium.setClickable(false);

                }
                if (oJarray.getJSONObject(3).getString("IsGasAvailable").equals("False")) {
                    et_diesel.setTextColor(getResources().getColor( R.color.view_bg));
                    img_diesel.setImageResource(R.drawable.grey_uncheck_update_price);
                    txt_diesel.setTextColor(getResources().getColor( R.color.view_bg));

                    et_diesel.setFocusable(false);
                    et_diesel.setFocusableInTouchMode(false);
                    et_diesel.setClickable(false);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        mProgressDialog.dismiss();
        ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    displayAlert("Updated Successfully.", true);
                    break;
            }
    }

    public class DecimalDigitsInputFilter implements InputFilter {
        Pattern mPattern;

        public DecimalDigitsInputFilter(int digitsBeforeZero, int digitsAfterZero) {
            mPattern = Pattern.compile("[0-9]{0," + (digitsBeforeZero - 1) + "}+((\\.[0-9]{0," + (digitsAfterZero - 1) + "})?)||(\\.)?");
        }

        @Override
        public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
            Matcher matcher = mPattern.matcher(dest);
            if (!matcher.matches())
                return "";
            return null;
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnUpdate_Gas_Station:
                String Regular_Price=et_regular_gas.getText().toString().trim();
                Regular_Price=Regular_Price.replace("$","");

                String Midgrade_Price=et_Midgrade.getText().toString().trim();
                Midgrade_Price=Midgrade_Price.replace("$","");

                String Premium_Price=et_Premium.getText().toString().trim();
                Premium_Price=Premium_Price.replace("$","");

                String Diesel_Price=et_diesel.getText().toString().trim();
                Diesel_Price=Diesel_Price.replace("$","");

                GSFuelPrice oGSFuelPrice = new GSFuelPrice();
                oGSFuelPrice.setGSId("103926");
                oGSFuelPrice.setRegularGasPrice(Regular_Price);
                oGSFuelPrice.setIsRegularGasAvailable(IsRegularGasAvailable);
                oGSFuelPrice.setMidgradePrice(Midgrade_Price);
                oGSFuelPrice.setIsMidgradeAvailable(IsMidgradeAvailable);
                oGSFuelPrice.setPremiumPrice(Premium_Price);
                oGSFuelPrice.setIsPremiumAvailable(IsPremiumAvailable);
                oGSFuelPrice.setDieselFuelPrice(Diesel_Price);
                oGSFuelPrice.setIsDieselFuelAvailable(IsDieselFuelAvailable);

                String jsonString=oGson.toJson(oGSFuelPrice,GSFuelPrice.class).toString();
                mProgressDialog.show();
                //Post oInsertUpdateApi Api
                PostApiClient oInsertUpdateApi=new PostApiClient(mOnResultReceived);
                oInsertUpdateApi.setRequestSource(RequestSource.GSPriceUpdate);
                oInsertUpdateApi.executePostRequest(API.fGSPriceUpdate(),jsonString);

                break;

            case R.id.img_regular_gas:
                if (img_regular_gas.getTag().equals("0")) {
                    img_regular_gas.setImageResource(R.drawable.grey_uncheck_update_price);
                    img_regular_gas.setTag("1");
                    et_regular_gas.setFocusable(false);
                    et_regular_gas.setFocusableInTouchMode(false);
                    et_regular_gas.setClickable(false);
                    et_regular_gas.setTextColor(getResources().getColor( R.color.view_bg));
                    txt_regular.setTextColor(getResources().getColor( R.color.view_bg));
                    IsRegularGasAvailable="false";
                } else {
                    img_regular_gas.setImageResource(R.drawable.grey_check_update_price);
                    img_regular_gas.setTag("0");
                    et_regular_gas.setFocusable(true);
                    et_regular_gas.setFocusableInTouchMode(true);
                    et_regular_gas.setClickable(true);
                    et_regular_gas.setTextColor(getResources().getColor( R.color.black));
                    txt_regular.setTextColor(getResources().getColor( R.color.black));
                    IsRegularGasAvailable="true";
                }

                break;
            case R.id.img_midgrade:
                if (img_midgrade.getTag().equals("0")) {
                    img_midgrade.setImageResource(R.drawable.grey_uncheck_update_price);
                    img_midgrade.setTag("1");

                    et_Midgrade.setFocusable(false);
                    et_Midgrade.setFocusableInTouchMode(false);
                    et_Midgrade.setClickable(false);

                    et_Midgrade.setTextColor(getResources().getColor( R.color.view_bg));
                    txt_midgrade.setTextColor(getResources().getColor( R.color.view_bg));

                    IsMidgradeAvailable="false";

                } else {
                    img_midgrade.setImageResource(R.drawable.grey_check_update_price);
                    img_midgrade.setTag("0");

                    et_Midgrade.setFocusable(true);
                    et_Midgrade.setFocusableInTouchMode(true);
                    et_Midgrade.setClickable(true);

                    et_Midgrade.setTextColor(getResources().getColor( R.color.black));
                    txt_midgrade.setTextColor(getResources().getColor( R.color.black));

                    IsMidgradeAvailable="true";
                }

                break;
            case R.id.img_premium:
                if (img_premium.getTag().equals("0")) {
                    img_premium.setImageResource(R.drawable.grey_uncheck_update_price);
                    img_premium.setTag("1");
                    et_Premium.setFocusable(false);
                    et_Premium.setFocusableInTouchMode(false);
                    et_Premium.setClickable(false);
                    et_Premium.setTextColor(getResources().getColor( R.color.view_bg));
                    txt_premium.setTextColor(getResources().getColor( R.color.view_bg));

                    IsPremiumAvailable="false";

                } else {
                    img_premium.setImageResource(R.drawable.grey_check_update_price);
                    img_premium.setTag("0");

                    et_Premium.setFocusable(true);
                    et_Premium.setFocusableInTouchMode(true);
                    et_Premium.setClickable(true);

                    et_Premium.setTextColor(getResources().getColor( R.color.black));
                    txt_premium.setTextColor(getResources().getColor( R.color.black));

                    IsPremiumAvailable="true";
                }
                break;
            case R.id.img_diesel:
                if (img_diesel.getTag().equals("0")) {
                    img_diesel.setImageResource(R.drawable.grey_uncheck_update_price);
                    img_diesel.setTag("1");
                    et_diesel.setFocusable(false);
                    et_diesel.setFocusableInTouchMode(false);
                    et_diesel.setClickable(false);

                    et_diesel.setTextColor(getResources().getColor( R.color.view_bg));
                    txt_diesel.setTextColor(getResources().getColor( R.color.view_bg));

                    IsDieselFuelAvailable="false";

                } else {
                    img_diesel.setImageResource(R.drawable.grey_check_update_price);
                    img_diesel.setTag("0");
                    et_diesel.setFocusable(true);
                    et_diesel.setFocusableInTouchMode(true);
                    et_diesel.setClickable(true);

                    et_diesel.setTextColor(getResources().getColor( R.color.black));
                    txt_diesel.setTextColor(getResources().getColor( R.color.black));

                    IsDieselFuelAvailable="true";
                }
                break;

            case R.id.img_back_updatefuel_price:
                this.finish();
                break;
            default:
                break;
        }
    }


    private TextWatcher mDateEntryWatcher = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String working = s.toString();
            working = working.replace("$", "");
            if (!working.equals("") && !working.equals(".")) {
                double value = Double.valueOf(working);
                if (value > 10) {
                    et_regular_gas.setText("10");
                    et_regular_gas.setSelection(et_regular_gas.getText().toString().length());
                    //  Toast.makeText(getApplicationContext(),"value can not be greater then 10",Toast.LENGTH_LONG).show();
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

    };

    private TextWatcher mDateEntryWatcher_midgrade = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String working = s.toString();
            working = working.replace("$", "");
            if (!working.equals("") && !working.equals(".")) {
                double value = Double.valueOf(working);
                if (value > 10) {
                    et_Midgrade.setText("10");
                    et_Midgrade.setSelection(et_Midgrade.getText().toString().length());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

    };

    private TextWatcher mDateEntryWatcher_premium = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String working = s.toString();
            working = working.replace("$", "");
            if (!working.equals("") && !working.equals(".")) {
                double value = Double.valueOf(working);
                if (value > 10) {
                    et_Premium.setText("10");
                    et_Premium.setSelection(et_Premium.getText().toString().length());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

    };

    private TextWatcher mDateEntryWatcher_diesel = new TextWatcher() {
        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            String working = s.toString();
            working = working.replace("$", "");
            if (!working.equals("") && !working.equals(".")) {
                double value = Double.valueOf(working);
                if (value > 10) {
                    et_diesel.setText("10");
                    et_diesel.setSelection(et_diesel.getText().toString().length());
                }
            }
        }

        @Override
        public void afterTextChanged(Editable s) {
        }

        @Override
        public void beforeTextChanged(CharSequence s, int start, int count, int after) {
        }

    };


    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityUpdateGasPrice.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {

                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }

}
