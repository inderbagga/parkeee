package com.parkeee.android.menu;

import android.app.Activity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;

import com.parkeee.android.R;

/**
 * Created by user on 03-12-2016.
 */

public class ActivityWelcomeInbox extends Activity implements View.OnClickListener {

    ImageView img_back;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_inbox_welcome);
        InitializeInterface();
    }

    private void InitializeInterface() {
        img_back = (ImageView) findViewById(R.id.img_back_inbox_welcome);
        img_back.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.img_back_inbox_welcome:
                this.finish();
                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
