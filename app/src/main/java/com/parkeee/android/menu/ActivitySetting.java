package com.parkeee.android.menu;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.telephony.PhoneStateListener;
import android.telephony.TelephonyManager;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.parkeee.android.MenuFragment;
import com.parkeee.android.R;
import com.parkeee.android.WebViewActivity;
import com.parkeee.android.utils.Utils;
import com.slidingmenu.lib.SlidingMenu;

/**
 * Created by user on 05-12-2016.
 */

public class ActivitySetting extends Activity implements View.OnClickListener {
    ImageView img_menu;
    public static SlidingMenu menu;
    TextView txt_help_faq;
    LinearLayout ll_help_faq,ll_feedback,ll_tell_friend,ll_rateus,ll_report_problem,ll_aboutus,ll_callus,ll_emailus;
    ImageView img_fb,img_google_plus,img_twitter,img_instagram;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_settings);
        InitializeInterface();
    }

    private void InitializeInterface() {
        img_menu = (ImageView) findViewById(R.id.img_menu_setting);
        img_menu.setOnClickListener(this);
        MenuFragment.SameActivity="Setting";
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);

        txt_help_faq=(TextView)findViewById(R.id.txt_help_faq);
        txt_help_faq.setText("HELP & FAQ");

        ll_help_faq=(LinearLayout)findViewById(R.id.ll_help_faq);
        ll_feedback=(LinearLayout)findViewById(R.id.ll_feedback);
        ll_tell_friend=(LinearLayout)findViewById(R.id.ll_tell_friend);
        ll_rateus=(LinearLayout)findViewById(R.id.ll_rateus);
        ll_report_problem=(LinearLayout)findViewById(R.id.ll_report_problem);
        ll_aboutus=(LinearLayout)findViewById(R.id.ll_aboutus);
        ll_callus=(LinearLayout)findViewById(R.id.ll_callus);
        ll_emailus=(LinearLayout)findViewById(R.id.ll_emailus);

        img_fb=(ImageView)findViewById(R.id.img_fb_setting);
        img_google_plus=(ImageView)findViewById(R.id.img_google_plus_setting);
        img_twitter=(ImageView)findViewById(R.id.img_twitter_setting);
        img_instagram=(ImageView)findViewById(R.id.img_instagram_setting);

        img_fb.setOnClickListener(this);
        img_google_plus.setOnClickListener(this);
        img_twitter.setOnClickListener(this);
        img_instagram.setOnClickListener(this);

        ll_help_faq.setOnClickListener(this);
        ll_feedback.setOnClickListener(this);
        ll_tell_friend.setOnClickListener(this);
        ll_rateus.setOnClickListener(this);
        ll_report_problem.setOnClickListener(this);
        ll_aboutus.setOnClickListener(this);
        ll_callus.setOnClickListener(this);
        ll_emailus.setOnClickListener(this);

        PhoneCallListener phoneCallListener = new PhoneCallListener();
        TelephonyManager telManager = (TelephonyManager) this
                .getSystemService(Context.TELEPHONY_SERVICE);
        telManager.listen(phoneCallListener,
                PhoneStateListener.LISTEN_CALL_STATE);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_menu_setting:
                menu.toggle();
                break;

            case R.id.ll_help_faq:
                Intent IWVH= new Intent(ActivitySetting.this, WebViewActivity.class);
                IWVH.putExtra("url","https://parkeee.com/faq");
                IWVH.putExtra("title","Help & FAQ");
                startActivity(IWVH);
                break;
            case R.id.ll_feedback:
                Uri uri = Uri.parse("market://details?id=" + getPackageName());
                Intent goToMarket = new Intent(Intent.ACTION_VIEW, uri);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                }

                break;
            case R.id.ll_tell_friend:
                String str = getResources().getString(R.string.referal_message) + "\n" + "Download Parkeee app and use Referral code:\n ml9982"+ "\n"+ Utils.URL_APPDOWNLOAD;
                Intent shareIntent = new Intent();
                shareIntent.setAction(Intent.ACTION_SEND);
                shareIntent.setType("text/plain");
                shareIntent.putExtra(Intent.EXTRA_TEXT, str);
                startActivity(Intent.createChooser(shareIntent,
                        "Tell a friend"));
                break;
            case R.id.ll_rateus:
                Uri uri1 = Uri.parse("market://details?id=" + getPackageName());
                Intent goToMarket1 = new Intent(Intent.ACTION_VIEW, uri1);
                // To count with Play market backstack, After pressing back button,
                // to taken back to our application, we need to add following flags to intent.
                goToMarket1.addFlags(Intent.FLAG_ACTIVITY_NO_HISTORY |
                        Intent.FLAG_ACTIVITY_CLEAR_WHEN_TASK_RESET |
                        Intent.FLAG_ACTIVITY_MULTIPLE_TASK);
                try {
                    startActivity(goToMarket1);
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW,
                            Uri.parse("http://play.google.com/store/apps/details?id=" + getPackageName())));
                }
                break;
            case R.id.ll_report_problem:
                String mDeviceDetails = "("+ Build.MANUFACTURER+" " + Build.MODEL+")" +"  "+ Build.VERSION.RELEASE;
                Log.e("TAG","Model Name"+mDeviceDetails);
                Intent intent1 = new Intent(Intent.ACTION_SENDTO);
                String uriText = "mailto:" + Uri.encode("reportapp@parkeee.com");
                Uri uri2 = Uri.parse(uriText);
                intent1.putExtra(Intent.EXTRA_SUBJECT, "Report a problem "+" "+mDeviceDetails);
                intent1.setData(uri2);
                startActivity(Intent.createChooser(intent1, "Send Email"));
                break;
            case R.id.ll_aboutus:

                startActivity(new Intent(ActivitySetting.this,ActivityAboutUs.class));

                break;
            case R.id.ll_callus:
                Intent phoneCallIntent = new Intent(Intent.ACTION_CALL);
                phoneCallIntent.setData(Uri.parse("tel:+1-844-727-5333"));
                startActivity(phoneCallIntent);

                break;
            case R.id.ll_emailus:
                Intent intent5 = new Intent(Intent.ACTION_SENDTO);
                String uriText3 = "mailto:" + Uri.encode("users@parkeee.com");
                Uri uri3 = Uri.parse(uriText3);

                intent5.setData(uri3);
                startActivity(Intent.createChooser(intent5, "Send Email"));
                break;
            case R.id.img_fb_setting:
                Intent i;
                i = new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.facebook.com/myparkeee"));
                startActivity(i);

                break;
            case R.id.img_google_plus_setting:
                Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/116271727953374631091"));
                intent.setPackage("com.google.android.apps.plus");
                if (intent.resolveActivity(getPackageManager()) != null) {
                    startActivity(intent);
                } else {
                    intent = new Intent(Intent.ACTION_VIEW, Uri.parse("https://plus.google.com/116271727953374631091"));
                    startActivity(intent);
                }
                break;
            case R.id.img_twitter_setting:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/myparkeee")));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://twitter.com/myparkeee")));
                }
                break;
            case R.id.img_instagram_setting:
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/parkeee4094/")));
                } catch (Exception e) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://www.instagram.com/parkeee4094/")));
                }
                break;
            default:
                break;
        }
    }

    private class PhoneCallListener extends PhoneStateListener {

        String TAG = "LOGGING PHONE CALL";

        private boolean phoneCalling = false;

        @Override
        public void onCallStateChanged(int state, String incomingNumber) {

            if (TelephonyManager.CALL_STATE_RINGING == state) {
                // phone ringing
                Log.i(TAG, "RINGING, number: " + incomingNumber);
            }

            if (TelephonyManager.CALL_STATE_OFFHOOK == state) {
                phoneCalling = true;
            }
            // When the call ends launch the main activity again
            if (TelephonyManager.CALL_STATE_IDLE == state) {
                if (phoneCalling) {
                    // restart app
                    Intent i = getBaseContext().getPackageManager()
                            .getLaunchIntentForPackage(
                                    getBaseContext().getPackageName());
                    i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                    startActivity(i);
                    phoneCalling = false;
                }
            }
        }
    }
    @Override
    public void onBackPressed() {
    }
}
