package com.parkeee.android.menu;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.media.MediaScannerConnection;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v4.app.FragmentActivity;

import com.parkeee.android.utils.InternalStorageContentProvider;
import com.parkeee.android.MenuFragment;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.Config;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.UploadProfilePicPUID;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Utils;
import com.slidingmenu.lib.SlidingMenu;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import eu.janmuller.android.simplecropimage.CropImage;
import io.realm.Realm;

/**
 * Created by user on 29-11-2016.
 */

public class ActivityMyProfile extends FragmentActivity implements View.OnClickListener ,OnResultReceived {

    ImageView img_menu_myProfile,img_user_profile;
    public static SlidingMenu menu;

    public static final int REQUEST_CODE_GALLERY      = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE   = 0x3;
    public static final String TAG = "MainActivity";
    private File mFileTemp;
    String filepath="",message;
    private Uri fileUri;
    public static final int MEDIA_TYPE_IMAGE = 1;
    private static final int CAMERA_CAPTURE_IMAGE_REQUEST_CODE = 100;
    public static final int MEDIA_TYPE_VIDEO = 2;
    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";

    ProgressDialog mProgressDialog;
    String encoded_File_Path="";
    LinearLayout ll_personalInfo,ll_cahnge_pswd;
    String puid="";
    Gson oGson;
    Context mContext;
    OnResultReceived mOnResultReceived;
    String ServerResult;
    InsertUpdateRegister oinsertupdateregister;
    private Realm realm;
    String ProfileImage_path="";

    TextView txt_username_my_profile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_my_profile);
        Initialize();
    }

    private void Initialize() {
        mContext = this;
        //get realm instance
        this.realm = RealmController.with(this).getRealm();
        oinsertupdateregister = RealmController.with(this).getRegisterInformation();
        if(oinsertupdateregister!=null){
            ProfileImage_path= oinsertupdateregister.getImagePath();
        }

        img_user_profile=(ImageView)findViewById(R.id.img_user_profile);
        img_user_profile.setOnClickListener(this);

        txt_username_my_profile=(TextView)findViewById(R.id.txt_username_my_profile);
        txt_username_my_profile.setText(Utils.getUserName(mContext));

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");
        mProgressDialog.setCancelable(false);

        if(!ProfileImage_path.equals("")){
            ProfileImage_path= Config.getImageBaseUrl()+ProfileImage_path;
            ProfileImage_path = ProfileImage_path.replace("\"", "");

            mProgressDialog.show();
            Picasso.with(mContext).invalidate(ProfileImage_path);
            Picasso.with(img_user_profile.getContext())
                    .load(ProfileImage_path)
                    .networkPolicy(NetworkPolicy.NO_CACHE)
                    .transform(new RoundedTransformation(100, 0))
                    .resize(96, 96).centerCrop().into(img_user_profile, new Callback() {
                @Override
                public void onSuccess() {
                    mProgressDialog.dismiss();
                }
                @Override
                public void onError() {
                }
            });
        }

        oGson=new Gson();

        mOnResultReceived = this;

        ll_personalInfo=(LinearLayout)findViewById(R.id.ll_personalInfo);
        ll_personalInfo.setOnClickListener(this);

        ll_cahnge_pswd=(LinearLayout)findViewById(R.id.ll_cahnge_pswd);
        ll_cahnge_pswd.setOnClickListener(this);

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        }
        else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }

        img_menu_myProfile=(ImageView)findViewById(R.id.img_menu_myProfile);
        img_menu_myProfile.setOnClickListener(this);


        MenuFragment.SameActivity="MyProfile";

        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.img_menu_myProfile:
                menu.toggle();
                break;
            case R.id.img_user_profile:
              //  Crop.pickImage(this,1);
                selectImage();
                break;

            case R.id.ll_personalInfo:
                startActivity(new Intent(ActivityMyProfile.this,ActivityPersonalInfo.class));
                break;

            case R.id.ll_cahnge_pswd:
                startActivity(new Intent(ActivityMyProfile.this,ActivityChangePassword.class));
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
    }

    private void selectImage() {

        final CharSequence[] options = { "Take Photo", "Choose from Gallery","Cancel" };
        AlertDialog.Builder builder = new AlertDialog.Builder(ActivityMyProfile.this);
        builder.setTitle("Add Photo!");
        builder.setItems(options, new DialogInterface.OnClickListener() {

            @Override

            public void onClick(DialogInterface dialog, int item) {

                if (options[item].equals("Take Photo"))
                {
                    takePicture();
                }
                else if (options[item].equals("Choose from Gallery"))
                {
                    openGallery();
                }
                else if (options[item].equals("Cancel")) {
                    dialog.dismiss();
                }
            }
        });
        builder.show();
    }

    private void takePicture() {
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            }
            else {
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
            //Filepath_BusinessCard=mImageCaptureUri.getPath();
        } catch (ActivityNotFoundException e) {
            Log.d(TAG, "cannot take picture", e);
        }
    }

    private void openGallery() {
        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }


    public Uri getImageUri(Context inContext, Bitmap inImage) {
        ByteArrayOutputStream bytes = new ByteArrayOutputStream();
        inImage.compress(Bitmap.CompressFormat.JPEG, 100, bytes);
        String path = MediaStore.Images.Media.insertImage(inContext.getContentResolver(), inImage, "Title", null);
        return Uri.parse(path);
    }
    public String getRealPathFromURI(Uri uri) {
        Cursor cursor = getContentResolver().query(uri, null, null, null, null);
        cursor.moveToFirst();
        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA);
        return cursor.getString(idx);
    }
    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {
        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
    private void startCropImage() {
        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);
        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 2);
        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);

    }

    public String getPath(Uri uri) {
        String[] projection = { MediaStore.Images.Media.DATA };
        @SuppressWarnings("deprecation")
        Cursor cursor = managedQuery(uri, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
        cursor.moveToFirst();

        return cursor.getString(column_index);
    }


    private void previewCapturedImage() {
        try {
            BitmapFactory.Options options = new BitmapFactory.Options();
            options.inSampleSize = 8;
            filepath=fileUri.getPath();
            final Bitmap bitmap = BitmapFactory.decodeFile(fileUri.getPath(),
                    options);
            Bitmap originalBitmap = bitmap;
            Bitmap resizedBitmap = Bitmap.createScaledBitmap(
                    originalBitmap, 4096, 4096, false);

            ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
            resizedBitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
            byte[] byteArray = byteArrayOutputStream .toByteArray();
            encoded_File_Path = Base64.encodeToString(byteArray, Base64.DEFAULT);

            Picasso.with(mContext)
                    .load(fileUri)
                    .transform(new RoundedTransformation(100, 0))
                    .resize(96, 96).centerCrop().into(img_user_profile);

            //img_user_profile.setImageBitmap(resizedBitmap);

        } catch (NullPointerException e) {
            e.printStackTrace();
        }
    }
   @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        super.onActivityResult(requestCode, resultCode, data);
        if (resultCode != RESULT_OK) {
            return;
        }
        Bitmap bitmap;
        switch (requestCode) {
            case REQUEST_CODE_GALLERY:
                try {
                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();
                    startCropImage();
                } catch (Exception e) {
                    Log.e(TAG, "Error while creating temp file", e);
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:
                String state = Environment.getExternalStorageState();
                if (Environment.MEDIA_MOUNTED.equals(state)) {
                    mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
                }
                else {
                    mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
                }
                startCropImage();
                break;


            case CAMERA_CAPTURE_IMAGE_REQUEST_CODE:
                previewCapturedImage();
                break;
            case REQUEST_CODE_CROP_IMAGE:
                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {
                    return;
                }

                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());
                Uri tempUri = getImageUri(getApplicationContext(), bitmap);
                File finalFile = new File(getRealPathFromURI(tempUri));
                filepath=finalFile.toString();

                ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.PNG, 80, byteArrayOutputStream);
                byte[] byteArray = byteArrayOutputStream .toByteArray();
                encoded_File_Path = Base64.encodeToString(byteArray, Base64.DEFAULT);

                mProgressDialog.show();
                UploadProfilePicPUID oUploadProfilePic=new UploadProfilePicPUID();
                oUploadProfilePic.setImageBase64String(encoded_File_Path);
                oUploadProfilePic.setPuid(Utils.getPuid(mContext));

                String jsonString=oGson.toJson(oUploadProfilePic,UploadProfilePicPUID.class).toString();
                //Post oInsertUpdateApi Api
                PostApiClient oChangePsswordApi=new PostApiClient(mOnResultReceived);
                oChangePsswordApi.setRequestSource(RequestSource.UplaodProfilePic);
                oChangePsswordApi.executePostRequest(API.fUploadProfilePicture(),jsonString);

                Picasso.with(mContext)
                        .load(tempUri)
                        .transform(new RoundedTransformation(100, 0))
                        .resize(96, 96).centerCrop().into(img_user_profile);


                break;
        }
    }

    @Override
    public void dispatchString(RequestSource from, final String what) {

        if (from.toString().equalsIgnoreCase("UplaodProfilePic")) {
            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                    break;
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                case "Image not uploaded":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        String Image_path=what;
                        @Override
                        public void run() {
                            realm.beginTransaction();
                            oinsertupdateregister.setImagePath(Image_path);
                            realm.commitTransaction();
                            displayAlert("Uploaded successfully", true);
                        }
                    });

                    break;
            }
        }

    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityMyProfile.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }


    public class RoundedTransformation implements
            com.squareup.picasso.Transformation {
        private final int radius;
        private final int margin; // dp
        // radius is corner radii in dp
        // margin is the board in dp
        public RoundedTransformation(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }
        @Override
        public Bitmap transform(final Bitmap source) {
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP,
                    Shader.TileMode.CLAMP));
            Bitmap output = Bitmap.createBitmap(source.getWidth(),
                    source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            canvas.drawRoundRect(new RectF(margin, margin, source.getWidth()
                    - margin, source.getHeight() - margin), radius, radius, paint);
            if (source != output) {
                source.recycle();
            }
            return output;
        }
        @Override
        public String key() {
            return "rounded";
        }
    }

    public void callBroadCast() {
        if (Build.VERSION.SDK_INT >= 14) {
            Log.e("-->", " >= 14");
            MediaScannerConnection.scanFile(this, new String[]{Environment.getExternalStorageDirectory().toString()}, null, new MediaScannerConnection.OnScanCompletedListener() {
                /*
                 *   (non-Javadoc)
                 * @see android.media.MediaScannerConnection.OnScanCompletedListener#onScanCompleted(java.lang.String, android.net.Uri)
                 */
                public void onScanCompleted(String path, Uri uri) {
                    Log.e("ExternalStorage", "Scanned " + path + ":");
                    Log.e("ExternalStorage", "-> uri=" + uri);
                }
            });
        }else {
            Log.e("-->", " < 14");
            sendBroadcast(new Intent(Intent.ACTION_MEDIA_MOUNTED,
                    Uri.parse("file://" + Environment.getExternalStorageDirectory())));
        }
    }
}
