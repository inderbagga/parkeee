package com.parkeee.android.menu;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;

import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.ChangePasswordPUID;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;

/**
 * Created by user on 02-12-2016.
 */

public class ActivityChangePassword extends Activity implements View.OnClickListener, OnResultReceived {

    ImageView img_back,img_email_verify;
    Button btnUpdate_Password;
    EditText et_current_pswd, et_new_pswd, et_confirm_new_pswd;
    Context mContext;
    OnResultReceived mOnResultReceived;
    ProgressDialog mProgressDialog;
    String puid, ServerResult;
    LinearLayout ll_click_to_verify;
    InsertUpdateRegister oinsertupdateregister;
    String Current_password;
    Gson oGson;
    TextView txt_user_email_changepswd,txt_email_verify;
    private Realm realm;

    Dialog mdialog;
    Button btn_positive_custom_dailog;
    TextView txt_custom_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_change_pswd);
        Initialize();
    }

    private void Initialize() {
        oGson=new Gson();
        mContext = this;
        mOnResultReceived = this;

        mdialog = new  Dialog(ActivityChangePassword.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        img_back = (ImageView) findViewById(R.id.img_back_change_pswd);
        img_back.setOnClickListener(this);

        txt_user_email_changepswd=(TextView)findViewById(R.id.txt_user_email_changepswd);

        ll_click_to_verify=(LinearLayout)findViewById(R.id.ll_click_to_verify);
        ll_click_to_verify.setOnClickListener(this);

        btnUpdate_Password = (Button) findViewById(R.id.btnUpdate_Password);
        btnUpdate_Password.setOnClickListener(this);

        et_current_pswd = (EditText) findViewById(R.id.et_current_pswd);
        et_new_pswd = (EditText) findViewById(R.id.et_new_pswd);
        et_confirm_new_pswd = (EditText) findViewById(R.id.et_confirm_pswd);

        img_email_verify=(ImageView)findViewById(R.id.img_email_verify);
        txt_email_verify=(TextView)findViewById(R.id.txt_email_verify);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        oinsertupdateregister = RealmController.with(this).getRegisterInformation();
        Current_password=oinsertupdateregister.getPassword();
        puid=oinsertupdateregister.getPuid();
        txt_user_email_changepswd.setText(oinsertupdateregister.getEmail());

        mProgressDialog.show();
        String CheckEmailVerifiedOrNot_Url = API.fCheckEmailVerifiedOrNot();
        CheckEmailVerifiedOrNot_Url = CheckEmailVerifiedOrNot_Url.concat(puid);
        GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
        oInsertUpdateApi.setRequestSource(RequestSource.CheckEmailVerified);
        oInsertUpdateApi.executeGetRequest(CheckEmailVerifiedOrNot_Url);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_back_change_pswd:
                this.finish();
                break;

            case R.id.ll_click_to_verify:
                if(!txt_email_verify.getText().toString().equals("verified")){
                    mProgressDialog.show();
                    String ResendEmailVerifiedOrNot_Url = API.fResendEmailVerificationLink();
                    ResendEmailVerifiedOrNot_Url = ResendEmailVerifiedOrNot_Url.concat(puid);
                    GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.ResendEmailVerification);
                    oInsertUpdateApi.executeGetRequest(ResendEmailVerifiedOrNot_Url);
                }

                break;

            case R.id.btnUpdate_Password:
                if (et_current_pswd.getText().toString().trim().equals("")) {
                    ErrorMessage("Current password field can't be blank.");
                } else if (et_new_pswd.getText().toString().trim().equals("")) {
                    ErrorMessage("New password field can't be blank.");
                } else if (et_new_pswd.length() < 8) {
                    ErrorMessage("Password must be between 8-20 character.");
                } else if (et_confirm_new_pswd.getText().toString().trim().equals("")) {

                    ErrorMessage("Confirm password field can't be blank.");
                } else if (!et_new_pswd.getText().toString().trim().equals(et_confirm_new_pswd.getText().toString().trim())) {

                    ErrorMessage("Password and confirm password not matched.");
                }else if (!et_current_pswd.getText().toString().trim().equals(Current_password)) {

                    ErrorMessage("Please enter the valid Current Password.");
                }else if (et_current_pswd.getText().toString().trim().equals(et_new_pswd.getText().toString().trim())) {

                    ErrorMessage("New Password can't be Current Password.");
                }else{
                    mProgressDialog.show();
                    ChangePasswordPUID oSaveInbox=new ChangePasswordPUID();
                    oSaveInbox.setPassword(et_new_pswd.getText().toString().trim());
                    oSaveInbox.setPuid(puid);
                    String jsonString=oGson.toJson(oSaveInbox,ChangePasswordPUID.class).toString();
                    //Post oInsertUpdateApi Api
                    PostApiClient oChangePsswordApi=new PostApiClient(mOnResultReceived);
                    oChangePsswordApi.setRequestSource(RequestSource.ChangePassword);
                    oChangePsswordApi.executePostRequest(API.fChangePassword(),jsonString);
                }

                break;
            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        if (from.toString().equalsIgnoreCase("CheckEmailVerified")) {
            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                    break;
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    if(what.equals("0")){
                        img_email_verify.setImageResource(R.drawable.icon_not_verified);
                        txt_email_verify.setText("click to verify");
                        txt_email_verify.setTextColor(getResources().getColor(R.color.red));
                    }else{
                        img_email_verify.setImageResource(R.drawable.icon_verified);
                        txt_email_verify.setText("verified");
                        txt_email_verify.setTextColor(getResources().getColor(R.color.green));
                    }

                    break;
            }
        }else if(from.toString().equalsIgnoreCase("ResendEmailVerification")){
            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                    displayAlert("Not send.", false);
                    break;
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    displayAlert("Sent link to registered email.", true);
                    break;
            }
        }else if(from.toString().equalsIgnoreCase("ChangePassword")){

            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                    displayAlert("Not changed.", false);
                    break;
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Current_password=et_new_pswd.getText().toString().trim();
                            et_new_pswd.setText("");
                            et_confirm_new_pswd.setText("");
                            et_current_pswd.setText("");
                            realm.beginTransaction();
                            oinsertupdateregister.setPassword(Current_password);
                            realm.commitTransaction();
                            displayAlert("Password changed successfully.", true);
                        }
                    });

                    break;
            }
        }
    }


    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityChangePassword.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                    ll_click_to_verify.setVisibility(View.GONE);
                                    ActivityChangePassword.this.finish();
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }

    private void ErrorMessage(String content) {
        txt_custom_dialog.setText(content);
        mdialog.show();
    }
}
