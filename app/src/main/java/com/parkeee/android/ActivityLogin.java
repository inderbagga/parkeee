package com.parkeee.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.provider.Settings;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.ValidateUser;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;

import org.json.JSONException;
import org.json.JSONObject;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;

/**
 * Created by user on 25-11-2016.
 */

public class ActivityLogin extends Activity implements View.OnClickListener, OnResultReceived {

    PanningView Panning_view;
    ImageView img_chkSignIn;
    int chked_value = 0;
    TextView txtForgotPassword;
    EditText et_UserName, et_Password;
    Button btnLogin;

    Context mContext;
    OnResultReceived mOnResultReceived;
    Gson oGson;
    ProgressDialog mProgressDialog;
    String ServerResult;
    private Realm realm;
    InsertUpdateRegister oinsertupdateregister;
    Dialog mdialog;
    Button btn_positive_custom_dailog;
    TextView txt_custom_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);
        Initailize();
    }

    private void Initailize() {
        mdialog = new  Dialog(ActivityLogin.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });

        oGson = new Gson();
        mOnResultReceived = this;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");
        mContext = this;
        this.realm = RealmController.with(this).getRealm();

        oinsertupdateregister = new InsertUpdateRegister();


        Panning_view = (PanningView) findViewById(R.id.panningView_Login);
        Panning_view.startPanning();
        img_chkSignIn = (ImageView) findViewById(R.id.chkSignIn);
        img_chkSignIn.setOnClickListener(this);
        txtForgotPassword = (TextView) findViewById(R.id.txtForgotPassword);
        txtForgotPassword.setOnClickListener(this);

        et_Password = (EditText) findViewById(R.id.etPassword_login);
        et_UserName = (EditText) findViewById(R.id.etUsername_login);

        btnLogin = (Button) findViewById(R.id.btnLogin);
        btnLogin.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.chkSignIn:
                if (chked_value == 0) {
                    img_chkSignIn.setImageResource(R.drawable.checked_white);
                    chked_value = 1;
                } else {
                    img_chkSignIn.setImageResource(R.drawable.unchecked_white);
                    chked_value = 0;
                }
                break;

            case R.id.txtForgotPassword:
                startActivity(new Intent(ActivityLogin.this, ActivityForgotPassword.class));
                break;

            case R.id.btnLogin:
                if (et_UserName.getText().toString().trim().equals("")) {
                    ErrorMessage("Email field can't be blank.");
                } else if (!isValidEmail((et_UserName.getText().toString().trim()))) {
                    ErrorMessage("Please enter the valid email.");
                } else if (et_Password.getText().toString().trim().equals("")) {
                    ErrorMessage("Password field can't be blank.");
                } else {
                    // startActivity(new Intent(ActivityLogin.this,ActivityNewDeviceLogin.class));

                    ValidateUser oValidateUser = new ValidateUser();
                    oValidateUser.setDevice_id(Settings.Secure.getString(mContext.getContentResolver(),
                            Settings.Secure.ANDROID_ID));
                    oValidateUser.setEmail(et_UserName.getText().toString().trim());
                    oValidateUser.setPassword(et_Password.getText().toString().trim());
                    oValidateUser.setDevice_type("Android");

                    String jsonString = oGson.toJson(oValidateUser, ValidateUser.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.ValidateUser);
                    oInsertUpdateApi.executePostRequest(API.fValidateUser(), jsonString);
                }
                break;
            default:
                break;
        }
    }

    private void ErrorMessage(String content) {
        txt_custom_dialog.setText(content);
        mdialog.show();
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void dispatchString(RequestSource from, String what) {
        final String Result=what;
        if(from.toString().equalsIgnoreCase("ValidateUser")){
            try {
                JSONObject obj = new JSONObject(what);
                ServerResult = obj.getString("result");
                switch (ServerResult) {
                    case "-2":
                    case "-3":
                        mProgressDialog.dismiss();
                        displayAlert("app error", false);
                        break;
                    case "0":
                        mProgressDialog.dismiss();
                        displayAlert("Incorrect email id or password.", false);
                        break;
                    case "-1":
                        mProgressDialog.dismiss();
                        displayAlert("Incorrect email id or password.", false);
                        break;
                    case "2":
                        ValidateUser oValidateUser = new ValidateUser();
                        oValidateUser.setDevice_id(Settings.Secure.getString(mContext.getContentResolver(),
                                Settings.Secure.ANDROID_ID));
                        oValidateUser.setEmail(et_UserName.getText().toString().trim());
                        oValidateUser.setPassword(et_Password.getText().toString().trim());
                        oValidateUser.setDevice_type("Android");

                        String jsonString = oGson.toJson(oValidateUser, ValidateUser.class).toString();

                        //Post oInsertUpdateApi Api
                        PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                        oInsertUpdateApi.setRequestSource(RequestSource.LoginNewDevice);
                        oInsertUpdateApi.executePostRequest(API.fLoginNewDevice(), jsonString);

                        break;
                    default:
                        JSONObject data= obj.getJSONObject("data");
                        saveDataIntoLocalDB(data);
                        break;
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }else{
            try {
                mProgressDialog.dismiss();
                JSONObject obj = new JSONObject(what);
                ServerResult = obj.getString("result");
                switch (ServerResult) {
                    case "-2":
                    case "-3":
                        displayAlert("app error", false);
                        break;
                    case "-1":
                        displayAlert(AppMessages.TECHNICAL_ERROR, false);
                        break;
                    default:
                        try {
                            JSONObject obj2 = new JSONObject(Result);
                            ServerResult = obj2.getString("result");
                            JSONObject data= obj2.getJSONObject("data");
                            saveDataIntoLocalDB(data);
                        }catch (Exception e){
                            e.printStackTrace();
                        }
                        break;
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    private void saveDataIntoLocalDB(final JSONObject data) throws JSONException {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                try {

                    Realm realm1 = Realm.getDefaultInstance();
                    realm1.beginTransaction();
                    realm1.deleteAll();
                    realm1.commitTransaction();
                    realm1.close();

                    oinsertupdateregister.setFirstname(data.getString("firstname"));
                    oinsertupdateregister.setLastname(data.getString("lastname"));
                    oinsertupdateregister.setNick_name(data.getString("nickname"));
                    oinsertupdateregister.setMobile(data.getString("mobile"));
                    oinsertupdateregister.setAddress(data.getString("address"));
                    oinsertupdateregister.setAddress1(data.getString("address1"));
                    oinsertupdateregister.setPuCity(data.getString("puCity"));
                    oinsertupdateregister.setPuCountry(data.getString("puCountry"));
                    oinsertupdateregister.setState(data.getString("puState"));
                    oinsertupdateregister.setDob(data.getString("dob"));
                    oinsertupdateregister.setDevice_type("Android");
                    oinsertupdateregister.setDevice_id(Settings.Secure.getString(mContext.getContentResolver(),
                            Settings.Secure.ANDROID_ID));
                    oinsertupdateregister.setEmail(et_UserName.getText().toString().trim());
                    oinsertupdateregister.setIsEmailVerified(data.getString("is_email_verified"));
                    oinsertupdateregister.setIsMobileVerified(data.getString("is_mobile_verified"));
                    oinsertupdateregister.setPassword(et_Password.getText().toString().trim());
                    oinsertupdateregister.setPostal_code(data.getString("postal_code"));
                    oinsertupdateregister.setPrimary_card(data.getString("primary_card_number"));
                    oinsertupdateregister.setImagePath(data.getString("puImgPath"));
                    oinsertupdateregister.setPuid(data.getString("puid"));
                    oinsertupdateregister.setReferal_code(data.getString("referal_code"));
                    oinsertupdateregister.setReg_date(data.getString("reg_date"));
                    oinsertupdateregister.setReward_points(data.getString("reward_points"));
                    oinsertupdateregister.setKeep_me_signed_in(String.valueOf(chked_value));
                    oinsertupdateregister.setLogOut("0");
                    realm.beginTransaction();
                    realm.copyToRealm(oinsertupdateregister);
                    realm.commitTransaction();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        startActivity(new Intent(ActivityLogin.this, ActivityDashboard.class));
    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityLogin.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {

                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }
}
