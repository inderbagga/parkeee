package com.parkeee.android.utils;

import android.app.Application;
import android.content.Context;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import com.parkeee.android.backend.RealmController;
import com.parkeee.android.model.tables.InsertUpdateRegister;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by user on 25-11-2016.
 */

public class Utils {

    public static final String URL_TC = "https://parkeee.com/termandcondition.html";
    public static final String URL_PP = "https://parkeee.com/privacy_policy.html";
    public static final String URL_EULA = "https://parkeee.com/eula.html";
    public static final String URL_APPDOWNLOAD = "https://parkeee.com/downloadapp.html";
    public static InsertUpdateRegister oinsertupdateregister;


    public static String AppDateFormat(String date){
        String ReturnDate=date;

        return ReturnDate;
    }

    public static String ServerDateFormat(String date){
        SimpleDateFormat sdf = new SimpleDateFormat("MMM dd, yyyy");
        Date testDate = null;
        try {
            testDate = sdf.parse(date);
        }catch(Exception ex){
            ex.printStackTrace();
        }
        SimpleDateFormat formatter = new SimpleDateFormat("MM-dd-yyyy");
        String newFormat = formatter.format(testDate);
        return newFormat;
    }

    public static int getCurrentDay() {
        int day = Calendar.getInstance().get(Calendar.DAY_OF_WEEK);
        if (day == 1) {
            return 7;
        } else {
            return day - 1;
        }
    }

    public static void hideKB(Context ctx, View v) {
        InputMethodManager mgr = (InputMethodManager) ctx
                .getSystemService(Context.INPUT_METHOD_SERVICE);
        mgr.hideSoftInputFromWindow(v.getWindowToken(), 0);
    }

    public static String getPuid(Context context) {
        oinsertupdateregister = RealmController.with((Application)context.getApplicationContext()).getRegisterInformation();
        String puid = oinsertupdateregister.getPuid();
        return  puid;
    }

    public static String getUserName(Context context) {
        oinsertupdateregister = RealmController.with((Application)context.getApplicationContext()).getRegisterInformation();
        String UserName = oinsertupdateregister.getFirstname()+" "+oinsertupdateregister.getLastname();
        return  UserName;
    }

}
