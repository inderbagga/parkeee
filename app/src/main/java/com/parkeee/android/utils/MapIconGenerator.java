package com.parkeee.android.utils;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.TextView;

import com.parkeee.android.R;

public class MapIconGenerator {
	Context ctx;

	private static final int ICON_GREY = R.drawable.icon_grey;
	private static final int ICON_PLATINUM=R.drawable.icon_platinum;
	private static final int ICON_GOLD = R.drawable.icon_gold;

	public MapIconGenerator(Context ctx) {
		this.ctx = ctx;
	}

	public Bitmap makeIcon(int drawable, String text) {
		TextView container = (TextView) LayoutInflater.from(ctx).inflate(
				R.layout.map_icon, null);
		container.setBackgroundResource(drawable);
		container.setText(text);
		int minSize = ctx.getResources().getDimensionPixelSize(
				R.dimen.gas_icon_grey_size);
		if (drawable == ICON_GOLD) {
			minSize = ctx.getResources().getDimensionPixelSize(
					R.dimen.gas_icon_blue_size);
		} else if (drawable == ICON_PLATINUM) {
			minSize = ctx.getResources().getDimensionPixelSize(
					R.dimen.gas_icon_green_size);
		}
		container.setMinWidth(minSize);
		int measureSpec = View.MeasureSpec.makeMeasureSpec(0,
				View.MeasureSpec.UNSPECIFIED);
		container.measure(measureSpec, measureSpec);

		int measuredWidth = container.getMeasuredWidth();
		int measuredHeight = container.getMeasuredHeight();

		container.layout(0, 0, measuredWidth, measuredHeight);

		Bitmap r = Bitmap.createBitmap(measuredWidth, measuredHeight,
				Bitmap.Config.ARGB_8888);
		r.eraseColor(Color.TRANSPARENT);

		Canvas canvas = new Canvas(r);
		container.draw(canvas);
		return r;
	}
}
