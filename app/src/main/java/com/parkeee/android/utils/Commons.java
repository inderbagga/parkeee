package com.parkeee.android.utils;

import android.content.Context;
import android.content.Intent;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.Uri;
import android.os.StrictMode;

import java.math.BigDecimal;
import java.math.RoundingMode;

/**
 * Created by inderbagga on 29/09/16.
 */
public class Commons {

    public static boolean isNetworkAvailable(Context mContext) {
        ConnectivityManager cm = (ConnectivityManager)mContext.
                getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = cm.getActiveNetworkInfo();

        StrictMode.ThreadPolicy policy = new StrictMode.
                ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);

        // if no network is available networkInfo will be null
        // otherwise check if we are connected
        if (networkInfo != null && networkInfo.isConnected()) {
            return true;
        }
        return false;
    }

    public static boolean isValidEmailAddress(String strEmailAddress)
    {
        return android.util.Patterns.EMAIL_ADDRESS.matcher(strEmailAddress).matches();
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        BigDecimal bd = new BigDecimal(value);
        bd = bd.setScale(places, RoundingMode.HALF_UP);
        return bd.doubleValue();
    }

    public static void gotoUrl(Context mContext,String mUrl){
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(mUrl));
        mContext.startActivity(intent);
    }
}
