package com.parkeee.android.utils;

/**
 * Created by user on 14-12-2016.
 */

public class AppMessages {

    public static String TECHNICAL_ERROR = "Sorry! The process failed due to some technical error. Please try after some time.";
    public static String NO_INTERNET = "Please Turn on cellular data or use Wi-Fi.";

    //.. COMMON
    public static String FIRST_NAME_BLANK = "First name field can't be blank.";
    public static String LAST_NAME_BLANK = "Last name can't be blank.";
    public static String MOBILE_NO_BLANK = "Mobile field can't be blank.";
    public static String MOBILE_NO_LENGTH = "Mobile number must be between 10-13 character.";
    public static String MOBILE_OTP_SENT = "Please check your registered mobile number and enter the OTP.";
    public static String ZIP_BLANK = "Zip field can't be blank.";
    public static String ZIP_LENGTH = "Zip code must be between 5-6 character.";
    public static String EMAIL_BLANK = "Email field can't be blank.";
    public static String VALID_EMAIL = "Please enter the valid email.";
    public static String ADDRESS_BLANK = "Address field can't be blank.";
    public static String CITY_BLANK = "City field can't be blank.";
    public static String STATE_BLANK = "State field can't be blank.";
    public static String COUNTRY_BLANK = "Please select the Country.";
    public static String REMINDER_DATE_BEFORE = "Reminder cannot be set for past Day or Today.";
    public static String REQUEST_FOR_ALLOW_LOCATION = "Location access is denied. Please go to the settings and allow the permission for loaction.";

    public static String COULD_NOT_SEND_EMAIL = "Your device could not send e-mail.  Please check e-mail configuration and try again.";

    public static String VERIFICATION_LINK_SENT = "Verification link has been sent to your registered email address.";

    //.. Change Password
    public static String CURRENT_PASSWORD_BLANK = "Current Password field can't be blank.";
    public static String CURRENT_PASSWORD_INVALID = "Please enter the valid Current Password.";
    public static String NEW_PASSWORD_BLANK = "New Password field can't be blank.";
    public static String NEW_PASSWORD_LENGTH = "New Password must be between 8-20 character.";
    public static String CONFIRM_NEW_PASSWORD_BLANK = "Confirm New Password field can't be blank.";
    public static String NEW_PASSWORD_CANT_CURRENT = "New Password can't be Current Password.";
    public static String NEW_PASSWORD_NOT_MATCHED = "New Password and Confirm New Password not matched.";

    //.. Camera
    public static String CAMERA_NOT_AVAILABLE = "Camera is not available on your device.";

    //.. Register
    public static String EMAIL_ALREADY_REGISTERED = "Email id already registered.";
    public static String PASSWORD_BLANK = "Password field can't be blank.";
    public static String PASSWORD_LENGTH = "Password must be between 8-20 character.";
    public static String CONFIRM_PASSWORD_BLANK = "Confirm Password field can't be blank.";
    public static String DOB_BLANK = "Please select the Date Of Birth (DOB).";
    public static String PASSWORD_NOT_MATCHED = "Password and confirm password not matched.";
    public static String ACCEPT_TERM = "Please accept terms and conditions.";

    //.. Login
    public static String INCORRECT_EMAIL_PASSWORD = "Incorrect email id or password.";
    public static String OTP_SENT = "Please check your registered email address and enter the security PIN.";
    public static String OTP_BLANK = "Security PIN field can't be blank.";
    public static String WRONG_OTP = "You have entered wrong security PIN.";

    //.. Forgot Password
    public static String EMAIL_NOT_REGISTERED = "This email address is not registered with us.";
    public static String FORGOT_SUCCESS = "We have successfully sent you a link to the registered email address. Please check the email for more details.";

    //.. Add Edit My Car
    public static String BRAND_BLANK = "Please select the Car Brand.";
    public static String COLOR_BLANK = "Please select the Car Color.";
    public static String LICENSE_PLATE_BLANK = "License Plate Number field can't be blank.";
    public static String CAR_TYPE_BLANK = "Please select the Car Type.";
    public static String REGISTRATION_NUMBER_BLANK = "Registration Number field can't be blank.";
    public static String INSPECTION_DATE_BLANK = "Please select the Inspection Date.";
    public static String INSPECTION_DATE_BEFORE = "Inspection Date reminder cannot be set for past Day or Today.";
    public static String REGISTRATION_EXPIRY_DATE_BLANK = "Please select the Registration Expiry Date.";
    public static String REGISTRATION_EXPIRY_DATE_BEFORE = "Registration Expiry Date reminder cannot be set for past Day or Today.";
    public static String INSPECTION_DATE_GREATER_THAN_EXPIRY = "Inspection Date cannot be set later than Registration Expiry Date.";

    //.. Delete My Car
    public static String DELETE_CAR_ALERT = "Are you sure you want to delete this Car?";

    //.. Add/Edit  Driving License
    public static String DL_ID_BLANK = "ID field can't be blank.";
    public static String DL_CLASS_BLANK = "Please select the Class.";
    public static String DL_ISSUE_DATE_BLANK = "Please select the Issue Date.";
    public static String DL_EXPIRY_DATE_BLANK = "Please select the Expiry Date.";
    public static String DL_FRONT_IMAGE_BLANK = "Please select the Driving license Front image.";
    public static String DL_BACK_IMAGE_BLANK = "Please select the Driving license Back image.";

    //.. Delete Driver License
    public static String DELETE_DL_ALERT = "Are you sure you want to delete this Driver License?";

    //.. Add/Edit  Insuranse
    public static String INS_CAR_BLANK = "Please select the Car.";
    public static String INS_COMPANY_BLANK = "Please select the Company.";
    public static String INS_OTHER_COMPANY_BLANK = "Other Company field can't be blank.";
    public static String INS_POLICY_BLANK = "Policy Number field can't be blank.";
    public static String INS_PREMIUM_BLANK = "Annual Premium field can't be blank.";
    public static String INS_PAYMENT_TYPE_BLANK = "Please select the Payment Type.";
    public static String INS_DUE_MONTH_BLANK = "Please select the Due Month.";
    public static String INS_DUE_DATE_BLANK = "Please select the Due Date.";
    public static String INS_EXPIRY_DATE_BLANK = "Please select the Expiration Date.";

    public static String INS_ALREADY_EXIST = "You already added this Insurance earlier";

    //.. Delete Insurance
    public static String DELETE_INS_ALERT = "Are you sure you want to delete this Insurance?";

    //.. Add/Edit  Violation
    public static String VL_CAR_BLANK = "Please select the Car.";
    public static String VL_TYPE_BLANK = "Please select the Violation Type.";
    public static String VL_NUMBER_BLANK = "Violation Number field can't be blank.";
    public static String VL_STATE_BLANK = "Please select the State.";
    public static String VL_AMOUNT_BLANK = "Violation Amount field can't be blank.";
    public static String VL_DATE_BLANK = "Please select the Violation Date.";
    public static String VL_DUE_DATE_BLANK = "Please select the Violation Due Date.";
    public static String VL_FRONT_IMAGE_BLANK = "Please select the Violation Front image.";
    public static String VL_BACK_IMAGE_BLANK = "Please select the Violation Back image.";

    public static String VL_ALREADY_EXIST = "You already added this Violation earlier";
    //.. Delete Insurance
    public static String DELETE_VL_ALERT = "Are you sure you want to delete this Violation?";

    //.. Add/Edit Other Bill
    public static String BILL_CATEGORY_BLANK = "Please select the Bill Category.";
    public static String BILL_OTHER_CATEGORY_BLANK = "Other Bill Category field can't be blank.";
    public static String BILL_NAME_BLANK = "Bill Name field can't be blank.";
    public static String BILL_AMOUNT_BLANK = "Amount Due field can't be blank.";
    public static String BILL_REMINDER_TYPE_BLANK = "Please select the Reminder Type.";
    public static String BILL_DUE_MONTH_BLANK = "Please select the Due Month.";
    public static String BILL_DUE_DATE_BLANK = "Please select the Due Date.";
    public static String BILL_DUE_DAY_BLANK = "Please select the Due Day.";

    //.. Delete Other Bill
    public static String DELETE_OTHER_BILL_ALERT = "Are you sure you want to delete Other Bills?";

    //.. Car Finder
    public static String REMINDER_TIME_BEFORE = "Reminder cannot be set for past.";
    public static String CANCEL_CF_REMINDER = "Are you sure you want to cancel reminder?";

    //.. GS Manager Login
    public static String GS_MANAGER_ID_BLANK = "Gas Station ID field can't be blank.";
    public static String GS_MANAGER_PIN_BLANK = "Security PIN can't be blank.";
    public static String GS_MANAGER_ACCOUNT_BLOCK = "Your Gas Station member account is blocked";
    public static String GS_MANAGER_INVALID_CREDENTIAL = "Invalid Gas Station ID or Security PIN!";

    public static String GS_MANAGER_PRICE_MORE = "Price can't be more than $10";

    //.. Gas Station
    public static String GS_FAV_ADDED = "Gas Station is added into your favorite list.";
    public static String GS_FAV_REMOVED = "Gas Station is removed from your favorite list.";

    //.. LOG OUT
    public static String LOGOUT_ALERT = "Are you sure you want to Logout?";
}
