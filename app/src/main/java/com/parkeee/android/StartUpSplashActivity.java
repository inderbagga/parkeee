package com.parkeee.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;

/**
 * Created by user on 24-11-2016.
 */

public class StartUpSplashActivity extends Activity {
    private final int SPLASH_DISPLAY_LENGTH = 500;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_startup_splash);
        new Handler().postDelayed(new Runnable(){
            @Override
            public void run() {
	                /* Create an Intent that will start the Menu-Activity. */
                Intent ISA = new Intent(StartUpSplashActivity.this,SplashActivity.class);
                StartUpSplashActivity.this.startActivity(ISA);
                StartUpSplashActivity.this.finish();
            }
        }, SPLASH_DISPLAY_LENGTH);
    }

}