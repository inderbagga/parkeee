package com.parkeee.android.adapter;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Typeface;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

/**
 * Created by BRAR on 5/29/15.
 */
public class MyArrayAdapter extends ArrayAdapter<String> {
    Context contxt;
    String fontName = "fonts/light.ttf";
    Typeface externalFont;

    public int selectedItem = -1;


    public MyArrayAdapter(Context context, int resource, String arr[]) {
        super(context, resource, arr);
        externalFont = Typeface.createFromAsset(context.getAssets(), fontName);
        contxt=context;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View v = super.getView(position, convertView, parent);
        ((TextView) v).setTypeface(externalFont);
        System.out.println(position+" POSTION"+v);
        return v;

    }


    public View getDropDownView(int position, View convertView, ViewGroup parent) {
        //System.out.println(position+" POSTION");
        //Toast.makeText(contxt, position+" POSTION", Toast.LENGTH_SHORT).show();
       /* View v = super.getDropDownView(position, convertView, parent);
        ((TextView) v).setTypeface(externalFont);

        return v;*/

        View v = null;
        v = super.getDropDownView(position, null, parent);
        // If this is the selected item position
        if (position == selectedItem && selectedItem != 0) {
            v.setBackgroundColor(Color.rgb(240, 240, 240));
        }
        else {
            // for other views
            v.setBackgroundColor(Color.WHITE);

        }
        return v;
    }
}
