package com.parkeee.android.adapter.realm;

import android.content.Context;

import com.parkeee.android.model.tables.ViolationInsertUpdateCache;

import io.realm.RealmResults;

public class RealmViolationsReminderAdapter extends RealmModelAdapter<ViolationInsertUpdateCache> {

    public RealmViolationsReminderAdapter(Context context, RealmResults<ViolationInsertUpdateCache> realmResults, boolean automaticUpdate) {

        super(context, realmResults, automaticUpdate);
    }
}