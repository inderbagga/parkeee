package com.parkeee.android.adapter.realm;

import android.content.Context;
import com.parkeee.android.model.tables.UserBillingInsertUpdateCache;

import io.realm.RealmResults;

public class RealmOtherBillsAdapter extends RealmModelAdapter<UserBillingInsertUpdateCache> {

    public RealmOtherBillsAdapter(Context context, RealmResults<UserBillingInsertUpdateCache> realmResults, boolean automaticUpdate) {

        super(context, realmResults, automaticUpdate);
    }
}