package com.parkeee.android.adapter;

import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.R;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.gas.ActivityGasStationDetail;
import com.parkeee.android.gas.ActivitySearchGasStation;
import com.parkeee.android.model.tables.AddNewCarTb;
import com.parkeee.android.model.tables.GSFavouriteTb;
import com.parkeee.android.utils.Utils;

import io.realm.RealmResults;

/**
 * Created by user on 26-12-2016.
 */

public class FavGasStationAdapter extends BaseAdapter {

    RealmResults<GSFavouriteTb>list;
    Context mContext;

    public FavGasStationAdapter(RealmResults<GSFavouriteTb> List, Context context) {
        list=List;
        mContext=context;
    }



    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int i) {
        return null;
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup viewGroup) {

        TextView txt_fav_gs_name ,txt_fav_gs_address, txt_fav_gs_lastupdated ,txt_fav_gs_price;
        LinearLayout ll_gs_price;

        LayoutInflater inflater ;
        inflater = LayoutInflater.from(mContext);
        convertView =  inflater.inflate(R.layout.lv_item_fav, null);
        txt_fav_gs_name=(TextView)convertView.findViewById(R.id.txt_fav_gs_name);
        txt_fav_gs_address=(TextView)convertView.findViewById(R.id.txt_fav_gs_address);
        txt_fav_gs_lastupdated=(TextView)convertView.findViewById(R.id.txt_fav_gs_lastupdated);
        txt_fav_gs_price=(TextView)convertView.findViewById(R.id.txt_fav_gs_price);

        ll_gs_price=(LinearLayout)convertView.findViewById(R.id.ll_gs_price);
        ll_gs_price.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent Intnt= new Intent(mContext, ActivityGasStationDetail.class);
                Intnt.putExtra("selectedGSID",list.get(position).getGsID());
                Intnt.putExtra("FTID",String.valueOf(list.get(position).getFtid()));
                Intnt.putExtra("DAY",String.valueOf(Utils.getCurrentDay()));
                Intnt.putExtra("puid",Utils.getPuid(mContext));
                mContext.startActivity(Intnt);

            }
        });

        txt_fav_gs_name.setText(list.get(position).getBrandName().toString());
        txt_fav_gs_address.setText(list.get(position).getStreet().toString()+", "+list.get(position).getCity().toString());
        txt_fav_gs_lastupdated.setText("Last updated on: "+list.get(position).getLastUpdateOn().toString());
        txt_fav_gs_price.setText(list.get(position).getPricePer().toString());

       // txt_Brand.setText(list.get(position).getBrand().toString());

        return convertView;
    }
}
