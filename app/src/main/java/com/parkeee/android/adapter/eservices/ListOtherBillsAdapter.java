package com.parkeee.android.adapter.eservices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.parkeee.android.R;
import com.parkeee.android.adapter.realm.RealmRecyclerViewAdapter;
import com.parkeee.android.backend.Config;
import com.parkeee.android.backend.Data;
import com.parkeee.android.backend.RealmController;

import com.parkeee.android.eservices.otherbills.EditOtherBill;
import com.parkeee.android.model.tables.UserBillingInsertUpdateCache;
import com.parkeee.android.views.CustomTextView;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.concurrent.TimeUnit;

import io.realm.Realm;

/**
 * @author Inder Bagga
 */
public class ListOtherBillsAdapter extends RealmRecyclerViewAdapter<UserBillingInsertUpdateCache> {

    Context oContext;
    private Realm realm;
    private Callback oCallback;
    private Activity oActivity;
    private boolean isDataViewVisible=true;
    private boolean isDeleteViewVisible=false;

    private ArrayList<Integer> mSelectedPositions;
    private ArrayList<Boolean> bSelectedStates;
    int bSize;

    public ListOtherBillsAdapter(Callback mCallback, Context mContext, Activity mActivity) {

        oContext=mContext;
        oCallback=mCallback;
        oCallback=mCallback;
        oActivity=mActivity;

        bSelectedStates = new ArrayList<>();
        mSelectedPositions = new ArrayList<>();

        bSize=RealmController.with(mActivity).getOtherBills().size();

        for(int index=0;index<bSize;index++){
            bSelectedStates.add(false);
        }
    }

    public interface Callback {

        void onIconClicked(int index);
        void onActivated();
        void onDeactivated();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate a new card view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_other_bills, parent, false);
        return new CardViewHolder(view);
    }

    public boolean isDeletionActivated() {

        return  isDeleteViewVisible;
    }

    public void deletionActivated() {
        isDataViewVisible=false;
        isDeleteViewVisible=true;
        notifyDataSetChanged();
    }

    public void clearSelected() {
        isDataViewVisible=true;
        isDeleteViewVisible=false;

        mSelectedPositions.clear();

        for(int index=0;index<bSize;index++){
            bSelectedStates.set(index,false);
        }

        notifyDataSetChanged();
    }


    public void deleteSelected(int index) {
        final boolean newState = !mSelectedPositions.contains(index);
        if (newState){
            mSelectedPositions.add(index);
            bSelectedStates.set(index,true);
        }
        else
        {
            mSelectedPositions.remove((Integer) index);
            bSelectedStates.set(index,false);
        }

        //notifyItemChanged(index);
    }

    public int getSelectedCount() {
        return mSelectedPositions.size();
    }

    public ArrayList<Integer> getSelectedPositions() {
        return mSelectedPositions;
    }

    // replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        realm = RealmController.getInstance().getRealm();

        // get the item
        final UserBillingInsertUpdateCache oUserBillingInsertUpdateCache = getItem(position);

        // cast the generic view holder to our specific one
        final CardViewHolder holder = (CardViewHolder) viewHolder;


        if(isDataViewVisible){
            holder.dataLayout.setVisibility(View.VISIBLE);
        } else holder.dataLayout.setVisibility(View.GONE);

        if(isDeleteViewVisible){
            holder.deleteLayout.setVisibility(View.VISIBLE);
            holder.deleteView.setVisibility(View.VISIBLE);
            holder.selectedView.setVisibility(View.GONE);

        }
        else holder.deleteLayout.setVisibility(View.GONE);


        holder.deleteLayout.setTag("icon:" + position);
        holder.deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] tag = ((String) view.getTag()).split(":");
                int index = Integer.parseInt(tag[1]);

                if (tag[0].equals("icon")) {
                    if (oCallback != null){

                        oCallback.onIconClicked(index);

                        if(mSelectedPositions.contains(index)){

                            holder.selectedView.setVisibility(View.VISIBLE);
                            holder.deleteView.setVisibility(View.GONE);
                        }
                        else {
                            holder.selectedView.setVisibility(View.GONE);
                            holder.deleteView.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });

        holder.actionEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Intent editIntent=new Intent(oContext, EditOtherBill.class);
                editIntent.putExtra("TIMESTAMP", oUserBillingInsertUpdateCache.getTimeStamp());
                oContext.startActivity(editIntent);
                oActivity.finishAffinity();
            }
        });

        holder.actionShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strText="Here is the Other Bill Info:\n"+
                        "Category:"+oUserBillingInsertUpdateCache.getCategoryName()+" \n"+
                        "Bill Name:"+oUserBillingInsertUpdateCache.getBillName()+" \n"+
                        "BillAmount:"+oUserBillingInsertUpdateCache.getBillAmount()+" \n"+
                        "BillType:"+oUserBillingInsertUpdateCache.getBillType();

                Intent intent2 = new Intent(); intent2.setAction(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                intent2.putExtra(Intent.EXTRA_TEXT, strText );
                oContext.startActivity(Intent.createChooser(intent2, "Share via"));
            }
        });

        holder.tvBill.setText(oUserBillingInsertUpdateCache.getBillName());
        holder.tvCategory.setText(oUserBillingInsertUpdateCache.getCategoryName());
        holder.tvReminderType.setText(Data.strArrayReminderTypes[Integer.parseInt(oUserBillingInsertUpdateCache.getBillType())]);
        holder.tvAmountDue.setText("$"+ oUserBillingInsertUpdateCache.getBillAmount());

        Calendar dueTimeCalendar=Calendar.getInstance();
        dueTimeCalendar.set(Calendar.HOUR_OF_DAY,0);
        dueTimeCalendar.set(Calendar.MINUTE,0);
        dueTimeCalendar.set(Calendar.SECOND,0);

        Date today = dueTimeCalendar.getTime();

        int iSelectedMonth=Integer.parseInt(oUserBillingInsertUpdateCache.getBill_due_month());
        int iSelectedDate=Integer.parseInt(oUserBillingInsertUpdateCache.getBill_due_day());
        int iReminderMode=Integer.parseInt(oUserBillingInsertUpdateCache.getBillType());

        switch(iReminderMode){

            case 0:

                iSelectedDate=Integer.parseInt(oUserBillingInsertUpdateCache.getBill_due_day_of_week());

                dueTimeCalendar.set(Calendar.DAY_OF_WEEK,iSelectedDate+1);
                while( dueTimeCalendar.getTime().compareTo(today) < 0 ){
                    dueTimeCalendar.add(Calendar.DAY_OF_WEEK,7);
                }

                break;

            case 1:
                dueTimeCalendar.set(Calendar.DAY_OF_MONTH,iSelectedDate);

                while( dueTimeCalendar.getTime().compareTo(today) < 0 ){
                    dueTimeCalendar.add(Calendar.MONTH,1);
                }

                break;

            case 2:
            case 3:
            case 4:
                dueTimeCalendar.set(Calendar.DAY_OF_MONTH,iSelectedDate);
                dueTimeCalendar.set(Calendar.MONTH,iSelectedMonth);

                while( dueTimeCalendar.getTime().compareTo(today) < 0 ){

                    switch (iReminderMode){

                        case 2: dueTimeCalendar.add(Calendar.MONTH,3);break;
                        case 3: dueTimeCalendar.add(Calendar.MONTH,6);break;
                        case 4: dueTimeCalendar.add(Calendar.YEAR,1);break;
                    }
                }
                break;
        }

        String strDueDate=new SimpleDateFormat(Config.DATE_FORMAT).format(dueTimeCalendar.getTime());
        holder.tvDueDate.setText(strDueDate);

        holder.tvDueTime.setText(findDueTime(dueTimeCalendar.getTime(),today));
    }

    private String findDueTime(Date dueTime,Date today) {

        long diff = dueTime.getTime() - today.getTime();

        long dueDays=TimeUnit.DAYS.convert(diff, TimeUnit.MILLISECONDS);

        if(dueDays==1)return "Tommorrow";
        else if(dueDays<30) return dueDays+" days";
        else return dueDays/30==1?"1 month":dueDays/30+" months";
    }

    // return the size of your data set (invoked by the layout manager)
    public int getItemCount() {

        if (getRealmAdapter() != null) {
            return getRealmAdapter().getCount();
        }
        return 0;
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        public FrameLayout deleteLayout;
        public FrameLayout dataLayout;

        public RelativeLayout deleteView;
        public RelativeLayout selectedView;

        public ImageView actionShare;
        public ImageView actionEdit;

        public CustomTextView tvBill;
        public CustomTextView tvCategory;
        public CustomTextView tvAmountDue;
        public CustomTextView tvReminderType;
        public CustomTextView tvDueDate,tvDueTime;

        public CardViewHolder(View itemView) {
            super(itemView);

            deleteLayout = (FrameLayout) itemView.findViewById(R.id.deleteLayout);
            dataLayout = (FrameLayout) itemView.findViewById(R.id.dataLayout);

            deleteView = (RelativeLayout) itemView.findViewById(R.id.deleteView);
            selectedView = (RelativeLayout) itemView.findViewById(R.id.selectedView);

            actionShare = (ImageView) itemView.findViewById(R.id.action_share);
            actionEdit = (ImageView) itemView.findViewById(R.id.action_edit);

            tvBill = (CustomTextView) itemView.findViewById(R.id.tv_bill);
            tvCategory = (CustomTextView) itemView.findViewById(R.id.tv_category);
            tvAmountDue = (CustomTextView) itemView.findViewById(R.id.tv_amount_due);
            tvReminderType = (CustomTextView) itemView.findViewById(R.id.tv_reminder_type);
            tvDueDate = (CustomTextView) itemView.findViewById(R.id.tv_due_date);
            tvDueTime = (CustomTextView) itemView.findViewById(R.id.tv_due_time);
        }
    }
}