package com.parkeee.android.adapter;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;

import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.menu.ActivityAddCar;
import com.parkeee.android.menu.ActivityMyCars;
import com.parkeee.android.model.params.DeleteCarPUID;
import com.parkeee.android.model.tables.AddNewCarTb;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.PostApiClient;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.utils.AppMessages;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.RealmResults;

/**
 * Created by user on 16-12-2016.
 */

public class MyCarsAdapter extends BaseAdapter implements OnResultReceived {

    RealmResults<AddNewCarTb>list;
    Context mContext;
    OnResultReceived mOnResultReceived;
    Gson oGson;
    ProgressDialog mProgressDialog;
    String ServerResult;
    AddNewCarTb oaddnewcartb;
    InsertUpdateRegister oinsertupdateregister;
    String Delete_Id="";


    public MyCarsAdapter(RealmResults<AddNewCarTb> List, Context context) {
        list=List;
        mContext=context;
        mOnResultReceived=this;
        oGson=new Gson();
        mProgressDialog=new ProgressDialog(mContext);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");
        oinsertupdateregister = RealmController.with((Application)mContext.getApplicationContext()).getRegisterInformation();
    }

    @Override
    public int getCount() {
        return list.size();
    }

    @Override
    public Object getItem(int position) {
        return null;
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {

        TextView txt_Brand,txt_plate_no,txt_car_type;
        LinearLayout ll_edit_mycars,ll_delete_mycars;
        LayoutInflater inflater ;
        inflater = LayoutInflater.from(mContext);
        convertView =  inflater.inflate(R.layout.listitem_mycars, null);
        txt_Brand=(TextView)convertView.findViewById(R.id.txt_brand);
        txt_plate_no=(TextView)convertView.findViewById(R.id.txt_plate_no);
        txt_car_type=(TextView)convertView.findViewById(R.id.txt_car_type);
        txt_Brand.setText(list.get(position).getBrand().toString());
        txt_plate_no.setText(list.get(position).getPlateNumber().toString());
        txt_car_type.setText(list.get(position).getType().toString());

        ll_edit_mycars=(LinearLayout)convertView.findViewById(R.id.ll_edit_mycars);
        ll_edit_mycars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent Intnt= new Intent(mContext, ActivityAddCar.class);
                Intnt.putExtra("is_Edit",true);
                Intnt.putExtra("id",list.get(position).getId().toString());
                mContext.startActivity(Intnt);
            }
        });

        ll_delete_mycars=(LinearLayout)convertView.findViewById(R.id.ll_delete_mycars);
        ll_delete_mycars.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                new SweetAlertDialog(mContext, SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Alert")
                        .setContentText("Are you sure you want to delete this car?")
                        .setCancelText("No")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();
                            }
                        })
                        .setConfirmText("Yes")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();

                                Delete_Id=list.get(position).getId().toString();
                                oaddnewcartb= RealmController.with((Application)mContext.getApplicationContext()).getCarItem(list.get(position).getId().toString());
                                DeleteCarPUID oDeleteCarPUID= new DeleteCarPUID();
                                oDeleteCarPUID.setBrand(oaddnewcartb.getBrand());
                                oDeleteCarPUID.setCar_type(oaddnewcartb.getType());
                                oDeleteCarPUID.setCarregno(oaddnewcartb.getRegNumber());
                                oDeleteCarPUID.setColor(oaddnewcartb.getColor());
                                oDeleteCarPUID.setExpiry_date(oaddnewcartb.getRegistrationDate());
                                oDeleteCarPUID.setInspection_date(oaddnewcartb.getInspectionDate());
                                oDeleteCarPUID.setLicence_plate_number(oaddnewcartb.getId());
                                oDeleteCarPUID.setModel(oaddnewcartb.getBrand());
                                oDeleteCarPUID.setNew_licence_plate_number(oaddnewcartb.getPlateNumber());
                                oDeleteCarPUID.setPuid(oinsertupdateregister.getPuid());
                                String jsonString=oGson.toJson(oDeleteCarPUID,DeleteCarPUID.class).toString();
                                mProgressDialog.show();
                                PostApiClient oInsertUpdateApi=new PostApiClient(mOnResultReceived);
                                oInsertUpdateApi.setRequestSource(RequestSource.DeleteCar);
                                oInsertUpdateApi.executePostRequest(API.fDeleteCar(),jsonString);

                            }
                        })
                        .show();

            }
        });

        return convertView;
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        mProgressDialog.dismiss();
        ServerResult=what.replace("\"","");
        switch(ServerResult){
            case "-2":
            case "-3":
                displayAlert("app error",false);
                break;
            case "-1":
                displayAlert(AppMessages.TECHNICAL_ERROR,false);
                break;
            default:
                displayAlert("Car Deleted Successfully",true);
                break;
        }
    }


    private void displayAlert(final String strMessage,final boolean is_successfully) {
        ((Activity)mContext).runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog=new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle("Parkeee"+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton("Ok", new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if(is_successfully){
                                    RealmController oRealmController= new RealmController((Application) mContext.getApplicationContext());
                                    oRealmController.removeCarItems(Delete_Id);
                                    mContext.startActivity(new Intent(mContext,ActivityMyCars.class));
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }
}
