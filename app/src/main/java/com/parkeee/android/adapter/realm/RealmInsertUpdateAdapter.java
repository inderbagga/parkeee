package com.parkeee.android.adapter.realm;

import android.content.Context;

import com.parkeee.android.model.tables.InsertUpdateCache;

import io.realm.RealmResults;

public class RealmInsertUpdateAdapter extends RealmModelAdapter<InsertUpdateCache> {

    public RealmInsertUpdateAdapter(Context context, RealmResults<InsertUpdateCache> realmResults, boolean automaticUpdate) {

        super(context, realmResults, automaticUpdate);
    }
}