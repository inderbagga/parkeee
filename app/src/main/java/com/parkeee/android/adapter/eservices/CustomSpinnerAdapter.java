package com.parkeee.android.adapter.eservices;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;
import android.widget.Toast;

import com.parkeee.android.R;

/**
 * Created by inderbagga on 05/12/16.
 */
public class CustomSpinnerAdapter extends ArrayAdapter<String> {

    Context _context;
    String[] objects;

    public CustomSpinnerAdapter(Context context, int textViewResourceId,
                           String[] objects) {
        super(context, textViewResourceId, objects);
        this._context=context;
        this.objects=objects;

    }

    @Override
    public View getDropDownView(int position, View convertView, ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        return getCustomView(position, convertView, parent);
    }

    public View getCustomView(final int position, View convertView, ViewGroup parent) {


        View row=LayoutInflater.from(_context).inflate(R.layout.list_item_textview, parent, false);
        TextView label=(TextView)row.findViewById(R.id.tv_id);
        label.setText(objects[position]);

        row.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                Toast.makeText(_context,objects[position],Toast.LENGTH_SHORT).show();
            }
        });

        return row;
    }
}