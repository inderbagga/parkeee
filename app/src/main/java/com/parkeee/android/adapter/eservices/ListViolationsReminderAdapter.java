package com.parkeee.android.adapter.eservices;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.parkeee.android.R;
import com.parkeee.android.adapter.realm.RealmRecyclerViewAdapter;
import com.parkeee.android.backend.Config;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.model.tables.ViolationInsertUpdateCache;
import com.parkeee.android.views.CustomTextView;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import io.realm.Realm;

/**
 * @author Inder Bagga
 */
public class ListViolationsReminderAdapter extends RealmRecyclerViewAdapter<ViolationInsertUpdateCache> {

    Context oContext;
    private Realm realm;
    private Callback oCallback;
    private Activity oActivity;
    private boolean isDataViewVisible=true;
    private boolean isDeleteViewVisible=false;

    private ArrayList<Integer> mSelectedPositions;
    private ArrayList<Boolean> bSelectedStates;
    int bSize;

    public ListViolationsReminderAdapter(Callback mCallback, Context mContext, Activity mActivity) {

        oContext=mContext;
        oCallback=mCallback;
        oCallback=mCallback;
        oActivity=mActivity;

        bSelectedStates = new ArrayList<>();
        mSelectedPositions = new ArrayList<>();

        bSize=RealmController.with(mActivity).getViolationsReminder().size();

        for(int index=0;index<bSize;index++){
            bSelectedStates.add(false);
        }
    }

    public interface Callback {

        void onIconClicked(int index);
        void onActivated();
        void onDeactivated();
    }

    @Override
    public CardViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        // inflate a new card view
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_view_violation_reminder, parent, false);
        return new CardViewHolder(view);
    }

    public boolean isDeletionActivated() {

        return  isDeleteViewVisible;
    }

    public void deletionActivated() {
        isDataViewVisible=false;
        isDeleteViewVisible=true;
        notifyDataSetChanged();
    }

    public void clearSelected() {
        isDataViewVisible=true;
        isDeleteViewVisible=false;

        mSelectedPositions.clear();

        for(int index=0;index<bSize;index++){
            bSelectedStates.set(index,false);
        }

        notifyDataSetChanged();
    }


    public void deleteSelected(int index) {
        final boolean newState = !mSelectedPositions.contains(index);
        if (newState){
            mSelectedPositions.add(index);
            bSelectedStates.set(index,true);
        }
        else
        {
            mSelectedPositions.remove((Integer) index);
            bSelectedStates.set(index,false);
        }
    }

    public int getSelectedCount() {
        return mSelectedPositions.size();
    }

    public ArrayList<Integer> getSelectedPositions() {
        return mSelectedPositions;
    }

    // replace the contents of a view (invoked by the layout manager)
    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, final int position) {

        realm = RealmController.getInstance().getRealm();

        // get the item
        final ViolationInsertUpdateCache oViolationInsertUpdateCache = getItem(position);

        // cast the generic view holder to our specific one
        final CardViewHolder holder = (CardViewHolder) viewHolder;

        if(isDataViewVisible){
            holder.dataLayout.setVisibility(View.VISIBLE);
        } else holder.dataLayout.setVisibility(View.GONE);

        if(isDeleteViewVisible){
            holder.deleteLayout.setVisibility(View.VISIBLE);
            holder.deleteView.setVisibility(View.VISIBLE);
            holder.selectedView.setVisibility(View.GONE);
        }
        else holder.deleteLayout.setVisibility(View.GONE);

        holder.deleteLayout.setTag("icon:" + position);
        holder.deleteLayout.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String[] tag = ((String) view.getTag()).split(":");
                int index = Integer.parseInt(tag[1]);

                if (tag[0].equals("icon")) {
                    if (oCallback != null){

                        oCallback.onIconClicked(index);

                        if(mSelectedPositions.contains(index)){

                            holder.selectedView.setVisibility(View.VISIBLE);
                            holder.deleteView.setVisibility(View.GONE);
                        }
                        else {
                            holder.selectedView.setVisibility(View.GONE);
                            holder.deleteView.setVisibility(View.VISIBLE);
                        }
                    }
                }
            }
        });

        holder.actionEdit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

              /*Intent editIntent=new Intent(oContext, EditOtherBill.class);
                editIntent.putExtra("TIMESTAMP", oViolationInsertUpdateCache.getTimeStamp());
                oContext.startActivity(editIntent);
                oActivity.finishAffinity();*/
            }
        });

        holder.actionShare.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String strText="Here is the Violation Reminder Info:\n"/*+
                        "Category:"+oViolationInsertUpdateCache.getCategoryName()+" \n"+
                        "Bill Name:"+oViolationInsertUpdateCache.getBillName()+" \n"+
                        "BillAmount:"+oViolationInsertUpdateCache.getBillAmount()+" \n"+
                        "BillType:"+oViolationInsertUpdateCache.getBillType()*/;

                Intent intent2 = new Intent(); intent2.setAction(Intent.ACTION_SEND);
                intent2.setType("text/plain");
                intent2.putExtra(Intent.EXTRA_TEXT, strText );
                oContext.startActivity(Intent.createChooser(intent2, "Share via"));
            }
        });

        try{
            Date violationDate=new SimpleDateFormat(Config.VL_DATE_FORMAT).parse(oViolationInsertUpdateCache.getViolation_date());
            holder.tvIssueDate.setText(new SimpleDateFormat(Config.DATE_FORMAT).format(violationDate));
        }catch(ParseException pe){}

        holder.tvCar.setText(oViolationInsertUpdateCache.getViolation_plate());
        holder.tvState.setText(oViolationInsertUpdateCache.getViolation_state());
        holder.tvViolation.setText("VIOLATION: "+oViolationInsertUpdateCache.getViolation_number());
        holder.tvViolationType.setText(oViolationInsertUpdateCache.getViolation_type());
        holder.tvAmount.setText("$"+ oViolationInsertUpdateCache.getViolation_amount());
    }

    // return the size of your data set (invoked by the layout manager)
    public int getItemCount() {

        if (getRealmAdapter() != null) {
            return getRealmAdapter().getCount();
        }
        return 0;
    }

    public static class CardViewHolder extends RecyclerView.ViewHolder {

        public FrameLayout deleteLayout;
        public FrameLayout dataLayout;

        public RelativeLayout deleteView;
        public RelativeLayout selectedView;

        public ImageView actionShare;
        public ImageView actionEdit;

        public CustomTextView tvViolation;
        public CustomTextView tvCar;
        public CustomTextView tvViolationType;
        public CustomTextView tvState;
        public CustomTextView tvIssueDate;
        public CustomTextView tvAmount;

        public CardViewHolder(View itemView) {
            super(itemView);

            deleteLayout = (FrameLayout) itemView.findViewById(R.id.deleteLayout);
            dataLayout = (FrameLayout) itemView.findViewById(R.id.dataLayout);

            deleteView = (RelativeLayout) itemView.findViewById(R.id.deleteView);
            selectedView = (RelativeLayout) itemView.findViewById(R.id.selectedView);

            actionShare = (ImageView) itemView.findViewById(R.id.action_share);
            actionEdit = (ImageView) itemView.findViewById(R.id.action_edit);

            tvViolation = (CustomTextView) itemView.findViewById(R.id.tv_violation);
            tvCar = (CustomTextView) itemView.findViewById(R.id.tv_car);
            tvViolationType = (CustomTextView) itemView.findViewById(R.id.tv_violation_type);
            tvState = (CustomTextView) itemView.findViewById(R.id.tv_state);
            tvIssueDate = (CustomTextView) itemView.findViewById(R.id.tv_issue_date);
            tvAmount = (CustomTextView) itemView.findViewById(R.id.tv_amount);
        }
    }
}