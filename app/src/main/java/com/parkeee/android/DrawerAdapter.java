package com.parkeee.android;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;


import java.util.ArrayList;


class DrawerItemBean {
    boolean isExpanded;
    private int icon;
    private String title, subtitle;

    public DrawerItemBean(int icon, String title, String sub) {
        super();
        this.icon = icon;
        this.title = title;
        this.subtitle = sub;
    }

    public int getIcon() {
        return icon;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public void setIsExpanded(boolean isExpanded) {
        this.isExpanded = isExpanded;
    }
}

@SuppressWarnings("unchecked")
public class DrawerAdapter extends BaseExpandableListAdapter {

    private final Context context;
    public ArrayList<DrawerItemBean> groupItem, tempChild;
    public ArrayList<Object> childItem = new ArrayList<Object>();
    public Activity activity;
    private String userName;

    public DrawerAdapter(Context context) {
        this.context = context;
        setGroupData();
        setChildGroupData();
    }

    public void setGroupData() {
        groupItem = new ArrayList<DrawerItemBean>();

//        groupItem.add(new DrawerItemBean(com.parkeee.llc.R.drawable.user, userName, null));
        groupItem.add(new DrawerItemBean(R.drawable.home, "Home", null));
        groupItem.add(new DrawerItemBean(R.drawable.user, "My Profile", null));

        groupItem.add(new DrawerItemBean(R.drawable.fav, "Favorites", null));
        groupItem.add(new DrawerItemBean(R.drawable.inbox, "Inbox", null));

      /* Commented by Amit on 27/07/2016
         Item of Drawer to hide from Drawer Adapter.
         Also you need to comment child from setChildGroupData(), otherwise it will out of bound exception at runitme*/
       // groupItem.add(new DrawerItemBean(com.parkeee.llc.R.drawable.history, "History", null));

        groupItem.add(new DrawerItemBean(R.drawable.my_cars, "My Cars", null));
        groupItem.add(new DrawerItemBean(R.drawable.referral, "Referral", null));
        groupItem.add(new DrawerItemBean(R.drawable.settings, "Settings", null));

        groupItem
                .add(new DrawerItemBean(R.drawable.partners, "Partners", null));
        groupItem.add(new DrawerItemBean(R.drawable.manage, "Manage",
                null));

    }

    public void setChildGroupData() {

        ArrayList<String> child = new ArrayList<String>();
        childItem.add(child);
        childItem.add(child);
        childItem.add(child);
        childItem.add(child);
        childItem.add(child);
        childItem.add(child);
        childItem.add(child);


        ArrayList<DrawerItemBean> child1 = new ArrayList<DrawerItemBean>();

        child1.add(new DrawerItemBean(R.drawable.parkeee_org_icon, "Parkeee.Org", null));
        child1.add(new DrawerItemBean(R.drawable.alsets_icon, "Alsets", null));
        child1.add(new DrawerItemBean(R.drawable.swatico_icon, "SWATico", null));

        // child1.add(new DrawerItemBean(R.drawable.sp_parked_my_car,
        // "Parking Lot", null));
        // child1.add(new DrawerItemBean(R.drawable.sp_service,
        // "Service Station",
        // null));
        childItem.add(child1);
        child1 = new ArrayList<DrawerItemBean>();
        child1.add(new DrawerItemBean(R.drawable.gas_station, "Gas Station", null));
        childItem.add(child1);

    }

    @Override
    public Object getChild(int groupPosition, int childPosition) {
        return null;
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return 0;
    }


    public void setCollapasedPostion(int pos) {
        getGroup(pos).setIsExpanded(false);
    }

    public void setExpandedPostion(int pos) {
        getGroup(pos).setIsExpanded(true);
    }


    @Override
    public View getChildView(int groupPosition, final int childPosition,
                             boolean isLastChild, View convertView, ViewGroup parent) {
        tempChild = (ArrayList) childItem.get(groupPosition);

        DrawerItemBean objBean = tempChild.get(childPosition);
        TextView txt, text1Sub;
        if (convertView == null) {
            LayoutInflater l = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (LinearLayout) l.inflate(R.layout.drawer_list_item,
                    null);
            // convertView = getGenericView(35);
        }

        convertView.setPadding(95, convertView.getPaddingTop(), convertView.getPaddingRight(), convertView.getPaddingBottom());
        ImageView img;
        img = (ImageView) convertView.findViewById(R.id.img);
        txt = (TextView) convertView.findViewById(R.id.text1);

        img.setImageResource(objBean.getIcon());

        // text = (TextView) convertView;

        txt.setText(objBean.getTitle());
        /*txt.setTextSize(12);*/
        convertView.setTag(objBean.getTitle());

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return ((ArrayList<String>) childItem.get(groupPosition)).size();
    }

    @Override
    public DrawerItemBean getGroup(int groupPosition) {
        return groupItem.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return groupItem.size();
    }

    @Override
    public void onGroupCollapsed(int groupPosition) {
        super.onGroupCollapsed(groupPosition);
    }

    @Override
    public void onGroupExpanded(int groupPosition) {
        super.onGroupExpanded(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return 0;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded,
                             View convertView, ViewGroup parent) {
        TextView txt, text1Sub;
        if (convertView == null) {
            LayoutInflater l = (LayoutInflater) context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = (LinearLayout) l.inflate(R.layout.drawer_list_item,
                    null);
            // convertView = getGenericView(35);
        }
        ImageView img, imgArrow;
        imgArrow = (ImageView) convertView.findViewById(R.id.imgArrow);
        img = (ImageView) convertView.findViewById(R.id.img);
        txt = (TextView) convertView.findViewById(R.id.text1);

        img.setImageResource(groupItem.get(groupPosition).getIcon());

        txt.setText(groupItem.get(groupPosition).getTitle());
        convertView.setTag(groupItem.get(groupPosition).getTitle());
        if (groupItem.get(groupPosition).getSubtitle() != null) {
            text1Sub = (TextView) convertView.findViewById(R.id.text1Sub);
            text1Sub.setVisibility(View.VISIBLE);
            text1Sub.setText(groupItem.get(groupPosition).getSubtitle());
        }
        if (((ArrayList<String>) childItem.get(groupPosition)).size() > 0) {
            imgArrow.setVisibility(View.VISIBLE);
        }

        if (groupItem.get(groupPosition).isExpanded) {
            imgArrow.setImageResource(R.drawable.menu_arrow_up);
        } else {
            imgArrow.setImageResource(R.drawable.menu_arrow_down);
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }

}
