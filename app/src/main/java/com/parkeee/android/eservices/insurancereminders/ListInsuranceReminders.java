package com.parkeee.android.eservices.insurancereminders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.parkeee.android.R;
import com.parkeee.android.adapter.eservices.ListInsuranceRemindersAdapter;
import com.parkeee.android.adapter.realm.RealmInsertUpdateAdapter;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.eservices.ServiceDashboard;
import com.parkeee.android.model.tables.InsertUpdateCache;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Commons;
import com.parkeee.android.views.CustomTextView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Inder Bagga on 11/30/2016.
 */
public class ListInsuranceReminders extends AppCompatActivity implements OnResultReceived,ListInsuranceRemindersAdapter.Callback,View.OnClickListener{

    Context mContext;
    ImageView actionAdd,actionBack,actionDelete;
    CustomTextView actionDone;
    ListInsuranceRemindersAdapter.Callback mCallback;
    OnResultReceived mOnResultReceived;

    private RecyclerView recyclerView;
    private CustomTextView emptyView;
    private Realm realmDB;
    ArrayList<Long> alTimeStampIds;
    private ProgressDialog mProgressDialog;
    ListInsuranceRemindersAdapter mListInsuranceRemindersAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_insurance_reminders);

        initializeUserInterface();
    }

    private void initializeUserInterface() {

        mContext=this;
        mCallback=this;
        mOnResultReceived=this;

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        actionAdd=(ImageView)findViewById(R.id.action_add);
        actionBack=(ImageView)findViewById(R.id.action_back);
        actionDelete=(ImageView)findViewById(R.id.action_delete);
        actionDone=(CustomTextView) findViewById(R.id.action_done);

        actionAdd.setOnClickListener(this);
        actionBack.setOnClickListener(this);
        actionDelete.setOnClickListener(this);
        actionDone.setOnClickListener(this);

        emptyView = (CustomTextView)findViewById(R.id.emptyView);
        recyclerView = (RecyclerView)findViewById(R.id.listView);
        recyclerView.setHasFixedSize(true);

        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        //get realm instance
        this.realmDB = RealmController.with(this).getRealm();

        displayInsuranceReminders();
    }

    private void displayInsuranceReminders() {

        mListInsuranceRemindersAdapter = new ListInsuranceRemindersAdapter(this,this,this);

        if(RealmController.with(this).hasInsuranceReminders()){

            RealmInsertUpdateAdapter realmAdapter = new RealmInsertUpdateAdapter(mContext, RealmController.with(this).getInsuranceReminders().sort("timeStamp", Sort.DESCENDING), true);

            // Set the data and tell the RecyclerView to draw
            mListInsuranceRemindersAdapter.setRealmAdapter(realmAdapter);
            mListInsuranceRemindersAdapter.notifyDataSetChanged();

            recyclerView.setAdapter(mListInsuranceRemindersAdapter);
            actionDelete.setVisibility(View.VISIBLE);
            emptyView.setVisibility(View.GONE);
            recyclerView.setVisibility(View.VISIBLE);

        }else{
            actionDelete.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.action_add:

                startActivity(new Intent(this,AddInsuranceReminder.class));
                finishAffinity();

                break;

            case R.id.action_delete:

                mCallback.onActivated();

                break;

            case R.id.action_done:

                if(mListInsuranceRemindersAdapter.getSelectedCount()>0){

                    AlertDialog.Builder confirmDialog=new AlertDialog.Builder(mContext);
                    confirmDialog.setTitle(getString(R.string.app_name)+" says,")
                            .setCancelable(false)
                            .setMessage(AppMessages.DELETE_INS_ALERT)
                            .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    showToast(mListInsuranceRemindersAdapter.getSelectedCount()+"");

                                    ArrayList<Long> alCachedIds=new ArrayList<Long>();
                                    alTimeStampIds=new ArrayList<Long>();
                                    ArrayList<String> alServerIds=new ArrayList<String>();

                                    for(int index = 0; index< mListInsuranceRemindersAdapter.getSelectedCount(); index++){

                                        InsertUpdateCache oInsertUpdateCache =RealmController.with(ListInsuranceReminders.this).getInsuranceReminders().get(mListInsuranceRemindersAdapter.getSelectedPositions().get(index));

                                        long strCachedId=oInsertUpdateCache.getTimeStamp();
                                        String strServerId=oInsertUpdateCache.getInsServerId();

                                        alTimeStampIds.add(strCachedId);

                                        if(oInsertUpdateCache.getIsINSUpdated().equals("1")||oInsertUpdateCache.getIsINSUploaded().equals("1")){
                                            alServerIds.add(strServerId);
                                        }else{
                                            alCachedIds.add(strCachedId);
                                        }
                                    }

                                    //In case Delete API call
                                    if(alServerIds.size()>0){

                                        Realm realm = RealmController.with(ListInsuranceReminders.this).getRealm();

                                        //call Delete API
                                        if(Commons.isNetworkAvailable(mContext)){

                                            String strPayloadInsuranceIDs=alServerIds.get(0);

                                            for(int index=1;index<alServerIds.size();index++){
                                                strPayloadInsuranceIDs=strPayloadInsuranceIDs+"-"+alServerIds.get(index);
                                            }

                                            Log.e("strPayloadInsuranceIDs",strPayloadInsuranceIDs);

                                            mProgressDialog.show();

                                            //Get oDeleteINSByServerIDs Api
                                            GetApiClient oDeleteINSByServerIDs=new GetApiClient(mOnResultReceived);
                                            oDeleteINSByServerIDs.setRequestSource(RequestSource.DeleteINSByServerIDs);
                                            oDeleteINSByServerIDs.executeGetRequest(API.gDeleteINSByServerIDs(strPayloadInsuranceIDs));

                                        }else{//set Delete flag 1

                                            for(int index=0;index<alServerIds.size();index++){

                                                RealmResults<InsertUpdateCache> results = realm.where(InsertUpdateCache.class).equalTo("InsServerId", alServerIds.get(index)).findAll();
                                                realm.beginTransaction();
                                                results.get(index).setIsINSDeleted("1");
                                                realm.commitTransaction();
                                            }

                                            //remove Cached Insurance Reminders
                                            for(int position=0;position<alCachedIds.size();position++){
                                                Log.e("alSelectedIds is "," "+alCachedIds.get(position));
                                                RealmController.with(ListInsuranceReminders.this).removeInsuranceReminder(alCachedIds.get(position));
                                            }

                                            onDeactivated();

                                            if(!RealmController.with(ListInsuranceReminders.this).hasInsuranceReminders()){
                                                emptyView.setVisibility(View.VISIBLE);
                                                recyclerView.setVisibility(View.GONE);
                                                actionDelete.setVisibility(View.GONE);
                                            }
                                        }
                                    }else{

                                        //remove Cached Insurance Reminders
                                        for(int position=0;position<alCachedIds.size();position++){
                                            Log.e("alSelectedIds is "," "+alCachedIds.get(position));
                                            RealmController.with(ListInsuranceReminders.this).removeInsuranceReminder(alCachedIds.get(position));
                                        }

                                        onDeactivated();

                                        if(!RealmController.with(ListInsuranceReminders.this).hasInsuranceReminders()){
                                            emptyView.setVisibility(View.VISIBLE);
                                            recyclerView.setVisibility(View.GONE);
                                            actionDelete.setVisibility(View.GONE);
                                        }
                                    }

                                }
                            })
                            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {

                                    dialogInterface.dismiss();
                                    onDeactivated();
                                }
                            })
                            .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                    onDeactivated();
                                }
                            })
                            .setOnKeyListener(new DialogInterface.OnKeyListener() {
                                @Override
                                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                    if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                        dialogInterface.dismiss();
                                        onDeactivated();
                                    }
                                    return true;
                                }
                            })
                            .show();

                }else{
                    onDeactivated();
                }

                break;

            case R.id.action_back:

                if(mListInsuranceRemindersAdapter.isDeletionActivated())onDeactivated();
                else closeActivity();
                break;
        }
    }

    private void showToast(final String strMessage) {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText( mContext, strMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void closeActivity(){
        startActivity(new Intent(this,ServiceDashboard.class));
        finishAffinity();
    }

    @Override
    public void onBackPressed() {

        if(mListInsuranceRemindersAdapter.isDeletionActivated())onDeactivated();
        else closeActivity();
    }

    @Override
    public void onIconClicked(int index) {

        mListInsuranceRemindersAdapter.deleteSelected(index);
    }

    @Override
    public void onActivated() {

        mListInsuranceRemindersAdapter.deletionActivated();

        actionAdd.setVisibility(View.GONE);
        actionDone.setVisibility(View.VISIBLE);
        actionDelete.setVisibility(View.GONE);
    }

    @Override
    public void onDeactivated() {

        mListInsuranceRemindersAdapter.clearSelected();
        actionAdd.setVisibility(View.VISIBLE);
        actionDelete.setVisibility(View.VISIBLE);
        actionDone.setVisibility(View.GONE);
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        mProgressDialog.dismiss();
        Log.e("what",what);
        showToast(what);

        switch(what){

            case "-2":
                displayAlert(getResources().getString(R.string.error_code_minus_two));
                break;
            case "-3" :
                displayAlert(getResources().getString(R.string.error_code_minus_three));
                break;
            case "-1":
                displayAlert(getResources().getString(R.string.error_code_minus_one));
                break;
            default:

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        //remove Cached Insurance Reminders
                        for(int position=0;position<alTimeStampIds.size();position++){
                            Log.e("alTimeStampIds is "," "+alTimeStampIds.get(position));
                            RealmController.with(ListInsuranceReminders.this).removeInsuranceReminder(alTimeStampIds.get(position));
                        }
                        onDeactivated();

                        if(!RealmController.with(ListInsuranceReminders.this).hasOtherBills()){
                            emptyView.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            actionDelete.setVisibility(View.GONE);
                        }
                    }
                });

                break;
        }
    }

    private void displayAlert(final String strMessage) {

       runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder exitDialog=new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name)+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });

    }
}