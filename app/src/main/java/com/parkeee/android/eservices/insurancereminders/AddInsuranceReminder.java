package com.parkeee.android.eservices.insurancereminders;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.DatePicker;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.Config;
import com.parkeee.android.backend.Data;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.menu.ActivityAddCar;
import com.parkeee.android.model.params.InsertUpdateINSByPUID;
import com.parkeee.android.model.tables.InsertUpdateCache;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Commons;
import com.parkeee.android.views.MaterialSpinner;
import com.parkeee.android.views.materialedittext.MaterialEditText;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;

/**
 * Created by Inder Bagga on 11/30/2016.
 */
public class AddInsuranceReminder extends AppCompatActivity implements View.OnClickListener,OnResultReceived {

    private Context mContext;
    private Gson oGson;
    private OnResultReceived mOnResultReceived;
    private ProgressDialog mProgressDialog;
    private Realm realm;

    int iSelectedMonth=-1;
    int iSelectedDate=-1;
    int iSelectedYear=-1;
    int iPremiumMode=-1;

    String strCar="";
    String strCompany="";
    String strPremiumType="";

    String strPolicyNumber;
    String strPremiumAmount;
    String strPrimaryDriver;
    String strExpiryDate;

    private long lastTappedTimeInMillis=0;
    private int remindMeDays=1;

    LinearLayout addCompanyView;
    FrameLayout monthView;
    TextView tvRemindDays;

    private boolean isSwitchEnabled=true;
    private boolean  hasOtherCompany=false;

    private Button bActionSave;
    private ImageView bActionBack;
    private ImageView incrementCount,decrementCount;

    private SwitchCompat switchView;
    private MaterialSpinner mCarSelector,mCompanySelector,mPremiumSelector,mMonthSelector,mDateSelector;
    private MaterialEditText etCar,etCompany,etPrimaryDriver,etPolicyNumber,etPremiumMode,etPremiumAmount,etDueMonth,etDueDate,etExpiryDate,etOtherCompany;

    String strPUID="";
    ArrayList<String> alAddedCars=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_insurance_reminder);

        initializeUserInterface();
    }

    private void initializeUserInterface() {

        mContext = this;
        mOnResultReceived=this;
        oGson=new Gson();

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        bActionSave=(Button) findViewById(R.id.bActionSave);
        bActionBack = (ImageView) findViewById(R.id.action_back);
        incrementCount = (ImageView) findViewById(R.id.iv_increment);
        decrementCount = (ImageView) findViewById(R.id.iv_decrement);
        switchView=(SwitchCompat)findViewById(R.id.switchView);
        tvRemindDays=(TextView)findViewById(R.id.tvRemindDays);
        addCompanyView = (LinearLayout) findViewById(R.id.add_company);
        monthView = (FrameLayout) findViewById(R.id.monthView);

        mCarSelector = (MaterialSpinner) findViewById(R.id.sp_car);
        mCompanySelector = (MaterialSpinner) findViewById(R.id.sp_company);
        mPremiumSelector = (MaterialSpinner) findViewById(R.id.sp_premium_mode);
        mMonthSelector = (MaterialSpinner) findViewById(R.id.sp_due_month);
        mDateSelector = (MaterialSpinner) findViewById(R.id.sp_due_date);

        etCar = (MaterialEditText) findViewById(R.id.et_car);
        etCompany = (MaterialEditText) findViewById(R.id.et_company);
        etPolicyNumber = (MaterialEditText) findViewById(R.id.et_policy_number);
        etPremiumMode = (MaterialEditText) findViewById(R.id.et_premium_mode);
        etPrimaryDriver = (MaterialEditText) findViewById(R.id.et_primary_driver);
        etPremiumAmount = (MaterialEditText) findViewById(R.id.et_premium_amount);
        etDueMonth = (MaterialEditText) findViewById(R.id.et_due_month);
        etDueDate = (MaterialEditText) findViewById(R.id.et_due_date);
        etExpiryDate = (MaterialEditText) findViewById(R.id.et_expiry_date);
        etOtherCompany = (MaterialEditText) findViewById(R.id.et_other_company);

        bActionBack.setOnClickListener(this);
        bActionSave.setOnClickListener(this);
        incrementCount.setOnClickListener(this);
        decrementCount.setOnClickListener(this);

        etCar.setOnClickListener(this);
        etCompany.setOnClickListener(this);
        etPremiumMode.setOnClickListener(this);
        etPrimaryDriver.setOnClickListener(this);
        etDueMonth.setOnClickListener(this);
        etDueDate.setOnClickListener(this);
        etExpiryDate.setOnClickListener(this);

        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                isSwitchEnabled=isChecked;
                switchView.setChecked(isChecked);
            }
        });

        etPremiumAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean b) {

                String strText=etPremiumAmount.getText().toString();

                if(!view.hasFocus()&&!strText.isEmpty()){
                    etPremiumAmount.setText("$"+strText);
                }else if(view.hasFocus()&&strText.startsWith("$")){
                    etPremiumAmount.setText(strText.substring(1,strText.length()));
                }
            }
        });

        addCompanyView.setVisibility(View.GONE);

        alAddedCars.clear();

        for(int index=0;index<RealmController.with(this).getAddedCarsInformation().size();index++){

            alAddedCars.add(RealmController.with(this).getAddedCarsInformation().get(index).getPlateNumber());
        }

        alAddedCars.add("Add New Car");

        mCarSelector.setAdapter(new CustomSpinnerAdapter(mContext, 1, R.layout.list_item_textview, alAddedCars.toArray(new String[alAddedCars.size()])));
        mCompanySelector.setAdapter(new CustomSpinnerAdapter(mContext, 2, R.layout.list_item_textview, Data.strArrayCompanies));
        mPremiumSelector.setAdapter(new CustomSpinnerAdapter(mContext, 3, R.layout.list_item_textview, Data.strArrayPremiumModes));
        mMonthSelector.setAdapter(new CustomSpinnerAdapter(mContext, 4, R.layout.list_item_textview, Data.strArrayMonthNames));

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        strPUID=RealmController.with(this).getRegisterInformation().getPuid();

    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this,ListInsuranceReminders.class));
        finishAffinity();
    }

    private void displayAlert(final String strMessage) {

        AddInsuranceReminder.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder exitDialog=new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name)+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();

            }
        });

    }


    private void showToast(final String strMessage)
    {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText( mContext, strMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void displayDatePicker(int iPremiumMode, int iSelectedMonth, int iSelectedDate) {

        final Calendar dueTimeCalendar = Calendar.getInstance();
        dueTimeCalendar.set(Calendar.HOUR_OF_DAY,0);
        dueTimeCalendar.set(Calendar.MINUTE,0);
        dueTimeCalendar.set(Calendar.SECOND,0);

        Date nowDate = dueTimeCalendar.getTime();
        int mYear= dueTimeCalendar.get(Calendar.YEAR);

        dueTimeCalendar.set(mYear,iSelectedMonth,iSelectedDate);

        while(dueTimeCalendar.getTime().compareTo(nowDate) < 0 ){

            switch (iPremiumMode){

                case 0: dueTimeCalendar.add(Calendar.YEAR,1);break;
                case 1: dueTimeCalendar.add(Calendar.MONTH,6);break;
                case 2: dueTimeCalendar.add(Calendar.MONTH,3);break;
                case 3: dueTimeCalendar.add(Calendar.MONTH,1);break;
            }
        }

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                final Calendar selectedTimeCalendar = Calendar.getInstance();
                selectedTimeCalendar.set(Calendar.HOUR_OF_DAY,0);
                selectedTimeCalendar.set(Calendar.MINUTE,0);
                selectedTimeCalendar.set(Calendar.SECOND,0);
                selectedTimeCalendar.set(Calendar.YEAR,year);
                selectedTimeCalendar.set(Calendar.MONTH,monthOfYear);
                selectedTimeCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                Date dueDate=dueTimeCalendar.getTime();
                Date selectedDate=selectedTimeCalendar.getTime();

                long dueTime=dueDate.getTime();
                long selectedTime=selectedDate.getTime();

                if(selectedTime>dueTime&&selectedTime-dueTime>60000){
                    etExpiryDate.setText(new SimpleDateFormat(Config.DATE_FORMAT).format(selectedDate));
                }
                else if(dueTime>selectedTime&&dueTime-selectedTime>60000){
                    displayAlert("Expiry date can't be before due date");
                }else  etExpiryDate.setText(new SimpleDateFormat(Config.DATE_FORMAT).format(selectedDate));

            }
        }, dueTimeCalendar.get(Calendar.YEAR), dueTimeCalendar.get(Calendar.MONTH), dueTimeCalendar.get(Calendar.DATE)).show();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.iv_increment:

                if(!isSwitchEnabled)return;

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    if(remindMeDays<10){
                        remindMeDays++;
                        tvRemindDays.setText(""+remindMeDays);
                    }
                }

                break;

            case R.id.iv_decrement:

                if(!isSwitchEnabled)return;

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    if(remindMeDays>1){
                        remindMeDays--;
                        tvRemindDays.setText(""+remindMeDays);
                    }
                }

                break;

            case R.id.action_back:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    startActivity(new Intent(this,ListInsuranceReminders.class));
                    finishAffinity();
                }

                break;

            case R.id.et_car:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mCarSelector.performClick();
                }

                break;

            case R.id.et_company:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mCompanySelector.performClick();
                }

                break;

            case R.id.et_due_month:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mMonthSelector.performClick();
                }

                break;

            case R.id.et_due_date:


                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();

                    if(iPremiumMode==-1){ displayAlert(AppMessages.INS_PAYMENT_TYPE_BLANK);}
                    else if(iSelectedMonth==-1){displayAlert(AppMessages.INS_DUE_MONTH_BLANK);}else mDateSelector.performClick();
                }

                break;

            case R.id.et_premium_mode:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mPremiumSelector.performClick();
                }

                break;

            case R.id.et_expiry_date:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();

                   if(iSelectedMonth==-1||iSelectedDate==-1) displayAlert("Please select due Date/Month");
                   else displayDatePicker(iPremiumMode,iSelectedMonth,iSelectedDate);
                }

                break;

            case R.id.bActionSave:

                if(System.currentTimeMillis()-lastTappedTimeInMillis< Config.TAP_DELAY){
                    return;
                }

                strPolicyNumber=etPolicyNumber.getText().toString();
                strPremiumAmount=etPremiumAmount.getText().toString();
                strPrimaryDriver=etPrimaryDriver.getText().toString();
                strExpiryDate=etExpiryDate.getText().toString();

                if(strPremiumAmount.startsWith("$")){
                    strPremiumAmount=strPremiumAmount.substring(1,strPremiumAmount.length());
                }

                if(strCar.isEmpty()){
                    displayAlert(AppMessages.INS_CAR_BLANK);
                    return;
                }

                if( hasOtherCompany){
                    if(etOtherCompany.getText().toString().trim().isEmpty()){
                        displayAlert(AppMessages.INS_OTHER_COMPANY_BLANK);
                        return;
                    }else strCompany=etOtherCompany.getText().toString();
                }else if(strCompany.trim().isEmpty()){

                    displayAlert(AppMessages.INS_COMPANY_BLANK);
                    return;
                }

                if(strPolicyNumber.trim().isEmpty()){

                    displayAlert(AppMessages.INS_POLICY_BLANK);
                    return ;
                }

                if(strPrimaryDriver.trim().isEmpty()){

                    displayAlert("Please type Primary Driver name");
                    return ;
                }

                if(iPremiumMode==-1){

                    displayAlert(AppMessages.INS_PAYMENT_TYPE_BLANK);
                    return;
                }

                switch (iPremiumMode){

                    case 3:
                        if(iSelectedDate==-1){
                            displayAlert(AppMessages.INS_DUE_DATE_BLANK);
                            return;
                        }
                        break;
                    case 0:
                    case 1:
                    case 2:

                        if(iSelectedMonth==-1){
                            displayAlert(AppMessages.INS_DUE_MONTH_BLANK);
                            return;
                        }

                        if(iSelectedDate==-1){
                            displayAlert(AppMessages.INS_DUE_DATE_BLANK);
                            return;
                        }
                        break;
                }

                if(strPremiumAmount.isEmpty()){

                    displayAlert(AppMessages.INS_PREMIUM_BLANK);
                    return;
                }

                if(strExpiryDate.isEmpty()){
                    displayAlert(AppMessages.INS_EXPIRY_DATE_BLANK);
                    return;
                }

                if(isSwitchEnabled){

                    Calendar dueTimeCalendar=Calendar.getInstance();
                    dueTimeCalendar.set(Calendar.HOUR_OF_DAY,0);
                    dueTimeCalendar.set(Calendar.MINUTE,0);
                    dueTimeCalendar.set(Calendar.SECOND,0);
                    Date today = dueTimeCalendar.getTime();
                    Date remindDate,dueDate=null;
                    switch(iPremiumMode){

                        case 3:
                            dueTimeCalendar.set(Calendar.DAY_OF_MONTH,iSelectedDate);

                            while( dueTimeCalendar.getTime().compareTo(today) < 0 ){
                                dueTimeCalendar.add(Calendar.MONTH,1);
                            }
                            dueDate=dueTimeCalendar.getTime();
                            dueTimeCalendar.add(Calendar.DAY_OF_YEAR,-remindMeDays);
                            remindDate=dueTimeCalendar.getTime();

                            if(remindDate.compareTo(today) < 0){
                                displayAlert(AppMessages.REMINDER_DATE_BEFORE);
                                return;
                            }

                            break;

                        case 0:
                        case 1:
                        case 2:
                            dueTimeCalendar.set(Calendar.DAY_OF_MONTH,iSelectedDate);
                            dueTimeCalendar.set(Calendar.MONTH,iSelectedMonth);

                            while( dueTimeCalendar.getTime().compareTo(today) < 0 ){

                                switch (iPremiumMode){

                                    case 2: dueTimeCalendar.add(Calendar.MONTH,3);break;
                                    case 1: dueTimeCalendar.add(Calendar.MONTH,6);break;
                                    case 0: dueTimeCalendar.add(Calendar.YEAR,1);break;
                                }
                            }

                            dueDate=dueTimeCalendar.getTime();
                            dueTimeCalendar.add(Calendar.DAY_OF_YEAR,-remindMeDays);
                            remindDate=dueTimeCalendar.getTime();

                            if(remindDate.compareTo(today) <= 0){
                                displayAlert(AppMessages.REMINDER_DATE_BEFORE);
                                return;
                            }

                            break;
                    }

                    Log.e("due_date",new SimpleDateFormat("ddMMMyyyy").format(dueDate.getTime()));
                }

                if(RealmController.with(AddInsuranceReminder.this).containsPolicy(strPolicyNumber)){
                    displayAlert(AppMessages.INS_ALREADY_EXIST);
                    return;
                }

                if(Commons.isNetworkAvailable(mContext)){

                    InsertUpdateINSByPUID oInsertUpdateINSByPUID=new InsertUpdateINSByPUID();
                    oInsertUpdateINSByPUID.setCar_plate(strCar);
                    oInsertUpdateINSByPUID.setCompany_name(strCompany);
                    oInsertUpdateINSByPUID.setDue_date_day(iSelectedDate+"");
                    oInsertUpdateINSByPUID.setDue_date_month(iSelectedMonth+"");
                    oInsertUpdateINSByPUID.setDue_date_year(""+iSelectedYear);
                    oInsertUpdateINSByPUID.setInsurance_id(strPolicyNumber);
                    oInsertUpdateINSByPUID.setExpire_date(strExpiryDate);
                    oInsertUpdateINSByPUID.setPrimary_driver(strPrimaryDriver);
                    oInsertUpdateINSByPUID.setPremium(strPremiumAmount);
                    oInsertUpdateINSByPUID.setPayment_type(""+iPremiumMode);//index based
                    oInsertUpdateINSByPUID.setRemind_before(remindMeDays+"");//number of days
                    oInsertUpdateINSByPUID.setRemind_before_type(""+iPremiumMode);//index based
                    oInsertUpdateINSByPUID.setPuid(strPUID);//user id
                    oInsertUpdateINSByPUID.setInsServerId("0");//to be replaced with server response
                    oInsertUpdateINSByPUID.setIsNotiEnabled(isSwitchEnabled?"1":"0");//0 false 1 enabled

                    String jsonString=oGson.toJson(oInsertUpdateINSByPUID,InsertUpdateINSByPUID.class).toString();

                    mProgressDialog.show();

                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi=new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.InsertUpdateINSByPUID);
                    oInsertUpdateApi.executePostRequest(API.pInsertUpdateINSByPUID(),jsonString);

                    Log.e("API",API.pInsertUpdateINSByPUID());
                    Log.e("jsonString",jsonString);

                }else{
                    InsertUpdateCache oInsertUpdateCache =new InsertUpdateCache();
                    oInsertUpdateCache.setCar_plate(strCar);
                    oInsertUpdateCache.setCompany_name(strCompany);
                    oInsertUpdateCache.setDue_date_day(iSelectedDate+"");
                    oInsertUpdateCache.setDue_date_month(iSelectedMonth+"");
                    oInsertUpdateCache.setDue_date_year(""+iSelectedYear);
                    oInsertUpdateCache.setInsurance_id(strPolicyNumber);
                    oInsertUpdateCache.setExpire_date(strExpiryDate);
                    oInsertUpdateCache.setPrimary_driver(strPrimaryDriver);
                    oInsertUpdateCache.setPremium(strPremiumAmount);
                    oInsertUpdateCache.setPayment_type(""+iPremiumMode);//index based
                    oInsertUpdateCache.setRemind_before(remindMeDays+"");//number of days
                    oInsertUpdateCache.setRemind_before_type(""+iPremiumMode);//index based
                    oInsertUpdateCache.setPuid(strPUID);//user id
                    oInsertUpdateCache.setInsServerId("0");//to be replaced with server response
                    oInsertUpdateCache.setIsNotiEnabled(isSwitchEnabled?"1":"0");//0 false 1 enabled

                    oInsertUpdateCache.setIsINSDeleted("0");
                    oInsertUpdateCache.setIsINSUpdated("0");
                    oInsertUpdateCache.setIsINSUploaded("0");
                    oInsertUpdateCache.setTimeStamp(System.currentTimeMillis());

                    realm.beginTransaction();
                    realm.copyToRealm(oInsertUpdateCache);
                    realm.commitTransaction();

                    showToast("Saved");
                    onBackPressed();
                }

                break;
        }
    }

    @Override
    public void dispatchString(RequestSource from, final String what) {

        mProgressDialog.dismiss();
        Log.e("what",what);
       showToast(what);

        switch(what){

            case "-2":
                displayAlert(getResources().getString(R.string.error_code_minus_two));
                break;
            case "-3" :
                displayAlert(getResources().getString(R.string.error_code_minus_three));
                break;
            case "0":
                displayAlert(getResources().getString(R.string.error_code_zero_insurance));
                break;
            case "-1":
                displayAlert(getResources().getString(R.string.error_code_minus_one));
                break;
            default:

                AddInsuranceReminder.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        InsertUpdateCache oInsertUpdateCache =new InsertUpdateCache();
                        oInsertUpdateCache.setCar_plate(strCar);
                        oInsertUpdateCache.setCompany_name(strCompany);
                        oInsertUpdateCache.setDue_date_day(iSelectedDate+"");
                        oInsertUpdateCache.setDue_date_month(iSelectedMonth+"");
                        oInsertUpdateCache.setDue_date_year(""+iSelectedYear);
                        oInsertUpdateCache.setInsurance_id(strPolicyNumber);
                        oInsertUpdateCache.setExpire_date(strExpiryDate);
                        oInsertUpdateCache.setPrimary_driver(strPrimaryDriver);
                        oInsertUpdateCache.setPremium(strPremiumAmount);
                        oInsertUpdateCache.setPayment_type(""+iPremiumMode);//index based
                        oInsertUpdateCache.setRemind_before(remindMeDays+"");//number of days
                        oInsertUpdateCache.setRemind_before_type(""+iPremiumMode);//index based
                        oInsertUpdateCache.setPuid(strPUID);//user id
                        oInsertUpdateCache.setInsServerId(what);//to be replaced with server response
                        oInsertUpdateCache.setIsNotiEnabled(isSwitchEnabled?"1":"0");//0 false 1 enabled

                        oInsertUpdateCache.setIsINSDeleted("0");
                        oInsertUpdateCache.setIsINSUpdated("0");
                        oInsertUpdateCache.setIsINSUploaded("1");
                        oInsertUpdateCache.setTimeStamp(System.currentTimeMillis());

                        realm.beginTransaction();
                        realm.copyToRealm(oInsertUpdateCache);
                        realm.commitTransaction();

                        showToast("Saved");
                        onBackPressed();
                    }
                });

                break;
        }

    }

    public class CustomSpinnerAdapter extends ArrayAdapter<String> {

        Context _context;
        String[] _objects;
        int _id;

        public CustomSpinnerAdapter(Context context, int id, int textViewResourceId,
                                    String[] objects) {
            super(context, textViewResourceId, objects);
            this._context=context;
            this._objects=objects;
            this._id=id;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(final int position, View convertView, ViewGroup parent) {

            View mContentView= LayoutInflater.from(_context).inflate(R.layout.list_item_textview, parent, false);

            TextView tvLabel=(TextView)mContentView.findViewById(R.id.tv_id);
            tvLabel.setText(_objects[position]);

            mContentView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    switch (_id){

                        case 1:

                            if(alAddedCars.size()-1==position){
                                startActivity(new Intent(AddInsuranceReminder.this, ActivityAddCar.class));
                               // finishAffinity();
                            }else{

                                etCar.setText(_objects[position]);
                                strCar=_objects[position];

                                try {
                                    Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                    method.setAccessible(true);
                                    method.invoke(mCarSelector);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            break;

                        case 2:

                            etCompany.setText(_objects[position]);

                            if(position==_objects.length-1) {
                                strCompany="";
                                hasOtherCompany=true;
                                addCompanyView.setVisibility(View.VISIBLE);
                            }
                            else {
                                hasOtherCompany=false;
                                strCompany=_objects[position];
                                addCompanyView.setVisibility(View.GONE);
                            }

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mCompanySelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;

                        case 3:

                            strPremiumType=_objects[position];
                            iPremiumMode=position;
                            etPremiumMode.setText(_objects[position]);
                            etExpiryDate.setText("");

                            switch(position){

                                case 0:
                                case 1:
                                case 2:
                                    monthView.setVisibility(View.VISIBLE);

                                    iSelectedDate=-1;
                                    iSelectedMonth=-1;
                                    etDueDate.setText("");
                                    etDueMonth.setText("");

                                    break;
                                case 3:
                                    iSelectedMonth=Calendar.getInstance().get(Calendar.MONTH);
                                    Calendar mCalendar=Calendar.getInstance();
                                    mCalendar.set(Calendar.MONTH,iSelectedMonth);
                                    int daysInMonth = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                                    iSelectedYear=mCalendar.get(Calendar.YEAR);

                                    ArrayList<String> alDates=new ArrayList<String>();

                                    for(int index=1;index<=daysInMonth;index++){
                                        alDates.add(index+"");
                                    }

                                    Log.e("dates",daysInMonth+"");
                                    mDateSelector.setAdapter(new CustomSpinnerAdapter(mContext, 5, R.layout.list_item_textview,(Arrays.copyOf(alDates.toArray(), alDates.toArray().length, String[].class))));
                                    etDueDate.setText("");
                                    etDueMonth.setText("");
                                    monthView.setVisibility(View.GONE);

                                    iSelectedDate=-1;

                                    break;
                            }

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mPremiumSelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;

                        case 4:

                            iSelectedMonth=position;

                            Calendar mCalendar=Calendar.getInstance();
                            mCalendar.set(Calendar.MONTH,iSelectedMonth);
                            int daysInMonth = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                            iSelectedYear=mCalendar.get(Calendar.YEAR);

                            ArrayList<String> alDates=new ArrayList<String>();

                            for(int index=1;index<=daysInMonth;index++){
                                alDates.add(index+"");
                            }

                            mDateSelector.setAdapter(new CustomSpinnerAdapter(mContext, 5, R.layout.list_item_textview,(Arrays.copyOf(alDates.toArray(), alDates.toArray().length, String[].class))));

                            etDueMonth.setText(_objects[position]);
                            etDueDate.setText("");
                            iSelectedDate=-1;

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mMonthSelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;

                        case 5:

                            iSelectedDate=Integer.parseInt(_objects[position]);

                            etDueDate.setText(_objects[position]);
                            etExpiryDate.setText("");

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mDateSelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                    }
                }
            });

            return mContentView;
        }
    }
}
