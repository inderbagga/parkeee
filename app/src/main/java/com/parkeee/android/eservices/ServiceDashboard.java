package com.parkeee.android.eservices;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.parkeee.android.ActivityDashboard;
import com.parkeee.android.R;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.eservices.insurancereminders.ListInsuranceReminders;
import com.parkeee.android.eservices.otherbills.ListOtherBills;
import com.parkeee.android.eservices.violationreminders.ListViolationReminders;
import com.parkeee.android.views.CustomTextView;

/**
 * Created by Inder Bagga on 11/30/2016.
 */
public class ServiceDashboard extends AppCompatActivity implements View.OnTouchListener,View.OnClickListener{

    ImageView _ImageView_1,_ImageView_2,_ImageView_3,_ImageView_4,_ImageView_5;
    ImageView actionBack;

    CustomTextView _HeaderTextView_1,_HeaderTextView_2,_HeaderTextView_3,_HeaderTextView_4,_HeaderTextView_5;
    CustomTextView _InfoTextView_1,_InfoTextView_2,_InfoTextView_3,_InfoTextView_4,_InfoTextView_5;

    LinearLayout _LinearGridLayout_1,_LinearGridLayout_2,_LinearGridLayout_3,_LinearGridLayout_4,_LinearGridLayout_5;
    LinearLayout _LinearContentLayout_1,_LinearContentLayout_2,_LinearContentLayout_3,_LinearContentLayout_4,_LinearContentLayout_5;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_service_dashboard);

        initializeUserInterface();
    }

    private void initializeUserInterface() {

        actionBack=(ImageView)findViewById(R.id.action_back);
        actionBack.setOnClickListener(this);

        _LinearGridLayout_1=(LinearLayout)findViewById(R.id.gridItem_1);
        _LinearGridLayout_2=(LinearLayout)findViewById(R.id.gridItem_2);
        _LinearGridLayout_3=(LinearLayout)findViewById(R.id.gridItem_3);
        _LinearGridLayout_4=(LinearLayout)findViewById(R.id.gridItem_4);
        _LinearGridLayout_5=(LinearLayout)findViewById(R.id.gridItem_5);

        _LinearContentLayout_1=(LinearLayout)findViewById(R.id.contentView_1);
        _LinearContentLayout_2=(LinearLayout)findViewById(R.id.contentView_2);
        _LinearContentLayout_3=(LinearLayout)findViewById(R.id.contentView_3);
        _LinearContentLayout_4=(LinearLayout)findViewById(R.id.contentView_4);
        _LinearContentLayout_5=(LinearLayout)findViewById(R.id.contentView_5);

        _HeaderTextView_1=(CustomTextView)findViewById(R.id.item_header_1);
        _HeaderTextView_2=(CustomTextView)findViewById(R.id.item_header_2);
        _HeaderTextView_3=(CustomTextView)findViewById(R.id.item_header_3);
        _HeaderTextView_4=(CustomTextView)findViewById(R.id.item_header_4);
        _HeaderTextView_5=(CustomTextView)findViewById(R.id.item_header_5);

        _InfoTextView_1=(CustomTextView)findViewById(R.id.item_info_1);
        _InfoTextView_2=(CustomTextView)findViewById(R.id.item_info_2);
        _InfoTextView_3=(CustomTextView)findViewById(R.id.item_info_3);
        _InfoTextView_4=(CustomTextView)findViewById(R.id.item_info_4);
        _InfoTextView_5=(CustomTextView)findViewById(R.id.item_info_5);

        _ImageView_1=(ImageView) findViewById(R.id.ic_item_1);
        _ImageView_2=(ImageView) findViewById(R.id.ic_item_2);
        _ImageView_3=(ImageView) findViewById(R.id.ic_item_3);
        _ImageView_4=(ImageView) findViewById(R.id.ic_item_4);
        _ImageView_5=(ImageView) findViewById(R.id.ic_item_5);

        int PADDING_UNIT=15;

        int iCountOtherBills=RealmController.with(this).getOtherBills().size();
        int iCountInsuranceReminders=RealmController.with(this).getInsuranceReminders().size();
        int iCountViolationReminders=RealmController.with(this).getViolationsReminder().size();

        _LinearGridLayout_1.setPadding(PADDING_UNIT,PADDING_UNIT,PADDING_UNIT/2,0);
        _HeaderTextView_1.setText(mHeaderStrings[0]);
        _InfoTextView_1.setText("count:0 | alert(s):0");
        _ImageView_1.setImageResource(mGrayIconIds[0]);
        _LinearContentLayout_1.setBackground(getResources().getDrawable(R.drawable.bg_gray_outline));

        _LinearGridLayout_2.setPadding(PADDING_UNIT,PADDING_UNIT,PADDING_UNIT/2,0);
        _HeaderTextView_2.setText(mHeaderStrings[1]);
        _InfoTextView_2.setText("count:"+iCountViolationReminders+" | alert(s):0");
        _ImageView_2.setImageResource(mGrayIconIds[1]);
        _LinearContentLayout_2.setBackground(getResources().getDrawable(R.drawable.bg_gray_outline));

        _LinearGridLayout_3.setPadding(PADDING_UNIT,PADDING_UNIT,PADDING_UNIT/2,0);
        _HeaderTextView_3.setText(mHeaderStrings[2]);
        _InfoTextView_3.setText("count:0 | alert(s):0");
        _ImageView_3.setImageResource(mGrayIconIds[2]);
        _LinearContentLayout_3.setBackground(getResources().getDrawable(R.drawable.bg_gray_outline));

        _LinearGridLayout_4.setPadding(PADDING_UNIT,PADDING_UNIT,PADDING_UNIT/2,0);
        _HeaderTextView_4.setText(mHeaderStrings[3]);
        _InfoTextView_4.setText("count:"+iCountOtherBills+" | alert(s):0");
        _ImageView_4.setImageResource(mGrayIconIds[3]);
        _LinearContentLayout_4.setBackground(getResources().getDrawable(R.drawable.bg_gray_outline));

        _LinearGridLayout_5.setPadding(PADDING_UNIT,PADDING_UNIT,PADDING_UNIT/2,0);
        _HeaderTextView_5.setText(mHeaderStrings[4]);
        _InfoTextView_5.setText("count:"+iCountInsuranceReminders+" | alert(s):0");
        _ImageView_5.setImageResource(mGrayIconIds[4]);
        _LinearContentLayout_5.setBackground(getResources().getDrawable(R.drawable.bg_gray_outline));

        _LinearContentLayout_1.setOnTouchListener(this);
        _LinearContentLayout_2.setOnTouchListener(this);
        _LinearContentLayout_3.setOnTouchListener(this);
        _LinearContentLayout_4.setOnTouchListener(this);
        _LinearContentLayout_5.setOnTouchListener(this);

    }

    // Keep all Icons in array
    public Integer[] mGrayIconIds = {
            R.drawable.icon_grey_dl_one, R.drawable.icon_grey_vl_two,
            R.drawable.icon_grey_vl_pay_three, R.drawable.icon_grey_four,
            R.drawable.icon_grey_ins_five
    };

    // Keep all Icons in array
    public Integer[] mWhiteIconIds = {

            R.drawable.icon_white_dl_one, R.drawable.icon_white_vl_two,
            R.drawable.icon_white_vl_pay_three, R.drawable.icon_white_bill_four,
            R.drawable.icon_white_ins_five
    };

    // Keep all Text in array
    public String[] mHeaderStrings = {
            "Driver License", "Violations Reminder",
            "Violations Pay", "Other Bills",
            "Insurance Reminders"
    };

    private void doSelect(int index, LinearLayout mLinearContentLayout, ImageView mImageView, CustomTextView mHeaderTextView, CustomTextView mInfoTextView) {

        switch (index){

            case 0:   mImageView.setImageResource(mWhiteIconIds[0]); break;
            case 1:   mImageView.setImageResource(mWhiteIconIds[1]);

                startActivity(new Intent(this,ListViolationReminders.class));
                finishAffinity();

                break;
            case 2:   mImageView.setImageResource(mWhiteIconIds[2]); break;
            case 3:   mImageView.setImageResource(mWhiteIconIds[3]);

                startActivity(new Intent(this,ListOtherBills.class));
                finishAffinity();

                break;
            case 4:   mImageView.setImageResource(mWhiteIconIds[4]);

                startActivity(new Intent(this,ListInsuranceReminders.class));
                finishAffinity();
                break;
        }

        mLinearContentLayout.setBackground(getResources().getDrawable(R.drawable.bg_primary_fill));
        mHeaderTextView.setTextColor(getResources().getColor(R.color.colorWhite));
        mInfoTextView.setTextColor(getResources().getColor(R.color.colorWhite));
    }

    private void defaultState(int index,LinearLayout mLinearContentLayout,ImageView mImageView,CustomTextView mHeaderTextView,CustomTextView mInfoTextView) {

        switch (index){

            case 0:   mImageView.setImageResource(mGrayIconIds[0]); break;
            case 1:   mImageView.setImageResource(mGrayIconIds[1]); break;
            case 2:   mImageView.setImageResource(mGrayIconIds[2]); break;
            case 3:   mImageView.setImageResource(mGrayIconIds[3]); break;
            case 4:   mImageView.setImageResource(mGrayIconIds[4]); break;
        }

        mLinearContentLayout.setBackground(getResources().getDrawable(R.drawable.bg_gray_outline));
        mHeaderTextView.setTextColor(getResources().getColor(R.color.colorText));
        mInfoTextView.setTextColor(getResources().getColor(R.color.colorText));
    }

    @Override
    public boolean onTouch(View view, MotionEvent motionEvent) {

        switch (view.getId()){

            case R.id.contentView_1:

                doSelect(0,_LinearContentLayout_1,_ImageView_1,_HeaderTextView_1,_InfoTextView_1);
                defaultState(1,_LinearContentLayout_2,_ImageView_2,_HeaderTextView_2,_InfoTextView_2);
                defaultState(2,_LinearContentLayout_3,_ImageView_3,_HeaderTextView_3,_InfoTextView_3);
                defaultState(3,_LinearContentLayout_4,_ImageView_4,_HeaderTextView_4,_InfoTextView_4);
                defaultState(4,_LinearContentLayout_5,_ImageView_5,_HeaderTextView_5,_InfoTextView_5);

                break;

            case  R.id.contentView_2:

                defaultState(0,_LinearContentLayout_1,_ImageView_1,_HeaderTextView_1,_InfoTextView_1);
                doSelect(1,_LinearContentLayout_2,_ImageView_2,_HeaderTextView_2,_InfoTextView_2);
                defaultState(2,_LinearContentLayout_3,_ImageView_3,_HeaderTextView_3,_InfoTextView_3);
                defaultState(3,_LinearContentLayout_4,_ImageView_4,_HeaderTextView_4,_InfoTextView_4);
                defaultState(4,_LinearContentLayout_5,_ImageView_5,_HeaderTextView_5,_InfoTextView_5);

                break;

            case  R.id.contentView_3:

                defaultState(0,_LinearContentLayout_1,_ImageView_1,_HeaderTextView_1,_InfoTextView_1);
                defaultState(1,_LinearContentLayout_2,_ImageView_2,_HeaderTextView_2,_InfoTextView_2);
                doSelect(2,_LinearContentLayout_3,_ImageView_3,_HeaderTextView_3,_InfoTextView_3);
                defaultState(3,_LinearContentLayout_4,_ImageView_4,_HeaderTextView_4,_InfoTextView_4);
                defaultState(4,_LinearContentLayout_5,_ImageView_5,_HeaderTextView_5,_InfoTextView_5);
                break;

            case  R.id.contentView_4:

                defaultState(0,_LinearContentLayout_1,_ImageView_1,_HeaderTextView_1,_InfoTextView_1);
                defaultState(1,_LinearContentLayout_2,_ImageView_2,_HeaderTextView_2,_InfoTextView_2);
                defaultState(2,_LinearContentLayout_3,_ImageView_3,_HeaderTextView_3,_InfoTextView_3);
                doSelect(3,_LinearContentLayout_4,_ImageView_4,_HeaderTextView_4,_InfoTextView_4);
                defaultState(4,_LinearContentLayout_5,_ImageView_5,_HeaderTextView_5,_InfoTextView_5);
                break;

            case  R.id.contentView_5:

                defaultState(0,_LinearContentLayout_1,_ImageView_1,_HeaderTextView_1,_InfoTextView_1);
                defaultState(1,_LinearContentLayout_2,_ImageView_2,_HeaderTextView_2,_InfoTextView_2);
                defaultState(2,_LinearContentLayout_3,_ImageView_3,_HeaderTextView_3,_InfoTextView_3);
                defaultState(3,_LinearContentLayout_4,_ImageView_4,_HeaderTextView_4,_InfoTextView_4);
                doSelect(4,_LinearContentLayout_5,_ImageView_5,_HeaderTextView_5,_InfoTextView_5);
                break;
        }

        return false;
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.action_back:

                onBackPressed();
                break;
        }
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this, ActivityDashboard.class));
        finishAffinity();
    }
}