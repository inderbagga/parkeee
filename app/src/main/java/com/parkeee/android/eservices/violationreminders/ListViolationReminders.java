package com.parkeee.android.eservices.violationreminders;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.parkeee.android.R;
import com.parkeee.android.adapter.eservices.ListViolationsReminderAdapter;
import com.parkeee.android.adapter.realm.RealmViolationsReminderAdapter;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.eservices.ServiceDashboard;
import com.parkeee.android.model.tables.ViolationInsertUpdateCache;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Commons;
import com.parkeee.android.views.CustomTextView;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by Inder Bagga on 11/30/2016.
 */
public class ListViolationReminders extends AppCompatActivity implements OnResultReceived,ListViolationsReminderAdapter.Callback,View.OnClickListener{

    Context mContext;
    ImageView actionAdd,actionBack,actionDelete;
    CustomTextView actionDone,headerText;
    ListViolationsReminderAdapter.Callback mCallback;

    private RecyclerView recyclerView;
    private CustomTextView emptyView;
    private Realm realmDB;

    ListViolationsReminderAdapter mListViolationsReminderAdapter;
    OnResultReceived mOnResultReceived;
    private ProgressDialog mProgressDialog;
    ArrayList<Long> alTimeStampIds;
    String strPUID="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_list_e_services);

        initializeUserInterface();
    }

    private void initializeUserInterface() {

        mContext=this;
        mCallback=this;
        mOnResultReceived=this;

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        actionAdd=(ImageView)findViewById(R.id.action_add);
        actionBack=(ImageView)findViewById(R.id.action_back);
        actionDelete=(ImageView)findViewById(R.id.action_delete);
        actionDone=(CustomTextView) findViewById(R.id.action_done);
        headerText=(CustomTextView) findViewById(R.id.headerText);

        actionAdd.setOnClickListener(this);
        actionBack.setOnClickListener(this);
        actionDelete.setOnClickListener(this);
        actionDone.setOnClickListener(this);

        emptyView = (CustomTextView)findViewById(R.id.emptyView);
        recyclerView = (RecyclerView)findViewById(R.id.listView);
        recyclerView.setHasFixedSize(true);
        headerText.setText(getString(R.string.violation_reminder_header_text));
        emptyView.setText(getString(R.string.no_violation_reminder_text));

        // use a linear layout manager since the cards are vertically scrollable
        final LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        layoutManager.setOrientation(LinearLayoutManager.VERTICAL);
        recyclerView.setLayoutManager(layoutManager);

        //get realm instance
        this.realmDB = RealmController.with(this).getRealm();

        //strPUID=RealmController.with(this).getRegisterInformation().getPuid();
        if(strPUID.isEmpty())strPUID="8396865563230";

        displayViolationReminders();
    }

   private void displayViolationReminders() {

        mListViolationsReminderAdapter = new ListViolationsReminderAdapter(this,this,this);

        if(RealmController.with(this).hasViolationsReminder()){

            RealmViolationsReminderAdapter realmAdapter = new RealmViolationsReminderAdapter(mContext, RealmController.with(this).getViolationsReminder().sort("timeStamp", Sort.DESCENDING), true);

            // Set the data and tell the RecyclerView to draw
            mListViolationsReminderAdapter.setRealmAdapter(realmAdapter);
            mListViolationsReminderAdapter.notifyDataSetChanged();

            recyclerView.setAdapter(mListViolationsReminderAdapter);

            emptyView.setVisibility(View.GONE);
            actionDelete.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.VISIBLE);

        }else{
            actionDelete.setVisibility(View.GONE);
            emptyView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
        }
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.action_add:

               startActivity(new Intent(this,AddViolationReminder.class));
                finishAffinity();

                break;

            case R.id.action_delete:

                mCallback.onActivated();

                break;

            case R.id.action_done:

                if(mListViolationsReminderAdapter.getSelectedCount()>0){

                    AlertDialog.Builder confirmDialog=new AlertDialog.Builder(mContext);
                    confirmDialog.setTitle(getString(R.string.app_name)+" says,")
                            .setCancelable(false)
                            .setMessage(AppMessages.DELETE_VL_ALERT)
                            .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    showToast(mListViolationsReminderAdapter.getSelectedCount()+"");

                                    ArrayList<Long> alCachedIds=new ArrayList<Long>();
                                    alTimeStampIds=new ArrayList<Long>();
                                    ArrayList<String> alServerIds=new ArrayList<String>();

                                    for(int index = 0; index< mListViolationsReminderAdapter.getSelectedCount(); index++){

                                        ViolationInsertUpdateCache oViolationInsertUpdateCache =RealmController.with(ListViolationReminders.this).getViolationsReminder().get(mListViolationsReminderAdapter.getSelectedPositions().get(index));

                                        long strCachedId=oViolationInsertUpdateCache.getTimeStamp();
                                        String strServerId=oViolationInsertUpdateCache.getViolation_id();

                                        alTimeStampIds.add(strCachedId);

                                        if(oViolationInsertUpdateCache.getIsINSUpdated().equals("1")||oViolationInsertUpdateCache.getIsINSUploaded().equals("1")){
                                            alServerIds.add(strServerId);
                                        }else{
                                            alCachedIds.add(strCachedId);
                                        }
                                    }

                                    //In case Delete API call
                                    if(alServerIds.size()>0){

                                        Realm realm = RealmController.with(ListViolationReminders.this).getRealm();

                                        //call Delete API
                                        if(Commons.isNetworkAvailable(mContext)){

                                            String strPayloadViolationIDs=alServerIds.get(0);

                                            for(int index=1;index<alServerIds.size();index++){
                                                strPayloadViolationIDs=strPayloadViolationIDs+"-"+alServerIds.get(index);
                                            }

                                            Log.e("strPayloadViolationIDs",strPayloadViolationIDs);

                                            mProgressDialog.show();

                                            //Get oDeleteVLByServerIDs Api
                                            GetApiClient oDeleteVLByServerIDs=new GetApiClient(mOnResultReceived);
                                            oDeleteVLByServerIDs.setRequestSource(RequestSource.DeleteVLByServerIDs);
                                            oDeleteVLByServerIDs.executeGetRequest(API.gDeleteVLByServerIDs(strPUID,strPayloadViolationIDs));

                                        }else{//set Delete flag 1

                                            for(int index=0;index<alServerIds.size();index++){

                                                RealmResults<ViolationInsertUpdateCache> results = realm.where(ViolationInsertUpdateCache.class).equalTo("violation_id", alServerIds.get(index)).findAll();
                                                realm.beginTransaction();
                                                results.get(index).setIsINSDeleted("1");
                                                realm.commitTransaction();
                                            }

                                            //remove Cached Insurance Reminders
                                            for(int position=0;position<alCachedIds.size();position++){
                                                Log.e("alSelectedIds is "," "+alCachedIds.get(position));
                                                RealmController.with(ListViolationReminders.this).removeViolationReminder(alCachedIds.get(position));
                                            }

                                            onDeactivated();

                                            if(!RealmController.with(ListViolationReminders.this).hasViolationsReminder()){
                                                emptyView.setVisibility(View.VISIBLE);
                                                recyclerView.setVisibility(View.GONE);
                                                actionDelete.setVisibility(View.GONE);
                                            }
                                        }
                                    }else{

                                        //remove Cached Violation Reminders
                                        for(int position=0;position<alCachedIds.size();position++){
                                            Log.e("alSelectedIds is "," "+alCachedIds.get(position));
                                            RealmController.with(ListViolationReminders.this).removeViolationReminder(alCachedIds.get(position));
                                        }

                                        onDeactivated();

                                        if(!RealmController.with(ListViolationReminders.this).hasViolationsReminder()){
                                            emptyView.setVisibility(View.VISIBLE);
                                            recyclerView.setVisibility(View.GONE);
                                            actionDelete.setVisibility(View.GONE);
                                        }
                                    }

                                }
                            })
                            .setPositiveButton(getString(R.string.cancel), new DialogInterface.OnClickListener() {
                                @Override
                                public void onClick(DialogInterface dialogInterface, int i) {

                                    dialogInterface.dismiss();
                                    onDeactivated();
                                }
                            })
                            .setOnCancelListener(new DialogInterface.OnCancelListener() {
                                @Override
                                public void onCancel(DialogInterface dialogInterface) {

                                    dialogInterface.dismiss();
                                    onDeactivated();
                                }
                            })
                            .setOnKeyListener(new DialogInterface.OnKeyListener() {
                                @Override
                                public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                    if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                        dialogInterface.dismiss();
                                        onDeactivated();
                                    }
                                    return true;
                                }
                            })
                            .show();

                }else{
                    onDeactivated();
                }
                break;

            case R.id.action_back:

                if(mListViolationsReminderAdapter.isDeletionActivated())onDeactivated();
                else closeActivity();
                break;
        }
    }

    private void showToast(final String strMessage) {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText( mContext, strMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void closeActivity(){
        startActivity(new Intent(this,ServiceDashboard.class));
        finishAffinity();
    }

    @Override
    public void onBackPressed() {

        if(mListViolationsReminderAdapter.isDeletionActivated())onDeactivated();
        else closeActivity();
    }

    @Override
    public void onIconClicked(int index) {

        mListViolationsReminderAdapter.deleteSelected(index);
    }

    @Override
    public void onActivated() {

        mListViolationsReminderAdapter.deletionActivated();

        actionAdd.setVisibility(View.GONE);
        actionDone.setVisibility(View.VISIBLE);
        actionDelete.setVisibility(View.GONE);
    }

    @Override
    public void onDeactivated() {

        mListViolationsReminderAdapter.clearSelected();
        actionAdd.setVisibility(View.VISIBLE);
        actionDelete.setVisibility(View.VISIBLE);
        actionDone.setVisibility(View.GONE);
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        mProgressDialog.dismiss();
        Log.e("what",what);
        showToast(what);

        switch(what){

            case "-2":
                displayAlert(getResources().getString(R.string.error_code_minus_two));
                break;
            case "-3" :
                displayAlert(getResources().getString(R.string.error_code_minus_three));
                break;
            case "-1":
                displayAlert(getResources().getString(R.string.error_code_minus_one));
                break;
            default:

                runOnUiThread(new Runnable() {

                    @Override
                    public void run() {

                        //remove Cached Violation Reminders
                        for(int position=0;position<alTimeStampIds.size();position++){
                            Log.e("alTimeStampIds is "," "+alTimeStampIds.get(position));
                            RealmController.with(ListViolationReminders.this).removeViolationReminder(alTimeStampIds.get(position));
                        }
                        onDeactivated();

                        if(!RealmController.with(ListViolationReminders.this).hasViolationsReminder()){
                            emptyView.setVisibility(View.VISIBLE);
                            recyclerView.setVisibility(View.GONE);
                            actionDelete.setVisibility(View.GONE);
                        }
                    }
                });

                break;
        }
    }

    private void displayAlert(final String strMessage) {

        ListViolationReminders.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                android.app.AlertDialog.Builder exitDialog=new android.app.AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name)+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();

            }
        });

    }
}