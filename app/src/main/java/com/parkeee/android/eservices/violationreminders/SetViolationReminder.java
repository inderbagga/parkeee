package com.parkeee.android.eservices.violationreminders;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Base64;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TableLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.Config;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;

import com.parkeee.android.model.params.ViolationInsertUpdate;
import com.parkeee.android.model.tables.ViolationInsertUpdateCache;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Commons;
import com.parkeee.android.utils.InternalStorageContentProvider;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import eu.janmuller.android.simplecropimage.CropImage;
import io.realm.Realm;

/**
 * Created by Inder Bagga on 11/30/2016.
 */
public class SetViolationReminder extends AppCompatActivity implements View.OnClickListener,OnResultReceived {

    private Context mContext;
    private Gson oGson;
    private OnResultReceived mOnResultReceived;
    private ProgressDialog mProgressDialog;
    private Realm realm;

    String strCar="";
    String strState="";
    String strViolationType="";

    String strViolationNumber;
    String strViolationAmount;
    String strDueDate;
    String strViolationDate;
    String formattedViolationDate;
    String formattedDueDate;

    private long lastTappedTimeInMillis=0;

    private Button bActionPayNow;
    private Button bActionSaveReminder;
    private ImageView bActionBack,imageFront,imageBack;
    private FrameLayout frameFront,frameBack;

    String strPUID="";

    private SwitchCompat switchView;
    TextView tvRemindDays;
    private int remindMeDays=1;
    private boolean isSwitchEnabled=true;
    private ImageView incrementCount,decrementCount;

    public static final String TAG = "SetViolationReminder";

    public static final String TEMP_PHOTO_FILE_NAME = "temp_photo.jpg";

    public static final int REQUEST_CODE_GALLERY      = 0x1;
    public static final int REQUEST_CODE_TAKE_PICTURE = 0x2;
    public static final int REQUEST_CODE_CROP_IMAGE   = 0x3;

    private File mFileTemp;
    private int imageType;//0 for Front and 1 for Back
    private String strBase64EncodedFront="";
    private String strBase64EncodedBack="";

    ImageView tipArrow;
    RelativeLayout tipLayout;
    TableLayout tipInfo;
    boolean isTipInfoVisible;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_set_violation_reminder);

        initializeUserInterface();
    }

    private void initializeUserInterface() {

        mContext = this;
        mOnResultReceived=this;
        oGson=new Gson();

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        tipArrow=(ImageView)findViewById(R.id.tipArrow);
        tipLayout=(RelativeLayout)findViewById(R.id.tipLayout);
        tipInfo=(TableLayout)findViewById(R.id.tipInfo);

        tipLayout.setOnClickListener(this);

        incrementCount = (ImageView) findViewById(R.id.iv_increment);
        decrementCount = (ImageView) findViewById(R.id.iv_decrement);
        switchView=(SwitchCompat)findViewById(R.id.switchView);
        tvRemindDays=(TextView)findViewById(R.id.tvRemindDays);

        frameFront=(FrameLayout) findViewById(R.id.frame_front);
        frameBack=(FrameLayout) findViewById(R.id.frame_back);

        bActionPayNow=(Button) findViewById(R.id.bActionPayNow);
        bActionSaveReminder=(Button) findViewById(R.id.bActionSaveReminder);
        bActionBack = (ImageView) findViewById(R.id.action_back);
        imageBack = (ImageView) findViewById(R.id.iv_back);
        imageFront = (ImageView) findViewById(R.id.iv_front);

        bActionBack.setOnClickListener(this);
        frameFront.setOnClickListener(this);
        frameBack.setOnClickListener(this);
        bActionPayNow.setOnClickListener(this);
        bActionSaveReminder.setOnClickListener(this);

        Intent addIntent=getIntent();

        strCar=addIntent.getStringExtra("strCar");
        strState=addIntent.getStringExtra("strState");
        strViolationType=addIntent.getStringExtra("strViolationType");

        strViolationNumber=addIntent.getStringExtra("strViolationNumber");
        strViolationAmount=addIntent.getStringExtra("strViolationAmount");
        strDueDate=addIntent.getStringExtra("strDueDate");
        strViolationDate=addIntent.getStringExtra("strViolationDate");

        try{
            Date dueDate=new SimpleDateFormat(Config.DATE_FORMAT).parse(strDueDate);
            Date violationDate=new SimpleDateFormat(Config.DATE_FORMAT).parse(strViolationDate);
            formattedDueDate=new SimpleDateFormat(Config.VL_DATE_FORMAT).format(dueDate);
            formattedViolationDate=new SimpleDateFormat(Config.VL_DATE_FORMAT).format(violationDate);
        }catch(ParseException pe){}

        incrementCount.setOnClickListener(this);
        decrementCount.setOnClickListener(this);

        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                isSwitchEnabled=isChecked;
                switchView.setChecked(isChecked);
            }
        });

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        strPUID=RealmController.with(this).getRegisterInformation().getPuid();
    }

    @Override
    public void onBackPressed() {

        finish();
    }

    private void displayAlert(final String strMessage) {

        SetViolationReminder.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder exitDialog=new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name)+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();

            }
        });

    }

    private void showToast(final String strMessage)
    {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText( mContext, strMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.tipLayout:

                if(isTipInfoVisible){
                    isTipInfoVisible=false;
                    tipArrow.setImageResource(R.drawable.arrow_down);
                    tipInfo.setVisibility(View.GONE);
                }else{
                    isTipInfoVisible=true;
                    tipArrow.setImageResource(R.drawable.arrow_up);
                    tipInfo.setVisibility(View.VISIBLE);
                }

                break;

            case R.id.frame_front:
                if(System.currentTimeMillis()-lastTappedTimeInMillis< Config.TAP_DELAY){
                    return;
                }
                lastTappedTimeInMillis=System.currentTimeMillis();

                capturePhoto(0);

                break;

            case R.id.frame_back:
                if(System.currentTimeMillis()-lastTappedTimeInMillis< Config.TAP_DELAY){
                    return;
                }
                lastTappedTimeInMillis=System.currentTimeMillis();

                capturePhoto(1);

                break;

            case R.id.action_back:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    onBackPressed();
                }

                break;

            case R.id.bActionSaveReminder:

                if(isSwitchEnabled){

                    Calendar dueTimeCalendar=Calendar.getInstance();
                    dueTimeCalendar.set(Calendar.HOUR_OF_DAY,0);
                    dueTimeCalendar.set(Calendar.MINUTE,0);
                    dueTimeCalendar.set(Calendar.SECOND,0);
                    Date today = dueTimeCalendar.getTime();
                    Date remindDate;

                    try{

                        Date dueDate=new SimpleDateFormat(Config.DATE_FORMAT).parse(strDueDate);

                        dueTimeCalendar.set(Calendar.DAY_OF_MONTH,dueDate.getDate());
                        dueTimeCalendar.set(Calendar.MONTH,dueDate.getMonth());
                        dueTimeCalendar.set(Calendar.YEAR,dueDate.getYear()+1900);

                        dueTimeCalendar.add(Calendar.DAY_OF_YEAR,-remindMeDays);
                        remindDate=dueTimeCalendar.getTime();

                        if(remindDate.compareTo(today) < 0){
                            displayAlert(AppMessages.REMINDER_DATE_BEFORE);
                            return;
                        }
                    }catch(ParseException pe){}
                }

                if(strBase64EncodedFront.isEmpty()){

                    displayAlert(AppMessages.VL_FRONT_IMAGE_BLANK);
                    return;
                }

                if(strBase64EncodedBack.isEmpty()){

                    displayAlert(AppMessages.VL_BACK_IMAGE_BLANK);
                    return;
                }

                if(RealmController.with(SetViolationReminder.this).containsViolation(strViolationNumber)){
                    displayAlert(AppMessages.VL_ALREADY_EXIST);
                    return;
                }

                if(Commons.isNetworkAvailable(mContext)){
                    ViolationInsertUpdate oViolationInsertUpdate=new ViolationInsertUpdate();
                    oViolationInsertUpdate.setViolation_plate(strCar);
                    oViolationInsertUpdate.setViolation_amount(strViolationAmount);
                    oViolationInsertUpdate.setViolation_number(strViolationNumber+"");
                    oViolationInsertUpdate.setViolation_state(strState+"");
                    oViolationInsertUpdate.setViolation_type(""+strViolationType);
                    oViolationInsertUpdate.setViolation_date(formattedViolationDate);
                    oViolationInsertUpdate.setDue_date(formattedDueDate);
                    oViolationInsertUpdate.setVl_front_base64string(strBase64EncodedFront);
                    oViolationInsertUpdate.setVl_back_base64string(strBase64EncodedBack);
                    oViolationInsertUpdate.setRemind_before_time(remindMeDays+"");
                    oViolationInsertUpdate.setRemind_before_type("0");
                    oViolationInsertUpdate.setPuid(strPUID);
                    oViolationInsertUpdate.setViolation_id("0");
                    oViolationInsertUpdate.setIsNotiEnabled(isSwitchEnabled?"1":"0");

                    String jsonString=oGson.toJson(oViolationInsertUpdate,ViolationInsertUpdate.class).toString();

                    mProgressDialog.show();

                    //Post oViolationInsertUpdate Api
                    PostApiClient oViolationInsertUpdateApi=new PostApiClient(mOnResultReceived);
                    oViolationInsertUpdateApi.setRequestSource(RequestSource.ViolationInsertUpdate);
                    oViolationInsertUpdateApi.executePostRequest(API.pViolationInsertUpdate(),jsonString);

                    Log.e("API",API.pViolationInsertUpdate());
                    Log.e("jsonString",jsonString);

                }else{
                    ViolationInsertUpdateCache oViolationInsertUpdateCache=new ViolationInsertUpdateCache();
                    oViolationInsertUpdateCache.setViolation_plate(strCar);
                    oViolationInsertUpdateCache.setViolation_amount(strViolationAmount);
                    oViolationInsertUpdateCache.setViolation_number(strViolationNumber+"");
                    oViolationInsertUpdateCache.setViolation_state(strState+"");
                    oViolationInsertUpdateCache.setViolation_type(""+strViolationType);
                    oViolationInsertUpdateCache.setViolation_date(formattedViolationDate);
                    oViolationInsertUpdateCache.setDue_date(formattedDueDate);
                    oViolationInsertUpdateCache.setVl_front_base64string(strBase64EncodedFront);
                    oViolationInsertUpdateCache.setVl_back_base64string(strBase64EncodedBack);
                    oViolationInsertUpdateCache.setRemind_before_time(remindMeDays+"");
                    oViolationInsertUpdateCache.setRemind_before_type("0");
                    oViolationInsertUpdateCache.setPuid(strPUID);
                    oViolationInsertUpdateCache.setViolation_id("0");
                    oViolationInsertUpdateCache.setIsNotiEnabled(isSwitchEnabled?"1":"0");

                    oViolationInsertUpdateCache.setIsINSDeleted("0");
                    oViolationInsertUpdateCache.setIsINSUpdated("0");
                    oViolationInsertUpdateCache.setIsINSUploaded("0");
                    oViolationInsertUpdateCache.setTimeStamp(System.currentTimeMillis());

                    realm.beginTransaction();
                    realm.copyToRealm(oViolationInsertUpdateCache);
                    realm.commitTransaction();

                    showToast("Saved");

                    startActivity(new Intent(SetViolationReminder.this,ListViolationReminders.class));
                    finishAffinity();
                }

                break;

            case R.id.iv_increment:

                if(!isSwitchEnabled)return;

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    if(remindMeDays<10){
                        remindMeDays++;
                        tvRemindDays.setText(""+remindMeDays);
                    }
                }

                break;

            case R.id.iv_decrement:

                if(!isSwitchEnabled)return;

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    if(remindMeDays>1){
                        remindMeDays--;
                        tvRemindDays.setText(""+remindMeDays);
                    }
                }

                break;
        }
    }

    private void capturePhoto(int photoType) {

        imageType=photoType;
        //check permissions

        String state = Environment.getExternalStorageState();
        if (Environment.MEDIA_MOUNTED.equals(state)) {
            mFileTemp = new File(Environment.getExternalStorageDirectory(), TEMP_PHOTO_FILE_NAME);
        }
        else {
            mFileTemp = new File(getFilesDir(), TEMP_PHOTO_FILE_NAME);
        }

        AlertDialog.Builder photoDialog=new AlertDialog.Builder(mContext);
        photoDialog.setCancelable(false)
                .setTitle(getString(R.string.app_name)+" says,")
                .setCancelable(false)
                .setMessage("Select Image Source")
                .setNegativeButton("FROM GALLERY", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        openGallery();
                    }
                })
                .setPositiveButton("CAPTURE PICTURE", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {

                        dialogInterface.dismiss();
                        takePicture();
                    }
                })
                .setOnCancelListener(new DialogInterface.OnCancelListener() {
                    @Override
                    public void onCancel(DialogInterface dialogInterface) {

                        dialogInterface.dismiss();
                    }
                })
                .setOnKeyListener(new DialogInterface.OnKeyListener() {
                    @Override
                    public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                        if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                            dialogInterface.dismiss();
                        }
                        return true;
                    }
                })
                .show();
    }

    @Override
    public void dispatchString(RequestSource from, final String what) {

        mProgressDialog.dismiss();
        Log.e("what",what);
        showToast(what);

        switch(what){

            case "-2":
                displayAlert(getResources().getString(R.string.error_code_minus_two));
                break;
            case "-3" :
                displayAlert(getResources().getString(R.string.error_code_minus_three));
                break;
            case "0":
                displayAlert(getResources().getString(R.string.error_code_zero_insurance));
                break;
            case "-1":
                displayAlert(getResources().getString(R.string.error_code_minus_one));
                break;
            default:

                SetViolationReminder.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        ViolationInsertUpdateCache oViolationInsertUpdateCache=new ViolationInsertUpdateCache();
                        oViolationInsertUpdateCache.setViolation_plate(strCar);
                        oViolationInsertUpdateCache.setViolation_amount(strViolationAmount);
                        oViolationInsertUpdateCache.setViolation_number(strViolationNumber+"");
                        oViolationInsertUpdateCache.setViolation_state(strState+"");
                        oViolationInsertUpdateCache.setViolation_type(""+strViolationType);
                        oViolationInsertUpdateCache.setViolation_date(formattedViolationDate);
                        oViolationInsertUpdateCache.setDue_date(formattedDueDate);
                        oViolationInsertUpdateCache.setVl_front_base64string(strBase64EncodedFront);
                        oViolationInsertUpdateCache.setVl_back_base64string(strBase64EncodedBack);
                        oViolationInsertUpdateCache.setRemind_before_time(remindMeDays+"");
                        oViolationInsertUpdateCache.setRemind_before_type("0");
                        oViolationInsertUpdateCache.setPuid(strPUID);

                        try{
                            JSONObject jsonObject=new JSONObject(what);
                            oViolationInsertUpdateCache.setViolation_id(jsonObject.getString("violation_id"));
                        }catch (JSONException je){}
                        oViolationInsertUpdateCache.setIsNotiEnabled(isSwitchEnabled?"1":"0");

                        oViolationInsertUpdateCache.setIsINSDeleted("0");
                        oViolationInsertUpdateCache.setIsINSUpdated("0");
                        oViolationInsertUpdateCache.setIsINSUploaded("1");
                        oViolationInsertUpdateCache.setTimeStamp(System.currentTimeMillis());

                        realm.beginTransaction();
                        realm.copyToRealm(oViolationInsertUpdateCache);
                        realm.commitTransaction();

                        showToast("Saved");

                        startActivity(new Intent(SetViolationReminder.this,ListViolationReminders.class));
                        finishAffinity();
                    }
                });

                break;
        }

    }

    private void takePicture() {

        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        try {
            Uri mImageCaptureUri = null;
            String state = Environment.getExternalStorageState();
            if (Environment.MEDIA_MOUNTED.equals(state)) {
                mImageCaptureUri = Uri.fromFile(mFileTemp);
            }
            else {
	        	/*
	        	 * The solution is taken from here: http://stackoverflow.com/questions/10042695/how-to-get-camera-result-as-a-uri-in-data-folder
	        	 */
                mImageCaptureUri = InternalStorageContentProvider.CONTENT_URI;
            }
            intent.putExtra(MediaStore.EXTRA_OUTPUT, mImageCaptureUri);
            intent.putExtra("return-data", true);
            startActivityForResult(intent, REQUEST_CODE_TAKE_PICTURE);
        } catch (ActivityNotFoundException e) {

            Log.d(TAG, "cannot take picture", e);
        }
    }

    private void openGallery() {

        Intent photoPickerIntent = new Intent(Intent.ACTION_PICK);
        photoPickerIntent.setType("image/*");
        startActivityForResult(photoPickerIntent, REQUEST_CODE_GALLERY);
    }

    private void startCropImage() {

        Intent intent = new Intent(this, CropImage.class);
        intent.putExtra(CropImage.IMAGE_PATH, mFileTemp.getPath());
        intent.putExtra(CropImage.SCALE, true);

        intent.putExtra(CropImage.ASPECT_X, 3);
        intent.putExtra(CropImage.ASPECT_Y, 2);

        startActivityForResult(intent, REQUEST_CODE_CROP_IMAGE);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        if (resultCode != RESULT_OK) {

            displayAlert("Operation Failed. Please try again. ");
            return;
        }

        Bitmap bitmap;

        switch (requestCode) {

            case REQUEST_CODE_GALLERY:

                try {

                    InputStream inputStream = getContentResolver().openInputStream(data.getData());
                    FileOutputStream fileOutputStream = new FileOutputStream(mFileTemp);
                    copyStream(inputStream, fileOutputStream);
                    fileOutputStream.close();
                    inputStream.close();

                    startCropImage();

                } catch (Exception e) {

                    Log.e(TAG, "Error while creating temp file", e);
                }

                break;
            case REQUEST_CODE_TAKE_PICTURE:

                startCropImage();
                break;
            case REQUEST_CODE_CROP_IMAGE:

                String path = data.getStringExtra(CropImage.IMAGE_PATH);
                if (path == null) {

                    return;
                }

                bitmap = BitmapFactory.decodeFile(mFileTemp.getPath());

                ByteArrayOutputStream baos = new ByteArrayOutputStream();
                bitmap.compress(Bitmap.CompressFormat.JPEG,100,baos);
                byte[] b = baos.toByteArray();
                String encodedString = Base64.encodeToString(b, Base64.DEFAULT);

                if(imageType==0){
                    imageFront.setImageBitmap(bitmap);
                    strBase64EncodedFront=encodedString;
                }else {
                    strBase64EncodedBack=encodedString;
                    imageBack.setImageBitmap(bitmap);
                }
                break;
        }
        super.onActivityResult(requestCode, resultCode, data);
    }

    public static void copyStream(InputStream input, OutputStream output)
            throws IOException {

        byte[] buffer = new byte[1024];
        int bytesRead;
        while ((bytesRead = input.read(buffer)) != -1) {
            output.write(buffer, 0, bytesRead);
        }
    }
}