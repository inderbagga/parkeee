package com.parkeee.android.eservices.violationreminders;

import android.app.AlertDialog;
import android.app.DatePickerDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.Config;
import com.parkeee.android.backend.Data;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.menu.ActivityAddCar;
import com.parkeee.android.model.tables.States;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Commons;
import com.parkeee.android.views.MaterialSpinner;
import com.parkeee.android.views.materialedittext.MaterialEditText;

import org.json.JSONArray;
import org.json.JSONException;

import java.lang.reflect.Method;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;
import io.realm.RealmResults;

/**
 * Created by Inder Bagga on 11/30/2016.
 */
public class AddViolationReminder extends AppCompatActivity implements View.OnClickListener,OnResultReceived {

    private Context mContext;
    private Gson oGson;
    private OnResultReceived mOnResultReceived;
    private ProgressDialog mProgressDialog;
    private Realm realm;

    int iViolationType=-1;

    String strCar="";
    String strState="";
    String strViolationType="";

    String strViolationNumber;
    String strViolationAmount;
    String strDueDate;
    String strViolationDate;

    private long lastTappedTimeInMillis=0;

    private Button bActionPayNow;
    private Button bActionSetReminder;
    private ImageView bActionBack;

    private MaterialSpinner mCarSelector,mStateSelector,mViolationTypeSelector;
    private MaterialEditText etCar,etState,etViolationType,etViolationNumber,etViolationAmount,etDueDate,etViolationDate;

    String strPUID="";
    ArrayList<String> alAddedCars=new ArrayList<String>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_violation_reminder);

        initializeUserInterface();
    }

    private void initializeUserInterface() {

        mContext = this;
        mOnResultReceived=this;
        oGson=new Gson();

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        bActionPayNow=(Button) findViewById(R.id.bActionPayNow);
        bActionSetReminder=(Button) findViewById(R.id.bActionSetReminder);
        bActionBack = (ImageView) findViewById(R.id.action_back);

        mCarSelector = (MaterialSpinner) findViewById(R.id.sp_car);
        mStateSelector = (MaterialSpinner) findViewById(R.id.sp_state);
        mViolationTypeSelector = (MaterialSpinner) findViewById(R.id.sp_violation_type);

        etCar = (MaterialEditText) findViewById(R.id.et_car);
        etState = (MaterialEditText) findViewById(R.id.et_state);
        etViolationAmount = (MaterialEditText) findViewById(R.id.et_violation_amount);
        etViolationNumber = (MaterialEditText) findViewById(R.id.et_violation_number);
        etViolationType = (MaterialEditText) findViewById(R.id.et_violation_type);
        etViolationDate = (MaterialEditText) findViewById(R.id.et_violation_date);
        etDueDate = (MaterialEditText) findViewById(R.id.et_due_date);

        bActionBack.setOnClickListener(this);
        bActionPayNow.setOnClickListener(this);
        bActionSetReminder.setOnClickListener(this);

        etCar.setOnClickListener(this);
        etState.setOnClickListener(this);
        etViolationType.setOnClickListener(this);
        etDueDate.setOnClickListener(this);
        etViolationDate.setOnClickListener(this);

        etViolationAmount.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean b) {

                String strText=etViolationAmount.getText().toString();

                if(!view.hasFocus()&&!strText.isEmpty()){
                    etViolationAmount.setText("$"+strText);
                }else if(view.hasFocus()&&strText.startsWith("$")){
                    etViolationAmount.setText(strText.substring(1,strText.length()));
                }
            }
        });

        alAddedCars.clear();

        for(int index=0;index<RealmController.with(this).getAddedCarsInformation().size();index++){

            alAddedCars.add(RealmController.with(this).getAddedCarsInformation().get(index).getPlateNumber());
        }

        alAddedCars.add("Add New Car");

        ArrayList<String> alStates=new ArrayList<String>();

        if(RealmController.with(this).hasStates()){

            RealmResults<States> realmStates=RealmController.with(this).getStates();

            for(int index=0;index<realmStates.size();index++){
                alStates.add(realmStates.get(index).getStateName());
            }
            mStateSelector.setAdapter(new CustomSpinnerAdapter(mContext, 2, R.layout.list_item_textview,alStates.toArray(new String[alStates.size()])));
        }else {
            //call Delete API
            if(Commons.isNetworkAvailable(mContext)){

                mProgressDialog.show();

                //Get oGetStatesForViolations Api
                GetApiClient oGetStatesForViolations=new GetApiClient(mOnResultReceived);
                oGetStatesForViolations.setRequestSource(RequestSource.GetStatesForViolations);
                oGetStatesForViolations.executeGetRequest(API.gGetStatesForViolations());

            }else{

                displayAlertAndRetry(AppMessages.NO_INTERNET);
            }
        }

        mCarSelector.setAdapter(new CustomSpinnerAdapter(mContext, 1, R.layout.list_item_textview, alAddedCars.toArray(new String[alAddedCars.size()])));
         mViolationTypeSelector.setAdapter(new CustomSpinnerAdapter(mContext, 3, R.layout.list_item_textview, Data.strArrayViolationTypes));

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

         strPUID=RealmController.with(this).getRegisterInformation().getPuid();
    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this,ListViolationReminders.class));
        finishAffinity();
    }

    private void displayAlert(final String strMessage) {

        AddViolationReminder.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder exitDialog=new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name)+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();

            }
        });

    }

    private void displayAlertAndRetry(final String strMessage) {

        AddViolationReminder.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder exitDialog=new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name)+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_refresh), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();
                                mProgressDialog.show();

                                //Get oGetStatesForViolations Api
                                GetApiClient oGetStatesForViolations=new GetApiClient(mOnResultReceived);
                                oGetStatesForViolations.setRequestSource(RequestSource.GetStatesForViolations);
                                oGetStatesForViolations.executeGetRequest(API.gGetStatesForViolations());
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                                dialogInterface.dismiss();
                                onBackPressed();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                    dialogInterface.dismiss();
                                    onBackPressed();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });

    }

    private void showToast(final String strMessage)
    {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText( mContext, strMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    private void selectViolationDate() {

        final Calendar todayCalendar = Calendar.getInstance();

        final Date today =todayCalendar.getTime();

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                final Calendar selectedTimeCalendar = Calendar.getInstance();
                selectedTimeCalendar.set(Calendar.HOUR_OF_DAY,0);
                selectedTimeCalendar.set(Calendar.MINUTE,0);
                selectedTimeCalendar.set(Calendar.SECOND,0);
                selectedTimeCalendar.set(Calendar.YEAR,year);
                selectedTimeCalendar.set(Calendar.MONTH,monthOfYear);
                selectedTimeCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                Date selectedDate=selectedTimeCalendar.getTime();

                long todayTime=today.getTime();
                long violationTime=selectedDate.getTime();

                if(todayTime>violationTime&&todayTime-violationTime>60000){
                    etViolationDate.setText(new SimpleDateFormat(Config.DATE_FORMAT).format(selectedDate));
                }
                else if(violationTime>todayTime&&violationTime-todayTime>60000){
                    displayAlert("Violation date can't be in Future");
                }else  etViolationDate.setText(new SimpleDateFormat(Config.DATE_FORMAT).format(selectedDate));

            }
        }, todayCalendar.get(Calendar.YEAR), todayCalendar.get(Calendar.MONTH), todayCalendar.get(Calendar.DATE)).show();

    }

    private void selectDueDate(final Date violationDate) {

        new DatePickerDialog(this, new DatePickerDialog.OnDateSetListener() {

            @Override
            public void onDateSet(DatePicker view, int year, int monthOfYear, int dayOfMonth) {

                final Calendar selectedTimeCalendar = Calendar.getInstance();
                selectedTimeCalendar.set(Calendar.HOUR_OF_DAY,0);
                selectedTimeCalendar.set(Calendar.MINUTE,0);
                selectedTimeCalendar.set(Calendar.SECOND,0);

                Date nowDate=new Date();
                nowDate.setHours(0);
                nowDate.setMinutes(0);
                nowDate.setSeconds(0);

                long nowTime=nowDate.getTime();

                selectedTimeCalendar.set(Calendar.YEAR,year);
                selectedTimeCalendar.set(Calendar.MONTH,monthOfYear);
                selectedTimeCalendar.set(Calendar.DAY_OF_MONTH,dayOfMonth);

                Date selectedDate=selectedTimeCalendar.getTime();

                long violationTime=violationDate.getTime();
                long selectedTime=selectedDate.getTime();

                if(nowTime>selectedTime&&nowTime-selectedTime>60000){
                    displayAlert("Due date can't be in the past");
                }
                else if(violationTime>selectedTime&&violationTime-selectedTime>60000){
                    displayAlert("Due date can't be before violation date");
                }else  etDueDate.setText(new SimpleDateFormat(Config.DATE_FORMAT).format(selectedDate));

            }
        }, violationDate.getYear()+1900,violationDate.getMonth(), violationDate.getDate()).show();

    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.action_back:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    onBackPressed();
                }

                break;

            case R.id.et_car:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mCarSelector.performClick();
                }

                break;

            case R.id.et_state:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mStateSelector.performClick();
                }

                break;

            case R.id.et_violation_type:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mViolationTypeSelector.performClick();
                }

                break;

            case R.id.et_violation_date:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();

                    selectViolationDate();
                }

                break;

            case R.id.et_due_date:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();

                    try{
                        if(etViolationDate.getText().toString().trim().isEmpty()) displayAlert("Please select Violation Date");
                        else selectDueDate(new SimpleDateFormat(Config.DATE_FORMAT).parse(etViolationDate.getText().toString().trim()));
                    }catch (ParseException pe){}
                }

                break;

            case R.id.bActionSetReminder:

                if(System.currentTimeMillis()-lastTappedTimeInMillis< Config.TAP_DELAY){
                    return;
                }

                strCar=etCar.getText().toString().trim();
                strViolationAmount=etViolationAmount.getText().toString().trim();
                strViolationType=etViolationType.getText().toString().trim();
                strViolationNumber=etViolationNumber.getText().toString().trim();

                strDueDate=etDueDate.getText().toString().trim();
                strViolationDate=etViolationDate.getText().toString().trim();

                if(strViolationAmount.startsWith("$")){
                    strViolationAmount=strViolationAmount.substring(1,strViolationAmount.length());
                }

                if(strCar.isEmpty()){

                    displayAlert(AppMessages.VL_CAR_BLANK);
                    return;
                }

                 if(strState.trim().isEmpty()){

                    displayAlert(AppMessages.VL_STATE_BLANK);
                    return;
                }

                if(strViolationType.trim().isEmpty()){

                    displayAlert(AppMessages.VL_TYPE_BLANK);
                    return ;
                }

                if(strViolationNumber.trim().isEmpty()){

                    displayAlert(AppMessages.VL_NUMBER_BLANK);
                    return ;
                }

                if(strViolationAmount.isEmpty()){

                    displayAlert(AppMessages.VL_AMOUNT_BLANK);
                    return;
                }

                if(strViolationDate.isEmpty()){
                    displayAlert(AppMessages.VL_DATE_BLANK);
                    return;
                }

                if(strDueDate.isEmpty()){
                    displayAlert(AppMessages.VL_DUE_DATE_BLANK);
                    return;
                }

                Intent setReminder=new Intent(AddViolationReminder.this,SetViolationReminder.class);
                setReminder.putExtra("strDueDate",strDueDate);
                setReminder.putExtra("strCar","CARTASK");
                setReminder.putExtra("strState",strState);
                setReminder.putExtra("strViolationType",strViolationType);;
                setReminder.putExtra("strViolationAmount",strViolationAmount);
                setReminder.putExtra("strViolationNumber",strViolationNumber);
                setReminder.putExtra("strViolationDate",strViolationDate);
                startActivity(setReminder);

                break;

            case R.id.bActionPayNow:

                break;
        }
    }

    @Override
    public void dispatchString(RequestSource from, final String what) {

        if(from==RequestSource.GetStatesForViolations){

            mProgressDialog.dismiss();
            Log.e("what",what);
            showToast(what);

            switch(what){

                case "-2":
                    displayAlertAndRetry(getResources().getString(R.string.error_code_minus_two));
                    break;
                case "-3" :
                    displayAlertAndRetry(getResources().getString(R.string.error_code_minus_three));
                    break;
                case "-1":
                    displayAlertAndRetry(getResources().getString(R.string.error_code_minus_one));
                    break;
                default:

                    runOnUiThread(new Runnable() {

                        @Override
                        public void run() {

                            try{

                                JSONArray jsonArray=new JSONArray(what);
                                ArrayList<String> alStates=new ArrayList<String>();
                                for(int index=0;index<jsonArray.length();index++){

                                    States mState=new States();
                                    String strState=jsonArray.getJSONObject(index).getString("StateName");
                                    mState.setStateName(strState);
                                    alStates.add(strState);

                                    realm.beginTransaction();
                                    realm.copyToRealm(mState);
                                    realm.commitTransaction();
                                }


                                mStateSelector.setAdapter(new CustomSpinnerAdapter(mContext, 2, R.layout.list_item_textview,alStates.toArray(new String[alStates.size()])));

                            }catch (JSONException je){
                                displayAlertAndRetry(je.getMessage());
                            }

                        }
                    });

                    break;
            }
        }
    }

    public class CustomSpinnerAdapter extends ArrayAdapter<String> {

        Context _context;
        String[] _objects;
        int _id;

        public CustomSpinnerAdapter(Context context, int id, int textViewResourceId,
                                    String[] objects) {
            super(context, textViewResourceId, objects);
            this._context=context;
            this._objects=objects;
            this._id=id;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(final int position, View convertView, ViewGroup parent) {

            View mContentView= LayoutInflater.from(_context).inflate(R.layout.list_item_textview, parent, false);

            TextView tvLabel=(TextView)mContentView.findViewById(R.id.tv_id);
            tvLabel.setText(_objects[position]);

            mContentView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    switch (_id){

                        case 1:

                            if(alAddedCars.size()-1==position){
                                startActivity(new Intent(AddViolationReminder.this, ActivityAddCar.class));
                                //finishAffinity();
                            }else{

                                etCar.setText(_objects[position]);
                                strCar=_objects[position];

                                try {
                                    Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                    method.setAccessible(true);
                                    method.invoke(mCarSelector);
                                } catch (Exception e) {
                                    e.printStackTrace();
                                }
                            }

                            break;

                        case 2:

                            etState.setText(_objects[position]);
                            strState=_objects[position];

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mStateSelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            break;

                        case 3:

                            strViolationType=_objects[position];
                            iViolationType=position;
                            etViolationType.setText(_objects[position]);

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mViolationTypeSelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                    }
                }
            });

            return mContentView;
        }
    }
}