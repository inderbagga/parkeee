package com.parkeee.android.eservices.otherbills;

import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CompoundButton;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RadioButton;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.Config;
import com.parkeee.android.backend.Data;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;

import com.parkeee.android.model.params.UserBillingInsertUpdate;
import com.parkeee.android.model.tables.UserBillingInsertUpdateCache;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Commons;
import com.parkeee.android.views.MaterialSpinner;
import com.parkeee.android.views.materialedittext.MaterialEditText;

import java.lang.reflect.Method;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Calendar;
import java.util.Date;

import io.realm.Realm;

/**
 * Created by Inder Bagga on 11/30/2016.
 */
public class AddOtherBill extends AppCompatActivity implements View.OnClickListener,OnResultReceived {

    private Context mContext;
    private Gson oGson;
    private Realm realm;
    private OnResultReceived mOnResultReceived;
    private ProgressDialog mProgressDialog;

    private int iSelectedMonth=-1;
    private int iSelectedDate=-1;
    private int iReminderMode=-1;
    private int remindMeDays=1;
    private long lastTappedTimeInMillis=0;

    private String strCategory="";
    private String strAmountDue;
    private String strBill;

    private boolean isSwitchEnabled=true;
    private boolean  hasOtherCategory=false;
    private boolean  shallRepeat=false;

    private LinearLayout addCategoryView;
    private FrameLayout monthView;
    private Button bActionSave;

    private ImageView bActionBack;
    private ImageView incrementCount,decrementCount;
    private TextView tvRemindDays;
    private SwitchCompat switchView;
    private MaterialSpinner mCategorySelector,mReminderSelector,mMonthSelector,mDateSelector;
    private MaterialEditText etCategory,etOtherCategory,etBill,etReminderMode,etAmountDue,etDueMonth,etDueDate;
    private RadioButton rbRepeat;
    String strPUID="";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_other_bill);

        initializeUserInterface();
    }

    private void initializeUserInterface() {

        mContext = this;
        mOnResultReceived=this;
        oGson=new Gson();
        realm = RealmController.with(this).getRealm();

        mProgressDialog=new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        bActionSave=(Button) findViewById(R.id.bActionSave);
        bActionBack = (ImageView) findViewById(R.id.action_back);
        incrementCount = (ImageView) findViewById(R.id.iv_increment);
        decrementCount = (ImageView) findViewById(R.id.iv_decrement);
        switchView=(SwitchCompat)findViewById(R.id.switchView);
        tvRemindDays=(TextView)findViewById(R.id.tvRemindDays);
        addCategoryView = (LinearLayout) findViewById(R.id.add_category);
        monthView = (FrameLayout) findViewById(R.id.monthView);
        rbRepeat = (RadioButton) findViewById(R.id.rbRepeat);

        mCategorySelector = (MaterialSpinner) findViewById(R.id.sp_category);
        mReminderSelector = (MaterialSpinner) findViewById(R.id.sp_reminder_mode);
        mMonthSelector = (MaterialSpinner) findViewById(R.id.sp_due_month);
        mDateSelector = (MaterialSpinner) findViewById(R.id.sp_due_date);

        etDueMonth = (MaterialEditText) findViewById(R.id.et_due_month);
        etDueDate = (MaterialEditText) findViewById(R.id.et_due_date);
        etOtherCategory = (MaterialEditText) findViewById(R.id.et_other_category);
        etCategory = (MaterialEditText) findViewById(R.id.et_category);
        etBill = (MaterialEditText) findViewById(R.id.et_bill);
        etAmountDue = (MaterialEditText) findViewById(R.id.et_amount_due);
        etReminderMode = (MaterialEditText) findViewById(R.id.et_reminder_type);

        bActionBack.setOnClickListener(this);
        bActionSave.setOnClickListener(this);
        incrementCount.setOnClickListener(this);
        decrementCount.setOnClickListener(this);
        etReminderMode.setOnClickListener(this);
        etDueMonth.setOnClickListener(this);
        etDueDate.setOnClickListener(this);
        etCategory.setOnClickListener(this);

        monthView.setVisibility(View.GONE);
        addCategoryView.setVisibility(View.GONE);

        etAmountDue.setOnFocusChangeListener(new View.OnFocusChangeListener() {

            @Override
            public void onFocusChange(View view, boolean b) {

                String strText=etAmountDue.getText().toString();

                if(!view.hasFocus()&&!strText.isEmpty()){
                    etAmountDue.setText("$"+strText);
                }else if(view.hasFocus()&&strText.startsWith("$")){
                    etAmountDue.setText(strText.substring(1,strText.length()));
                }
            }
        });

        switchView.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {

                isSwitchEnabled=isChecked;
                switchView.setChecked(isChecked);
            }
        });

        rbRepeat.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if(shallRepeat){
                    shallRepeat=false;
                    rbRepeat.setChecked(false);
                }else{
                    shallRepeat=true;
                    rbRepeat.setChecked(true);
                }
            }
        });

        mCategorySelector.setAdapter(new CustomSpinnerAdapter(mContext, 1, R.layout.list_item_textview, Data.strArrayCategories));
        mReminderSelector.setAdapter(new CustomSpinnerAdapter(mContext, 2, R.layout.list_item_textview, Data.strArrayReminderTypes));
        mMonthSelector.setAdapter(new CustomSpinnerAdapter(mContext, 3, R.layout.list_item_textview, Data.strArrayMonthNames));

        strPUID=RealmController.with(this).getRegisterInformation().getPuid();
        //if(strPUID.isEmpty())strPUID="8396865563230";

    }

    @Override
    public void onBackPressed() {

        startActivity(new Intent(this,ListOtherBills.class));
        finishAffinity();
    }

    private void displayAlert(final String strMessage) {

        AddOtherBill.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {

                AlertDialog.Builder exitDialog=new AlertDialog.Builder(mContext);
                exitDialog.setTitle(getString(R.string.app_name)+" says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {

                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if(keyEvent.getKeyCode()==KeyEvent.KEYCODE_BACK){

                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();

            }
        });

    }


    private void showToast(final String strMessage)
    {
        runOnUiThread(new Runnable() {
            public void run()
            {
                Toast.makeText( mContext, strMessage, Toast.LENGTH_LONG).show();
            }
        });
    }

    @Override
    public void onClick(View view) {

        switch (view.getId()){

            case R.id.iv_increment:

                if(!isSwitchEnabled)return;

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    if(remindMeDays<10){
                        remindMeDays++;
                        tvRemindDays.setText(""+remindMeDays);
                    }
                }

                break;

            case R.id.iv_decrement:

                if(!isSwitchEnabled)return;

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    if(remindMeDays>1){
                        remindMeDays--;
                        tvRemindDays.setText(""+remindMeDays);
                    }
                }

                break;

            case R.id.action_back:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    onBackPressed();
                }

                break;

            case R.id.et_category:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mCategorySelector.performClick();
                }

                break;

            case R.id.et_due_month:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mMonthSelector.performClick();
                }

                break;

            case R.id.et_due_date:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();

                    if(iReminderMode==-1){ displayAlert(AppMessages.BILL_REMINDER_TYPE_BLANK);}
                    else if(iSelectedMonth==-1){displayAlert(AppMessages.BILL_DUE_MONTH_BLANK);}else mDateSelector.performClick();
                }

                break;

            case R.id.et_reminder_type:

                if(System.currentTimeMillis()-lastTappedTimeInMillis> Config.TAP_DELAY){

                    lastTappedTimeInMillis=System.currentTimeMillis();
                    mReminderSelector.performClick();
                }

                break;

            case R.id.bActionSave:

                if(System.currentTimeMillis()-lastTappedTimeInMillis< Config.TAP_DELAY){
                    return;
                }

                strAmountDue=etAmountDue.getText().toString();
                strBill=etBill.getText().toString();

                if(strAmountDue.startsWith("$")){
                    strAmountDue=strAmountDue.substring(1,strAmountDue.length());
                }

                if( hasOtherCategory){
                    if(etOtherCategory.getText().toString().trim().isEmpty()){
                        displayAlert(AppMessages.BILL_OTHER_CATEGORY_BLANK);
                        return;
                    }else strCategory=etOtherCategory.getText().toString();
                }else if(strCategory.trim().isEmpty()){

                    displayAlert(AppMessages.BILL_CATEGORY_BLANK);
                    return;
                }

                if(strBill.isEmpty()){

                    displayAlert(AppMessages.BILL_NAME_BLANK);
                    return;
                }

                if(strAmountDue.isEmpty()){

                    displayAlert(AppMessages.BILL_AMOUNT_BLANK);
                    return;
                }

                if(iReminderMode==-1){

                    displayAlert(AppMessages.BILL_REMINDER_TYPE_BLANK);
                    return;
                }

                switch (iReminderMode){

                    case 0:

                        if(iSelectedDate==-1){
                            displayAlert(AppMessages.BILL_DUE_DAY_BLANK);
                            return;
                        }
                        break;
                    case 1:
                        if(iSelectedDate==-1){
                            displayAlert(AppMessages.BILL_DUE_DATE_BLANK);
                            return;
                        }
                        break;
                    case 2:
                    case 3:
                    case 4:

                        if(iSelectedMonth==-1){
                            displayAlert(AppMessages.BILL_DUE_MONTH_BLANK);
                            return;
                        }

                        if(iSelectedDate==-1){
                            displayAlert(AppMessages.BILL_DUE_DATE_BLANK);
                            return;
                        }
                        break;
                }

                if(isSwitchEnabled){

                    Calendar dueTimeCalendar=Calendar.getInstance();
                    dueTimeCalendar.set(Calendar.HOUR_OF_DAY,0);
                    dueTimeCalendar.set(Calendar.MINUTE,0);
                    dueTimeCalendar.set(Calendar.SECOND,0);
                    Date today = dueTimeCalendar.getTime();
                    Date remindDate,dueDate=null;
                    switch(iReminderMode){

                        case 0:
                            dueTimeCalendar.set(Calendar.DAY_OF_WEEK,iSelectedDate+1);
                            while( dueTimeCalendar.getTime().compareTo(today) < 0 ){
                                dueTimeCalendar.add(Calendar.DAY_OF_WEEK,7);
                            }

                            dueDate=dueTimeCalendar.getTime();
                            dueTimeCalendar.add(Calendar.DAY_OF_YEAR,-remindMeDays);
                            remindDate=dueTimeCalendar.getTime();

                            Log.e("remindDate",new SimpleDateFormat("ddMMMyyyy").format(remindDate.getTime()));

                            if(remindDate.compareTo(today) < 0){
                                displayAlert(AppMessages.REMINDER_DATE_BEFORE);
                                return;
                            }

                            break;

                        case 1:
                            dueTimeCalendar.set(Calendar.DAY_OF_MONTH,iSelectedDate);

                            while( dueTimeCalendar.getTime().compareTo(today) < 0 ){
                                dueTimeCalendar.add(Calendar.MONTH,1);
                            }
                            dueDate=dueTimeCalendar.getTime();
                            dueTimeCalendar.add(Calendar.DAY_OF_YEAR,-remindMeDays);
                            remindDate=dueTimeCalendar.getTime();

                            if(remindDate.compareTo(today) < 0){
                                displayAlert(AppMessages.REMINDER_DATE_BEFORE);
                                return;
                            }

                            break;

                        case 2:
                        case 3:
                        case 4:
                            dueTimeCalendar.set(Calendar.DAY_OF_MONTH,iSelectedDate);
                            dueTimeCalendar.set(Calendar.MONTH,iSelectedMonth);

                            while( dueTimeCalendar.getTime().compareTo(today) < 0 ){

                                switch (iReminderMode){

                                    case 2: dueTimeCalendar.add(Calendar.MONTH,3);break;
                                    case 3: dueTimeCalendar.add(Calendar.MONTH,6);break;
                                    case 4: dueTimeCalendar.add(Calendar.YEAR,1);break;
                                }
                            }

                            dueDate=dueTimeCalendar.getTime();
                            dueTimeCalendar.add(Calendar.DAY_OF_YEAR,-remindMeDays);
                            remindDate=dueTimeCalendar.getTime();

                            if(remindDate.compareTo(today) <= 0){
                                displayAlert(AppMessages.REMINDER_DATE_BEFORE);
                                return;
                            }

                            break;
                    }

                    Log.e("due_date",new SimpleDateFormat("ddMMMyyyy").format(dueDate.getTime()));
                }

                if(Commons.isNetworkAvailable(mContext)){

                    UserBillingInsertUpdate oUserBillingInsertUpdate=new UserBillingInsertUpdate();
                    oUserBillingInsertUpdate.setBill_due_day(iReminderMode==0?"-1":iSelectedDate+"");
                    oUserBillingInsertUpdate.setBill_due_month((iReminderMode==0||iReminderMode==1?"-1":iSelectedMonth+""));
                    oUserBillingInsertUpdate.setBill_due_day_of_week(iReminderMode==0?iSelectedDate+"":"-1");
                    oUserBillingInsertUpdate.setIsRepeat(shallRepeat?"1":"0");
                    oUserBillingInsertUpdate.setReminderBefore(remindMeDays+"");
                    oUserBillingInsertUpdate.setBillType(iReminderMode+"");
                    oUserBillingInsertUpdate.setBillAmount(strAmountDue);
                    oUserBillingInsertUpdate.setBillName(strBill);
                    oUserBillingInsertUpdate.setCategoryName(strCategory);
                    oUserBillingInsertUpdate.setBillId("0");
                    oUserBillingInsertUpdate.setIsNotiEnabled(isSwitchEnabled?"1":"0");
                    oUserBillingInsertUpdate.setPuid(strPUID);

                    String jsonString=oGson.toJson(oUserBillingInsertUpdate,UserBillingInsertUpdate.class).toString();

                    mProgressDialog.show();

                    //Post oUserBillingInsertUpdateApi Api
                    PostApiClient oUserBillingInsertUpdateApi=new PostApiClient(mOnResultReceived);
                    oUserBillingInsertUpdateApi.setRequestSource(RequestSource.UserBillingInsertUpdate);
                    oUserBillingInsertUpdateApi.executePostRequest(API.pUserBillingInsertUpdate(),jsonString);

                    Log.e("API",API.pUserBillingInsertUpdate());
                    Log.e("jsonString",jsonString);

                }else{
                    UserBillingInsertUpdateCache oUserBillingInsertUpdateCache=new UserBillingInsertUpdateCache();
                    oUserBillingInsertUpdateCache.setBill_due_day(iReminderMode==0?"-1":iSelectedDate+"");
                    oUserBillingInsertUpdateCache.setBill_due_month((iReminderMode==0||iReminderMode==1?"-1":iSelectedMonth+""));
                    oUserBillingInsertUpdateCache.setBill_due_day_of_week(iReminderMode==0?iSelectedDate+"":"-1");
                    oUserBillingInsertUpdateCache.setIsRepeat(shallRepeat?"1":"0");
                    oUserBillingInsertUpdateCache.setReminderBefore(remindMeDays+"");
                    oUserBillingInsertUpdateCache.setBillType(iReminderMode+"");
                    oUserBillingInsertUpdateCache.setBillAmount(strAmountDue);
                    oUserBillingInsertUpdateCache.setBillName(strBill);
                    oUserBillingInsertUpdateCache.setCategoryName(strCategory);
                    oUserBillingInsertUpdateCache.setBillId("0");
                    oUserBillingInsertUpdateCache.setIsNotiEnabled(isSwitchEnabled?"1":"0");
                    oUserBillingInsertUpdateCache.setPuid(strPUID);

                    oUserBillingInsertUpdateCache.setIsINSDeleted("0");
                    oUserBillingInsertUpdateCache.setIsINSUpdated("0");
                    oUserBillingInsertUpdateCache.setIsINSUploaded("0");
                    oUserBillingInsertUpdateCache.setTimeStamp(System.currentTimeMillis());

                    realm.beginTransaction();
                    realm.copyToRealm(oUserBillingInsertUpdateCache);
                    realm.commitTransaction();

                    showToast("Saved");
                    onBackPressed();
                }

                break;
        }
    }

    @Override
    public void dispatchString(RequestSource from, final String what) {

        mProgressDialog.dismiss();
        Log.e("what",what);
        showToast(what);

        switch(what){

            case "-2":
                displayAlert(getResources().getString(R.string.error_code_minus_two));
                break;
            case "-3" :
                displayAlert(getResources().getString(R.string.error_code_minus_three));
                break;

            case "0":
                displayAlert(getResources().getString(R.string.error_code_zero_bills));
                break;
            case "-1":
                displayAlert(getResources().getString(R.string.error_code_minus_one));
                break;
            default:

                AddOtherBill.this.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {

                        UserBillingInsertUpdateCache oUserBillingInsertUpdateCache=new UserBillingInsertUpdateCache();
                        oUserBillingInsertUpdateCache.setBill_due_day(iReminderMode==0?"-1":iSelectedDate+"");
                        oUserBillingInsertUpdateCache.setBill_due_month((iReminderMode==0||iReminderMode==1?"-1":iSelectedMonth+""));
                        oUserBillingInsertUpdateCache.setBill_due_day_of_week(iReminderMode==0?iSelectedDate+"":"-1");
                        oUserBillingInsertUpdateCache.setIsRepeat(shallRepeat?"1":"0");
                        oUserBillingInsertUpdateCache.setReminderBefore(remindMeDays+"");
                        oUserBillingInsertUpdateCache.setBillType(iReminderMode+"");
                        oUserBillingInsertUpdateCache.setBillAmount(strAmountDue);
                        oUserBillingInsertUpdateCache.setBillName(strBill);
                        oUserBillingInsertUpdateCache.setCategoryName(strCategory);
                        oUserBillingInsertUpdateCache.setBillId(what);
                        oUserBillingInsertUpdateCache.setIsNotiEnabled(isSwitchEnabled?"1":"0");
                        oUserBillingInsertUpdateCache.setPuid(strPUID);

                        oUserBillingInsertUpdateCache.setIsINSDeleted("0");
                        oUserBillingInsertUpdateCache.setIsINSUpdated("0");
                        oUserBillingInsertUpdateCache.setIsINSUploaded("1");
                        oUserBillingInsertUpdateCache.setTimeStamp(System.currentTimeMillis());

                        realm.beginTransaction();
                        realm.copyToRealm(oUserBillingInsertUpdateCache);
                        realm.commitTransaction();

                        showToast("Saved");
                        onBackPressed();
                    }
                });

                break;
        }

    }

    public class CustomSpinnerAdapter extends ArrayAdapter<String> {

        Context _context;
        String[] _objects;
        int _id;

        public CustomSpinnerAdapter(Context context, int id, int textViewResourceId,
                                    String[] objects) {
            super(context, textViewResourceId, objects);
            this._context=context;
            this._objects=objects;
            this._id=id;

        }

        @Override
        public View getDropDownView(int position, View convertView, ViewGroup parent) {

            return getCustomView(position, convertView, parent);
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent) {

            return getCustomView(position, convertView, parent);
        }

        public View getCustomView(final int position, View convertView, ViewGroup parent) {

            View mContentView= LayoutInflater.from(_context).inflate(R.layout.list_item_textview, parent, false);

            TextView tvLabel=(TextView)mContentView.findViewById(R.id.tv_id);
            tvLabel.setText(_objects[position]);

            mContentView.setOnClickListener(new View.OnClickListener() {

                @Override
                public void onClick(View view) {

                    switch (_id){

                        case 1:

                            etCategory.setText(_objects[position]);

                            if(position==_objects.length-1) {
                                strCategory="";
                                hasOtherCategory=true;
                                addCategoryView.setVisibility(View.VISIBLE);
                            }
                            else {
                                hasOtherCategory=false;
                                strCategory=_objects[position];
                                addCategoryView.setVisibility(View.GONE);
                            }

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mCategorySelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;

                        case 2:

                            iReminderMode=position;
                            etReminderMode.setText(_objects[position]);

                            switch(position){

                                case 3:
                                case 4:
                                case 2:
                                    monthView.setVisibility(View.VISIBLE);

                                    mDateSelector.setFloatingLabelText(getResources().getString(R.string.due_date));
                                    mDateSelector.setHint(getResources().getString(R.string.due_date));

                                    etDueDate.setFloatingLabelText(getResources().getString(R.string.due_date));
                                    etDueDate.setHint(getResources().getString(R.string.due_date));

                                    iSelectedDate=-1;
                                    iSelectedMonth=-1;
                                    etDueDate.setText("");
                                    etDueMonth.setText("");

                                    break;

                                case 0:

                                    mDateSelector.setFloatingLabelText(getResources().getString(R.string.due_day));
                                    mDateSelector.setHint(getResources().getString(R.string.due_day));

                                    etDueDate.setFloatingLabelText(getResources().getString(R.string.due_day));
                                    etDueDate.setHint(getResources().getString(R.string.due_day));

                                    iSelectedMonth=Calendar.getInstance().get(Calendar.MONTH);
                                    iSelectedDate=-1;

                                    mDateSelector.setAdapter(new CustomSpinnerAdapter(mContext, 4, R.layout.list_item_textview,Data.strArrayDayNames));
                                    etDueDate.setText("");
                                    etDueMonth.setText("");
                                    monthView.setVisibility(View.GONE);
                                    break;

                                case 1:

                                    mDateSelector.setFloatingLabelText(getResources().getString(R.string.due_date));
                                    mDateSelector.setHint(getResources().getString(R.string.due_date));

                                    etDueDate.setFloatingLabelText(getResources().getString(R.string.due_date));
                                    etDueDate.setHint(getResources().getString(R.string.due_date));

                                    iSelectedMonth=Calendar.getInstance().get(Calendar.MONTH);
                                    Calendar _Calendar=Calendar.getInstance();
                                    _Calendar.set(Calendar.MONTH,iSelectedMonth);
                                    int daysInMonth = _Calendar.getActualMaximum(Calendar.DAY_OF_MONTH);
                                    iSelectedDate=-1;
                                    ArrayList<String> _alDates=new ArrayList<String>();

                                    for(int index=1;index<=daysInMonth;index++){
                                        _alDates.add(index+"");
                                    }

                                    Log.e("dates",daysInMonth+"");
                                    mDateSelector.setAdapter(new CustomSpinnerAdapter(mContext, 4, R.layout.list_item_textview,(Arrays.copyOf(_alDates.toArray(), _alDates.toArray().length, String[].class))));
                                    etDueDate.setText("");
                                    etDueMonth.setText("");
                                    monthView.setVisibility(View.GONE);
                                    break;
                            }

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mReminderSelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;

                        case 3:

                            iSelectedMonth=position;

                            Calendar mCalendar=Calendar.getInstance();
                            mCalendar.set(Calendar.MONTH,iSelectedMonth);
                            int daysInMonth = mCalendar.getActualMaximum(Calendar.DAY_OF_MONTH);

                            ArrayList<String> alDates=new ArrayList<String>();

                            for(int index=1;index<=daysInMonth;index++){
                                alDates.add(index+"");
                            }

                            Log.e("dates",daysInMonth+"");
                            mDateSelector.setAdapter(new CustomSpinnerAdapter(mContext, 4, R.layout.list_item_textview,(Arrays.copyOf(alDates.toArray(), alDates.toArray().length, String[].class))));

                            etDueMonth.setText(_objects[position]);
                            etDueDate.setText("");
                            iSelectedDate=-1;

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mMonthSelector);
                            } catch (Exception e) {e.printStackTrace();}

                            break;

                        case 4:

                            iSelectedDate=iReminderMode==0?position:Integer.parseInt(_objects[position]);

                            etDueDate.setText(_objects[position]);

                            try {
                                Method method = Spinner.class.getDeclaredMethod("onDetachedFromWindow");
                                method.setAccessible(true);
                                method.invoke(mDateSelector);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }

                            break;
                    }
                }
            });

            return mContentView;
        }
    }
}
