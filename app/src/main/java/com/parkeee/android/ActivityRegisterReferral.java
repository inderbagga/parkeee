package com.parkeee.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.InsertReferralRegister;
import com.parkeee.android.processor.PostApiClient;
import android.view.KeyEvent;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

/**
 * Created by myoffice on 11/26/2016.
 */

public class ActivityRegisterReferral extends Activity implements View.OnClickListener, OnResultReceived {

    ImageView img_scan_referral_code;
    Button btnRegister_Referral_Continue;
    Spinner sp_reftype;
    EditText et_referral_code;
    Gson oGson;
    ProgressDialog mProgressDialog;
    OnResultReceived mOnResultReceived;
    Context mContext;
    String ServerResult = "";
    String puid;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_register_referral);
        Initialize();
    }

    private void Initialize() {
        final SharedPreferences prefs = getSharedPreferences("RegisterResponse",
                MODE_PRIVATE);
        puid= prefs.getString("puid", "");

        mContext = ActivityRegisterReferral.this;
        oGson = new Gson();
        mOnResultReceived = this;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        img_scan_referral_code = (ImageView) findViewById(R.id.img_scan_referral_code);
        img_scan_referral_code.setOnClickListener(this);

        btnRegister_Referral_Continue = (Button) findViewById(R.id.btnRegister_Referral_Continue);
        btnRegister_Referral_Continue.setOnClickListener(this);

        et_referral_code = (EditText) findViewById(R.id.et_referral_code);

        sp_reftype = (Spinner) findViewById(R.id.sp_reftype);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner_item, getResources().getStringArray(R.array.arrRefType));
        sp_reftype.setAdapter(adapter);
    }


    @Override
    public void onClick(View view) {
        switch (view.getId()) {

            case R.id.img_scan_referral_code:
                IntentIntegrator integrator = new IntentIntegrator(this);
                integrator.setDesiredBarcodeFormats(IntentIntegrator.ONE_D_CODE_TYPES);
                integrator.setPrompt("Scan a barcode");
                integrator.setResultDisplayDuration(0);
                integrator.setWide();  // Wide scanning rectangle, may work better for 1D barcodes
                integrator.setCameraId(0);  // Use a specific camera of the device
                integrator.initiateScan();
                break;

            case R.id.btnRegister_Referral_Continue:

                if(et_referral_code.getText().toString().trim().equals("")){
                    startActivity(new Intent(ActivityRegisterReferral.this, ActivityVerification.class));
                }else{
                    String Ref_type = "";
                    if (sp_reftype.getSelectedItem().equals("Friend")) {
                        Ref_type = "fr";
                    } else {
                        Ref_type = "gs";
                    }
                    InsertReferralRegister oInsertReferralRegister = new InsertReferralRegister();
                    if(!et_referral_code.getText().toString().trim().equals("")){
                        oInsertReferralRegister.setRef_code(et_referral_code.getText().toString().trim());
                    }else{
                        oInsertReferralRegister.setRef_code("");
                    }
                    oInsertReferralRegister.setRef_type(Ref_type);
                    oInsertReferralRegister.setPuid(puid);

                    String jsonString = oGson.toJson(oInsertReferralRegister, InsertReferralRegister.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.InsertReferralRegister);
                    oInsertUpdateApi.executePostRequest(API.fInsertUpdateRegisterReferralPUID(), jsonString);
                }

                break;
            default:
                break;

        }
    }

    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        //retrieve scan result
        IntentResult scanningResult = IntentIntegrator.parseActivityResult(requestCode, resultCode, intent);

        if (scanningResult != null) {
            //we have a result
            String scanContent = scanningResult.getContents();
            String scanFormat = scanningResult.getFormatName();

            // display it on screen
            /*formatTxt.setText("FORMAT: " + scanFormat);
            contentTxt.setText("CONTENT: " + scanContent);*/

            if (scanContent != null) {
                Toast.makeText(getApplicationContext(), scanContent, Toast.LENGTH_LONG).show();
            }
        } else {
            Toast toast = Toast.makeText(getApplicationContext(), "No scan data received!", Toast.LENGTH_SHORT);
            toast.show();
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void dispatchString(RequestSource from, String what) {
        mProgressDialog.dismiss();
        ServerResult = what.replace("\"", "");
        switch (ServerResult) {
            case "-2":
            case "-3":
                displayAlert("app error", false);
                break;
            case "0":
            case "-1":
                displayAlert("Sorry! The registration process failed due to some technical error. Please try after some time.", false);
                break;
            default:
                startActivity(new Intent(ActivityRegisterReferral.this, ActivityVerification.class));
                break;
        }
    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityRegisterReferral.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }
}