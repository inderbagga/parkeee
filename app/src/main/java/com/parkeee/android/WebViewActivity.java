package com.parkeee.android;
import android.app.Activity;
import android.app.ProgressDialog;
import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.webkit.WebChromeClient;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.ImageView;
import android.widget.TextView;

import cn.pedant.SweetAlert.SweetAlertDialog;

public class WebViewActivity extends Activity implements View.OnClickListener {
    WebView webView;
    ProgressDialog pd;
    TextView txt_ContentHeading_webview;
    ImageView img_back_webview;
   // SweetAlertDialog pDialog;
   ProgressDialog mProgressDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_webview);

        Initialize();
    }

    private void Initialize() {
        txt_ContentHeading_webview=(TextView)findViewById(R.id.txt_ContentHeading_webview);
        img_back_webview=(ImageView)findViewById(R.id.img_back_webview);
        img_back_webview.setOnClickListener(this);
        pd= new ProgressDialog(this);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");
        mProgressDialog.show();
       /* pDialog = new SweetAlertDialog(this, SweetAlertDialog.PROGRESS_TYPE);
        pDialog.getProgressHelper().setBarColor(Color.parseColor("#7CC419"));
        pDialog.getProgressHelper().setRimColor(Color.parseColor("#404040"));
        pDialog.setTitleText("");
        pDialog.setCancelable(false);
        pDialog.show();*/

        String url = getIntent().getExtras().getString("url");
        String title = getIntent().getExtras().getString("title");
        //txt_ContentHeading_webview.setText(title);

        webView = (WebView) findViewById(R.id.webView);

        webView.loadUrl(url);
        webView.setWebChromeClient(new WebChromeClient());
        webView.clearCache(true);

        webView.setWebViewClient(new WebViewClient() {
            public boolean shouldOverrideUrlLoading(WebView view, String url) {
                System.out.println(url);
                view.loadUrl(url);
                return true;
            }
            public void onPageFinished(WebView view, String url) {
                mProgressDialog.cancel();
                //pd.cancel();
            }
            public void onReceivedError(WebView view, int errorCode, String description, String failingUrl) {
               // pd.cancel();
                mProgressDialog.cancel();
            }
        });

        WebSettings webSetting = webView.getSettings();
        webSetting.setJavaScriptEnabled(true);
        webSetting.setLoadWithOverviewMode(true);
        webSetting.setUseWideViewPort(true);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.img_back_webview:
                this.finish();
                break;
            default:
                break;
        }
    }
}