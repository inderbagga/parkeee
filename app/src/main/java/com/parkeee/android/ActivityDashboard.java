package com.parkeee.android;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.v4.app.FragmentActivity;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.parkeee.android.backend.RealmController;
import com.parkeee.android.eservices.ServiceDashboard;
import com.parkeee.android.gas.ActivitySearchGasStation;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.slidingmenu.lib.SlidingMenu;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;


/**
 * Created by user on 28-11-2016.
 */

public class ActivityDashboard extends FragmentActivity implements View.OnClickListener {

    public static SlidingMenu menu;
    ImageView img_menu_dashboard, img_parkeee_org, img_alsets, img_swatico;
    LinearLayout ll_gas_station;
    private Realm realm;
    InsertUpdateRegister oinsertupdateregister;
    LinearLayout ll_Eservices;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_dashboard);
        Initialize();
    }

    private void Initialize() {
        //get realm instance
        this.realm = RealmController.with(this).getRealm();
        oinsertupdateregister = RealmController.with(this).getRegisterInformation();

        ll_Eservices=(LinearLayout)findViewById(R.id.ll_Eservices);
        ll_Eservices.setOnClickListener(this);

        img_parkeee_org = (ImageView) findViewById(R.id.img_parkeee_org);
        img_parkeee_org.setOnClickListener(this);
        img_alsets = (ImageView) findViewById(R.id.img_alsets);
        img_alsets.setOnClickListener(this);
        img_swatico = (ImageView) findViewById(R.id.img_swatico);
        img_swatico.setOnClickListener(this);

        img_menu_dashboard = (ImageView) findViewById(R.id.img_menu_dashboard);
        img_menu_dashboard.setOnClickListener(this);

        ll_gas_station=(LinearLayout)findViewById(R.id.ll_gas_station);
        ll_gas_station.setOnClickListener(this);

        MenuFragment.SameActivity = "Home";
        menu = new SlidingMenu(this);
        menu.setMode(SlidingMenu.LEFT);
        menu.setTouchModeAbove(SlidingMenu.TOUCHMODE_FULLSCREEN);
        menu.setShadowWidthRes(R.dimen.shadow_width);
        menu.setShadowDrawable(R.drawable.shadow);
        menu.setBehindOffsetRes(R.dimen.slidingmenu_offset);
        menu.setFadeDegree(0.35f);
        menu.attachToActivity(this, SlidingMenu.SLIDING_CONTENT);
        menu.setMenu(R.layout.menu);
        menu.setSlidingEnabled(true);

    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_menu_dashboard:
                menu.toggle();
                break;

            case R.id.ll_Eservices:

                startActivity(new Intent(ActivityDashboard.this, ServiceDashboard.class));

                break;

            case R.id.img_parkeee_org:
                Intent IASP = (new Intent(ActivityDashboard.this, ActivityStaticPages.class));
                IASP.putExtra("PageName", "Parkeee.org");
                IASP.putExtra("is_sidemenu", false);
                startActivity(IASP);
                break;

            case R.id.img_alsets:
                Intent IASP2 = (new Intent(ActivityDashboard.this, ActivityStaticPages.class));
                IASP2.putExtra("PageName", "Alsets");
                IASP2.putExtra("is_sidemenu", false);
                startActivity(IASP2);
                break;

            case R.id.img_swatico:
                Intent IASP3 = (new Intent(ActivityDashboard.this, ActivityStaticPages.class));
                IASP3.putExtra("PageName", "Swatico");
                IASP3.putExtra("is_sidemenu", false);
                startActivity(IASP3);
                break;

            case R.id.ll_gas_station:

                SharedPreferences.Editor prefs = getSharedPreferences(
                        "GSSearchedLOcation", MODE_PRIVATE).edit();
                prefs.putString("isAlreadySearchedForLoc", "0");
                prefs.putString("location_address", "");
                prefs.putString("location_lat", "");
                prefs.putString("location_lng", "");
                prefs.commit();
                /*SharedPreferences settings = getSharedPreferences("PreferencesName", Context.MODE_PRIVATE);
                settings.edit().remove("KeyName").commit();*/
                startActivity(new Intent(ActivityDashboard.this, ActivitySearchGasStation.class));
                break;

            default:
                break;
        }
    }

    @Override
    public void onBackPressed() {

        new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Alert")
                .setContentText("Are you sure you want to Exit?")
                .setCancelText("No")
                .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                    }
                })
                .setConfirmText("Yes")
                .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                    @Override
                    public void onClick(SweetAlertDialog sweetAlertDialog) {
                        sweetAlertDialog.cancel();
                       /* realm.beginTransaction();
                        oinsertupdateregister.setLogOut("1");
                        realm.commitTransaction();*/
                        finishAffinity();
                        //startActivity(new Intent(ActivityDashboard.this,SplashActivity.class));
                    }
                })
        .show();
    }
}
