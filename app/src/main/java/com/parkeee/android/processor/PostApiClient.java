package com.parkeee.android.processor;

import android.util.Log;

import com.google.gson.Gson;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RequestSource;

import java.io.IOException;

import okhttp3.Call;
import okhttp3.Callback;
import okhttp3.MediaType;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.RequestBody;
import okhttp3.Response;

/**
 * Created by inderbagga on 26/09/16.
 */
public class PostApiClient {

    Gson oGson = new Gson();
    OnResultReceived mOnResultReceived;
    RequestSource from;
    OkHttpClient mOkHttpClient = new OkHttpClient();;

    public PostApiClient(OnResultReceived mOnResultReceived) {

        this.mOnResultReceived=mOnResultReceived;
    }

    public void setRequestSource(RequestSource from) {
        this.from = from;
    }

    public static final MediaType JSON = MediaType.parse("application/json; charset=utf-8");

    public void  executePostRequest(String strApiUrl,String jsonString){

        RequestBody body = RequestBody.create(JSON, jsonString);

        Request request = new Request.Builder()
                .header("Accept", "application/json")
                .header("Content-Type", "application/json")
                .url(strApiUrl)
                .post(body)
                .build();

        mOkHttpClient.newCall(request).enqueue(new Callback() {

            @Override
            public void onFailure(final Call call, IOException e) {

                mOnResultReceived.dispatchString(from,printException("-2"));
            }

            @Override
            public void onResponse(Call call, final Response response) {

                try{
                    mOnResultReceived.dispatchString(from,response.body().string());
                }
                catch (IOException ioe){Log.e("result ", ioe.getMessage());mOnResultReceived.dispatchString(from,printException("-2"));}
                catch (Exception e){Log.e("result ",e.getMessage());mOnResultReceived.dispatchString(from,printException("-3"));}
            }
        });
    }

    private String printException(String message) {

       return message;//oGson.toJson(new HandleException(Text.getExceptionMessage(message)));
    }
}