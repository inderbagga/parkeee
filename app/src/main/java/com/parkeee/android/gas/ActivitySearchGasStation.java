package com.parkeee.android.gas;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.location.Address;
import android.location.Geocoder;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import android.text.Editable;
import android.text.TextUtils;
import android.text.TextWatcher;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.AutoCompleteTextView;
import android.widget.BaseAdapter;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GoogleApiAvailability;
import com.google.android.gms.common.GooglePlayServicesNotAvailableException;
import com.google.android.gms.common.GooglePlayServicesRepairableException;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.common.api.Status;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.places.Place;
import com.google.android.gms.location.places.ui.PlaceAutocomplete;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptor;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.gson.JsonArray;
import com.parkeee.android.ActivityDashboard;
import com.parkeee.android.ActivityVerification;
import com.parkeee.android.R;
import com.parkeee.android.adapter.FavGasStationAdapter;
import com.parkeee.android.adapter.PlacesAutoCompleteAdapter;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.GasStationResults;
import com.parkeee.android.model.params.SaveInbox;
import com.parkeee.android.model.tables.GSFavouriteTb;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.MapIconGenerator;
import com.parkeee.android.utils.SphericalUtil;
import com.parkeee.android.utils.Text;
import com.parkeee.android.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Locale;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.RealmResults;

import static android.R.id.progress;

/**
 * Created by user on 08-12-2016.
 */

public class ActivitySearchGasStation extends Activity implements OnMapReadyCallback, GoogleMap.OnMapLoadedCallback
        , GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, View.OnClickListener, OnResultReceived,
        AdapterView.OnItemClickListener {

    Context ctx;
    MapFragment mapFragment;
    double longitude;
    double latitude;
    boolean isGPSEnabled = false;
    boolean isNetworkEnabled = false;

    ProgressDialog mProgressDialog;
    MapIconGenerator objMIG;

    BitmapDescriptor bg_grey;
    Bitmap bm_grey;
    ImageView img_filter, img_listview;
    AlertDialog.Builder ExitAlertDialog;
    // LogCat tag
    private static final String TAG = ActivitySearchGasStation.class.getSimpleName();
    private final static int PLAY_SERVICES_RESOLUTION_REQUEST = 1000;
    private Location mLastLocation;
    // Google client to interact with Google API
    private GoogleApiClient mGoogleApiClient;
    // boolean flag to toggle periodic location updates
    private boolean mRequestingLocationUpdates = false;
    private LocationRequest mLocationRequest;
    // Location updates intervals in sec
    private static int UPDATE_INTERVAL = 10000; // 10 sec
    private static int FATEST_INTERVAL = 5000; // 5 sec
    private static int DISPLACEMENT = 10; // 10 meters

    ImageView img_back, img_cancel_searching, img_get_current_location,img_gs_listing;

    /*EditText et_serach_gas_station;*/
    TextView txt_serach_gas_station;

    String DisplayAddress = "";

    //ListView lv_search_location;

    OnResultReceived mOnResultReceived;

    String Address_lat, Address_lng;

    public double location_lat, location_lng;
    double radius = 5.0;
    private int fuelType = 1;
    private int day;
    ArrayList<String> mAmeneties = new ArrayList<String>();
    ArrayList<String> mBrands = new ArrayList<String>();
    int mRegistered = 1;
    int mUnRegistered = 0;
    boolean isSortByPrice = true;

    String ServerResult;

    LinearLayout ll_list_gs_search;
    FrameLayout  fl_map_gs_search;

    Context mContext;

    InsertUpdateRegister oinsertupdateregister;
    String puid;

    ArrayList<GasStationResults> gasStationResultsArr = new ArrayList<GasStationResults>();

    private ArrayList<Marker> mMarkerArray = new ArrayList<Marker>();

    AutoCompleteTextView autoCompleteLocation;

    Intent intnt;

    ListView lv_gs_listing;

    GasStationResults gsr_unregistered;


    LinearLayout lnrGasDetailNReg;
    TextView txtName_gs,txtAddress_gs;
    ImageView img_unregistered_gas_station,img_Directions,img_close_unregistred_detailview;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_gas_station);

        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        ctx = ActivitySearchGasStation.this;
        mOnResultReceived = this;
        oinsertupdateregister = RealmController.with(this).getRegisterInformation();
        puid = oinsertupdateregister.getPuid();

        intnt = getIntent();
        if (intnt != null) {
            radius = intnt.getDoubleExtra("SearchRadius", 5);
        }

        // check availability of play services
        if (checkPlayServices()) {
            // Building the GoogleApi client
            buildGoogleApiClient();
        }

        InitializeInterface();

    }

    private boolean checkPlayServices() {
        GoogleApiAvailability googleAPI = GoogleApiAvailability.getInstance();
        int resultCode = googleAPI.isGooglePlayServicesAvailable(this);

        if (resultCode != ConnectionResult.SUCCESS) {
            if (googleAPI.isUserResolvableError(resultCode)) {
                googleAPI.getErrorDialog(this, resultCode,
                        PLAY_SERVICES_RESOLUTION_REQUEST).show();
            } else {
                Toast.makeText(getApplicationContext(),
                        "This device is not supported.", Toast.LENGTH_LONG)
                        .show();
                finish();
            }
            return false;
        }
        return true;
    }

    private void displayLocation() {

        final SharedPreferences prefs = getSharedPreferences("GSSearchedLOcation",
                MODE_PRIVATE);

        String isAlreadySearchedForLoc = prefs.getString("isAlreadySearchedForLoc", "");

        if (isAlreadySearchedForLoc == "1"){

            latitude = Double.parseDouble(prefs.getString("location_lat", ""));
            longitude = Double.parseDouble(prefs.getString("location_lng", ""));

            location_lat = Double.parseDouble(prefs.getString("location_lat", ""));
            location_lng = Double.parseDouble(prefs.getString("location_lng", ""));

            txt_serach_gas_station.setText(prefs.getString("location_address", ""));

            mProgressDialog.show();
            FetchDataFromServer();


        }else{
            mLastLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);

            if (mLastLocation != null) {
                latitude = mLastLocation.getLatitude();
                longitude = mLastLocation.getLongitude();
                (new GetAddressTask(this)).execute(mLastLocation);

            } else {
                ExitAlertDialog = new AlertDialog.Builder(ActivitySearchGasStation.this);
                ExitAlertDialog.setTitle("Gps!");
                ExitAlertDialog.setMessage("Please enabled your Gps network for tag locations!");
                ExitAlertDialog.setNegativeButton("Open location settings", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        Intent myIntent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                        ctx.startActivity(myIntent);
                        startActivityForResult(myIntent, 5469);
                        dialog.cancel();
                    }
                });
                ExitAlertDialog.setPositiveButton("Cancel", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        dialog.cancel();
                    }
                });
                ExitAlertDialog.show();
            }
        }
    }

    /**
     * Creating google api client object
     */
    protected synchronized void buildGoogleApiClient() {
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API).build();
    }

    private void InitializeInterface() {
        lnrGasDetailNReg=(LinearLayout)findViewById(R.id.lnrGasDetailNReg);
        txtName_gs=(TextView)findViewById(R.id.txtName_gs);
        txtAddress_gs=(TextView)findViewById(R.id.txtAddress_gs);

        img_unregistered_gas_station=(ImageView)findViewById(R.id.img_unregistered_gas_station);
        img_Directions=(ImageView)findViewById(R.id.img_Directions);
        img_close_unregistred_detailview=(ImageView)findViewById(R.id.img_close_unregistred_detailview);

        img_Directions.setOnClickListener(this);
        img_close_unregistred_detailview.setOnClickListener(this);


        Picasso.with(img_unregistered_gas_station.getContext())
                .load(R.drawable.icon_un_gs_default_cover)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .transform(new RoundedTransformation(100, 0))
                .resize(96, 96).centerCrop().into(img_unregistered_gas_station, new Callback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError() {
            }
        });



        autoCompleteLocation = (AutoCompleteTextView) findViewById(R.id.autoCompleteLocation);
        autoCompleteLocation.setTypeface(Typeface.createFromAsset(getAssets(), "fonts/light.ttf"));
        autoCompleteLocation.setOnItemClickListener(this);

        day = Utils.getCurrentDay();

        txt_serach_gas_station = (TextView) findViewById(R.id.txt_serach_gas_station);
        txt_serach_gas_station.setOnClickListener(this);

        mContext = this;

        lv_gs_listing=(ListView)findViewById(R.id.lv_gs_listing);


        img_gs_listing=(ImageView)findViewById(R.id.img_gs_listing);
        img_gs_listing.setOnClickListener(this);

        ll_list_gs_search=(LinearLayout)findViewById(R.id.ll_list_gs_search);
        fl_map_gs_search=(FrameLayout)findViewById(R.id.fl_map_gs_search);

        //lv_search_location=(ListView)findViewById(R.id.lv_search_location);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        img_back = (ImageView) findViewById(R.id.img_back_search_gas_station);
        img_back.setOnClickListener(this);

        img_filter = (ImageView) findViewById(R.id.img_filter);
        img_filter.setOnClickListener(this);

        img_cancel_searching = (ImageView) findViewById(R.id.img_cancel_searching);
        img_cancel_searching.setOnClickListener(this);

        img_get_current_location = (ImageView) findViewById(R.id.img_get_current_location);
        img_get_current_location.setOnClickListener(this);

        objMIG = new MapIconGenerator(ActivitySearchGasStation.this);
        bm_grey = objMIG.makeIcon(R.drawable.icon_grey, "N/A");
        bg_grey = BitmapDescriptorFactory.fromBitmap(bm_grey);

        /*et_serach_gas_station=(EditText)findViewById(R.id.et_serach_gas_station);
        et_serach_gas_station.setCursorVisible(false);*/
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        googleMap.clear();
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        mMarkerArray = new ArrayList<Marker>();
        for (int i = 0; i < gasStationResultsArr.size(); i++) {
            GasStationResults gsr = gasStationResultsArr.get(i);
            Marker m = googleMap.addMarker(new MarkerOptions().position(new LatLng(Double.parseDouble(gsr.getLatitude()), Double.parseDouble(gsr.getLongitude()))));
            mMarkerArray.add(m);
            String benefitType = gsr.getPackageName();
            String prc = "N/A";
            m.setSnippet(gsr.getGSId());
            if (benefitType.equalsIgnoreCase("Parkeee Business Platinum")) {
                if (gsr.getPricePerGallonInDoller() > 0) {
                    prc = gsr.getPricePerGallonInDoller().toString();
                }
                Bitmap bm = objMIG.makeIcon(R.drawable.icon_platinum, prc);
                m.setIcon(BitmapDescriptorFactory.fromBitmap(bm));
            } else if (benefitType.equalsIgnoreCase("Parkeee Business Gold")) {
                if (gsr.getPricePerGallonInDoller() > 0) {
                    prc = gsr.getPricePerGallonInDoller().toString();
                }
                Bitmap bm = objMIG.makeIcon(R.drawable.icon_gold, prc);
                m.setIcon(BitmapDescriptorFactory.fromBitmap(bm));
            } else {
                m.setIcon(bg_grey);
                m.setTag(i);
                m.setSnippet("Unregistered");
            }

          //  m.setTitle(gsr.getGSId());

            double oneMilesInMeter = 1609.34;
            LatLng lt = new LatLng(Double.parseDouble(gsr.getLatitude()), Double.parseDouble(gsr.getLongitude()));
            Double ds = radius * oneMilesInMeter;
            googleMap.setOnMarkerClickListener(new GoogleMap.OnMarkerClickListener() {
                @Override
                public boolean onMarkerClick(Marker marker) {
                    if(marker.getSnippet().equals("Unregistered")){
                        lnrGasDetailNReg.setVisibility(View.VISIBLE);
                        gsr_unregistered  = gasStationResultsArr.get(Integer.parseInt(marker.getTag().toString()));
                        txtName_gs.setText(gsr_unregistered.getBrandName().toString());
                        txtAddress_gs.setText(gsr_unregistered.getCity()+", "+gsr_unregistered.getStreet());

                    }else{
                        String Gs_id=marker.getSnippet();
                        Intent Intnt= new Intent(ActivitySearchGasStation.this, ActivityGasStationDetail.class);
                        Intnt.putExtra("selectedGSID",Gs_id);
                        Intnt.putExtra("FTID",String.valueOf(fuelType));
                        Intnt.putExtra("DAY",String.valueOf(Utils.getCurrentDay()));
                        Intnt.putExtra("puid",puid);
                        startActivity(Intnt);
                    }

                    return false;
                }
            });

           // googleMap.animateCamera(getZoomForDistance(lt, ds));
            googleMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location_lat, location_lng), 10.0f));
            //LatLngBounds Bounds = new LatLngBounds(new LatLng(Double.parseDouble(gsr.getLatitude()), new LatLng(Double.parseDouble(gsr.getLatitude());
            //googleMap.zoomToFitMarkers(mMarkerArray);
        }
       /* for (Marker marker : mMarkerArray) {
            builder.include(marker.getPosition());
        }
        LatLngBounds bounds = builder.build();
        int width = getResources().getDisplayMetrics().widthPixels;
        int height = getResources().getDisplayMetrics().heightPixels;
        int padding = (int) (width * 0.10); // offset from edges of the map 12% of screen
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, width, height, padding);
        googleMap.animateCamera(cu);*/
        mProgressDialog.hide();
    }

    @Override
    public void onMapLoaded() {

    }

    private CameraUpdate getZoomForDistance(LatLng originalPosition, double distance) {
        LatLng rightBottom = SphericalUtil.computeOffset(originalPosition, distance, 135);
        LatLng leftTop = SphericalUtil.computeOffset(originalPosition, distance, -45);
        LatLngBounds sBounds = new LatLngBounds(new LatLng(rightBottom.latitude, leftTop.longitude), new LatLng(leftTop.latitude, rightBottom.longitude));
        return CameraUpdateFactory.newLatLngBounds(sBounds,0);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == 5469) {
            displayLocation();

        }
    }

    @Override
    protected void onStart() {
        super.onStart();
        if (mGoogleApiClient != null) {
            mGoogleApiClient.connect();
        }
    }

    @Override
    protected void onResume() {
        super.onResume();
        checkPlayServices();
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        displayLocation();

    }

    @Override
    public void onConnectionSuspended(int i) {
        mGoogleApiClient.connect();
    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {

            case R.id.img_close_unregistred_detailview:
                lnrGasDetailNReg.setVisibility(View.GONE);
                break;

            case R.id.img_Directions:

                Intent intent = new Intent(Intent.ACTION_VIEW,
                        Uri.parse("http://maps.google.com/maps?saddr=" + location_lat + "," + location_lng + "&daddr=" +Double.parseDouble(gsr_unregistered.getLatitude()) + "," + Double.parseDouble(gsr_unregistered.getLongitude())));
                startActivity(intent);

                break;

            case R.id.img_back_search_gas_station:
                this.finish();
                break;

            case R.id.img_gs_listing:
                if(img_gs_listing.getTag().equals("0")){
                    ll_list_gs_search.setVisibility(View.VISIBLE);
                    fl_map_gs_search.setVisibility(View.GONE);
                    img_gs_listing.setImageResource(R.drawable.icon_map);
                    img_gs_listing.setTag("1");
                }else{
                    ll_list_gs_search.setVisibility(View.GONE);
                    fl_map_gs_search.setVisibility(View.VISIBLE);
                    img_gs_listing.setImageResource(R.drawable.icon_list);
                    img_gs_listing.setTag("0");
                }
                break;

            case R.id.img_filter:
                this.finish();
                Intent IASF = new Intent(ActivitySearchGasStation.this, ActivitySearchFilter.class);
                IASF.putExtra("SearchRadius", radius);
                IASF.putExtra("Ameneties_array", mAmeneties);
                IASF.putExtra("mBrands_array", mBrands);
                IASF.putExtra("isSortByPrice", isSortByPrice);
                IASF.putExtra("fuelType", fuelType);
                IASF.putExtra("mUnRegistered", mUnRegistered);
                startActivity(IASF);
                break;

            case R.id.img_cancel_searching:
                txt_serach_gas_station.setText("");
                autoCompleteLocation.setText("");
                //lv_search_location.setVisibility(View.GONE);
                break;

            case R.id.img_get_current_location:
                final SharedPreferences prefs = getSharedPreferences("CurrentAddress",
                        MODE_PRIVATE);
                String address_loc= prefs.getString("Address", "");
                if(!txt_serach_gas_station.getText().toString().trim().equals(address_loc)){
                    txt_serach_gas_station.setText(address_loc);
                    txt_serach_gas_station.requestFocus();
                    getLocationLatLong();
                }

                break;

            case R.id.txt_serach_gas_station:
                showAutoComplete(true);
                break;

            default:
                break;
        }
    }


    private void showAutoComplete(boolean show) {
        if (show) {
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    autoCompleteLocation.setVisibility(View.VISIBLE);
                    txt_serach_gas_station.setVisibility(View.GONE);
                    autoCompleteLocation.setText(txt_serach_gas_station.getText());
                    autoCompleteLocation.setAdapter(new PlacesAutoCompleteAdapter(
                            ActivitySearchGasStation.this, R.layout.list_item_autocomplete));
                    autoCompleteLocation.requestFocus();
                    InputMethodManager mgr = (InputMethodManager)
                            getSystemService(Context.INPUT_METHOD_SERVICE);
                    mgr.showSoftInput(autoCompleteLocation, 0);
                }
            });
        } else {
            autoCompleteLocation.setVisibility(View.GONE);
            txt_serach_gas_station.setVisibility(View.VISIBLE);
            autoCompleteLocation.setAdapter(null);
        }
    }


    @Override
    public void dispatchString(RequestSource from, String what) {
        final ArrayList<String> al_search_places = new ArrayList<String>();
        if (from.toString().equalsIgnoreCase("SearchGasStationPlaces")) {
            final String Result = what;
            switch (what) {
                case "-3":
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            try {
                                JSONObject Objct = new JSONObject(Result);
                                JSONArray OResult = Objct.getJSONArray("predictions");
                                for (int m = 0; m < OResult.length(); m++) {
                                    al_search_places.add(OResult.getJSONObject(m).getString("description"));
                                }
                                if (al_search_places.size() > 1) {
                                    //lv_search_location.setVisibility(View.VISIBLE);
                                }
                                // lv_search_location.setAdapter(new dataListAdapter(al_search_places));
                                String kns = "";
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    break;
            }
        } else if (from.toString().equalsIgnoreCase("LocationLatLong")) {
            try {
                JSONObject Objct = new JSONObject(what);
                JSONArray OResult = Objct.getJSONArray("results");
                JSONObject objc = OResult.getJSONObject(0).getJSONObject("geometry");
                JSONObject objc_loc = objc.getJSONObject("location");

                location_lat = Double.parseDouble(objc_loc.getString("lat"));
                location_lng = Double.parseDouble(objc_loc.getString("lng"));

                SharedPreferences.Editor prefs = getSharedPreferences(
                        "GSSearchedLOcation", MODE_PRIVATE).edit();
                prefs.putString("isAlreadySearchedForLoc", "1");
                prefs.putString("location_address", txt_serach_gas_station.getText().toString());
                prefs.putString("location_lat", String.valueOf(location_lat));
                prefs.putString("location_lng", String.valueOf(location_lng));

                prefs.commit();

                FetchDataFromServer();

            } catch (JSONException e) {
                e.printStackTrace();
            }
        } else if (from.toString().equalsIgnoreCase("GetGasStations")) {
            gasStationResultsArr.clear();
            ServerResult = what.replace("\"[", "[");
            ServerResult = ServerResult.replace("\"]", "]");
            ServerResult = ServerResult.replace("\\", "");

            try {
                JSONArray responseArr = new JSONArray(ServerResult);
                if (responseArr.length() > 0) {
                    for (int i = 0; i < responseArr.length(); i++) {
                        JSONObject obj = responseArr.getJSONObject(i);
                        GasStationResults gsr = new GasStationResults();
                        gsr.setGSId(obj.getString("GSId"));
                        gsr.setBrandName(obj.getString("BrandName"));
                        gsr.setLatitude(obj.getString("Latitude"));
                        gsr.setLongitude(obj.getString("Longitude"));
                        gsr.setStreet(obj.getString("Street"));
                        gsr.setPricePerGallonInDoller(obj.getDouble("PricePerGallonInDoller"));
                        gsr.setPackageName(obj.getString("PackageName"));
                        gsr.setLastUpdateOn(obj.getString("LastUpdateOn"));
                        gsr.setCoffeeDeal(obj.getString("CoffeeDeal"));
                        gsr.setZip(obj.getString("Zip"));
                        gsr.setCity(obj.getString("City"));

                        //.. Get Miles
                        String mDistance = String.valueOf(CalculationByDistance(location_lat, location_lng, Double.parseDouble(obj.getString("Latitude")), Double.parseDouble(obj.getString("Longitude"))));
                        double digit = Double.parseDouble(mDistance);
                        gsr.setDistance(round(Double.parseDouble(String.format("%.2f", digit)), 1));

                        gasStationResultsArr.add(gsr);
                    }
                    showMapData();

                   //lv_gs_listing.setAdapter(new GasStationAdapter(gasStationResultsArr));
                    GasStationAdapter gasStationAdapter = new GasStationAdapter(gasStationResultsArr);
                    lv_gs_listing.setAdapter(gasStationAdapter);

                    if (isSortByPrice) {
                        Collections.sort(gasStationResultsArr, new Comparator<GasStationResults>() {
                            public int compare(GasStationResults obj1, GasStationResults obj2) {
                                return obj1.getPricePerGallonInDoller().compareTo(obj2.getPricePerGallonInDoller());
                            }
                        });
                    } else {
                        Collections.sort(gasStationResultsArr, new Comparator<GasStationResults>() {
                            public int compare(GasStationResults obj1, GasStationResults obj2) {
                                return obj1.getDistance().compareTo(obj2.getDistance());
                            }
                        });
                    }
                    gasStationAdapter.notifyDataSetChanged();



                } else {
                    displayAlert("No gas station found.", false);
                    showMapData();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    public class GasStationAdapter extends BaseAdapter {

        ArrayList<GasStationResults> list;
        public GasStationAdapter( ArrayList<GasStationResults> List) {
            list=List;

        }
        @Override
        public int getCount() {
            return list.size();
        }

        @Override
        public Object getItem(int i) {
            return null;
        }

        @Override
        public long getItemId(int i) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup viewGroup) {

            TextView txt_fav_gs_name ,txt_fav_gs_address, txt_fav_gs_lastupdated ,txt_fav_gs_price,txt_pergallon;
            LinearLayout ll_gs_price;

            LayoutInflater inflater ;
            inflater = LayoutInflater.from(mContext);
            convertView =  inflater.inflate(R.layout.lv_item_fav, null);
            txt_fav_gs_name=(TextView)convertView.findViewById(R.id.txt_fav_gs_name);
            txt_fav_gs_address=(TextView)convertView.findViewById(R.id.txt_fav_gs_address);
            txt_fav_gs_lastupdated=(TextView)convertView.findViewById(R.id.txt_fav_gs_lastupdated);
            txt_fav_gs_price=(TextView)convertView.findViewById(R.id.txt_fav_gs_price);
            txt_pergallon=(TextView)convertView.findViewById(R.id.txt_pergallon);

            ll_gs_price=(LinearLayout)convertView.findViewById(R.id.ll_gs_price);
            ll_gs_price.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    Intent Intnt= new Intent(mContext, ActivityGasStationDetail.class);
                    Intnt.putExtra("selectedGSID",list.get(position).getGSId());
                    Intnt.putExtra("FTID",String.valueOf(fuelType));
                    Intnt.putExtra("DAY",String.valueOf(Utils.getCurrentDay()));
                    Intnt.putExtra("puid",Utils.getPuid(mContext));
                    mContext.startActivity(Intnt);
                }
            });

            txt_fav_gs_name.setText(list.get(position).getBrandName().toString());
            txt_fav_gs_address.setText(list.get(position).getStreet().toString()+", "+list.get(position).getCity().toString()+"\n"+list.get(position).getDistance()+" miles away");
            txt_fav_gs_lastupdated.setText("Last updated on: "+list.get(position).getLastUpdateOn().toString());
            if(list.get(position).getPricePerGallonInDoller()==0.0){
                txt_fav_gs_price.setText("N/A");
                txt_pergallon.setVisibility(View.GONE);
            }else{
                txt_fav_gs_price.setText("$"+list.get(position).getPricePerGallonInDoller().toString());
                txt_pergallon.setVisibility(View.VISIBLE);
            }


            return convertView;
        }
    }

    public double CalculationByDistance(double fromLat, double fromLong, double toLat, double toLong) {

        double earthRadius = 6371000; //meters
        double dLat = Math.toRadians(toLat-fromLat);
        double dLng = Math.toRadians(toLong-fromLong);
        double a = Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(Math.toRadians(fromLat)) * Math.cos(Math.toRadians(toLat)) *
                        Math.sin(dLng/2) * Math.sin(dLng/2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
        double dist = (earthRadius * c);

        return dist/1609.344;
    }

    private void showMapData() {
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                mapFragment = (MapFragment) getFragmentManager().findFragmentById(R.id.map);
                mapFragment.getMapAsync(ActivitySearchGasStation.this);
            }
        });
    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivitySearchGasStation.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }


    //================ handle autocomplete click===================

    @Override
    public void onItemClick(AdapterView<?> adapterView, View view, int i, long l) {
        try {
            String txt = autoCompleteLocation.getText().toString();
            System.out.println("---Crash On Click  " + txt);
            txt_serach_gas_station.setText(txt);
            autoCompleteLocation.setThreshold(2);
            showAutoComplete(false);
            txt_serach_gas_station.requestFocus();
            getLocationLatLong();
            Utils.hideKB(ActivitySearchGasStation.this, autoCompleteLocation);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private class GetAddressTask extends AsyncTask<Location, Void, String> {
        Context mContext;

        public GetAddressTask(Context context) {
            super();
            mContext = context;
        }

        @Override
        protected String doInBackground(Location... params) {
            Geocoder geocoder =
                    new Geocoder(mContext, Locale.getDefault());
            // Get the current location from the input parameter list
            Location loc = params[0];
            // Create a list to contain the result address
            List<Address> addresses = null;
            try {
                addresses = geocoder.getFromLocation(loc.getLatitude(),
                        loc.getLongitude(), 1);

                // If the reverse geocode returned an address
                if (addresses != null && addresses.size() > 0) {
                    // Get the first address
                    Address address = addresses.get(0);

                    DisplayAddress = address.getAddressLine(0).toString() + " " + address.getAddressLine(1).toString()
                            + " " + address.getAddressLine(2).toString() + " " + address.getAddressLine(3).toString();


            /*
             * Format the first line of address (if available),
             * city, and country name.
             */
                    String addressText = String.format(
                            "%s, %s, %s",
                            // If there's a street address, add it
                            address.getMaxAddressLineIndex() > 0 ?
                                    address.getAddressLine(0) : "",
                            // Locality is usually a city
                            address.getLocality(),
                            // The country of the address
                            address.getCountryName());
                    // Return the text
                    return address.toString();
                } else {
                    return "No address found";
                }
            } catch (IOException e1) {
                Log.e("LocationSampleActivity",
                        "IO Exception in getFromLocation()");
                e1.printStackTrace();
                return ("IO Exception trying to get address");
            } catch (IllegalArgumentException e2) {
                // Error message to post in the log
                String errorString = "Illegal arguments " +
                        Double.toString(loc.getLatitude()) +
                        " , " +
                        Double.toString(loc.getLongitude()) +
                        " passed to address service";
                Log.e("LocationSampleActivity", errorString);
                e2.printStackTrace();
                return errorString;
            }
        }

        @Override
        protected void onPostExecute(String address) {
            txt_serach_gas_station.setText(DisplayAddress);
            SharedPreferences.Editor editor1 = getSharedPreferences(
                    "CurrentAddress", MODE_PRIVATE).edit();
            editor1.putString("Address", DisplayAddress);
            editor1.commit();
            getLocationLatLong();
        }
    }
    public void getLocationLatLong() {
        mProgressDialog.show();
        PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
        oInsertUpdateApi.setRequestSource(RequestSource.LocationLatLong);
        oInsertUpdateApi.executePostRequest(API.fgetLocationLatLong() + txt_serach_gas_station.getText().toString().trim(), "");
    }

    public void FetchDataFromServer() {
        if (intnt != null) {
            radius = intnt.getDoubleExtra("SearchRadius", 5);
            mAmeneties = intnt.getStringArrayListExtra("Ameneties_array");
            mBrands = intnt.getStringArrayListExtra("mBrands_array");
            fuelType = intnt.getIntExtra("fuelType", 1);
            mUnRegistered = intnt.getIntExtra("mUnRegistered", 0);
            isSortByPrice = intnt.getBooleanExtra("isSortByPrice", true);
        }
        String finalAmenities = "all";
        String finalBrands = "any";
        if(mAmeneties!=null){
            if (mAmeneties.size() > 0 ) {
                finalAmenities = TextUtils.join("-", mAmeneties);
            }
        }
        if(mBrands!=null){
            if (mBrands.size() > 0) {
                finalBrands = TextUtils.join("-", mBrands);
            }
        }
        String GetGasStation_Url = API.fGetGasStationUrl();
        GetGasStation_Url = GetGasStation_Url + location_lat + "/" + location_lng + "/" + radius + "/" + fuelType + "/" + day + "/" + puid + "/" + mUnRegistered + "/" + finalAmenities + "/" + finalBrands;
        GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
        oInsertUpdateApi.setRequestSource(RequestSource.GetGasStations);
        oInsertUpdateApi.executeGetRequest(GetGasStation_Url);
    }


    public class RoundedTransformation implements
            Transformation {
        private final int radius;
        private final int margin; // dp

        // radius is corner radii in dp
        // margin is the board in dp
        public RoundedTransformation(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        @Override
        public Bitmap transform(final Bitmap source) {
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP,
                    Shader.TileMode.CLAMP));
            Bitmap output = Bitmap.createBitmap(source.getWidth(),
                    source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            canvas.drawRoundRect(new RectF(margin, margin, source.getWidth()
                    - margin, source.getHeight() - margin), radius, radius, paint);
            if (source != output) {
                source.recycle();
            }
            return output;
        }

        @Override
        public String key() {
            return "rounded";
        }
    }


    @Override
    public void onBackPressed() {
        super.onBackPressed();
        this.finish();
    }
}
