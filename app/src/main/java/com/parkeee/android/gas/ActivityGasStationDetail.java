package com.parkeee.android.gas;

import android.app.AlertDialog;
import android.app.Application;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewTreeObserver;
import android.widget.Button;
import android.widget.EditText;
import android.widget.FrameLayout;
import android.widget.HorizontalScrollView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.appindexing.Action;
import com.google.android.gms.appindexing.Thing;
import com.google.gson.Gson;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.Deal;
import com.parkeee.android.model.params.GSFavourite;
import com.parkeee.android.model.params.InsertUpdateRegisterPUID;
import com.parkeee.android.model.params.Price;
import com.parkeee.android.model.params.Reply;
import com.parkeee.android.model.params.Reviews;
import com.parkeee.android.model.params.SaveDeal;
import com.parkeee.android.model.params.StationDetailResponse;
import com.parkeee.android.model.params.SubmitReview;
import com.parkeee.android.model.params.SubmitReviewReply;
import com.parkeee.android.model.tables.GSFavouriteTb;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.BarCodeUtils;
import com.parkeee.android.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.Serializable;
import java.io.UnsupportedEncodingException;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import io.realm.Realm;

/**
 * Created by user on 20-12-2016.
 */

public class ActivityGasStationDetail extends FragmentActivity implements View.OnClickListener, OnResultReceived, DealClickListener {


    ImageView img_back, img_gs_icon;
    LinearLayout ll_last_updated, ll_expand_gas_station_price, ll_submit_review, ll_Reviews_section,ll_amenities,ll_near_By;
    Context mContext;
    ViewPager viewPagerDealz;

    StationDetailResponse stationDetailResponse;
    ArrayList<StationDetailResponse> responseList;

    private LinearLayout mLnrDotsView;

    HorizontalScrollView h_scrollView, hsv_near_by;

    ImageView img_rating_star_1, img_rating_star_2, img_rating_star_3, img_rating_star_4, img_rating_star_5;

    Button btn_cancel_rating_review, btn_submit_rating_review;

    ScrollView sv_parent;

    EditText et_user_review;

    TextView txt_gas_station_name, txt_gs_rating, txt_gs_address, txt_gs_timing, txt_last_updated, txt_regular_prices;
    ImageView img_star1, img_star2, img_star3, img_star4, img_star5;
    ImageView img_gs_direction;
    Button btn_gs_status;

    Gson oGson;

    ImageView img_fav_gs_detail, img_share_gs_detail, img_call_gs_detail;


    TextView txt_viewall_gs_replies,txt_regular_price, txt_midgrade_price, txt_premium_price, txt_diesel_price,txt_reviews_text;

    OnResultReceived mOnResultReceived;

    ProgressDialog mProgressDialog;

    String FTID = "";

    String ServerResult;
    int Rating = 0;

    int current_pos=0;

    LinearLayout ll_start_rating,ll_deals_detail_2;
    FrameLayout fl_deals_detail;
    private Realm realm;


    TextView txt_deal_name, txt_description_detail, txt_termandcond_detail, txt_expires_detail, txt_like_deal, txt_dislike_deal;
    ImageView imgBC, img_like_deal, img_dislike_deal;
    LinearLayout ll_Done_deal_detail, ll_like_deal, ll_dislike_deal;

    View v1, v2, v3, v4;
    String Brand_Name = "";
    String Deal_Id;
    TextView txtDealCode;

    public int clickedReplyReviewIndex = -1;

    Dialog mdialog;
    Button btn_positive_custom_dailog;
    TextView txt_custom_dialog;

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    // private GoogleApiClient client;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gas_station_detail);
        InitializeInterface();
        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        // client = new GoogleApiClient.Builder(this).addApi(AppIndex.API).build();
    }

    private void InitializeInterface() {
        ll_deals_detail_2=(LinearLayout)findViewById(R.id.ll_deals_detail_2);
        ll_deals_detail_2.setOnClickListener(this);

        oGson = new Gson();

        mdialog = new  Dialog(ActivityGasStationDetail.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        // Initialize Deals Variables
        txt_deal_name = (TextView) findViewById(R.id.txt_deal_name);
        ll_Done_deal_detail = (LinearLayout) findViewById(R.id.ll_Done_deal_detail);
        ll_Done_deal_detail.setOnClickListener(this);
        imgBC = (ImageView) findViewById(R.id.imgBC);

        ll_like_deal = (LinearLayout) findViewById(R.id.ll_like_deal);
        ll_dislike_deal = (LinearLayout) findViewById(R.id.ll_dislike_deal);

        txtDealCode = (TextView) findViewById(R.id.txtDealCode);


        txt_like_deal = (TextView) findViewById(R.id.txt_like_deal);
        txt_dislike_deal = (TextView) findViewById(R.id.txt_dislike_deal);

        img_like_deal = (ImageView) findViewById(R.id.img_like_deal);
        img_dislike_deal = (ImageView) findViewById(R.id.img_dislike_deal);

        txt_description_detail = (TextView) findViewById(R.id.txt_description_detail);
        txt_termandcond_detail = (TextView) findViewById(R.id.txt_termandcond_detail);
        txt_expires_detail = (TextView) findViewById(R.id.txt_expires_detail);

        //=================End=================

        txt_viewall_gs_replies=(TextView)findViewById(R.id.txt_viewall_gs_replies);
        txt_viewall_gs_replies.setOnClickListener(this);

        btn_gs_status = (Button) findViewById(R.id.btn_gs_status);

        txt_reviews_text=(TextView)findViewById(R.id.txt_reviews_text);

        ll_amenities=(LinearLayout)findViewById(R.id.ll_amenities);
        ll_near_By=(LinearLayout)findViewById(R.id.ll_near_By);

        txt_regular_price = (TextView) findViewById(R.id.txt_regular_prices_gs_detail);
        txt_midgrade_price = (TextView) findViewById(R.id.txt_midgrade_prices_gs_detail);
        txt_premium_price = (TextView) findViewById(R.id.txt_premium_prices_gs_detail);
        txt_diesel_price = (TextView) findViewById(R.id.txt_diesel_prices_gs_detail);


        txt_gas_station_name = (TextView) findViewById(R.id.txt_gas_station_name);
        txt_gs_rating = (TextView) findViewById(R.id.txt_gs_rating);
        txt_gs_address = (TextView) findViewById(R.id.txt_gs_address);
        txt_gs_timing = (TextView) findViewById(R.id.txt_gs_timing);
        txt_last_updated = (TextView) findViewById(R.id.txt_last_updated);

        img_star1 = (ImageView) findViewById(R.id.img_star1);
        img_star2 = (ImageView) findViewById(R.id.img_star2);
        img_star3 = (ImageView) findViewById(R.id.img_star3);
        img_star4 = (ImageView) findViewById(R.id.img_star4);
        img_star5 = (ImageView) findViewById(R.id.img_star5);


        ll_Reviews_section = (LinearLayout) findViewById(R.id.ll_Reviews_section);
        ll_start_rating = (LinearLayout) findViewById(R.id.ll_start_rating);

        img_fav_gs_detail = (ImageView) findViewById(R.id.img_fav_gs_detail);
        img_fav_gs_detail.setOnClickListener(this);

        img_share_gs_detail = (ImageView) findViewById(R.id.img_share_gs_detail);
        img_share_gs_detail.setOnClickListener(this);

        img_call_gs_detail = (ImageView) findViewById(R.id.img_call_gs_detail);
        img_call_gs_detail.setOnClickListener(this);

        fl_deals_detail = (FrameLayout) findViewById(R.id.fl_deals_detail);

        mOnResultReceived = this;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        sv_parent = (ScrollView) findViewById(R.id.sv_parent);
        et_user_review = (EditText) findViewById(R.id.et_user_review);
        et_user_review.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                if (v.getId() == R.id.et_user_review) {
                    v.getParent().requestDisallowInterceptTouchEvent(true);
                    switch (event.getAction() & MotionEvent.ACTION_MASK) {
                        case MotionEvent.ACTION_UP:
                            v.getParent().requestDisallowInterceptTouchEvent(false);
                            break;
                    }
                }
                return false;
            }
        });

        h_scrollView = (HorizontalScrollView) findViewById(R.id.hsv_amenities);
        hsv_near_by = (HorizontalScrollView) findViewById(R.id.hsv_near_by);

        ll_submit_review = (LinearLayout) findViewById(R.id.ll_submit_review);

        btn_submit_rating_review = (Button) findViewById(R.id.btn_submit_rating_review);
        btn_submit_rating_review.setOnClickListener(this);

        btn_cancel_rating_review = (Button) findViewById(R.id.btn_cancel_rating_review);
        btn_cancel_rating_review.setOnClickListener(this);

        img_rating_star_1 = (ImageView) findViewById(R.id.img_rating_star_1);
        img_rating_star_2 = (ImageView) findViewById(R.id.img_rating_star_2);
        img_rating_star_3 = (ImageView) findViewById(R.id.img_rating_star_3);
        img_rating_star_4 = (ImageView) findViewById(R.id.img_rating_star_4);
        img_rating_star_5 = (ImageView) findViewById(R.id.img_rating_star_5);

        img_rating_star_1.setOnClickListener(this);
        img_rating_star_2.setOnClickListener(this);
        img_rating_star_3.setOnClickListener(this);
        img_rating_star_4.setOnClickListener(this);
        img_rating_star_5.setOnClickListener(this);

        mLnrDotsView = (LinearLayout) findViewById(R.id.lnrDotsView);

        viewPagerDealz = (ViewPager) findViewById(R.id.viewPagerDealz);
        viewPagerDealz.setPageMargin(10);

        img_back = (ImageView) findViewById(R.id.img_back_gas_station_detail);
        img_back.setOnClickListener(this);
        img_gs_icon = (ImageView) findViewById(R.id.img_gs_icon);
        mContext = this;

        Picasso.with(img_gs_icon.getContext())
                .load(R.drawable.icon_gs_default_cover)
                .networkPolicy(NetworkPolicy.NO_CACHE)
                .transform(new RoundedTransformation(100, 0))
                .resize(96, 96).centerCrop().into(img_gs_icon, new Callback() {
            @Override
            public void onSuccess() {
            }

            @Override
            public void onError() {
            }
        });


        ll_last_updated = (LinearLayout) findViewById(R.id.ll_last_updated);
        ll_last_updated.setOnClickListener(this);
        ll_expand_gas_station_price = (LinearLayout) findViewById(R.id.ll_expand_gas_station_price);

        FetchDataFromServer();

    }

    private void FetchDataFromServer() {
        String selectedGSID = "";
        FTID = "";
        String DAY = "";
        String puid = "";
        Intent intnt = getIntent();
        if (intnt != null) {
            selectedGSID = intnt.getStringExtra("selectedGSID");
            FTID = intnt.getStringExtra("FTID");
            DAY = intnt.getStringExtra("DAY");
            puid = intnt.getStringExtra("puid");
            String GetGasStationDetailById_Url = API.fGetGasStationDetailById();
            GetGasStationDetailById_Url = GetGasStationDetailById_Url + selectedGSID + "/" + FTID + "/" + DAY + "/" + puid;

            mProgressDialog.show();
            GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
            oInsertUpdateApi.setRequestSource(RequestSource.fGetGasStationDetailById);
            oInsertUpdateApi.executeGetRequest(GetGasStationDetailById_Url);

        }
    }

    @Override
    public void dispatchString(RequestSource from, String what) {


        mProgressDialog.cancel();
        if (from.toString().equalsIgnoreCase("fGetGasStationDetailById")) {
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-1":
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    ServerResult = what.replace("\"[", "[");
                    ServerResult = ServerResult.replace("\"]", "]");
                    ServerResult = ServerResult.replace("\\", "");
                    try {

                        final JSONArray responseArr = new JSONArray(ServerResult);
                        responseList = new ArrayList<>();
                        stationDetailResponse = new StationDetailResponse();

                        if (responseArr.length() > 0) {
                            for (int i = 0; i < responseArr.length(); i++) {
                                final JSONObject obj = responseArr.getJSONObject(i);
                                JSONArray deals_array = obj.getJSONArray("Deals");
                                JSONArray price_array = obj.getJSONArray("Prices");

                                for (int o = 0; o < price_array.length(); o++) {
                                    Price oprice = new Price();
                                    oprice.setDieselFuelPrice(price_array.getJSONObject(o).getDouble("DieselFuelPrice"));
                                    oprice.setMidgradePrice(price_array.getJSONObject(o).getDouble("MidgradePrice"));
                                    oprice.setPremiumPrice(price_array.getJSONObject(o).getDouble("PremiumPrice"));
                                    oprice.setRegularGasPrice(price_array.getJSONObject(o).getDouble("RegularGasPrice"));
                                    oprice.setIsDieselFuelAvailable(price_array.getJSONObject(o).getBoolean("IsDieselFuelAvailable"));
                                    oprice.setIsMidgradeAvailable(price_array.getJSONObject(o).getBoolean("IsMidgradeAvailable"));
                                    oprice.setIsPremiumAvailable(price_array.getJSONObject(o).getBoolean("IsPremiumAvailable"));
                                    oprice.setIsRegularGasAvailable(price_array.getJSONObject(o).getBoolean("IsRegularGasAvailable"));

                                }

                                Brand_Name = obj.getString("BrandName");
                                for (int m = 0; m < deals_array.length(); m++) {
                                    Deal odeal = new Deal();
                                    odeal.setDealId(deals_array.getJSONObject(m).getString("DealId"));
                                    odeal.setDeal(deals_array.getJSONObject(m).getString("Deal"));
                                    odeal.setDealDetail(deals_array.getJSONObject(m).getString("DealDetail"));
                                    odeal.setEndDate(deals_array.getJSONObject(m).getString("EndDate"));
                                    odeal.setDealTerms(deals_array.getJSONObject(m).getString("DealTerms"));
                                    odeal.setAccessCode(deals_array.getJSONObject(m).getString("AccessCode"));
                                    odeal.setLikes(deals_array.getJSONObject(m).getString("Likes"));
                                    odeal.setDisLikes(deals_array.getJSONObject(m).getString("DisLikes"));
                                    odeal.setIsLike(Integer.parseInt(deals_array.getJSONObject(m).getString("IsLike")));
                                    odeal.setIsDisLikes(Integer.parseInt(deals_array.getJSONObject(m).getString("IsDisLikes")));
                                    if (deals_array.getJSONObject(m).getString("IsDefaultDeal").equals("true")) {
                                        odeal.setIsDefaultDeal(true);
                                    } else {
                                        odeal.setIsDefaultDeal(false);
                                    }
                                    stationDetailResponse.deals.add(odeal);
                                }
                                stationDetailResponse.setGSId(obj.getInt("GSId"));
                                stationDetailResponse.setGSIdEnc(obj.getString("GSIdEnc"));
                                stationDetailResponse.setBusinessName(obj.getString("BusinessName"));
                                stationDetailResponse.setIsDefaultDeal(obj.getInt("IsDefaultDeal"));
                                stationDetailResponse.setDefaultDealLikes(obj.getInt("DefaultDealLikes"));
                                stationDetailResponse.setDefaultDealDisLikes(obj.getInt("DefaultDealDisLikes"));
                                stationDetailResponse.setIsLikeDefaultDeal(obj.getInt("IsLikeDefaultDeal"));
                                stationDetailResponse.setCoverImage(obj.getString("CoverImage"));
                                stationDetailResponse.setLogoPath(obj.getString("LogoPath"));
                                stationDetailResponse.setBrandName(obj.getString("BrandName"));
                                stationDetailResponse.setBusinessPhone(obj.getString("BusinessPhone"));
                                stationDetailResponse.setAvgRating(obj.getString("AvgRating"));
                                stationDetailResponse.setZip(obj.getString("Zip"));
                                stationDetailResponse.setLatitude(obj.getString("Latitude"));
                                stationDetailResponse.setLongitude(obj.getString("Longitude"));
                                stationDetailResponse.setStreet(obj.getString("Street"));
                                stationDetailResponse.setPricePerGallonInDoller(obj.getDouble("PricePerGallonInDoller"));
                                stationDetailResponse.setLastUpdateOn(obj.getString("LastUpdateOn"));
                                stationDetailResponse.setTiming(obj.getString("Timing"));
                                stationDetailResponse.setIsOpen(obj.getString("IsOpen"));
                                stationDetailResponse.setDayNo(obj.getString("DayNo"));
                                stationDetailResponse.setCity(obj.getString("City"));
                                stationDetailResponse.setAmenities(obj.getString("Amenities"));
                                stationDetailResponse.setPackageName(obj.getString("PackageName"));
                                stationDetailResponse.setStars(obj.getString("Stars"));
                                stationDetailResponse.setReview(obj.getString("Review"));
                                stationDetailResponse.setTotalReviews(obj.getString("TotalReviews"));

                                try {
                                    JSONArray Reviews_array = null;
                                    JSONArray Price_array = null;
                                    Price_array = obj.getJSONArray("Prices");
                                    for (int k = 0; k < price_array.length(); k++) {
                                        Price oprice = new Price();
                                        oprice.setDieselFuelPrice(Double.parseDouble(Price_array.getJSONObject(k).getString("DieselFuelPrice")));
                                        oprice.setRegularGasPrice(Double.parseDouble(Price_array.getJSONObject(k).getString("RegularGasPrice")));
                                        oprice.setMidgradePrice(Double.parseDouble(Price_array.getJSONObject(k).getString("MidgradePrice")));
                                        oprice.setPremiumPrice(Double.parseDouble(Price_array.getJSONObject(k).getString("PremiumPrice")));

                                        if (Price_array.getJSONObject(k).getString("IsDieselFuelAvailable").equals("true")) {
                                            oprice.setIsDieselFuelAvailable(true);
                                        } else {
                                            oprice.setIsDieselFuelAvailable(false);
                                        }

                                        if (Price_array.getJSONObject(k).getString("IsRegularGasAvailable").equals("true")) {
                                            oprice.setIsRegularGasAvailable(true);
                                        } else {
                                            oprice.setIsRegularGasAvailable(false);
                                        }

                                        if (Price_array.getJSONObject(k).getString("IsPremiumAvailable").equals("true")) {
                                            oprice.setIsPremiumAvailable(true);
                                        } else {
                                            oprice.setIsPremiumAvailable(false);
                                        }

                                        if (Price_array.getJSONObject(k).getString("IsMidgradeAvailable").equals("true")) {
                                            oprice.setIsMidgradeAvailable(true);
                                        } else {
                                            oprice.setIsMidgradeAvailable(false);
                                        }

                                        stationDetailResponse.prices = oprice;

                                    }

                                    Reviews_array = obj.getJSONArray("Reviews");

                                    for (int p = 0; p < Reviews_array.length(); p++) {
                                        JSONObject joreview = new JSONObject();
                                        joreview = Reviews_array.getJSONObject(p);
                                        Reviews oreviews = new Reviews();
                                        oreviews.setPuid(joreview.getString("puid"));
                                        oreviews.setStars(joreview.getString("Stars"));
                                        oreviews.setReview(joreview.getString("Review"));
                                        oreviews.setPuFirstName(joreview.getString("puFirstName"));
                                        oreviews.setPuLastName(joreview.getString("puLastName"));
                                        oreviews.setProfile_pic(joreview.getString("profile_pic"));
                                        oreviews.setReviewId(joreview.getString("reviewId"));
                                        oreviews.setSubmit_date(joreview.getString("submit_date"));
                                        oreviews.setOwner_response(joreview.getString("owner_response"));
                                        oreviews.setResponse_date(joreview.getString("response_date"));
                                        oreviews.setReplies_count(joreview.getString("replies_count"));
                                        oreviews.setMinutes(joreview.getString("Minutes"));
                                        oreviews.setHours(joreview.getString("Hours"));
                                        oreviews.setDays(joreview.getString("Days"));
                                        oreviews.setResMinutes(joreview.getString("ResMinutes"));
                                        oreviews.setResHours(joreview.getString("ResHours"));
                                        oreviews.setResDays(joreview.getString("ResDays"));

                                        JSONArray Replies_array = null;
                                        Replies_array = joreview.getJSONArray("Replies");
                                        ArrayList<Reply> al_reply = new ArrayList<Reply>();

                                        for (int q = 0; q < Replies_array.length(); q++) {
                                            Reply oreply = new Reply();
                                            oreply.setPuid(Replies_array.getJSONObject(q).getString("puid"));
                                            oreply.setReplyId(Replies_array.getJSONObject(q).getString("ReplyId"));
                                            oreply.setReviewReply(Replies_array.getJSONObject(q).getString("ReviewReply"));
                                            oreply.setPuFirstName(Replies_array.getJSONObject(q).getString("puFirstName"));
                                            oreply.setPuLastName(Replies_array.getJSONObject(q).getString("puLastName"));
                                            oreply.setProfile_pic(Replies_array.getJSONObject(q).getString("profile_pic"));
                                            oreply.setReviewId(Replies_array.getJSONObject(q).getString("reviewId"));
                                            oreply.setSubmit_date(Replies_array.getJSONObject(q).getString("submit_date"));
                                            oreply.setMinutes(Replies_array.getJSONObject(q).getString("Minutes"));
                                            oreply.setHours(Replies_array.getJSONObject(q).getString("Hours"));
                                            oreply.setDays(Replies_array.getJSONObject(q).getString("Days"));
                                            oreviews.Replies.add(oreply);
                                        }
                                        stationDetailResponse.reviews.add(oreviews);
                                    }

                                } catch (JSONException e) {
                                    e.printStackTrace();
                                }
                                responseList.add(stationDetailResponse);
                            }

                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {

                                    if(Integer.parseInt(stationDetailResponse.getTotalReviews())>3){
                                        txt_viewall_gs_replies.setVisibility(View.VISIBLE);
                                    }

                                    if(stationDetailResponse.getReviews().size()>0){
                                        txt_reviews_text.setVisibility(View.VISIBLE);
                                    }else{
                                        txt_reviews_text.setVisibility(View.GONE);
                                    }

                                    String[] amenitiesTagArr = {"26", "20", "1", "25", "10", "2", "24", "3", "23", "5", "22", "21", "6", "7", "8", "9", "11", "12", "4"};
                                    String[] amenitiesImagesArr = {"air", "attendent", "beer", "candies", "cig", "coffee", "dinner", "drivethrough", "fastfood", "fullservice", "ice", "indoor", "cybercafe", "latto", "nosmoking", "selfservice", "malefemale", "handicap", "wifi"};

                                    String[] nearByTagArr = {"13", "14", "15", "16", "17", "18", "19"};
                                    String[] nearByImagesArr = {"hotel", "lake", "library", "museum", "park", "servicestation", "swimming"};


                                    List<String> amenitiesTagArrList = new ArrayList<String>(Arrays.asList(amenitiesTagArr));
                                    List<String> nearByTagArrList = new ArrayList<String>(Arrays.asList(nearByTagArr));

                                    LinearLayout topLinearLayout = new LinearLayout(mContext);
                                    topLinearLayout.setOrientation(LinearLayout.HORIZONTAL);

                                    LinearLayout topLinearLayout2 = new LinearLayout(mContext);
                                    topLinearLayout2.setOrientation(LinearLayout.HORIZONTAL);

                                    LinearLayout.LayoutParams vp = new LinearLayout.LayoutParams(100, 100);
                                    vp.setMargins(20, 0, 0, 0);

                                    if(stationDetailResponse.getAmenities().equals("")){
                                        ll_amenities.setVisibility(View.GONE);
                                    }else{
                                        ll_amenities.setVisibility(View.VISIBLE);
                                    }

                                    String[] amenitiesArr = stationDetailResponse.getAmenities().split(",");

                                    for (int i = 0; i < amenitiesArr.length; i++) {
                                        final ImageView imageView = new ImageView(mContext);
                                        imageView.setTag(i);
                                        imageView.setLayoutParams(vp);

                                        if (amenitiesTagArrList.contains(amenitiesArr[i])) {
                                            Resources res = getResources();
                                            String mDrawableName = amenitiesImagesArr[amenitiesTagArrList.indexOf(amenitiesArr[i])];
                                            int resID = res.getIdentifier(mDrawableName, "drawable", getPackageName());
                                            Drawable drawable = res.getDrawable(resID);
                                            imageView.setImageDrawable(drawable);
                                            topLinearLayout.addView(imageView);
                                        }

                                        if (nearByTagArrList.contains(amenitiesArr[i])) {
                                            Resources res = getResources();
                                            String mDrawableName = nearByImagesArr[nearByTagArrList.indexOf(amenitiesArr[i])];
                                            int resID = res.getIdentifier(mDrawableName, "drawable", getPackageName());
                                            Drawable drawable = res.getDrawable(resID);
                                            imageView.setImageDrawable(drawable);
                                            topLinearLayout2.addView(imageView);
                                            ll_near_By.setVisibility(View.VISIBLE);
                                        }
                                    }

                                    h_scrollView.addView(topLinearLayout);
                                    hsv_near_by.addView(topLinearLayout2);


                                    GSFavouriteTb oGSFavouriteTb = RealmController.with((Application) mContext.getApplicationContext()).getGasStationFav(String.valueOf(stationDetailResponse.getGSId()));

                                    if (oGSFavouriteTb != null) {
                                        img_fav_gs_detail.setImageResource(R.drawable.icon_gs_detail_fav_active);
                                        img_fav_gs_detail.setTag("1");
                                    }

                                    txt_gas_station_name.setText(stationDetailResponse.getBrandName().toString());

                                    double d_avg_rating = Double.parseDouble(stationDetailResponse.getAvgRating());
                                    double round_value = round(Double.parseDouble(String.format("%.2f", d_avg_rating)), 1);
                                    txt_gs_rating.setText(String.valueOf(round_value));

                                    txt_gs_address.setText(stationDetailResponse.getStreet().toString() + ", " + stationDetailResponse.getCity().toString());
                                    txt_gs_timing.setText(stationDetailResponse.getTiming().toString());
                                    txt_last_updated.setText(stationDetailResponse.getLastUpdateOn().toString());
                                    Double avg_rating_double = Double.parseDouble(stationDetailResponse.getAvgRating());
                                    int avg_rating = avg_rating_double.intValue();

                                    if (avg_rating < 1) {
                                    } else if (avg_rating <= 1) {
                                        img_star1.setImageResource(R.drawable.icon_rating_star_active);
                                    } else if (avg_rating <= 2) {
                                        img_star1.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star2.setImageResource(R.drawable.icon_rating_star_active);
                                    } else if (avg_rating <= 3) {
                                        img_star1.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star2.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star3.setImageResource(R.drawable.icon_rating_star_active);
                                    } else if (avg_rating <= 4) {
                                        img_star1.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star2.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star3.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star4.setImageResource(R.drawable.icon_rating_star_active);
                                    } else if (avg_rating <= 5) {
                                        img_star1.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star2.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star3.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star4.setImageResource(R.drawable.icon_rating_star_active);
                                        img_star5.setImageResource(R.drawable.icon_rating_star_active);
                                    }

                                    String gs_icon_path = stationDetailResponse.getLogoPath();
                                    gs_icon_path = gs_icon_path.replace("\"[", "[");
                                    gs_icon_path = gs_icon_path.replace("\"]", "]");
                                    gs_icon_path = gs_icon_path.replace("\\", "");

                                    if (!gs_icon_path.equals("")) {
                                        gs_icon_path = gs_icon_path.replace("~", "https://owner.parkeee.com");
                                        Picasso.with(img_gs_icon.getContext())
                                                .load(gs_icon_path)
                                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                                .into(img_gs_icon, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                    }

                                                    @Override
                                                    public void onError() {
                                                    }
                                                });
                                    }

                                    if (stationDetailResponse.getIsOpen().equals("True")) {
                                        btn_gs_status.setText("OPEN 24 HOURS");
                                    } else {
                                        btn_gs_status.setText("CLOSED");
                                        btn_gs_status.setBackground(getResources().getDrawable(R.drawable.view_shape_red));
                                    }

                                    txt_regular_price.setText(String.valueOf(stationDetailResponse.getPrices().getRegularGasPrice()));
                                    txt_midgrade_price.setText(String.valueOf(stationDetailResponse.getPrices().getMidgradePrice()));
                                    txt_premium_price.setText(String.valueOf(stationDetailResponse.getPrices().getPremiumPrice()));
                                    txt_diesel_price.setText(String.valueOf(stationDetailResponse.getPrices().getDieselFuelPrice()));


                                    // img_star1,img_star2,img_star3,img_star4,img_star5;
                                    LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(15, 15);
                                    params.setMargins(10, 0, 0, 0);
                                    for (int i = 0; i < responseList.get(0).deals.size(); i++) {
                                        ImageView ii = new ImageView(mContext);
                                        ii.setBackgroundResource(R.drawable.light_dot);
                                        if (i == 0) {
                                            ii.setBackgroundResource(R.drawable.dark_dot);
                                        } else {
                                            ii.setBackgroundResource(R.drawable.light_dot);
                                        }
                                        ii.setLayoutParams(params);
                                        mLnrDotsView.addView(ii);
                                    }


                                    viewPagerDealz.setAdapter(new CustomViewPagerAdapter(getSupportFragmentManager(), responseList.get(0).deals));
                                    viewPagerDealz
                                            .setOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener() {
                                                @Override
                                                public void onPageSelected(int postion) {
                                                    for (int i = 0; i < mLnrDotsView.getChildCount(); i++) {
                                                        ImageView img1 = (ImageView) mLnrDotsView
                                                                .getChildAt(i);
                                                        if (i == postion) {
                                                            img1.setImageResource(R.drawable.dark_dot);
                                                        } else {
                                                            img1.setImageResource(R.drawable.light_dot);
                                                        }
                                                    }
                                                }
                                            });

                                    ArrayList<Reviews> tempElements = new ArrayList<Reviews>(responseList.get(0).reviews);
                                    Collections.reverse(tempElements);
                                    stationDetailResponse.reviews = tempElements;
                                    for (int p = 0; p < responseList.get(0).reviews.size(); p++) {
                                        int indx = 0;
                                        current_pos=p;
                                        TextView txt_review_user_name, txt_user_review;
                                        ImageView img_rating1, img_rating2, img_rating3, img_rating4, img_rating5, img_user_rv1;
                                        LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        v1 = vi.inflate(R.layout.reviews_view_1, null);
                                        txt_review_user_name = (TextView) v1.findViewById(R.id.txt_review_user_name);
                                        txt_user_review = (TextView) v1.findViewById(R.id.txt_user_review);
                                        img_rating1 = (ImageView) v1.findViewById(R.id.img_rating1_user_review);
                                        img_rating2 = (ImageView) v1.findViewById(R.id.img_rating2_user_review);
                                        img_rating3 = (ImageView) v1.findViewById(R.id.img_rating3_user_review);
                                        img_rating4 = (ImageView) v1.findViewById(R.id.img_rating4_user_review);
                                        img_rating5 = (ImageView) v1.findViewById(R.id.img_rating5_user_review);
                                        img_user_rv1 = (ImageView) v1.findViewById(R.id.img_user_rv1);
                                        String user_path = responseList.get(0).reviews.get(p).getProfile_pic().toString();

                                        user_path = user_path.replace("\"[", "[");
                                        user_path = user_path.replace("\"]", "]");
                                        user_path = user_path.replace("\\", "");

                                        if (!user_path.equals("")) {
                                            user_path = "http://owner.parkeee.net/owner/" + user_path;
                                            Picasso.with(img_user_rv1.getContext())
                                                    .load(user_path)
                                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                                    .transform(new RoundedTransformation(100, 0))
                                                    .resize(96, 96).centerCrop().into(img_user_rv1, new Callback() {
                                                @Override
                                                public void onSuccess() {
                                                }

                                                @Override
                                                public void onError() {
                                                }
                                            });
                                        }


                                        txt_review_user_name.setText(responseList.get(0).reviews.get(p).getPuFirstName() + " " + responseList.get(0).reviews.get(p).getPuLastName());
                                        txt_user_review.setText(responseList.get(0).reviews.get(p).getReview());

                                        if (!responseList.get(0).reviews.get(p).getStars().equals("")) {
                                            if (responseList.get(0).reviews.get(p).getStars().equals("1")) {
                                                img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            } else if (responseList.get(0).reviews.get(p).getStars().equals("2")) {
                                                img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                            } else if (responseList.get(0).reviews.get(p).getStars().equals("3")) {
                                                img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                            } else if (responseList.get(0).reviews.get(p).getStars().equals("4")) {
                                                img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating4.setImageResource(R.drawable.icon_rating_star_active);
                                            } else if (responseList.get(0).reviews.get(p).getStars().equals("5")) {
                                                img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating4.setImageResource(R.drawable.icon_rating_star_active);
                                                img_rating5.setImageResource(R.drawable.icon_rating_star_active);
                                            }
                                        }

                                        ll_Reviews_section.addView(v1, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                        indx++;
                                        if (!responseList.get(0).reviews.get(p).getOwner_response().equals("")) {
                                            TextView txt_reply_from_owner, txt_owner_reply;
                                            LayoutInflater vi_2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                            v2 = vi_2.inflate(R.layout.review_view_2, null);
                                            txt_reply_from_owner = (TextView) v2.findViewById(R.id.txt_reply_from_owner);
                                            txt_owner_reply = (TextView) v2.findViewById(R.id.txt_owner_reply);

                                            txt_reply_from_owner.setText("Reply from " + Brand_Name);
                                            txt_owner_reply.setText(responseList.get(0).reviews.get(p).getOwner_response());
                                            ll_Reviews_section.addView(v2, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                            indx++;
                                        }


                                        for (int m = 0; m < responseList.get(0).reviews.get(p).getReplies().size(); m++) {

                                            TextView txt_replies_content, txt_replies_user_name;
                                            ImageView img_user_rv3;
                                            LayoutInflater vi_3 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                            v3 = vi_3.inflate(R.layout.review_view_3, null);
                                            txt_replies_content = (TextView) v3.findViewById(R.id.txt_replies_content);
                                            txt_replies_user_name = (TextView) v3.findViewById(R.id.txt_replies_user_name);
                                            txt_replies_content.setText(responseList.get(0).reviews.get(p).getReplies().get(m).getReviewReply());
                                            txt_replies_user_name.setText(responseList.get(0).reviews.get(p).getReplies().get(m).getPuFirstName() + " " + responseList.get(0).reviews.get(p).getReplies().get(m).getPuLastName());

                                            img_user_rv3 = (ImageView) v3.findViewById(R.id.img_user_rv3);

                                            String user_path_v3 = responseList.get(0).reviews.get(p).getReplies().get(m).getProfile_pic();
                                            user_path_v3 = user_path_v3.replace("\"[", "[");
                                            user_path_v3 = user_path_v3.replace("\"]", "]");
                                            user_path_v3 = user_path_v3.replace("\\", "");

                                            if (!user_path_v3.equals("")) {
                                                user_path_v3 = "http://owner.parkeee.net/owner/" + user_path_v3;
                                                Picasso.with(img_user_rv3.getContext())
                                                        .load(user_path_v3)
                                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                                        .transform(new RoundedTransformation(100, 0))
                                                        .resize(96, 96).centerCrop().into(img_user_rv3, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                    }

                                                    @Override
                                                    public void onError() {
                                                    }
                                                });
                                            }


                                            ll_Reviews_section.addView(v3, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                            indx++;
                                        }


                                        final LinearLayout ll_reply, ll_reply_submit;
                                        final Button btnReply_review, btnReply_review_submit, btn_review_cancel;
                                        final EditText et_review_submit;
                                        final TextView txt_review_count;

                                        LayoutInflater vi_4 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        v4 = vi_4.inflate(R.layout.review_view_4, null);
                                        ll_reply = (LinearLayout) v4.findViewById(R.id.ll_reply);
                                        ll_reply_submit = (LinearLayout) v4.findViewById(R.id.ll_reply_submit);
                                        et_review_submit = (EditText) v4.findViewById(R.id.et_review_submit);
                                        txt_review_count = (TextView) v4.findViewById(R.id.txt_review_count);
                                        txt_review_count.setTag(p);
                                        txt_review_count.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {

                                                Reviews review = responseList.get(0).reviews.get(Integer.parseInt(v.getTag().toString()));

                                                Intent intnt = new Intent(ActivityGasStationDetail.this, ActivityGasStationReplies.class);
                                                intnt.putExtra("Review_Id", review.getReviewId());
                                                intnt.putExtra("User_Name", review.getPuFirstName()+" "+review.getPuLastName());
                                                intnt.putExtra("User_Review", review.getReview());
                                                intnt.putExtra("User_Image", review.getProfile_pic());
                                                intnt.putExtra("User_Rating", review.getStars());
                                                intnt.putExtra("submit_date", review.getSubmit_date());
                                                startActivity(intnt);
                                            }
                                        });

                                        int reply_count = Integer.parseInt(responseList.get(0).reviews.get(p).getReplies_count());
                                        if (reply_count > 3) {
                                            txt_review_count.setVisibility(View.VISIBLE);
                                            txt_review_count.setText(reply_count + " Replies");
                                        }
                                        btnReply_review = (Button) v4.findViewById(R.id.btnReply_review);
                                        btnReply_review.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View view) {
                                                ll_reply.setVisibility(View.GONE);
                                                ll_reply_submit.setVisibility(View.VISIBLE);
                                            }
                                        });

                                        btnReply_review_submit = (Button) v4.findViewById(R.id.btnReply_review_submit);
                                        btnReply_review_submit.setTag(p);
                                        btnReply_review_submit.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                if(!et_review_submit.getText().toString().trim().equals("")){
                                                    clickedReplyReviewIndex = Integer.parseInt(btnReply_review_submit.getTag().toString());
                                                    String puid = Utils.getPuid(mContext);
                                                    SubmitReviewReply osubmitbyuser = new SubmitReviewReply();
                                                    osubmitbyuser.setReplyId("0");
                                                    osubmitbyuser.setPuid(puid);
                                                    osubmitbyuser.setReviewReply(et_review_submit.getText().toString().trim());
                                                    osubmitbyuser.setReviewId(responseList.get(0).reviews.get(clickedReplyReviewIndex).getReviewId());
                                                    String jsonString = oGson.toJson(osubmitbyuser, SubmitReviewReply.class).toString();
                                                    mProgressDialog.show();
                                                    //Post oInsertUpdateApi Api
                                                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                                                    oInsertUpdateApi.setRequestSource(RequestSource.SaveGasStationReviewReply);
                                                    oInsertUpdateApi.executePostRequest(API.fSaveGasStationReviewReply(), jsonString);
                                                    ll_reply.setVisibility(View.VISIBLE);
                                                    ll_reply_submit.setVisibility(View.GONE);
                                                }else{
                                                    txt_custom_dialog.setText("Empty reply can't be added.");
                                                    mdialog.show();
                                                }

                                            }
                                        });

                                        btn_review_cancel = (Button) v4.findViewById(R.id.btn_review_cancel);
                                        btn_review_cancel.setOnClickListener(new View.OnClickListener() {
                                            @Override
                                            public void onClick(View v) {
                                                clickedReplyReviewIndex = -1;
                                                ll_reply.setVisibility(View.VISIBLE);
                                                ll_reply_submit.setVisibility(View.GONE);
                                            }
                                        });
                                        ll_Reviews_section.addView(v4, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                                    }
                                    et_user_review.setText(stationDetailResponse.getReview().toString());

                                    setStarsRating();
                                }
                            });
                        }
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }
        } else if (from.toString().equalsIgnoreCase("SaveGSRatingByUser")) {

            switch (ServerResult) {
                case "-2":
                case "-3":
                    mProgressDialog.dismiss();
                    displayAlert("app error", false);
                    break;
                case "0":
                    mProgressDialog.dismiss();
                    displayAlert("Review not submitted, please try again after sometime.", false);
                    break;
                case "-1":
                    mProgressDialog.dismiss();
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    stationDetailResponse.setStars(String.valueOf(Rating));
                    ServerResult = what.replace("\"[", "[");
                    ServerResult = ServerResult.replace("\"]", "]");
                    ServerResult = ServerResult.replace("\\", "");
                    stationDetailResponse.reviews.clear();
                    try {
                        final JSONArray Reviews_array = new JSONArray(ServerResult);
                        for (int p = 0; p < Reviews_array.length(); p++) {
                            JSONObject joreview = new JSONObject();
                            joreview = Reviews_array.getJSONObject(p);
                            Reviews oreviews = new Reviews();
                            oreviews.setPuid(joreview.getString("puid"));
                            oreviews.setStars(joreview.getString("Stars"));
                            oreviews.setReview(joreview.getString("Review"));
                            oreviews.setPuFirstName(joreview.getString("puFirstName"));
                            oreviews.setPuLastName(joreview.getString("puLastName"));
                            oreviews.setProfile_pic(joreview.getString("profile_pic"));
                            oreviews.setReviewId(joreview.getString("reviewId"));
                            oreviews.setSubmit_date(joreview.getString("submit_date"));
                            oreviews.setOwner_response(joreview.getString("owner_response"));
                            oreviews.setResponse_date(joreview.getString("response_date"));
                            oreviews.setReplies_count(joreview.getString("replies_count"));
                            oreviews.setMinutes(joreview.getString("Minutes"));
                            oreviews.setHours(joreview.getString("Hours"));
                            oreviews.setDays(joreview.getString("Days"));
                            oreviews.setResMinutes(joreview.getString("ResMinutes"));
                            oreviews.setResHours(joreview.getString("ResHours"));
                            oreviews.setResDays(joreview.getString("ResDays"));

                            JSONArray Replies_array = null;
                            Replies_array = joreview.getJSONArray("Replies");
                            ArrayList<Reply> al_reply = new ArrayList<Reply>();

                            for (int q = 0; q < Replies_array.length(); q++) {
                                Reply oreply = new Reply();
                                oreply.setPuid(Replies_array.getJSONObject(q).getString("puid"));
                                oreply.setReplyId(Replies_array.getJSONObject(q).getString("ReplyId"));
                                oreply.setReviewReply(Replies_array.getJSONObject(q).getString("ReviewReply"));
                                oreply.setPuFirstName(Replies_array.getJSONObject(q).getString("puFirstName"));
                                oreply.setPuLastName(Replies_array.getJSONObject(q).getString("puLastName"));
                                oreply.setProfile_pic(Replies_array.getJSONObject(q).getString("profile_pic"));
                                oreply.setReviewId(Replies_array.getJSONObject(q).getString("reviewId"));
                                oreply.setSubmit_date(Replies_array.getJSONObject(q).getString("submit_date"));
                                oreply.setMinutes(Replies_array.getJSONObject(q).getString("Minutes"));
                                oreply.setHours(Replies_array.getJSONObject(q).getString("Hours"));
                                oreply.setDays(Replies_array.getJSONObject(q).getString("Days"));
                                oreviews.Replies.add(oreply);
                            }
                            stationDetailResponse.reviews.add(oreviews);
                        }


                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ll_submit_review.setVisibility(View.GONE);
                                ll_Reviews_section.removeAllViews();
                                ArrayList<Reviews> tempElements = new ArrayList<Reviews>(responseList.get(0).reviews);
                                Collections.reverse(tempElements);
                                stationDetailResponse.reviews = tempElements;
                                for (int p = 0; p < responseList.get(0).reviews.size(); p++) {
                                    int indx = 0;
                                    TextView txt_review_user_name, txt_user_review;
                                    ImageView img_rating1, img_rating2, img_rating3, img_rating4, img_rating5, img_user_rv1;
                                    LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    v1 = vi.inflate(R.layout.reviews_view_1, null);
                                    txt_review_user_name = (TextView) v1.findViewById(R.id.txt_review_user_name);
                                    txt_user_review = (TextView) v1.findViewById(R.id.txt_user_review);
                                    img_rating1 = (ImageView) v1.findViewById(R.id.img_rating1_user_review);
                                    img_rating2 = (ImageView) v1.findViewById(R.id.img_rating2_user_review);
                                    img_rating3 = (ImageView) v1.findViewById(R.id.img_rating3_user_review);
                                    img_rating4 = (ImageView) v1.findViewById(R.id.img_rating4_user_review);
                                    img_rating5 = (ImageView) v1.findViewById(R.id.img_rating5_user_review);

                                    txt_review_user_name.setText(responseList.get(0).reviews.get(p).getPuFirstName() + " " + responseList.get(0).reviews.get(p).getPuLastName());
                                    txt_user_review.setText(responseList.get(0).reviews.get(p).getReview());

                                    img_user_rv1 = (ImageView) v1.findViewById(R.id.img_user_rv1);
                                    String user_path = responseList.get(0).reviews.get(p).getProfile_pic().toString();

                                    user_path = user_path.replace("\"[", "[");
                                    user_path = user_path.replace("\"]", "]");
                                    user_path = user_path.replace("\\", "");

                                    if (!user_path.equals("")) {
                                        user_path = "http://owner.parkeee.net/owner/" + user_path;
                                        Picasso.with(img_user_rv1.getContext())
                                                .load(user_path)
                                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                                .transform(new RoundedTransformation(100, 0))
                                                .resize(96, 96).centerCrop().into(img_user_rv1, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                            }

                                            @Override
                                            public void onError() {
                                            }
                                        });
                                    }
                                    if (!responseList.get(0).reviews.get(p).getStars().equals("")) {
                                        if (responseList.get(0).reviews.get(p).getStars().equals("1")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                        } else if (responseList.get(0).reviews.get(p).getStars().equals("2")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                        } else if (responseList.get(0).reviews.get(p).getStars().equals("3")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                        } else if (responseList.get(0).reviews.get(p).getStars().equals("4")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating4.setImageResource(R.drawable.icon_rating_star_active);
                                        } else if (responseList.get(0).reviews.get(p).getStars().equals("5")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating4.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating5.setImageResource(R.drawable.icon_rating_star_active);
                                        }
                                    }

                                    ll_Reviews_section.addView(v1, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                    indx++;
                                    if (!responseList.get(0).reviews.get(p).getOwner_response().equals("")) {
                                        TextView txt_reply_from_owner, txt_owner_reply;
                                        LayoutInflater vi_2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        v2 = vi_2.inflate(R.layout.review_view_2, null);
                                        txt_reply_from_owner = (TextView) v2.findViewById(R.id.txt_reply_from_owner);
                                        txt_owner_reply = (TextView) v2.findViewById(R.id.txt_owner_reply);

                                        txt_reply_from_owner.setText("Reply from " + Brand_Name);
                                        txt_owner_reply.setText(responseList.get(0).reviews.get(p).getOwner_response());
                                        ll_Reviews_section.addView(v2, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                        indx++;
                                    }

                                    for (int m = 0; m < responseList.get(0).reviews.get(p).getReplies().size(); m++) {

                                        TextView txt_replies_content, txt_replies_user_name;
                                        ImageView img_user_rv3;
                                        LayoutInflater vi_3 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        v3 = vi_3.inflate(R.layout.review_view_3, null);
                                        txt_replies_content = (TextView) v3.findViewById(R.id.txt_replies_content);
                                        txt_replies_user_name = (TextView) v3.findViewById(R.id.txt_replies_user_name);
                                        img_user_rv3 = (ImageView) v3.findViewById(R.id.img_user_rv3);

                                        txt_replies_content.setText(responseList.get(0).reviews.get(p).getReplies().get(m).getReviewReply());
                                        txt_replies_user_name.setText(responseList.get(0).reviews.get(p).getReplies().get(m).getPuFirstName() + " " + responseList.get(0).reviews.get(p).getReplies().get(m).getPuLastName());


                                        String user_path_v3 = responseList.get(0).reviews.get(p).getReplies().get(m).getProfile_pic();
                                        user_path_v3 = user_path_v3.replace("\"[", "[");
                                        user_path_v3 = user_path_v3.replace("\"]", "]");
                                        user_path_v3 = user_path_v3.replace("\\", "");

                                        if (!user_path_v3.equals("")) {
                                            user_path_v3 = "http://owner.parkeee.net/owner/" + user_path_v3;
                                            Picasso.with(img_user_rv3.getContext())
                                                    .load(user_path_v3)
                                                    .networkPolicy(NetworkPolicy.NO_CACHE)
                                                    .transform(new RoundedTransformation(100, 0))
                                                    .resize(96, 96).centerCrop().into(img_user_rv3, new Callback() {
                                                @Override
                                                public void onSuccess() {
                                                }

                                                @Override
                                                public void onError() {
                                                }
                                            });
                                        }

                                        ll_Reviews_section.addView(v3, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                        indx++;
                                    }

                                    final LinearLayout ll_reply, ll_reply_submit;
                                    final Button btnReply_review, btnReply_review_submit, btn_review_cancel;
                                    final EditText et_review_submit;
                                    final TextView txt_review_count;

                                    LayoutInflater vi_4 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    v4 = vi_4.inflate(R.layout.review_view_4, null);
                                    ll_reply = (LinearLayout) v4.findViewById(R.id.ll_reply);
                                    ll_reply_submit = (LinearLayout) v4.findViewById(R.id.ll_reply_submit);
                                    btnReply_review = (Button) v4.findViewById(R.id.btnReply_review);
                                    btnReply_review_submit = (Button) v4.findViewById(R.id.btnReply_review_submit);
                                    btnReply_review_submit.setTag(p);
                                    et_review_submit = (EditText) v4.findViewById(R.id.et_review_submit);

                                    txt_review_count = (TextView) v4.findViewById(R.id.txt_review_count);

                                    txt_review_count.setTag(p);
                                    txt_review_count.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Reviews review = responseList.get(0).reviews.get(Integer.parseInt(v.getTag().toString()));

                                            Intent intnt = new Intent(ActivityGasStationDetail.this, ActivityGasStationReplies.class);
                                            intnt.putExtra("Review_Id", review.getReviewId());
                                            intnt.putExtra("User_Name", review.getPuFirstName()+" "+review.getPuLastName());
                                            intnt.putExtra("User_Review", review.getReview());
                                            intnt.putExtra("User_Image", review.getProfile_pic());
                                            intnt.putExtra("User_Rating", review.getStars());
                                            intnt.putExtra("submit_date", review.getSubmit_date());
                                            startActivity(intnt);
                                        }
                                    });

                                    int reply_count = Integer.parseInt(responseList.get(0).reviews.get(p).getReplies_count());
                                    if (reply_count > 3) {
                                        txt_review_count.setVisibility(View.VISIBLE);
                                        txt_review_count.setText(reply_count + " Replies");
                                    }

                                    btnReply_review.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            ll_reply.setVisibility(View.GONE);
                                            ll_reply_submit.setVisibility(View.VISIBLE);
                                        }
                                    });

                                    btnReply_review_submit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if(!et_review_submit.getText().toString().trim().equals("")){
                                                clickedReplyReviewIndex = Integer.parseInt(btnReply_review_submit.getTag().toString());
                                                String puid = Utils.getPuid(mContext);
                                                SubmitReviewReply osubmitbyuser = new SubmitReviewReply();
                                                osubmitbyuser.setReplyId("0");
                                                osubmitbyuser.setPuid(puid);
                                                osubmitbyuser.setReviewReply(et_review_submit.getText().toString().trim());
                                                osubmitbyuser.setReviewId(responseList.get(0).reviews.get(clickedReplyReviewIndex).getReviewId());
                                                String jsonString = oGson.toJson(osubmitbyuser, SubmitReviewReply.class).toString();
                                                mProgressDialog.show();
                                                //Post oInsertUpdateApi Api
                                                PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                                                oInsertUpdateApi.setRequestSource(RequestSource.SaveGasStationReviewReply);
                                                oInsertUpdateApi.executePostRequest(API.fSaveGasStationReviewReply(), jsonString);
                                                ll_reply.setVisibility(View.VISIBLE);
                                                ll_reply_submit.setVisibility(View.GONE);
                                            }else{
                                                txt_custom_dialog.setText("Empty reply can't be added.");
                                                mdialog.show();
                                            }
                                        }
                                    });

                                    btn_review_cancel = (Button) v4.findViewById(R.id.btn_review_cancel);
                                    btn_review_cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            clickedReplyReviewIndex = -1;
                                            ll_reply.setVisibility(View.VISIBLE);
                                            ll_reply_submit.setVisibility(View.GONE);
                                        }
                                    });


                                    ll_Reviews_section.addView(v4, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                                }


                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }

                    break;
            }
        } else if (from.toString().equalsIgnoreCase("SaveGasStationReviewReply")) {

            switch (what) {
                case "-2":
                case "-3":
                    mProgressDialog.dismiss();
                    displayAlert("app error", false);
                    break;
                case "0":
                    mProgressDialog.dismiss();
                    displayAlert("Review not submitted, please try again after sometime.", false);
                    break;
                case "-1":
                    mProgressDialog.dismiss();
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    try {
                        JSONObject joreviewsubmit = new JSONObject(what);
                        for (int p = 0; p < responseList.get(0).reviews.size(); p++) {
                            if (responseList.get(0).reviews.get(p).getReviewId().equals(joreviewsubmit.getString("reviewId"))) {
                                Reply oreply = new Reply();
                                oreply.setPuid(joreviewsubmit.getString("puid"));
                                oreply.setReplyId(joreviewsubmit.getString("ReplyId"));
                                oreply.setReviewReply(joreviewsubmit.getString("ReviewReply"));
                                oreply.setPuFirstName(joreviewsubmit.getString("puFirstName"));
                                oreply.setPuLastName(joreviewsubmit.getString("puLastName"));
                                oreply.setProfile_pic(joreviewsubmit.getString("profile_pic"));
                                oreply.setReviewId(joreviewsubmit.getString("reviewId"));
                                oreply.setSubmit_date(joreviewsubmit.getString("submit_date"));
                                oreply.setMinutes(joreviewsubmit.getString("Minutes"));
                                oreply.setHours(joreviewsubmit.getString("Hours"));
                                oreply.setDays(joreviewsubmit.getString("Days"));
                                responseList.get(0).reviews.get(p).getReplies().add(oreply);
                            }
                        }

                        runOnUiThread(new Runnable() {
                            @Override
                            public void run() {
                                ll_Reviews_section.removeAllViews();
                                /*ArrayList<Reviews> tempElements = new ArrayList<Reviews>(responseList.get(0).reviews);
                                Collections.reverse(tempElements);
                                stationDetailResponse.reviews = tempElements;*/

                                if (clickedReplyReviewIndex != -1){
                                    int lastRepliesCount = Integer.parseInt(responseList.get(0).reviews.get(clickedReplyReviewIndex).getReplies_count());
                                    responseList.get(0).reviews.get(clickedReplyReviewIndex).setReplies_count(String.valueOf(lastRepliesCount+1));
                                }
                                for (int p = 0; p < responseList.get(0).reviews.size(); p++) {

                                    int indx = 0;
                                    TextView txt_review_user_name, txt_user_review;
                                    ImageView img_user_rv1, img_rating1, img_rating2, img_rating3, img_rating4, img_rating5;
                                    LayoutInflater vi = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    v1 = vi.inflate(R.layout.reviews_view_1, null);
                                    txt_review_user_name = (TextView) v1.findViewById(R.id.txt_review_user_name);
                                    txt_user_review = (TextView) v1.findViewById(R.id.txt_user_review);
                                    img_rating1 = (ImageView) v1.findViewById(R.id.img_rating1_user_review);
                                    img_rating2 = (ImageView) v1.findViewById(R.id.img_rating2_user_review);
                                    img_rating3 = (ImageView) v1.findViewById(R.id.img_rating3_user_review);
                                    img_rating4 = (ImageView) v1.findViewById(R.id.img_rating4_user_review);
                                    img_rating5 = (ImageView) v1.findViewById(R.id.img_rating5_user_review);
                                    img_user_rv1 = (ImageView) v1.findViewById(R.id.img_user_rv1);

                                    txt_review_user_name.setText(responseList.get(0).reviews.get(p).getPuFirstName() + " " + responseList.get(0).reviews.get(p).getPuLastName());
                                    txt_user_review.setText(responseList.get(0).reviews.get(p).getReview());

                                    String user_path = responseList.get(0).reviews.get(p).getProfile_pic().toString();

                                    user_path = user_path.replace("\"[", "[");
                                    user_path = user_path.replace("\"]", "]");
                                    user_path = user_path.replace("\\", "");

                                    if (!user_path.equals("")) {
                                        user_path = "http://owner.parkeee.net/owner/" + user_path;
                                        Picasso.with(img_user_rv1.getContext())
                                                .load(user_path)
                                                .networkPolicy(NetworkPolicy.NO_CACHE)
                                                .transform(new RoundedTransformation(100, 0))
                                                .resize(96, 96).centerCrop().into(img_user_rv1, new Callback() {
                                            @Override
                                            public void onSuccess() {
                                            }

                                            @Override
                                            public void onError() {
                                            }
                                        });
                                    }


                                    if (!responseList.get(0).reviews.get(p).getStars().equals("")) {
                                        if (responseList.get(0).reviews.get(p).getStars().equals("1")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                        } else if (responseList.get(0).reviews.get(p).getStars().equals("2")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                        } else if (responseList.get(0).reviews.get(p).getStars().equals("3")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                        } else if (responseList.get(0).reviews.get(p).getStars().equals("4")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating4.setImageResource(R.drawable.icon_rating_star_active);
                                        } else if (responseList.get(0).reviews.get(p).getStars().equals("5")) {
                                            img_rating1.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating2.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating3.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating4.setImageResource(R.drawable.icon_rating_star_active);
                                            img_rating5.setImageResource(R.drawable.icon_rating_star_active);
                                        }
                                    }

                                    ll_Reviews_section.addView(v1, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                    indx++;
                                    if (!responseList.get(0).reviews.get(p).getOwner_response().equals("")) {
                                        TextView txt_reply_from_owner, txt_owner_reply;
                                        LayoutInflater vi_2 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                        v2 = vi_2.inflate(R.layout.review_view_2, null);
                                        txt_reply_from_owner = (TextView) v2.findViewById(R.id.txt_reply_from_owner);
                                        txt_owner_reply = (TextView) v2.findViewById(R.id.txt_owner_reply);

                                        txt_reply_from_owner.setText("Reply from " + Brand_Name);
                                        txt_owner_reply.setText(responseList.get(0).reviews.get(p).getOwner_response());
                                        ll_Reviews_section.addView(v2, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                        indx++;
                                    }

                                    int review_replies_count = 0;
                                    review_replies_count = Integer.parseInt(responseList.get(0).reviews.get(p).getReplies_count());

                                  /*  ArrayList<Reply> Elements = new ArrayList<Reply>(responseList.get(0).reviews.get(p).getReplies());
                                    Collections.reverse(Elements);
                                    responseList.get(0).reviews.get(p).setReplies(Elements) ;*/

                                    for (int m = 0; m < responseList.get(0).reviews.get(p).getReplies().size(); m++) {
                                        if (m <= 2) {
                                            TextView txt_replies_content, txt_replies_user_name;
                                            ImageView img_user_rv3;
                                            LayoutInflater vi_3 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                            v3 = vi_3.inflate(R.layout.review_view_3, null);
                                            txt_replies_content = (TextView) v3.findViewById(R.id.txt_replies_content);
                                            txt_replies_user_name = (TextView) v3.findViewById(R.id.txt_replies_user_name);
                                            img_user_rv3 = (ImageView) v3.findViewById(R.id.img_user_rv3);
                                            txt_replies_content.setText(responseList.get(0).reviews.get(p).getReplies().get(m).getReviewReply());
                                            txt_replies_user_name.setText(responseList.get(0).reviews.get(p).getReplies().get(m).getPuFirstName() + " " + responseList.get(0).reviews.get(p).getReplies().get(m).getPuLastName());

                                            String user_path_v3 = responseList.get(0).reviews.get(p).getReplies().get(m).getProfile_pic();
                                            user_path_v3 = user_path_v3.replace("\"[", "[");
                                            user_path_v3 = user_path_v3.replace("\"]", "]");
                                            user_path_v3 = user_path_v3.replace("\\", "");

                                            if (!user_path_v3.equals("")) {
                                                user_path_v3 = "http://owner.parkeee.net/owner/" + user_path_v3;
                                                Picasso.with(img_user_rv3.getContext())
                                                        .load(user_path_v3)
                                                        .networkPolicy(NetworkPolicy.NO_CACHE)
                                                        .transform(new RoundedTransformation(100, 0))
                                                        .resize(96, 96).centerCrop().into(img_user_rv3, new Callback() {
                                                    @Override
                                                    public void onSuccess() {
                                                    }

                                                    @Override
                                                    public void onError() {
                                                    }
                                                });
                                            }


                                            ll_Reviews_section.addView(v3, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
                                            indx++;
                                        }
                                    }

                                    final LinearLayout ll_reply, ll_reply_submit;
                                    final Button btnReply_review, btnReply_review_submit, btn_review_cancel;
                                    final EditText et_review_submit;
                                    final TextView txt_review_count;

                                    LayoutInflater vi_4 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
                                    v4 = vi_4.inflate(R.layout.review_view_4, null);
                                    ll_reply = (LinearLayout) v4.findViewById(R.id.ll_reply);
                                    ll_reply_submit = (LinearLayout) v4.findViewById(R.id.ll_reply_submit);
                                    btnReply_review = (Button) v4.findViewById(R.id.btnReply_review);
                                    btnReply_review_submit = (Button) v4.findViewById(R.id.btnReply_review_submit);
                                    btnReply_review_submit.setTag(p);
                                    et_review_submit = (EditText) v4.findViewById(R.id.et_review_submit);

                                    txt_review_count = (TextView) v4.findViewById(R.id.txt_review_count);

                                    txt_review_count.setTag(p);
                                    txt_review_count.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            Reviews review = responseList.get(0).reviews.get(Integer.parseInt(v.getTag().toString()));

                                            Intent intnt = new Intent(ActivityGasStationDetail.this, ActivityGasStationReplies.class);
                                            intnt.putExtra("Review_Id", review.getReviewId());
                                            intnt.putExtra("User_Name", review.getPuFirstName()+" "+review.getPuLastName());
                                            intnt.putExtra("User_Review", review.getReview());
                                            intnt.putExtra("User_Image", review.getProfile_pic());
                                            intnt.putExtra("User_Rating", review.getStars());
                                            intnt.putExtra("submit_date", review.getSubmit_date());
                                            startActivity(intnt);
                                        }
                                    });

                                    if (review_replies_count > 3) {
                                        txt_review_count.setVisibility(View.VISIBLE);
                                        txt_review_count.setText(review_replies_count + " Replies");
                                    }

                                    btnReply_review.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            ll_reply.setVisibility(View.GONE);
                                            ll_reply_submit.setVisibility(View.VISIBLE);
                                        }
                                    });

                                    btnReply_review_submit.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View view) {
                                            if(!et_review_submit.getText().toString().trim().equals("")){
                                                clickedReplyReviewIndex = Integer.parseInt(btnReply_review_submit.getTag().toString());
                                                String puid = Utils.getPuid(mContext);
                                                SubmitReviewReply osubmitbyuser = new SubmitReviewReply();
                                                osubmitbyuser.setReplyId("0");
                                                osubmitbyuser.setPuid(puid);
                                                osubmitbyuser.setReviewReply(et_review_submit.getText().toString().trim());
                                                osubmitbyuser.setReviewId(responseList.get(0).reviews.get(clickedReplyReviewIndex).getReviewId());
                                                String jsonString = oGson.toJson(osubmitbyuser, SubmitReviewReply.class).toString();
                                                mProgressDialog.show();
                                                //Post oInsertUpdateApi Api
                                                PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                                                oInsertUpdateApi.setRequestSource(RequestSource.SaveGasStationReviewReply);
                                                oInsertUpdateApi.executePostRequest(API.fSaveGasStationReviewReply(), jsonString);
                                                ll_reply.setVisibility(View.VISIBLE);
                                                ll_reply_submit.setVisibility(View.GONE);
                                            }else{
                                                txt_custom_dialog.setText("Empty reply can't be added.");
                                                mdialog.show();
                                            }
                                        }
                                    });

                                    btn_review_cancel = (Button) v4.findViewById(R.id.btn_review_cancel);
                                    btn_review_cancel.setOnClickListener(new View.OnClickListener() {
                                        @Override
                                        public void onClick(View v) {
                                            clickedReplyReviewIndex = -1;
                                            ll_reply.setVisibility(View.VISIBLE);
                                            ll_reply_submit.setVisibility(View.GONE);
                                        }
                                    });

                                    ll_Reviews_section.addView(v4, indx, new ViewGroup.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));

                                }
                            }
                        });

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                    break;
            }

        } else if (from.toString().equalsIgnoreCase("SaveDealLikeDislike")) {

            switch (what) {
                case "-2":
                case "-3":
                    mProgressDialog.dismiss();
                    displayAlert("app error", false);
                    break;
                case "0":
                    mProgressDialog.dismiss();
                    displayAlert("Review not submitted, please try again after sometime.", false);
                    break;
                case "-1":
                    mProgressDialog.dismiss();
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:

                    break;
            }
        } else if (from.toString().equalsIgnoreCase("GSFavourite")) {

            switch (what) {
                case "-2":
                case "-3":
                    mProgressDialog.dismiss();
                    displayAlert("app error", false);
                    break;
                case "0":
                    mProgressDialog.dismiss();
                    displayAlert("Review not submitted, please try again after sometime.", false);
                    break;
                case "-1":
                    mProgressDialog.dismiss();
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            img_fav_gs_detail.setImageResource(R.drawable.icon_gs_detail_fav_active);
                            img_fav_gs_detail.setTag("1");

                            GSFavouriteTb oGSFavouriteTb = new GSFavouriteTb();
                            oGSFavouriteTb.setBrandName(stationDetailResponse.getBrandName().toString());
                            oGSFavouriteTb.setGsID(stationDetailResponse.getGSId().toString());
                            oGSFavouriteTb.setLat(stationDetailResponse.getLatitude().toString());
                            oGSFavouriteTb.setLng(stationDetailResponse.getLongitude().toString());
                            oGSFavouriteTb.setStreet(stationDetailResponse.getStreet().toString());
                            oGSFavouriteTb.setCity(stationDetailResponse.getCity().toString());
                            oGSFavouriteTb.setZip(stationDetailResponse.getZip().toString());
                            oGSFavouriteTb.setPricePer(stationDetailResponse.getPricePerGallonInDoller().toString());
                            oGSFavouriteTb.setCoffeeDeal("1");
                            oGSFavouriteTb.setLastUpdateOn(stationDetailResponse.getLastUpdateOn().toString());
                            oGSFavouriteTb.setPkg(stationDetailResponse.getPackageName().toString());
                            oGSFavouriteTb.setFtid(FTID);
                            // Persist your data easily
                            realm.beginTransaction();
                            realm.copyToRealm(oGSFavouriteTb);
                            realm.commitTransaction();
                            // displayAlert("Inserted Successfully",false);
                        }
                    });

                    break;
            }
        } else if (from.toString().equalsIgnoreCase("GSFavouriteRemove")) {
            switch (what) {
                case "-2":
                case "-3":
                    mProgressDialog.dismiss();
                    displayAlert("app error", false);
                    break;
                case "0":
                    mProgressDialog.dismiss();
                    displayAlert("Review not submitted, please try again after sometime.", false);
                    break;
                case "-1":
                    mProgressDialog.dismiss();
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            img_fav_gs_detail.setImageResource(R.drawable.icon_gs_detail_fav_default);
                            img_fav_gs_detail.setTag("0");
                            RealmController oRealmController = new RealmController((Application) mContext.getApplicationContext());
                            oRealmController.removeFavStations(String.valueOf(stationDetailResponse.getGSId()));
                        }
                    });

                    break;
            }
        }
    }

    public static double round(double value, int places) {
        if (places < 0) throw new IllegalArgumentException();

        long factor = (long) Math.pow(10, places);
        value = value * factor;
        long tmp = Math.round(value);
        return (double) tmp / factor;
    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityGasStationDetail.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }


    private void setStarsRating() {

        if (stationDetailResponse.getStars().equals("1")) {
            img_rating_star_1.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_1.setTag("1");

            img_rating_star_2.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_2.setTag("0");
            img_rating_star_3.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_3.setTag("0");
            img_rating_star_4.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_4.setTag("0");
            img_rating_star_5.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_5.setTag("0");
        } else if (stationDetailResponse.getStars().equals("2")) {
            img_rating_star_1.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_2.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_1.setTag("1");
            img_rating_star_2.setTag("1");

            img_rating_star_3.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_3.setTag("0");
            img_rating_star_4.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_4.setTag("0");
            img_rating_star_5.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_5.setTag("0");
        } else if (stationDetailResponse.getStars().equals("3")) {
            img_rating_star_1.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_2.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_3.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_1.setTag("1");
            img_rating_star_2.setTag("1");
            img_rating_star_3.setTag("1");

            img_rating_star_4.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_4.setTag("0");
            img_rating_star_5.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_5.setTag("0");

        } else if (stationDetailResponse.getStars().equals("4")) {
            img_rating_star_1.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_2.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_3.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_4.setImageResource(R.drawable.icon_rating_star_active);

            img_rating_star_1.setTag("1");
            img_rating_star_2.setTag("1");
            img_rating_star_3.setTag("1");
            img_rating_star_4.setTag("1");

            img_rating_star_5.setImageResource(R.drawable.icon_rating_star_default);
            img_rating_star_5.setTag("0");

        } else if (stationDetailResponse.getStars().equals("5")) {
            img_rating_star_1.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_2.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_3.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_4.setImageResource(R.drawable.icon_rating_star_active);
            img_rating_star_5.setImageResource(R.drawable.icon_rating_star_active);

            img_rating_star_1.setTag("1");
            img_rating_star_2.setTag("1");
            img_rating_star_3.setTag("1");
            img_rating_star_4.setTag("1");
            img_rating_star_5.setTag("1");
        }

    }


    @Override
    public void onDealClick(final int pos, final Deal deal) {
        fl_deals_detail.setVisibility(View.VISIBLE);
        ll_deals_detail_2.setVisibility(View.VISIBLE);
        txt_deal_name.setText(deal.getDeal().toString());
        txt_description_detail.setText(deal.getDealDetail().toString());
        txt_termandcond_detail.setText(deal.getDealTerms().toString());
        txt_expires_detail.setText(deal.getEndDate().toString());
        Deal_Id = deal.getDealId();
        txtDealCode.setText(deal.getAccessCode());

        img_like_deal.setImageResource(R.drawable.like_check_white);
        txt_like_deal.setText("0");
        txt_like_deal.setTextColor(getResources().getColor(R.color.white));
        ll_like_deal.setTag("0");

        img_dislike_deal.setImageResource(R.drawable.like_cross_white);
        txt_dislike_deal.setText("0");
        txt_dislike_deal.setTextColor(getResources().getColor(R.color.white));
        ll_dislike_deal.setTag("0");


        if (deal.getIsLike().equals(1)) {
            img_like_deal.setImageResource(R.drawable.like_check_green);
            txt_like_deal.setText("1");
            txt_like_deal.setTextColor(getResources().getColor(R.color.green));
            ll_like_deal.setTag("1");
        }
        if (deal.getIsDisLikes().equals(1)) {
            img_dislike_deal.setImageResource(R.drawable.like_cross_red);
            txt_dislike_deal.setText("1");
            txt_dislike_deal.setTextColor(getResources().getColor(R.color.red));
            ll_dislike_deal.setTag("1");
        }


        ViewTreeObserver vto = imgBC.getViewTreeObserver();
        vto.addOnPreDrawListener(new ViewTreeObserver.OnPreDrawListener() {
            public boolean onPreDraw() {
                imgBC.getViewTreeObserver().removeOnPreDrawListener(this);
                int finalHeight = imgBC.getMeasuredHeight();
                int finalWidth = imgBC.getMeasuredWidth();
                System.out.println("Height: " + finalHeight + " Width: " + finalWidth);
                try {
                    imgBC.setImageBitmap(BarCodeUtils.genrateBarCode(deal.getAccessCode(), finalWidth, finalHeight));

                } catch (Exception e) {
                    e.printStackTrace();

                }
                return true;
            }
        });


        ll_like_deal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_like_deal.getTag().equals("0")) {
                    img_like_deal.setImageResource(R.drawable.like_check_green);
                    txt_like_deal.setText("1");
                    txt_like_deal.setTextColor(getResources().getColor(R.color.green));
                    ll_like_deal.setTag("1");

                    responseList.get(0).deals.get(pos).setIsLike(1);
                    responseList.get(0).deals.get(pos).setIsDisLikes(0);

                    img_dislike_deal.setImageResource(R.drawable.like_cross_white);
                    txt_dislike_deal.setText("0");
                    txt_dislike_deal.setTextColor(getResources().getColor(R.color.white));
                    ll_dislike_deal.setTag("0");


                    String puid = Utils.getPuid(mContext);
                    SaveDeal oSaveDeal = new SaveDeal();
                    oSaveDeal.setDealId(Deal_Id);
                    oSaveDeal.setIsDefault("1");
                    oSaveDeal.setIsLike("1");
                    oSaveDeal.setIsDislike("0");
                    oSaveDeal.setPuid(puid);
                    String jsonString = oGson.toJson(oSaveDeal, SaveDeal.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.SaveDealLikeDislike);
                    oInsertUpdateApi.executePostRequest(API.fSaveDealLikeDislike(), jsonString);


                } else {

                    responseList.get(0).deals.get(pos).setIsLike(0);

                    img_like_deal.setImageResource(R.drawable.like_check_white);
                    txt_like_deal.setText("0");
                    txt_like_deal.setTextColor(getResources().getColor(R.color.white));
                    ll_like_deal.setTag("0");


                    String puid = Utils.getPuid(mContext);
                    SaveDeal oSaveDeal = new SaveDeal();
                    oSaveDeal.setDealId(Deal_Id);
                    oSaveDeal.setIsDefault("1");
                    oSaveDeal.setIsLike("0");
                    oSaveDeal.setIsDislike("0");
                    oSaveDeal.setPuid(puid);
                    String jsonString = oGson.toJson(oSaveDeal, SaveDeal.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.SaveDealLikeDislike);
                    oInsertUpdateApi.executePostRequest(API.fSaveDealLikeDislike(), jsonString);

                }
            }
        });

        ll_dislike_deal.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (ll_dislike_deal.getTag().equals("0")) {
                    img_dislike_deal.setImageResource(R.drawable.like_cross_red);
                    txt_dislike_deal.setText("1");
                    txt_dislike_deal.setTextColor(getResources().getColor(R.color.red));
                    ll_dislike_deal.setTag("1");

                    responseList.get(0).deals.get(pos).setIsDisLikes(1);
                    responseList.get(0).deals.get(pos).setIsLike(0);

                    img_like_deal.setImageResource(R.drawable.like_check_white);
                    txt_like_deal.setText("0");
                    txt_like_deal.setTextColor(getResources().getColor(R.color.white));
                    ll_like_deal.setTag("0");


                    String puid = Utils.getPuid(mContext);
                    SaveDeal oSaveDeal = new SaveDeal();
                    oSaveDeal.setDealId(Deal_Id);
                    oSaveDeal.setIsDefault("1");
                    oSaveDeal.setIsLike("0");
                    oSaveDeal.setIsDislike("1");
                    oSaveDeal.setPuid(puid);
                    String jsonString = oGson.toJson(oSaveDeal, SaveDeal.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.SaveDealLikeDislike);
                    oInsertUpdateApi.executePostRequest(API.fSaveDealLikeDislike(), jsonString);

                } else {
                    responseList.get(0).deals.get(pos).setIsDisLikes(0);

                    img_dislike_deal.setImageResource(R.drawable.like_cross_white);
                    txt_dislike_deal.setText("0");
                    txt_dislike_deal.setTextColor(getResources().getColor(R.color.white));
                    ll_dislike_deal.setTag("0");

                    String puid = Utils.getPuid(mContext);
                    SaveDeal oSaveDeal = new SaveDeal();
                    oSaveDeal.setDealId(Deal_Id);
                    oSaveDeal.setIsDefault("1");
                    oSaveDeal.setIsLike("0");
                    oSaveDeal.setIsDislike("0");
                    oSaveDeal.setPuid(puid);
                    String jsonString = oGson.toJson(oSaveDeal, SaveDeal.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.SaveDealLikeDislike);
                    oInsertUpdateApi.executePostRequest(API.fSaveDealLikeDislike(), jsonString);
                }
            }
        });


    }

    class CustomViewPagerAdapter extends FragmentStatePagerAdapter {
        List<Deal> lst;

        public CustomViewPagerAdapter(FragmentManager fm, List<Deal> lst) {
            super(fm);
            this.lst = lst;
        }

        public Fragment getItem(int arg0) {
            DealzFragment dealzFragment = new DealzFragment();
            Bundle b = new Bundle();
            b.putInt("position", arg0);
            b.putSerializable("dealinfo", lst.get(arg0));
            dealzFragment.setArguments(b);
            return dealzFragment;
        }

        public int getCount() {
            return lst.size();
        }
    }


    private void shareGasStationInfo() {
        String encoded_string = "";
        String obj = responseList.get(0).getGSIdEnc();
        try {
            encoded_string = URLEncoder.encode(obj, "utf-8");
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
        }

        StringBuilder shareBody = new StringBuilder();
        shareBody.append("Gas Station info:\n\n").
                append(responseList.get(0).getBrandName()).append("\n").
                append(responseList.get(0).getStreet() + ", " + responseList.get(0).getZip()).append("\n").
                append("Contact: " + responseList.get(0).getBusinessPhone()).append("\n\n").
                append("Prices: ").append("\n").
                append("Regular: $" + responseList.get(0).prices.getRegularGasPrice()).append("\n").
                append("Midgrade: $" + responseList.get(0).prices.getMidgradePrice()).append("\n").
                append("Premium: $" + responseList.get(0).prices.getPremiumPrice()).append("\n").
                append("Diesel: $" + responseList.get(0).prices.getDieselFuelPrice()).append("\n\n").
                append("https://parkeee.com/search-result?q=" + encoded_string).append("\n\n").
                append("Shared via Parkeee-" + " A Driver's Best Friends");

        Intent sharingIntent = new Intent(Intent.ACTION_SEND);
        sharingIntent.setType("text/plain");
        sharingIntent.putExtra(Intent.EXTRA_SUBJECT, "Shared via Parkeee");
        sharingIntent.putExtra(Intent.EXTRA_TEXT, shareBody.toString());
        startActivity(Intent.createChooser(sharingIntent, "Share Gas Station Details"));

    }

    void callStationDialog() {

        try {
            Intent callIntent = new Intent(Intent.ACTION_CALL);
            callIntent.setData(Uri.parse("tel:" + stationDetailResponse.getBusinessPhone()));
            startActivity(callIntent);

        } catch (ActivityNotFoundException activityException) {
        }
    }


    @Override
    public void onClick(View view) {

        switch (view.getId()) {

            case R.id.txt_viewall_gs_replies:
                Intent intnt= new Intent(ActivityGasStationDetail.this,ActivityGsReview.class);
                intnt.putExtra("GSId",String.valueOf(stationDetailResponse.getGSId()));
                startActivity(intnt);
                break;

            case R.id.img_share_gs_detail:
                shareGasStationInfo();
                break;

            case R.id.img_call_gs_detail:
                callStationDialog();
                break;

            case R.id.img_fav_gs_detail:

                if (img_fav_gs_detail.getTag().equals("0")) {
                    String puid = Utils.getPuid(mContext);
                    GSFavourite oSGSFavourite = new GSFavourite();
                    oSGSFavourite.setGsid(String.valueOf(stationDetailResponse.getGSId()));
                    oSGSFavourite.setPuid(puid);
                    String jsonString = oGson.toJson(oSGSFavourite, GSFavourite.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.GSFavourite);
                    oInsertUpdateApi.executePostRequest(API.fAddGSInFav(), jsonString);
                } else {
                    String puid = Utils.getPuid(mContext);
                    GSFavourite oSGSFavourite = new GSFavourite();
                    oSGSFavourite.setGsid(String.valueOf(stationDetailResponse.getGSId()));
                    oSGSFavourite.setPuid(puid);
                    String jsonString = oGson.toJson(oSGSFavourite, GSFavourite.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.GSFavouriteRemove);
                    oInsertUpdateApi.executePostRequest(API.fRemoveGSFromFav(), jsonString);
                }

                break;

            case R.id.ll_Done_deal_detail:
                fl_deals_detail.setVisibility(View.GONE);
                break;

            case R.id.ll_deals_detail_2:
                fl_deals_detail.setVisibility(View.GONE);
                ll_deals_detail_2.setVisibility(View.GONE);
                break;

            case R.id.img_back_gas_station_detail:
                this.finish();
                break;

            case R.id.img_gs_icon:

                break;

            case R.id.ll_last_updated:

                if (ll_last_updated.getTag().equals("0")) {
                    ll_expand_gas_station_price.setVisibility(View.VISIBLE);
                    ll_last_updated.setTag("1");
                } else {
                    ll_expand_gas_station_price.setVisibility(View.GONE);
                    ll_last_updated.setTag("0");
                }
                break;


            case R.id.img_rating_star_1:
                Rating = 1;
                setRatingStar(1);
                break;
            case R.id.img_rating_star_2:
                Rating = 2;
                setRatingStar(2);
                break;
            case R.id.img_rating_star_3:
                Rating = 3;
                setRatingStar(3);
                break;
            case R.id.img_rating_star_4:
                Rating = 4;
                setRatingStar(4);
                break;
            case R.id.img_rating_star_5:
                Rating = 5;
                setRatingStar(5);
                break;

            case R.id.btn_submit_rating_review:
                String puid = Utils.getPuid(mContext);
                SubmitReview osubmitbyuser = new SubmitReview();
                osubmitbyuser.setGSId(String.valueOf(stationDetailResponse.getGSId()));
                osubmitbyuser.setPuid(puid);
                osubmitbyuser.setReview(et_user_review.getText().toString().trim());
                osubmitbyuser.setStars(String.valueOf(Rating));

                String jsonString = oGson.toJson(osubmitbyuser, SubmitReview.class).toString();
                mProgressDialog.show();
                //Post oInsertUpdateApi Api
                PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                oInsertUpdateApi.setRequestSource(RequestSource.SaveGSRatingByUser);
                oInsertUpdateApi.executePostRequest(API.fSaveGSRatingByUser(), jsonString);


                break;
            case R.id.btn_cancel_rating_review:
                ll_submit_review.setVisibility(View.GONE);
                setStarsRating();

                break;


            default:
                break;
        }
    }

    private void setRatingStar(int rating) {

        ArrayList<ImageView> imageViewsArrList = new ArrayList<ImageView>();
        imageViewsArrList.add(img_rating_star_1);
        imageViewsArrList.add(img_rating_star_2);
        imageViewsArrList.add(img_rating_star_3);
        imageViewsArrList.add(img_rating_star_4);
        imageViewsArrList.add(img_rating_star_5);


        for (int i = 0; i < 5; i++) {
            if (i < rating) {
                imageViewsArrList.get(i).setImageResource(R.drawable.icon_rating_star_active);
            } else {
                imageViewsArrList.get(i).setImageResource(R.drawable.icon_rating_star_default);
            }
        }

        ll_submit_review.setVisibility(View.VISIBLE);
        sv_parent.post(new Runnable() {
            public void run() {
                sv_parent.smoothScrollTo(0, ll_start_rating.getTop());
            }
        });
    }

    /**
     * ATTENTION: This was auto-generated to implement the App Indexing API.
     * See https://g.co/AppIndexing/AndroidStudio for more information.
     */
    public Action getIndexApiAction() {
        Thing object = new Thing.Builder()
                .setName("ActivityGasStationDetail Page") // TODO: Define a title for the content shown.
                // TODO: Make sure this auto-generated URL is correct.
                .setUrl(Uri.parse("http://[ENTER-YOUR-URL-HERE]"))
                .build();
        return new Action.Builder(Action.TYPE_VIEW)
                .setObject(object)
                .setActionStatus(Action.STATUS_TYPE_COMPLETED)
                .build();
    }

    /*@Override
    public void onStart() {
        super.onStart();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        client.connect();
        AppIndex.AppIndexApi.start(client, getIndexApiAction());
    }

    @Override
    public void onStop() {
        super.onStop();

        // ATTENTION: This was auto-generated to implement the App Indexing API.
        // See https://g.co/AppIndexing/AndroidStudio for more information.
        AppIndex.AppIndexApi.end(client, getIndexApiAction());
        client.disconnect();
    }*/


    public class RoundedTransformation implements
            Transformation {
        private final int radius;
        private final int margin; // dp

        // radius is corner radii in dp
        // margin is the board in dp
        public RoundedTransformation(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        @Override
        public Bitmap transform(final Bitmap source) {
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP,
                    Shader.TileMode.CLAMP));
            Bitmap output = Bitmap.createBitmap(source.getWidth(),
                    source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            canvas.drawRoundRect(new RectF(margin, margin, source.getWidth()
                    - margin, source.getHeight() - margin), radius, radius, paint);
            if (source != output) {
                source.recycle();
            }
            return output;
        }

        @Override
        public String key() {
            return "rounded";
        }
    }
}
