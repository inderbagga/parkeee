package com.parkeee.android.gas;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Application;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.ActivityVerification;
import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.menu.ActivityAddCar;
import com.parkeee.android.menu.ActivityMyCars;
import com.parkeee.android.model.params.DeleteCarPUID;
import com.parkeee.android.model.params.Reply;
import com.parkeee.android.model.params.Reviews;
import com.parkeee.android.model.params.SubmitReviewReply;
import com.parkeee.android.model.tables.AddNewCarTb;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Utils;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.RealmResults;

/**
 * Created by myoffice on 12/25/2016.
 */

public class ActivityGasStationReplies extends Activity implements View.OnClickListener, OnResultReceived {

    ImageView img_user_gs_replies, img_rating1_gs_replies, img_rating2_gs_replies, img_rating3_gs_replies, img_rating4_gs_replies,
            img_rating5_gs_replies, img_back_gas_station_replies;
    TextView txt_user_name_gs_replies, txt_user_review_gs_replies, txt_date_gs_replies;
    ListView lv_gs_replies;

    OnResultReceived mOnResultReceived;
    ProgressDialog mProgressDialog;
    Context mContext;
    String ServerResult, Review_Id, User_Name, User_Review, User_Image;
    int User_Rating;

    ArrayList<Reply> ar_review = new ArrayList<Reply>();

    Reviews review;

    View footerView;

    Gson oGson;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gasstation_replies);
        InitializeUserInterface();
    }

    private void InitializeUserInterface() {
        oGson = new Gson();
        mContext = this;
        mOnResultReceived = this;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        img_user_gs_replies = (ImageView) findViewById(R.id.img_user_gs_replies);
        img_rating1_gs_replies = (ImageView) findViewById(R.id.img_rating1_gs_replies);
        img_rating2_gs_replies = (ImageView) findViewById(R.id.img_rating2_gs_replies);
        img_rating3_gs_replies = (ImageView) findViewById(R.id.img_rating3_gs_replies);
        img_rating4_gs_replies = (ImageView) findViewById(R.id.img_rating4_gs_replies);
        img_rating5_gs_replies = (ImageView) findViewById(R.id.img_rating5_gs_replies);
        img_back_gas_station_replies = (ImageView) findViewById(R.id.img_back_gas_station_replies);

        img_back_gas_station_replies.setOnClickListener(this);

        txt_user_name_gs_replies = (TextView) findViewById(R.id.txt_user_name_gs_replies);
        txt_user_review_gs_replies = (TextView) findViewById(R.id.txt_user_review_gs_replies);
        txt_date_gs_replies = (TextView) findViewById(R.id.txt_date_gs_replies);

        lv_gs_replies = (ListView) findViewById(R.id.lv_gs_replies);

        final LinearLayout ll_reply, ll_reply_submit;
        final Button btnReply_review, btnReply_review_submit, btn_review_cancel;
        final EditText et_review_submit;

        LayoutInflater vi_4 = (LayoutInflater) getApplicationContext().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        footerView = vi_4.inflate(R.layout.review_view_4, null);

        ll_reply = (LinearLayout) footerView.findViewById(R.id.ll_reply);
        ll_reply_submit = (LinearLayout) footerView.findViewById(R.id.ll_reply_submit);
        btnReply_review = (Button) footerView.findViewById(R.id.btnReply_review);
        btnReply_review_submit = (Button) footerView.findViewById(R.id.btnReply_review_submit);
        et_review_submit = (EditText) footerView.findViewById(R.id.et_review_submit);
        btnReply_review.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                ll_reply.setVisibility(View.GONE);
                ll_reply_submit.setVisibility(View.VISIBLE);
            }
        });

        btnReply_review_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                String puid = Utils.getPuid(mContext);
                SubmitReviewReply osubmitbyuser = new SubmitReviewReply();
                osubmitbyuser.setReplyId("0");
                osubmitbyuser.setPuid(puid);
                osubmitbyuser.setReviewReply(et_review_submit.getText().toString().trim());
                osubmitbyuser.setReviewId(Review_Id);
                String jsonString = oGson.toJson(osubmitbyuser, SubmitReviewReply.class).toString();
                mProgressDialog.show();
                //Post oInsertUpdateApi Api
                PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                oInsertUpdateApi.setRequestSource(RequestSource.SaveGasStationReviewReply);
                oInsertUpdateApi.executePostRequest(API.fSaveGasStationReviewReply(), jsonString);
                ll_reply.setVisibility(View.VISIBLE);
                ll_reply_submit.setVisibility(View.GONE);

            }
        });

        btn_review_cancel = (Button) footerView.findViewById(R.id.btn_review_cancel);
        btn_review_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                ll_reply.setVisibility(View.VISIBLE);
                ll_reply_submit.setVisibility(View.GONE);
            }
        });


        Intent intnt = getIntent();
        if (intnt != null) {
            Review_Id = intnt.getStringExtra("Review_Id");
            User_Name = intnt.getStringExtra("User_Name");
            User_Review = intnt.getStringExtra("User_Review");
            User_Image = intnt.getStringExtra("User_Image");
            User_Rating = Integer.parseInt(intnt.getStringExtra("User_Rating"));

            txt_user_name_gs_replies.setText(User_Name);
            txt_user_review_gs_replies.setText(User_Review);

            if (!User_Image.equals("")) {
                User_Image = "http://owner.parkeee.net/owner/" + User_Image;
                Picasso.with(img_user_gs_replies.getContext())
                        .load(User_Image)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .transform(new RoundedTransformation(100, 0))
                        .resize(96, 96).centerCrop().into(img_user_gs_replies, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });
            }

            if (User_Rating < 1) {
            } else if (User_Rating <= 1) {
                img_rating1_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
            } else if (User_Rating <= 2) {
                img_rating1_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating2_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
            } else if (User_Rating <= 3) {
                img_rating1_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating2_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating3_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
            } else if (User_Rating <= 4) {
                img_rating1_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating2_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating3_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating4_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
            } else if (User_Rating <= 5) {
                img_rating1_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating2_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating3_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating4_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
                img_rating5_gs_replies.setImageResource(R.drawable.icon_rating_star_active);
            }


            String Url = API.fGetReviewRepliesByReviewId();
            Url = Url.concat(Review_Id);
            mProgressDialog.show();
            GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
            oInsertUpdateApi.setRequestSource(RequestSource.GetReviewRepliesByReviewId);
            oInsertUpdateApi.executeGetRequest(Url);
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_back_gas_station_replies:

                this.finish();

                break;

            default:
                break;
        }

    }

    public class RoundedTransformation implements
            Transformation {
        private final int radius;
        private final int margin; // dp

        // radius is corner radii in dp
        // margin is the board in dp
        public RoundedTransformation(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        @Override
        public Bitmap transform(final Bitmap source) {
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP,
                    Shader.TileMode.CLAMP));
            Bitmap output = Bitmap.createBitmap(source.getWidth(),
                    source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            canvas.drawRoundRect(new RectF(margin, margin, source.getWidth()
                    - margin, source.getHeight() - margin), radius, radius, paint);
            if (source != output) {
                source.recycle();
            }
            return output;
        }

        @Override
        public String key() {
            return "rounded";
        }
    }

    @Override
    public void dispatchString(RequestSource from, final String what) {
        mProgressDialog.dismiss();
        if (from.toString().equalsIgnoreCase("GetReviewRepliesByReviewId")) {
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ServerResult = what.replace("\"[", "[");
                            ServerResult = ServerResult.replace("\"]", "]");
                            ServerResult = ServerResult.replace("\\", "");
                            try {
                                JSONArray Ja_replies = new JSONArray(ServerResult);
                                for (int j = 0; j < Ja_replies.length(); j++) {
                                    Reply oreviews = new Reply();
                                    oreviews.setPuid(Ja_replies.getJSONObject(j).getString("puid"));
                                    oreviews.setPuFirstName(Ja_replies.getJSONObject(j).getString("FirstName"));
                                    oreviews.setPuLastName(Ja_replies.getJSONObject(j).getString("LastName"));
                                    oreviews.setReviewReply(Ja_replies.getJSONObject(j).getString("Review"));
                                    oreviews.setProfile_pic(Ja_replies.getJSONObject(j).getString("profile_pic"));
                                    oreviews.setSubmit_date(Ja_replies.getJSONObject(j).getString("submit_date"));
                                    oreviews.setReviewId(Ja_replies.getJSONObject(j).getString("ReviewId"));
                                    oreviews.setReplyId(Ja_replies.getJSONObject(j).getString("ReplyId"));
                                    oreviews.setMinutes(Ja_replies.getJSONObject(j).getString("Minutes"));
                                    oreviews.setHours(Ja_replies.getJSONObject(j).getString("Hours"));
                                    oreviews.setDays(Ja_replies.getJSONObject(j).getString("Days"));
                                    ar_review.add(oreviews);
                                }
                                lv_gs_replies.setAdapter(new ReviewRepliesAdapter(ar_review));
                                lv_gs_replies.addFooterView(footerView);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                    break;
            }
        } else if(from.toString().equalsIgnoreCase("SaveGasStationReviewReply")){
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ServerResult = what.replace("\"[", "[");
                            ServerResult = ServerResult.replace("\"]", "]");
                            ServerResult = ServerResult.replace("\\", "");
                            try {
                                JSONObject Ja_replies = new JSONObject(ServerResult);
                                Reply oreviews = new Reply();
                                oreviews.setPuid(Ja_replies.getString("puid"));
                                oreviews.setPuFirstName(Ja_replies.getString("puFirstName"));
                                oreviews.setPuLastName(Ja_replies.getString("puLastName"));
                                oreviews.setReviewReply(Ja_replies.getString("ReviewReply"));
                                oreviews.setProfile_pic(Ja_replies.getString("profile_pic"));
                                oreviews.setSubmit_date(Ja_replies.getString("submit_date"));
                                oreviews.setReviewId(Ja_replies.getString("reviewId"));
                                oreviews.setReplyId(Ja_replies.getString("ReplyId"));
                                oreviews.setMinutes(Ja_replies.getString("Minutes"));
                                oreviews.setHours(Ja_replies.getString("Hours"));
                                oreviews.setDays(Ja_replies.getString("Days"));
                                ar_review.add(oreviews);
                                lv_gs_replies.setAdapter(new ReviewRepliesAdapter(ar_review));
                                // lv_gs_replies.addFooterView(footerView);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });
                    break;
            }
        }
    }

    public class ReviewRepliesAdapter extends BaseAdapter {
        ArrayList<Reply> ar_list;

        public ReviewRepliesAdapter(ArrayList<Reply> Ar_List) {
            ar_list = Ar_List;
        }

        @Override
        public int getCount() {
            return ar_list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {
            LayoutInflater inflater;
            ImageView img_user_rv3;
            TextView txt_replies_user_name,txt_replies_content;
            String User_Image_reply="";
            inflater = LayoutInflater.from(getApplicationContext());
            convertView = inflater.inflate(R.layout.review_view_3, null);
            txt_replies_user_name=(TextView)convertView.findViewById(R.id.txt_replies_user_name);
            txt_replies_content=(TextView)convertView.findViewById(R.id.txt_replies_content);
            img_user_rv3=(ImageView)convertView.findViewById(R.id.img_user_rv3);

            txt_replies_user_name.setText(ar_list.get(position).getPuFirstName()+" "+ar_list.get(position).getPuLastName());
            txt_replies_content.setText(ar_list.get(position).getReviewReply());
            User_Image_reply=ar_list.get(position).getProfile_pic();
            if (!User_Image_reply.equals("")) {
                User_Image_reply = "http://owner.parkeee.net/owner/" + User_Image_reply;
                Picasso.with(img_user_rv3.getContext())
                        .load(User_Image_reply)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .transform(new RoundedTransformation(100, 0))
                        .resize(96, 96).centerCrop().into(img_user_rv3, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });
            }

            return convertView;
        }
    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityGasStationReplies.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }
}
