package com.parkeee.android.gas;
import com.parkeee.android.model.params.Deal;


public interface DealClickListener {

    public void onDealClick(int pos, Deal deal);
}
