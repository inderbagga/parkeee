package com.parkeee.android.gas;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapShader;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.RectF;
import android.graphics.Shader;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.parkeee.android.R;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.Reply;
import com.parkeee.android.model.params.Reviews;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.utils.AppMessages;
import com.squareup.picasso.Callback;
import com.squareup.picasso.NetworkPolicy;
import com.squareup.picasso.Picasso;
import com.squareup.picasso.Transformation;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;

/**
 * Created by user on 30-12-2016.
 */

public class ActivityGsReview extends Activity implements View.OnClickListener,OnResultReceived {

    ListView lv_all_reviews;

    OnResultReceived mOnResultReceived;
    ProgressDialog mProgressDialog;
    Context mContext;
    String ServerResult;
    ImageView img_back_gas_station_reviews;

    ArrayList<Reviews>olist_reviews= new ArrayList<Reviews>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_gs_reviews);
        InitializeInterface();
    }

    private void InitializeInterface() {
        img_back_gas_station_reviews = (ImageView) findViewById(R.id.img_back_gas_station_reviews);
        img_back_gas_station_reviews.setOnClickListener(this);
        lv_all_reviews=(ListView)findViewById(R.id.lv_all_reviews);
        mContext = this;
        mOnResultReceived = this;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");
        String GsId="";
        Intent intnt= getIntent();
        if(intnt!=null){
            GsId=intnt.getStringExtra("GSId");
        }

        String Url=API.fGetReviewsByGSID();
        Url=Url+GsId+"/"+"100"+"/"+"0"+"/"+"0";

        mProgressDialog.show();
        GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
        oInsertUpdateApi.setRequestSource(RequestSource.GetReviewsByGSID);
        oInsertUpdateApi.executeGetRequest(Url);
    }

    @Override
    public void onClick(View view) {
        switch (view.getId()) {
            case R.id.img_back_gas_station_reviews:
                this.finish();
                break;
        }
    }

    @Override
    public void dispatchString(RequestSource from, final String what) {
        if (from.toString().equalsIgnoreCase("GetReviewsByGSID")) {
            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                case "-1":
                    displayAlert(AppMessages.TECHNICAL_ERROR, false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            ServerResult=what;
                            ServerResult = ServerResult.replace("\"[", "[");
                            ServerResult = ServerResult.replace("\"]", "]");
                            ServerResult = ServerResult.replace("\\", "");

                            try {
                                JSONArray Ja_reviews= new JSONArray(ServerResult);

                                for(int l=0;l<Ja_reviews.length();l++){
                                    Reviews oreviews= new Reviews();
                                    oreviews.setReplies_count(Ja_reviews.getJSONObject(l).getString("replies_count"));
                                    oreviews.setReviewId(Ja_reviews.getJSONObject(l).getString("review_id"));
                                    oreviews.setPuid(Ja_reviews.getJSONObject(l).getString("puid"));
                                    oreviews.setSubmit_date(Ja_reviews.getJSONObject(l).getString("submit_date"));
                                    oreviews.setPuFirstName(Ja_reviews.getJSONObject(l).getString("FirstName"));
                                    oreviews.setPuLastName(Ja_reviews.getJSONObject(l).getString("LastName"));
                                    oreviews.setReview(Ja_reviews.getJSONObject(l).getString("Review"));
                                    oreviews.setStars(Ja_reviews.getJSONObject(l).getString("star_rating"));
                                    oreviews.setProfile_pic(Ja_reviews.getJSONObject(l).getString("profile_pic"));
                                    oreviews.setOwner_response(Ja_reviews.getJSONObject(l).getString("owner_response"));
                                    oreviews.setResponse_date(Ja_reviews.getJSONObject(l).getString("response_date"));
                                    oreviews.setMinutes(Ja_reviews.getJSONObject(l).getString("Minutes"));
                                    oreviews.setHours(Ja_reviews.getJSONObject(l).getString("Hours"));
                                    oreviews.setDays(Ja_reviews.getJSONObject(l).getString("Days"));
                                    oreviews.setResMinutes(Ja_reviews.getJSONObject(l).getString("ResMinutes"));
                                    oreviews.setResHours(Ja_reviews.getJSONObject(l).getString("ResHours"));
                                    oreviews.setResDays(Ja_reviews.getJSONObject(l).getString("ResDays"));
                                    olist_reviews.add(oreviews);
                                }

                                lv_all_reviews.setAdapter(new ReviewsAdapter(olist_reviews) );

                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    });


                    break;
            }
        }
    }

    public class ReviewsAdapter extends BaseAdapter {
        ArrayList<Reviews> ar_list;

        public ReviewsAdapter(ArrayList<Reviews> Ar_List) {
            ar_list = Ar_List;
        }

        @Override
        public int getCount() {
            return ar_list.size();
        }

        @Override
        public Object getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return 0;
        }

        @Override
        public View getView(final int position, View convertView, ViewGroup parent) {

            LayoutInflater inflater;
            String User_Image_reply = "";
            ImageView img_user_pic_review, img_rating1_review, img_rating2_review, img_rating3_review, img_rating4_review, img_rating5_review;
            TextView txt_review_count, txt_content_review, txt_username_review;

            inflater = LayoutInflater.from(getApplicationContext());
            convertView = inflater.inflate(R.layout.list_items_reviews, null);
            txt_username_review = (TextView) convertView.findViewById(R.id.txt_username_review);
            txt_content_review = (TextView) convertView.findViewById(R.id.txt_content_review);
            txt_review_count = (TextView) convertView.findViewById(R.id.txt_review_count);
            txt_review_count.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    /*Review_Id = intnt.getStringExtra("Review_Id");
                    User_Name = intnt.getStringExtra("User_Name");
                    User_Review = intnt.getStringExtra("User_Review");
                    User_Image = intnt.getStringExtra("User_Image");
                    User_Rating = Integer.parseInt(intnt.getStringExtra("User_Rating"));*/

                    Intent intnt = new Intent(ActivityGsReview.this, ActivityGasStationReplies.class);
                    intnt.putExtra("Review_Id", ar_list.get(position).getReviewId());
                    intnt.putExtra("User_Name", ar_list.get(position).getPuFirstName() + " " + ar_list.get(position).getPuLastName());
                    intnt.putExtra("User_Review", ar_list.get(position).getReview());
                    intnt.putExtra("User_Image", ar_list.get(position).getProfile_pic());
                    intnt.putExtra("User_Rating", ar_list.get(position).getStars());
                    startActivity(intnt);


                }
            });
            img_user_pic_review = (ImageView) convertView.findViewById(R.id.img_user_pic_review);
            img_rating1_review = (ImageView) convertView.findViewById(R.id.img_rating1_review);
            img_rating2_review = (ImageView) convertView.findViewById(R.id.img_rating2_review);
            img_rating3_review = (ImageView) convertView.findViewById(R.id.img_rating3_review);
            img_rating4_review = (ImageView) convertView.findViewById(R.id.img_rating4_review);
            img_rating5_review = (ImageView) convertView.findViewById(R.id.img_rating5_review);

            if (Integer.parseInt(ar_list.get(position).getStars()) < 1) {
            } else if (Integer.parseInt(ar_list.get(position).getStars()) <= 1) {
                img_rating1_review.setImageResource(R.drawable.icon_rating_star_active);
            } else if (Integer.parseInt(ar_list.get(position).getStars()) <= 2) {
                img_rating1_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating2_review.setImageResource(R.drawable.icon_rating_star_active);
            } else if (Integer.parseInt(ar_list.get(position).getStars()) <= 3) {
                img_rating1_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating2_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating3_review.setImageResource(R.drawable.icon_rating_star_active);
            } else if (Integer.parseInt(ar_list.get(position).getStars()) <= 4) {
                img_rating1_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating2_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating3_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating4_review.setImageResource(R.drawable.icon_rating_star_active);
            } else if (Integer.parseInt(ar_list.get(position).getStars()) <= 5) {
                img_rating1_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating2_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating3_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating4_review.setImageResource(R.drawable.icon_rating_star_active);
                img_rating5_review.setImageResource(R.drawable.icon_rating_star_active);
            }


            if (Integer.parseInt(ar_list.get(position).getReplies_count()) > 0) {
                if (Integer.parseInt(ar_list.get(position).getReplies_count()) > 1) {
                    txt_review_count.setText(ar_list.get(position).getReplies_count() + " Replies");
                } else {
                    txt_review_count.setText(ar_list.get(position).getReplies_count() + " Reply");
                }

            }


            txt_content_review.setText(ar_list.get(position).getReview());
            txt_username_review.setText(ar_list.get(position).getPuFirstName() + " " + ar_list.get(position).getPuLastName());
            User_Image_reply = ar_list.get(position).getProfile_pic();
            if (!User_Image_reply.equals("")) {
                User_Image_reply = "http://owner.parkeee.net/owner/" + User_Image_reply;
                Picasso.with(img_user_pic_review.getContext())
                        .load(User_Image_reply)
                        .networkPolicy(NetworkPolicy.NO_CACHE)
                        .transform(new RoundedTransformation(100, 0))
                        .resize(96, 96).centerCrop().into(img_user_pic_review, new Callback() {
                    @Override
                    public void onSuccess() {
                    }

                    @Override
                    public void onError() {
                    }
                });
            }

            return convertView;
        }
    }

    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityGsReview.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }

    public class RoundedTransformation implements
            Transformation {
        private final int radius;
        private final int margin; // dp

        // radius is corner radii in dp
        // margin is the board in dp
        public RoundedTransformation(final int radius, final int margin) {
            this.radius = radius;
            this.margin = margin;
        }

        @Override
        public Bitmap transform(final Bitmap source) {
            final Paint paint = new Paint();
            paint.setAntiAlias(true);
            paint.setShader(new BitmapShader(source, Shader.TileMode.CLAMP,
                    Shader.TileMode.CLAMP));
            Bitmap output = Bitmap.createBitmap(source.getWidth(),
                    source.getHeight(), Bitmap.Config.ARGB_8888);
            Canvas canvas = new Canvas(output);
            canvas.drawRoundRect(new RectF(margin, margin, source.getWidth()
                    - margin, source.getHeight() - margin), radius, radius, paint);
            if (source != output) {
                source.recycle();
            }
            return output;
        }

        @Override
        public String key() {
            return "rounded";
        }
    }
}
