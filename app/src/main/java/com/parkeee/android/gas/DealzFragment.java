package com.parkeee.android.gas;

import android.app.Activity;
import android.content.Context;
import android.content.pm.ActivityInfo;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.parkeee.android.R;
import com.parkeee.android.model.params.Deal;



public class DealzFragment extends Fragment {
    private int position;
    private Deal deals;
    private DealClickListener dealClickListener;
    private TextView txtDealTitle, txtDealInfo, txtExpDate;


    @SuppressWarnings("deprecation")
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        try {
            dealClickListener = (DealClickListener) activity;
        } catch (ClassCastException castException) {
        }
    }
    String code = "";
    int defDeal;
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        getActivity().  setRequestedOrientation(ActivityInfo.SCREEN_ORIENTATION_PORTRAIT);
        View v = inflater.inflate(R.layout.icard_dealz, container, false);
        Bundle b = getArguments();
        if (b != null) {
            position = b.getInt("position");
            deals = (Deal) b.getSerializable("dealinfo");
            code = deals.getAccessCode();
        }
        txtDealInfo = (TextView) v.findViewById(R.id.txtDealInfo);
        txtDealTitle = (TextView) v.findViewById(R.id.txtDealTitle);
        txtExpDate = (TextView) v.findViewById(R.id.tv_expire_date);

        txtDealTitle.setText(deals.getDeal());
        txtDealInfo.setText(deals.getDealDetail());
        txtExpDate.setText("Expires: "+deals.getEndDate());
        v.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dealClickListener.onDealClick(position, deals);
            }
        });

        return v;
    }
}
