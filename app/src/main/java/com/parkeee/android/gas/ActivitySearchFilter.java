package com.parkeee.android.gas;

import android.app.Activity;
import android.content.Intent;
import android.graphics.Color;
import android.graphics.PorterDuff;
import android.graphics.PorterDuffColorFilter;
import android.os.Bundle;
import android.support.v4.content.res.ResourcesCompat;
import android.view.View;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.parkeee.android.R;

import java.util.ArrayList;

/**
 * Created by user on 10-12-2016.
 */

public class ActivitySearchFilter extends Activity implements View.OnClickListener {

    Spinner sp_fueltype;
    SeekBar seekBar_Distance;
    double radius=5.0;
    TextView txt_radius_filter;
    Button btn_cancel_search_filter,btn_search_filter;
    ImageView img_lowest_price,img_nearest,img_unregistered;
    Button btn_shell,btn_bp,btn_chevron,btn_kangaroo,btn_speedway,btn_exxon,btn_mobil,btn_sunoco,btn_race_trac,btn_citgo,btn_marathon,
            btn_arco,btn_7eleven ,btn_valero,btn_76,btn_chumberland,btn_stripes,btn_jackson,btn_mapco,btn_gulf;
    Button  btn_air,btn_attendant,btn_beer,btn_candies,btn_cigarette,btn_coffee,btn_dinner,btn_fastfood,btn_ice,btn_drivethrough,btn_fullservice,
            btn_indoor,btn_lotto,btn_toilets,btn_internet_cafe,btn_no_smoking,btn_selfservice,btn_wheelchair,btn_wifi,btn_hotel,btn_lake,btn_library
            ,btn_museum,btn_park,btn_service_station,btn_swimming_pool;

     ArrayList<String> mAmeneties = new ArrayList<String >();
     ArrayList<String> mBrands = new ArrayList<String >();
     int fuelType = 1;
     int mRegistered = 1;
     int mUnRegistered = 0;
     boolean isSortByPrice = true;


    ArrayList<Button> brandsArrList = new ArrayList<Button>();
    ArrayList<Button> amenitiesArrList = new ArrayList<Button>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_search_filter);
        InitializeInterface();
    }

    private void InitializeInterface() {

        sp_fueltype=(Spinner)findViewById(R.id.sp_fueltype);
        ArrayAdapter<String> adapter = new ArrayAdapter<String>(this, R.layout.spinner2_item,getResources().getStringArray(R.array.arrFuelType));
        sp_fueltype.setAdapter(adapter);

        txt_radius_filter=(TextView)findViewById(R.id.txt_radius_filter);

        seekBar_Distance=(SeekBar)findViewById(R.id.seekBar_Distance);
        seekBar_Distance.setOnSeekBarChangeListener(new SeekBarListener());
        seekBar_Distance.getProgressDrawable().setColorFilter(new PorterDuffColorFilter(0xFFFF5a00, PorterDuff.Mode.MULTIPLY));

        btn_cancel_search_filter=(Button)findViewById(R.id.btn_cancel_search_filter);
        btn_cancel_search_filter.setOnClickListener(this);

        btn_search_filter=(Button)findViewById(R.id.btn_search_filter);
        btn_search_filter.setOnClickListener(this);

        img_lowest_price=(ImageView)findViewById(R.id.img_lowest_price);
        img_lowest_price.setOnClickListener(this);

        img_nearest=(ImageView)findViewById(R.id.img_nearest);
        img_nearest.setOnClickListener(this);

        img_unregistered=(ImageView)findViewById(R.id.img_unregistered);
        img_unregistered.setOnClickListener(this);

        btn_shell=(Button)findViewById(R.id.btn_shell);
        btn_shell.setTag(20);
        btn_shell.setOnClickListener(this);

        btn_bp=(Button)findViewById(R.id.btn_bp);
        btn_bp.setTag(3);
        btn_bp.setOnClickListener(this);

        btn_chevron=(Button)findViewById(R.id.btn_chevron);
        btn_chevron.setTag(4);
        btn_chevron.setOnClickListener(this);

        btn_kangaroo=(Button)findViewById(R.id.btn_kangaroo);
        btn_kangaroo.setTag(61);
        btn_kangaroo.setOnClickListener(this);

        btn_speedway=(Button)findViewById(R.id.btn_speedway);
        btn_speedway.setTag(24);
        btn_speedway.setOnClickListener(this);

        btn_exxon=(Button)findViewById(R.id.btn_exxon);
        btn_exxon.setTag(8);
        btn_exxon.setOnClickListener(this);

        btn_mobil=(Button)findViewById(R.id.btn_mobil);
        btn_mobil.setTag(16);
        btn_mobil.setOnClickListener(this);

        btn_sunoco=(Button)findViewById(R.id.btn_sunoco);
        btn_sunoco.setTag(25);
        btn_sunoco.setOnClickListener(this);

        btn_race_trac=(Button)findViewById(R.id.btn_race_trac);
        btn_race_trac.setTag(35);
        btn_race_trac.setOnClickListener(this);

        btn_citgo=(Button)findViewById(R.id.btn_citgo);
        btn_citgo.setTag(5);
        btn_citgo.setOnClickListener(this);

        btn_marathon=(Button)findViewById(R.id.btn_marathon);
        btn_marathon.setTag(32);
        btn_marathon.setOnClickListener(this);

        btn_arco=(Button)findViewById(R.id.btn_arco);
        btn_arco.setTag(2);
        btn_arco.setOnClickListener(this);

        btn_7eleven=(Button)findViewById(R.id.btn_7eleven);
        btn_7eleven.setTag(39);
        btn_7eleven.setOnClickListener(this);

        btn_valero=(Button)findViewById(R.id.btn_valero);
        btn_valero.setTag(27);
        btn_valero.setOnClickListener(this);

        btn_76=(Button)findViewById(R.id.btn_76 );
        btn_76.setTag(21);
        btn_76.setOnClickListener(this);

        btn_chumberland=(Button)findViewById(R.id.btn_chumberland);
        btn_chumberland.setTag(48);
        btn_chumberland.setOnClickListener(this);

        btn_stripes=(Button)findViewById(R.id.btn_stripes);
        btn_stripes.setTag(43);
        btn_stripes.setOnClickListener(this);

        btn_jackson=(Button)findViewById(R.id.btn_jackson);
        btn_jackson.setTag(76);
        btn_jackson.setOnClickListener(this);

        btn_mapco=(Button)findViewById(R.id.btn_mapco);
        btn_mapco.setTag(204);
        btn_mapco.setOnClickListener(this);

        btn_gulf=(Button)findViewById(R.id.btn_gulf);
        btn_gulf.setTag(11);
        btn_gulf.setOnClickListener(this);

        btn_air=(Button)findViewById(R.id.btn_air);
        btn_air.setTag(26);
        btn_air.setOnClickListener(this);

        btn_attendant=(Button)findViewById(R.id.btn_attendant);
        btn_attendant.setTag(20);
        btn_attendant.setOnClickListener(this);

        btn_beer=(Button)findViewById(R.id.btn_beer);
        btn_beer.setTag(1);
        btn_beer.setOnClickListener(this);

        btn_candies=(Button)findViewById(R.id.btn_candies);
        btn_candies.setTag(25);
        btn_candies.setOnClickListener(this);

        btn_cigarette=(Button)findViewById(R.id.btn_cigarette);
        btn_cigarette.setTag(10);
        btn_cigarette.setOnClickListener(this);

        btn_coffee=(Button)findViewById(R.id.btn_coffee);
        btn_coffee.setTag(2);
        btn_coffee.setOnClickListener(this);

        btn_dinner=(Button)findViewById(R.id.btn_dinner);
        btn_dinner.setTag(24);
        btn_dinner.setOnClickListener(this);

        btn_fastfood=(Button)findViewById(R.id.btn_fastfood);
        btn_fastfood.setTag(23);
        btn_fastfood.setOnClickListener(this);

        btn_ice=(Button)findViewById(R.id.btn_ice);
        btn_ice.setTag(22);
        btn_ice.setOnClickListener(this);

        btn_drivethrough=(Button)findViewById(R.id.btn_drivethrough);
        btn_drivethrough.setTag(3);
        btn_drivethrough.setOnClickListener(this);

        btn_fullservice=(Button)findViewById(R.id.btn_fullservice);
        btn_fullservice.setTag(5);
        btn_fullservice.setOnClickListener(this);

        btn_indoor=(Button)findViewById(R.id.btn_indoor);
        btn_indoor.setTag(21);
        btn_indoor.setOnClickListener(this);

        btn_lotto=(Button)findViewById(R.id.btn_lotto);
        btn_lotto.setTag(7);
        btn_lotto.setOnClickListener(this);

        btn_toilets=(Button)findViewById(R.id.btn_toilets);
        btn_toilets.setTag(11);
        btn_toilets.setOnClickListener(this);

        btn_internet_cafe=(Button)findViewById(R.id.btn_internet_cafe);
        btn_internet_cafe.setTag(6);
        btn_internet_cafe.setOnClickListener(this);

        btn_no_smoking=(Button)findViewById(R.id.btn_no_smoking);
        btn_no_smoking.setTag(8);
        btn_no_smoking.setOnClickListener(this);

        btn_selfservice=(Button)findViewById(R.id.btn_selfservice);
        btn_selfservice.setTag(9);
        btn_selfservice.setOnClickListener(this);

        btn_wheelchair=(Button)findViewById(R.id.btn_wheelchair);
        btn_wheelchair.setTag(12);
        btn_wheelchair.setOnClickListener(this);

        btn_wifi=(Button)findViewById(R.id.btn_wifi);
        btn_wifi.setTag(4);
        btn_wifi.setOnClickListener(this);

        btn_hotel=(Button)findViewById(R.id.btn_hotel);
        btn_hotel.setTag(13);
        btn_hotel.setOnClickListener(this);

        btn_lake=(Button)findViewById(R.id.btn_lake);
        btn_lake.setTag(14);
        btn_lake.setOnClickListener(this);

        btn_library=(Button)findViewById(R.id.btn_library);
        btn_library.setTag(15);
        btn_library.setOnClickListener(this);

        btn_museum=(Button)findViewById(R.id.btn_museum);
        btn_museum.setTag(16);
        btn_museum.setOnClickListener(this);

        btn_park=(Button)findViewById(R.id.btn_park);
        btn_park.setTag(17);
        btn_park.setOnClickListener(this);

        btn_service_station=(Button)findViewById(R.id.btn_service_station);
        btn_service_station.setTag(18);
        btn_service_station.setOnClickListener(this);

        btn_swimming_pool=(Button)findViewById(R.id.btn_swimming_pool);
        btn_swimming_pool.setTag(19);
        btn_swimming_pool.setOnClickListener(this);

        brandsArrList.add(btn_shell);
        brandsArrList.add(btn_bp);
        brandsArrList.add(btn_chevron);
        brandsArrList.add(btn_kangaroo);
        brandsArrList.add(btn_speedway);
        brandsArrList.add(btn_exxon);
        brandsArrList.add(btn_mobil);
        brandsArrList.add(btn_sunoco);
        brandsArrList.add(btn_race_trac);
        brandsArrList.add(btn_citgo);
        brandsArrList.add(btn_marathon);
        brandsArrList.add(btn_arco);
        brandsArrList.add(btn_7eleven);
        brandsArrList.add(btn_valero);
        brandsArrList.add(btn_76);
        brandsArrList.add(btn_chumberland);
        brandsArrList.add(btn_stripes);
        brandsArrList.add(btn_jackson);
        brandsArrList.add(btn_mapco);
        brandsArrList.add(btn_gulf);
        //..
        amenitiesArrList.add(btn_air);
        amenitiesArrList.add(btn_attendant);
        amenitiesArrList.add(btn_beer);
        amenitiesArrList.add(btn_candies);
        amenitiesArrList.add(btn_cigarette);
        amenitiesArrList.add(btn_coffee);
        amenitiesArrList.add(btn_dinner);
        amenitiesArrList.add(btn_fastfood);
        amenitiesArrList.add(btn_ice);
        amenitiesArrList.add(btn_drivethrough);
        amenitiesArrList.add(btn_fullservice);
        amenitiesArrList.add(btn_indoor);
        amenitiesArrList.add(btn_lotto);
        amenitiesArrList.add(btn_toilets);
        amenitiesArrList.add(btn_internet_cafe);
        amenitiesArrList.add(btn_no_smoking);
        amenitiesArrList.add(btn_selfservice);
        amenitiesArrList.add(btn_wheelchair);
        amenitiesArrList.add(btn_wifi);
        amenitiesArrList.add(btn_hotel);
        amenitiesArrList.add(btn_lake);
        amenitiesArrList.add(btn_library);
        amenitiesArrList.add(btn_museum);
        amenitiesArrList.add(btn_park);
        amenitiesArrList.add(btn_service_station);
        amenitiesArrList.add(btn_swimming_pool);

        Intent intnt= getIntent();
        if(intnt!=null){
            radius=intnt.getDoubleExtra("SearchRadius",5.0);
            if (radius == 1.0) {
                txt_radius_filter.setText("Mile"+" "+1);
                seekBar_Distance.setProgress(1);
            } else if (radius >= 1.0) {
                int d=(int)radius;
                seekBar_Distance.setProgress(d);
                txt_radius_filter.setText("Miles"+" "+String.valueOf(d));
            }

            mAmeneties = intnt.getStringArrayListExtra("Ameneties_array");
            mBrands = intnt.getStringArrayListExtra("mBrands_array");
            isSortByPrice = intnt.getBooleanExtra("isSortByPrice", true);
            fuelType=intnt.getIntExtra("fuelType",0);
            mUnRegistered=intnt.getIntExtra("mUnRegistered",0);

            sp_fueltype.setSelection(fuelType-1);

            if(mUnRegistered==1){
                img_unregistered.setImageResource(R.drawable.selected_checkbox);
                img_unregistered.setTag("1");
            }else{
                img_unregistered.setImageResource(R.drawable.default_checkbox);
                img_unregistered.setTag("0");
            }

            if(isSortByPrice==false){
                img_nearest.setImageResource(R.drawable.selected_checkbox);
                img_lowest_price.setImageResource(R.drawable.default_checkbox);
                img_nearest.setTag("1");
                img_lowest_price.setTag("1");

            }

            if (mAmeneties!=null){
                for (int i=0; i<mAmeneties.size(); i++){
                    for (int j=0; j<amenitiesArrList.size(); j++) {
                        if (amenitiesArrList.get(j).getTag().toString().equals(mAmeneties.get(i))){
                            amenitiesArrList.get(j).setTextColor(Color.WHITE);
                            amenitiesArrList.get(j).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_selected_bg, null));
                            break;
                        }
                    }
                }
            }else{
                mAmeneties = new ArrayList<String >();
            }

            if (mBrands!=null){
                for (int i=0; i<mBrands.size(); i++){
                    for (int j=0; j<brandsArrList.size(); j++) {
                        if (brandsArrList.get(j).getTag().toString().equals(mBrands.get(i))){
                            brandsArrList.get(j).setTextColor(Color.WHITE);
                            brandsArrList.get(j).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_selected_bg, null));
                            break;
                        }
                    }
                }
            }else{
                mBrands = new ArrayList<String >();
            }
        }
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btn_search_filter:
                this.finish();
                Intent IASGS=new Intent(ActivitySearchFilter.this,ActivitySearchGasStation.class);
                IASGS.putExtra("SearchRadius",radius);
                IASGS.putExtra("fuelType",(sp_fueltype.getSelectedItemPosition()+1));
                IASGS.putExtra("Ameneties_array", mAmeneties);
                IASGS.putExtra("mBrands_array", mBrands);
                IASGS.putExtra("mUnRegistered",Integer.parseInt(img_unregistered.getTag().toString()));
                IASGS.putExtra("isSortByPrice", isSortByPrice);
                startActivity(IASGS);
                break;

            case R.id.img_lowest_price:
                if(img_lowest_price.getTag().equals("0")){
                    img_lowest_price.setImageResource(R.drawable.default_checkbox);
                    img_nearest.setImageResource(R.drawable.selected_checkbox);
                    img_lowest_price.setTag("1");
                }else{
                    isSortByPrice = true;
                    img_lowest_price.setImageResource(R.drawable.selected_checkbox);
                    img_nearest.setImageResource(R.drawable.default_checkbox);
                    img_lowest_price.setTag("0");
                }
                break;

            case R.id.img_nearest:
                if(img_nearest.getTag().equals("0")){
                    img_nearest.setImageResource(R.drawable.selected_checkbox);
                    img_lowest_price.setImageResource(R.drawable.default_checkbox);
                    img_nearest.setTag("1");
                    isSortByPrice = false;
                }else{
                    img_lowest_price.setImageResource(R.drawable.selected_checkbox);
                    img_nearest.setImageResource(R.drawable.default_checkbox);
                    img_nearest.setTag("0");
                }
                break;
            case R.id.img_unregistered:
                if(img_unregistered.getTag().equals("0")){
                    img_unregistered.setImageResource(R.drawable.selected_checkbox);
                    img_unregistered.setTag("1");
                }else{
                    img_unregistered.setImageResource(R.drawable.default_checkbox);
                    img_unregistered.setTag("0");
                }
                break;
            case R.id.btn_shell:
                setBrandsBtn(btn_shell.getTag().toString());
                break;
            case R.id.btn_bp:
                setBrandsBtn(btn_bp.getTag().toString());
                break;
            case R.id.btn_chevron:
                setBrandsBtn(btn_chevron.getTag().toString());
                break;
            case R.id.btn_kangaroo:
                setBrandsBtn(btn_kangaroo.getTag().toString());
                break;
            case R.id.btn_speedway:
                setBrandsBtn(btn_speedway.getTag().toString());
                break;
            case R.id.btn_exxon:
                setBrandsBtn(btn_exxon.getTag().toString());
                break;
            case R.id.btn_mobil:
                setBrandsBtn(btn_mobil.getTag().toString());
                break;
            case R.id.btn_sunoco:
                setBrandsBtn(btn_sunoco.getTag().toString());
                break;
            case R.id.btn_race_trac:
                setBrandsBtn(btn_race_trac.getTag().toString());
                break;
            case R.id.btn_citgo:
                setBrandsBtn(btn_citgo.getTag().toString());
                break;
            case R.id.btn_marathon:
                setBrandsBtn(btn_marathon.getTag().toString());
                break;
            case R.id.btn_arco:
                setBrandsBtn(btn_arco.getTag().toString());
                break;
            case R.id.btn_7eleven:
                setBrandsBtn(btn_7eleven.getTag().toString());
                break;
            case R.id.btn_valero:
                setBrandsBtn(btn_valero.getTag().toString());
                break;
            case R.id.btn_76:
                setBrandsBtn(btn_76.getTag().toString());
                break;
            case R.id.btn_chumberland:
                setBrandsBtn(btn_chumberland.getTag().toString());
                break;
            case R.id.btn_stripes:
                setBrandsBtn(btn_stripes.getTag().toString());
                break;
            case R.id.btn_jackson:
                setBrandsBtn(btn_jackson.getTag().toString());
                break;
            case R.id.btn_mapco:
                setBrandsBtn(btn_mapco.getTag().toString());
                break;

            case R.id.btn_gulf:
                setBrandsBtn(btn_gulf.getTag().toString());
                break;
            case R.id.btn_air:
                setmAmenetiesBtn(btn_air.getTag().toString());
                break;
            case R.id.btn_attendant:
                setmAmenetiesBtn(btn_attendant.getTag().toString());
                break;
            case R.id.btn_beer:
                setmAmenetiesBtn(btn_beer.getTag().toString());
                break;
            case R.id.btn_candies:
                setmAmenetiesBtn(btn_candies.getTag().toString());
                break;
            case R.id.btn_cigarette:
                setmAmenetiesBtn(btn_cigarette.getTag().toString());
                break;
            case R.id.btn_coffee:
                setmAmenetiesBtn(btn_coffee.getTag().toString());
                break;
            case R.id.btn_dinner:
                setmAmenetiesBtn(btn_dinner.getTag().toString());
                break;
            case R.id.btn_fastfood:
                setmAmenetiesBtn(btn_fastfood.getTag().toString());
                break;
            case R.id.btn_ice:
                setmAmenetiesBtn(btn_ice.getTag().toString());
                break;
            case R.id.btn_drivethrough:
                setmAmenetiesBtn(btn_drivethrough.getTag().toString());
                break;
            case R.id.btn_fullservice:
                setmAmenetiesBtn(btn_fullservice.getTag().toString());
                break;
            case R.id.btn_indoor:
                setmAmenetiesBtn(btn_indoor.getTag().toString());
                break;
            case R.id.btn_lotto:
                setmAmenetiesBtn(btn_lotto.getTag().toString());
                break;
            case R.id.btn_toilets:
                setmAmenetiesBtn(btn_toilets.getTag().toString());
                break;

            case R.id.btn_internet_cafe:
                setmAmenetiesBtn(btn_internet_cafe.getTag().toString());
                break;
            case R.id.btn_no_smoking:
                setmAmenetiesBtn(btn_no_smoking.getTag().toString());
                break;
            case R.id.btn_selfservice:
                setmAmenetiesBtn(btn_selfservice.getTag().toString());
                break;
            case R.id.btn_wheelchair:
                setmAmenetiesBtn(btn_wheelchair.getTag().toString());
                break;
            case R.id.btn_wifi:
                setmAmenetiesBtn(btn_wifi.getTag().toString());
                break;
            case R.id.btn_hotel:
                setmAmenetiesBtn(btn_hotel.getTag().toString());
                break;
            case R.id.btn_lake:
                setmAmenetiesBtn(btn_lake.getTag().toString());
                break;
            case R.id.btn_library:
                setmAmenetiesBtn(btn_library.getTag().toString());
                break;
            case R.id.btn_museum:
                setmAmenetiesBtn(btn_museum.getTag().toString());
                break;
            case R.id.btn_park:
                setmAmenetiesBtn(btn_park.getTag().toString());
                break;
            case R.id.btn_service_station:
                setmAmenetiesBtn(btn_service_station.getTag().toString());
                break;
            case R.id.btn_swimming_pool:
                setmAmenetiesBtn(btn_swimming_pool.getTag().toString());
                break;

            case R.id.btn_cancel_search_filter:
                this.finish();
                Intent IASGSC=new Intent(ActivitySearchFilter.this,ActivitySearchGasStation.class);
                IASGSC.putExtra("SearchRadius",radius);
                IASGSC.putExtra("fuelType",(sp_fueltype.getSelectedItemPosition()+1));
                IASGSC.putExtra("Ameneties_array", mAmeneties);
                IASGSC.putExtra("mBrands_array", mBrands);
                IASGSC.putExtra("mUnRegistered",Integer.parseInt(img_unregistered.getTag().toString()));
                IASGSC.putExtra("isSortByPrice", isSortByPrice);
                startActivity(IASGSC);
                break;

            default:
                break;
        }
    }

    private void setBrandsBtn(String brandTag) {

        for (int i=0; i<brandsArrList.size();i++){
            if (brandsArrList.get(i).getTag().toString().equals(brandTag)){
                if(mBrands.contains(brandTag)){
                    brandsArrList.get(i).setTextColor(Color.BLACK);
                    brandsArrList.get(i).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_transparent_black, null));
                    mBrands.remove(brandTag);
                }else{
                    brandsArrList.get(i).setTextColor(Color.WHITE);
                    brandsArrList.get(i).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_selected_bg, null));
                    mBrands.add(brandTag);
                }
                break;
            }
        }
    }

    private void setmAmenetiesBtn(String amenitieTag) {

        for (int i=0; i<amenitiesArrList.size();i++){
            if (amenitiesArrList.get(i).getTag().toString().equals(amenitieTag)){
                if(mAmeneties.contains(amenitieTag)){
                    amenitiesArrList.get(i).setTextColor(Color.BLACK);
                    amenitiesArrList.get(i).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_transparent_black, null));
                    mAmeneties.remove(amenitieTag);
                }else{
                    amenitiesArrList.get(i).setTextColor(Color.WHITE);
                    amenitiesArrList.get(i).setBackground(ResourcesCompat.getDrawable(getResources(), R.drawable.btn_selected_bg, null));
                    mAmeneties.add(amenitieTag);
                }
                break;
            }
        }
    }


    private class SeekBarListener implements SeekBar.OnSeekBarChangeListener {
        @Override
        public void onStopTrackingTouch(SeekBar arg0) {
        }
        @Override
        public void onStartTrackingTouch(SeekBar arg0) {
        }

        @Override
        public void onProgressChanged(SeekBar arg0, int arg1, boolean arg2) {
            if (arg1 == 0) {
                arg1 = 1;
            } else if (arg1 == 1) {
                txt_radius_filter.setText("Mile"+" "+arg1);
            } else if (arg1 >= 1) {
                txt_radius_filter.setText("Miles"+" "+arg1);
            }
            radius = arg1;
        }
    }
}
