package com.parkeee.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.text.Editable;
import android.text.Html;
import android.text.Selection;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.utils.AppMessages;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import cn.pedant.SweetAlert.SweetAlertDialog;

/**
 * Created by user on 28-11-2016.
 */

public class ActivityVerification extends Activity implements View.OnClickListener, OnResultReceived {

    TextView txt_ResendOTP;
    Button btnVerify_Verification;
    EditText et_PhoneNumber, et_Otp;
    String a, ServerResult = "", Mob_no, puid,ServerOTP;
    int keyDel;
    OnResultReceived mOnResultReceived;
    ProgressDialog mProgressDialog;
    Context mContext;
    boolean Is_Resend;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_verification);
        Initialize();
    }

    private void Initialize() {
        mContext = this;
        mOnResultReceived = this;
        txt_ResendOTP = (TextView) findViewById(R.id.txt_ResendOTP);
        btnVerify_Verification = (Button) findViewById(R.id.btnVerify_Verification);
        btnVerify_Verification.setOnClickListener(this);

        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        final SpannableString ss = new SpannableString(Html.fromHtml("(If you have not received the OTP then Click here to resend the OTP.)"));
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                Selection.setSelection(ss, 0);
                textView.invalidate();
                Is_Resend=true;
                Mob_no=et_PhoneNumber.getText().toString().trim();
                Mob_no=Mob_no.replace("-","");
                Mob_no="1-"+Mob_no;
                String Otp_Url = API.fOTPsent();
                Otp_Url = Otp_Url.concat(Mob_no);
                mProgressDialog.show();
                GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
                oInsertUpdateApi.setRequestSource(RequestSource.OTPSent);
                oInsertUpdateApi.executeGetRequest(Otp_Url);
            }
        };

        ss.setSpan(span1, 39, 49, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        txt_ResendOTP.setText(ss);
        txt_ResendOTP.setMovementMethod(LinkMovementMethod.getInstance());

        et_PhoneNumber = (EditText) findViewById(R.id.et_PhoneNumber_Verification);
        et_PhoneNumber.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean flag = true;
                String eachBlock[] = et_PhoneNumber.getText().toString().split("-");
                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 3) {
                        Log.v("11111111111111111111", "cc" + flag + eachBlock[i].length());
                    }
                }
                if (flag) {
                    et_PhoneNumber.setOnKeyListener(new View.OnKeyListener() {

                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {

                        if (((et_PhoneNumber.getText().length() + 1) % 4) == 0) {
                            if (et_PhoneNumber.getText().toString().split("-").length <= 2) {
                                et_PhoneNumber.setText(et_PhoneNumber.getText() + "-");
                                et_PhoneNumber.setSelection(et_PhoneNumber.getText().length());
                            }
                        }
                        a = et_PhoneNumber.getText().toString();
                    } else {
                        a = et_PhoneNumber.getText().toString();
                        keyDel = 0;
                    }

                } else {
                    et_PhoneNumber.setText(a);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });
        et_Otp = (EditText) findViewById(R.id.et_Otp_verification);

        final SharedPreferences prefs = getSharedPreferences("RegisterResponse",
                MODE_PRIVATE);
        Mob_no = prefs.getString("mobile_no", "");
        et_PhoneNumber.setText( prefs.getString("display_mobile_no", ""));
        Mob_no = Mob_no.replace("-", "");
        puid = prefs.getString("puid", "");


        // ====================calling OTP sent api==================
        String Otp_Url = API.fOTPsent();
        Otp_Url = Otp_Url.concat(Mob_no);
        mProgressDialog.show();
        GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
        oInsertUpdateApi.setRequestSource(RequestSource.OTPSent);
        oInsertUpdateApi.executeGetRequest(Otp_Url);

    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.btnVerify_Verification:
                if (et_PhoneNumber.getText().toString().trim().equals("")) {
                    new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Alert")
                            .setContentText("Mobile field can't be blank.")
                            .setConfirmText("Ok")
                            .show();
                } else if (et_PhoneNumber.length() - 2 < 10) {
                    new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Alert")
                            .setContentText("Mobile number must be between 10-13 character.")
                            .setConfirmText("Ok")
                            .show();
                } else if (et_Otp.getText().toString().trim().equals("")) {
                    new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Alert")
                            .setContentText("OTP field can't be blank.")
                            .setConfirmText("Ok")
                            .show();
                } else if (!et_Otp.getText().toString().trim().equals(ServerOTP)) {
                    new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                            .setTitleText("Alert")
                            .setContentText(AppMessages.WRONG_OTP)
                            .setConfirmText("Ok")
                            .show();
                }  else {
                    String MobileVerification_Url = API.fMobileVerification();
                    MobileVerification_Url = MobileVerification_Url.concat(puid + "/" + Mob_no);

                    mProgressDialog.show();
                    GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.MobileVerification);
                    oInsertUpdateApi.executeGetRequest(MobileVerification_Url);
                }
                break;
        }
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        if (from.toString().equalsIgnoreCase("OTPSent")) {
            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                case "-1":
                    displayAlert("Sorry! The registration process failed due to some technical error. Please try after some time.", false);
                    break;
                default:
                    if(Is_Resend){
                        ServerOTP=ServerResult;
                        displayAlert(AppMessages.MOBILE_OTP_SENT, false);
                    }else{
                        ServerOTP=ServerResult;
                    }
                    break;
            }
        } else {
            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                case "-1":
                    displayAlert("Sorry! The registration process failed due to some technical error. Please try after some time.", false);
                    break;
                default:
                    startActivity(new Intent(ActivityVerification.this, ActivityDashboard.class));
                    this.finish();
                    break;
            }
        }
    }


    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityVerification.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }
}
