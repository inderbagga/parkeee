package com.parkeee.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;

import android.view.View;
import android.widget.Button;

import com.parkeee.android.backend.RealmController;
import com.parkeee.android.model.tables.InsertUpdateRegister;

public class SplashActivity extends Activity implements View.OnClickListener {

    PanningView panningView;
    Button btn_Login, btn_Register;
    InsertUpdateRegister oinsertupdateregister;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        oinsertupdateregister = RealmController.with(this).getRegisterInformation();
        if(oinsertupdateregister!=null){
            if(oinsertupdateregister.getKeep_me_signed_in().equals("1")&& oinsertupdateregister.getLogOut().equals("0")&& !oinsertupdateregister.getPuid().equals("")){
                startActivity(new Intent(SplashActivity.this,ActivityDashboard.class));
                this.finish();
            }else{
                setContentView(R.layout.activity_splash);
                Initailize();
            }
        }else{
            setContentView(R.layout.activity_splash);
            Initailize();
        }
    }

    private void Initailize() {
        panningView = (PanningView) findViewById(R.id.panningView);
        panningView.startPanning();
        btn_Login = (Button) findViewById(R.id.btnLogin);
        btn_Login.setOnClickListener(this);
        btn_Register = (Button) findViewById(R.id.btnRegister);
        btn_Register.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.btnLogin:
                startActivity(new Intent(SplashActivity.this,ActivityLogin.class));
                break;

            case R.id.btnRegister:
                startActivity(new Intent(SplashActivity.this,ActivityRegister.class));
                break;

            default:
                break;
        }
    }
}
