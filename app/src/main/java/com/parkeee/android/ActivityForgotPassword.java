package com.parkeee.android;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.KeyEvent;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.processor.GetApiClient;
import com.parkeee.android.utils.AppMessages;

/**
 * Created by user on 28-11-2016.
 */

public class ActivityForgotPassword extends Activity implements View.OnClickListener,OnResultReceived {

    ImageView img_back_forgot;
    Button btnForgot_pswd;
    EditText et_email_id_forgot;
    String ServerResult;
    OnResultReceived mOnResultReceived;
    ProgressDialog mProgressDialog;
    Context mContext;

    Dialog mdialog;
    Button btn_positive_custom_dailog;
    TextView txt_custom_dialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_forgot_password);
        Initialize();
    }

    private void Initialize() {
        mContext = this;
        mOnResultReceived = this;
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        img_back_forgot=(ImageView)findViewById(R.id.img_back_forgot);
        img_back_forgot.setOnClickListener(this);

        btnForgot_pswd=(Button)findViewById(R.id.btnForgot_pswd);
        btnForgot_pswd.setOnClickListener(this);

        et_email_id_forgot=(EditText)findViewById(R.id.et_email_id_forgot);

        mdialog = new  Dialog(ActivityForgotPassword.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });
    }

    @Override
    public void onClick(View v) {

       switch (v.getId()){
           case R.id.img_back_forgot:
               this.finish();
               break;
           case R.id.btnForgot_pswd:
               if(et_email_id_forgot.getText().toString().trim().equals("")){
                   ErrorMessage("Email field can't be blank.");
               }else if(!isValidEmail((et_email_id_forgot.getText().toString().trim()))){
                   ErrorMessage("Please enter the valid email.");
               } else{
                   mProgressDialog.show();
                   String Forgotpaswd_Url = API.fForgotPassword();
                   Forgotpaswd_Url = Forgotpaswd_Url.concat(et_email_id_forgot.getText().toString().trim());
                   GetApiClient oInsertUpdateApi = new GetApiClient(mOnResultReceived);
                   oInsertUpdateApi.setRequestSource(RequestSource.ForgotPassword);
                   oInsertUpdateApi.executeGetRequest(Forgotpaswd_Url);
               }

               break;
       }
    }

    private void ErrorMessage(String content) {
        txt_custom_dialog.setText(content);
        mdialog.show();
    }

    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        if (from.toString().equalsIgnoreCase("ForgotPassword")) {
            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "0":
                    displayAlert("EMAIL NOT REGISTERED.", false);
                    break;
                case "-1":
                    displayAlert("Sorry! The registration process failed due to some technical error. Please try after some time.", false);
                    break;
                default:
                    displayAlert(AppMessages.FORGOT_SUCCESS, true);

                    break;
            }
        }
    }


    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityForgotPassword.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                    startActivity(new Intent(ActivityForgotPassword.this,ActivityLogin.class));
                                    ActivityForgotPassword.this.finish();
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {
                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }
}
