package com.parkeee.android;


import android.animation.Animator;
import android.animation.ValueAnimator;
import android.app.Activity;
import android.app.Fragment;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.menu.ActivityFavorites;
import com.parkeee.android.menu.ActivityGasStation;
import com.parkeee.android.menu.ActivityInbox;
import com.parkeee.android.menu.ActivityMyCars;
import com.parkeee.android.menu.ActivityMyProfile;
import com.parkeee.android.menu.ActivityReferralMain;
import com.parkeee.android.menu.ActivitySetting;
import com.parkeee.android.model.tables.InsertUpdateRegister;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.TextView;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;

public class MenuFragment extends Fragment implements View.OnClickListener {

    boolean Is_logout;
    Context ctx;
    Animator mAnimator;
    private ExpandableListView mDrawerList;
    public static String SameActivity;
    TextView txt_signed_in_as;
    InsertUpdateRegister oinsertupdateregister;
    String Email="";
    LinearLayout ll_Logout;
    private Realm realm;

    interface MenuClickInterFace {
        void onListitemClick(String item);
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        super.onActivityCreated(savedInstanceState);
        ctx=getActivity();
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // TODO Auto-generated method stub
        oinsertupdateregister = RealmController.with(getActivity()).getRegisterInformation();
        Email=oinsertupdateregister.getEmail().toString();
        this.realm = RealmController.with(getActivity()).getRealm();

        View view = inflater.inflate(R.layout.parkeee_menu, container, false);
        mDrawerList=(ExpandableListView)view.findViewById(R.id.left_drawer);
        txt_signed_in_as=(TextView)view.findViewById(R.id.txt_signed_in_as);
        txt_signed_in_as.setText(txt_signed_in_as.getText().toString().concat(Email));

        ll_Logout=(LinearLayout)view.findViewById(R.id.ll_Logout);
        ll_Logout.setOnClickListener(this);

        mDrawerList.setOnGroupClickListener(new DrawerItemClickListener(getActivity()));
        mDrawerList.setOnChildClickListener(new DrawerItemClickListener(getActivity()));
        final DrawerAdapter adapter = new DrawerAdapter(getActivity());
        mDrawerList.setAdapter(adapter);


        mDrawerList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {
            @Override
            public void onGroupCollapse(int i) {

                adapter.setCollapasedPostion(i);
            }
        });
        mDrawerList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int j) {
                adapter.setExpandedPostion(j);
            }
        });

        return view;
    }

    private class DrawerItemClickListener implements ExpandableListView.OnGroupClickListener,
            ExpandableListView.OnChildClickListener {
        public DrawerItemClickListener(Activity act) {
        }

        private void closeDrawer() {

        }

        @Override
        public boolean onChildClick(ExpandableListView parent, View v,
                                    int groupPosition, int childPosition, long id) {
            switch (groupPosition) {
                case 8: {
                    switch (childPosition) {
                        case 0:

                            if(SameActivity.equals("GasStation")){
                                ActivityGasStation.menu.toggle();
                            }else{
                                getActivity().finish();
                                startActivity(new Intent(getActivity(),ActivityGasStation.class));
                            }

                            break;
                    }
                    break;
                }

                case 7: {
                    /*if(SameActivity.equals("ActivityStaticPage")){
                        ActivityStaticPages.menu.toggle();
                    }else{
                        getActivity().finish();
                        Intent ISP= new Intent(getActivity(),ActivityStaticPages.class);
                        ISP.putExtra("child_pos",childPosition);
                        ISP.putExtra("is_sidemenu",true);
                        startActivity(ISP);
                    }*/
                    getActivity().finish();
                    Intent ISP= new Intent(getActivity(),ActivityStaticPages.class);
                    ISP.putExtra("child_pos",childPosition);
                    ISP.putExtra("is_sidemenu",true);
                    startActivity(ISP);


                    break;
                }
                default:
                    break;
            }
            closeDrawer();
            return false;
        }

        @Override
        public boolean onGroupClick(ExpandableListView parent, View v,
                                    int groupPosition, long id) {
            switch (groupPosition) {
                case 0:
                    if(SameActivity.equals("Home")){
                        ActivityDashboard.menu.toggle();
                    }else{
                        getActivity().finish();
                        startActivity(new Intent(getActivity(),ActivityDashboard.class));
                    }
                    break;
                case 1:
                    if(SameActivity.equals("MyProfile")){
                        ActivityMyProfile.menu.toggle();
                    }else{
                        getActivity().finish();
                        startActivity(new Intent(getActivity(),ActivityMyProfile.class));
                    }
                    break;
                case 2:
                    if(SameActivity.equals("Favorites")){
                        ActivityFavorites.menu.toggle();
                    }else{
                        getActivity().finish();
                        startActivity(new Intent(getActivity(),ActivityFavorites.class));
                    }
                    break;
                case 3:
                    if(SameActivity.equals("Inbox")){
                        ActivityInbox.menu.toggle();
                    }else{
                        getActivity().finish();
                        startActivity(new Intent(getActivity(),ActivityInbox.class));
                    }
                    break;

                case 4:
                    if(SameActivity.equals("MyCars")){
                        ActivityMyCars.menu.toggle();
                    }else{
                        getActivity().finish();
                        startActivity(new Intent(getActivity(),ActivityMyCars.class));
                    }

                    break;
                case 5:
                    if(SameActivity.equals("ReferralMain")){
                        ActivityReferralMain.menu.toggle();
                    }else{
                        getActivity().finish();
                        startActivity(new Intent(getActivity(),ActivityReferralMain.class));
                    }
                    break;
                case 6:
                    if(SameActivity.equals("Setting")){
                        ActivitySetting.menu.toggle();
                    }else{
                        getActivity().finish();
                        startActivity(new Intent(getActivity(),ActivitySetting.class));
                    }

                    break;
                default:
                    break;
            }
            return false;
        }
    }

    private void expand(View summary) {
        //set Visible
        summary.setVisibility(View.VISIBLE);

        final int widthSpec = View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED);
        summary.measure(widthSpec, 300);

        mAnimator = slideAnimator(0, 300, summary);

        mAnimator.start();
    }

    private void collapse(final View summary) {
        int finalHeight = summary.getHeight();

        ValueAnimator mAnimator = slideAnimator(finalHeight, 0, summary);

        mAnimator.addListener(new Animator.AnimatorListener() {
            @Override
            public void onAnimationEnd(Animator animator) {
                //Height=0, but it set visibility to GONE
                summary.setVisibility(View.GONE);
            }

            @Override
            public void onAnimationStart(Animator animator) {
            }

            @Override
            public void onAnimationCancel(Animator animator) {
            }

            @Override
            public void onAnimationRepeat(Animator animator) {
            }
        });
        mAnimator.start();
    }


    private ValueAnimator slideAnimator(int start, int end, final View summary) {

        ValueAnimator animator = ValueAnimator.ofInt(start, end);

        animator.addUpdateListener(new ValueAnimator.AnimatorUpdateListener() {
            @Override
            public void onAnimationUpdate(ValueAnimator valueAnimator) {
                //Update Height
                int value = (Integer) valueAnimator.getAnimatedValue();

                ViewGroup.LayoutParams layoutParams = summary.getLayoutParams();
                layoutParams.height = value;
                summary.setLayoutParams(layoutParams);
            }
        });
        return animator;
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){

            case R.id.ll_Logout:

                new SweetAlertDialog(getActivity(), SweetAlertDialog.WARNING_TYPE)
                        .setTitleText("Alert")
                        .setContentText("Are you sure you want to Logout?")
                        .setCancelText("No")
                        .setCancelClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();
                            }
                        })
                        .setConfirmText("Yes")
                        .setConfirmClickListener(new SweetAlertDialog.OnSweetClickListener() {
                            @Override
                            public void onClick(SweetAlertDialog sweetAlertDialog) {
                                sweetAlertDialog.cancel();
                                realm.beginTransaction();
                                oinsertupdateregister.setLogOut("1");
                                realm.commitTransaction();
                                getActivity().finishAffinity();
                                startActivity(new Intent(getActivity(),SplashActivity.class));
                            }
                        })
                        .show();
                break;

            default:
                break;
        }
    }
}


