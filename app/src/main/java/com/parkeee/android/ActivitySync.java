package com.parkeee.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.widget.ProgressBar;
import android.widget.Toast;

/**
 * Created by user on 08-12-2016.
 */

public class ActivitySync extends Activity implements Runnable {

    private ProgressBar progressBar;
    private int progressStatus = 0;
    private Handler handler = new Handler();
    boolean next;

    Handler h = new Handler() {
        @Override
        public void handleMessage(Message msg) {
            super.handleMessage(msg);
            if (next) {
               Toast.makeText(getApplicationContext(),"w",Toast.LENGTH_SHORT).show();
            }
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sync);
        InitializeInterface();
    }

    private void InitializeInterface() {
        progressBar = (ProgressBar) findViewById(R.id.progressBar2);
        new Thread(new Runnable() {
            public void run() {
                while (progressStatus < 100) {
                    progressStatus += 1;
                    // Update the progress bar and display the
                    //current value in the text view
                    handler.post(new Runnable() {
                        public void run() {
                            progressBar.setProgress(progressStatus);
                           // textView.setText(progressStatus+"/"+progressBar.getMax());
                        }
                    });
                    try {
                        // Sleep for 200 milliseconds.
                        //Just to display the progress slowly
                        Thread.sleep(50);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }
                }
                ActivitySync.this.finish();
                startActivity(new Intent(ActivitySync.this,ActivityDashboard.class));

            }
        }).start();

    }

    @Override
    public void run() {

    }
}
