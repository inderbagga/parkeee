
package com.parkeee.android.model.params;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Deal implements Serializable{


    private String dealId;

    private String deal;

    private String dealDetail;

    private String endDate;

    private String dealTerms;

    private String accessCode;

    private String likes;

    private String disLikes;

    private Integer isLike;

    private Integer isDisLikes;

    private Boolean isDefaultDeal;




    /**
     * 
     * @return
     *     The dealId
     */
    public String getDealId() {
        return dealId;
    }

    /**
     * 
     * @param dealId
     *     The DealId
     */
    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    /**
     * 
     * @return
     *     The deal
     */
    public String getDeal() {
        return deal;
    }

    /**
     * 
     * @param deal
     *     The Deal
     */
    public void setDeal(String deal) {
        this.deal = deal;
    }

    /**
     * 
     * @return
     *     The dealDetail
     */
    public String getDealDetail() {
        return dealDetail;
    }

    /**
     * 
     * @param dealDetail
     *     The DealDetail
     */
    public void setDealDetail(String dealDetail) {
        this.dealDetail = dealDetail;
    }

    /**
     * 
     * @return
     *     The endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * 
     * @param endDate
     *     The EndDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }

    /**
     * 
     * @return
     *     The dealTerms
     */
    public String getDealTerms() {
        return dealTerms;
    }

    /**
     * 
     * @param dealTerms
     *     The DealTerms
     */
    public void setDealTerms(String dealTerms) {
        this.dealTerms = dealTerms;
    }

    /**
     * 
     * @return
     *     The accessCode
     */
    public String getAccessCode() {
        return accessCode;
    }

    /**
     * 
     * @param accessCode
     *     The AccessCode
     */
    public void setAccessCode(String accessCode) {
        this.accessCode = accessCode;
    }

    /**
     * 
     * @return
     *     The likes
     */
    public String getLikes() {
        return likes;
    }

    /**
     * 
     * @param likes
     *     The Likes
     */
    public void setLikes(String likes) {
        this.likes = likes;
    }

    /**
     * 
     * @return
     *     The disLikes
     */
    public String getDisLikes() {
        return disLikes;
    }

    /**
     * 
     * @param disLikes
     *     The DisLikes
     */
    public void setDisLikes(String disLikes) {
        this.disLikes = disLikes;
    }

    /**
     * 
     * @return
     *     The isLike
     */
    public Integer getIsLike() {
        return isLike;
    }

    /**
     * 
     * @param isLike
     *     The IsLike
     */
    public void setIsLike(Integer isLike) {
        this.isLike = isLike;
    }

    /**
     * 
     * @return
     *     The isDisLikes
     */
    public Integer getIsDisLikes() {
        return isDisLikes;
    }

    /**
     * 
     * @param isDisLikes
     *     The IsDisLikes
     */
    public void setIsDisLikes(Integer isDisLikes) {
        this.isDisLikes = isDisLikes;
    }

    /**
     * 
     * @return
     *     The isDefaultDeal
     */
    public Boolean getIsDefaultDeal() {
        return isDefaultDeal;
    }

    /**
     * 
     * @param isDefaultDeal
     *     The IsDefaultDeal
     */
    public void setIsDefaultDeal(Boolean isDefaultDeal) {
        this.isDefaultDeal = isDefaultDeal;
    }

}
