package com.parkeee.android.model.params;

/**
 * Created by user on 16-12-2016.
 */

public class DeleteCarPUID {

    private String brand;
    private String car_type;
    private String carregno;
    private String color;
    private String expiry_date;
    private String inspection_date;
    private String licence_plate_number;
    private String model;
    private String new_licence_plate_number;
    private String puid;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getCar_type() {
        return car_type;
    }

    public void setCar_type(String car_type) {
        this.car_type = car_type;
    }

    public String getCarregno() {
        return carregno;
    }

    public void setCarregno(String carregno) {
        this.carregno = carregno;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String getExpiry_date() {
        return expiry_date;
    }

    public void setExpiry_date(String expiry_date) {
        this.expiry_date = expiry_date;
    }

    public String getInspection_date() {
        return inspection_date;
    }

    public void setInspection_date(String inspection_date) {
        this.inspection_date = inspection_date;
    }

    public String getLicence_plate_number() {
        return licence_plate_number;
    }

    public void setLicence_plate_number(String licence_plate_number) {
        this.licence_plate_number = licence_plate_number;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getNew_licence_plate_number() {
        return new_licence_plate_number;
    }

    public void setNew_licence_plate_number(String new_licence_plate_number) {
        this.new_licence_plate_number = new_licence_plate_number;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
