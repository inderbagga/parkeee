package com.parkeee.android.model.tables;

import io.realm.RealmObject;
import io.realm.annotations.Index;

/**
 * Created by inderbagga on 10/12/16.
 */

public class InsertUpdateCache extends RealmObject {

    private String car_plate;
    private String due_date_month;

    @Index
    private String insurance_id;
    private String due_date_year;
    private String remind_before_type;
    private String expire_date;
    private String InsServerId;
    private String company_name;
    private String premium;
    private String payment_type;
    private String isNotiEnabled;
    private String due_date_day;
    private String remind_before;
    private String primary_driver;
    private String puid;

    private String isINSDeleted;
    private String isINSUpdated;
    private String isINSUploaded;
    private long timeStamp;

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getIsINSDeleted() {
        return isINSDeleted;
    }

    public void setIsINSDeleted(String isINSDeleted) {
        this.isINSDeleted = isINSDeleted;
    }

    public String getIsINSUpdated() {
        return isINSUpdated;
    }

    public void setIsINSUpdated(String isINSUpdated) {
        this.isINSUpdated = isINSUpdated;
    }

    public String getIsINSUploaded() {
        return isINSUploaded;
    }

    public void setIsINSUploaded(String isINSUploaded) {
        this.isINSUploaded = isINSUploaded;
    }


    public String getCar_plate() {
        return car_plate;
    }

    public void setCar_plate(String car_plate) {
        this.car_plate = car_plate;
    }

    public String getDue_date_month() {
        return due_date_month;
    }

    public void setDue_date_month(String due_date_month) {
        this.due_date_month = due_date_month;
    }

    public String getInsurance_id() {
        return insurance_id;
    }

    public void setInsurance_id(String insurance_id) {
        this.insurance_id = insurance_id;
    }

    public String getDue_date_year() {
        return due_date_year;
    }

    public void setDue_date_year(String due_date_year) {
        this.due_date_year = due_date_year;
    }

    public String getRemind_before_type() {
        return remind_before_type;
    }

    public void setRemind_before_type(String remind_before_type) {
        this.remind_before_type = remind_before_type;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getInsServerId() {
        return InsServerId;
    }

    public void setInsServerId(String insServerId) {
        InsServerId = insServerId;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getIsNotiEnabled() {
        return isNotiEnabled;
    }

    public void setIsNotiEnabled(String isNotiEnabled) {
        this.isNotiEnabled = isNotiEnabled;
    }

    public String getDue_date_day() {
        return due_date_day;
    }

    public void setDue_date_day(String due_date_day) {
        this.due_date_day = due_date_day;
    }

    public String getRemind_before() {
        return remind_before;
    }

    public void setRemind_before(String remind_before) {
        this.remind_before = remind_before;
    }

    public String getPrimary_driver() {
        return primary_driver;
    }

    public void setPrimary_driver(String primary_driver) {
        this.primary_driver = primary_driver;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
