package com.parkeee.android.model.params;

/**
 * Created by user on 14-12-2016.
 */

public class SaveInbox {

    private String MsgID;
    private String Title;
    private String Message;
    private String MsgDate;
    private String MsgType;
    private String ReminderDate;
    private String ReminderId;
    private String puid;

    public String getMsgID() {
        return MsgID;
    }

    public void setMsgID(String msgID) {
        MsgID = msgID;
    }

    public String getTitle() {
        return Title;
    }

    public void setTitle(String title) {
        Title = title;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getMsgDate() {
        return MsgDate;
    }

    public void setMsgDate(String msgDate) {
        MsgDate = msgDate;
    }

    public String getMsgType() {
        return MsgType;
    }

    public void setMsgType(String msgType) {
        MsgType = msgType;
    }

    public String getReminderDate() {
        return ReminderDate;
    }

    public void setReminderDate(String reminderDate) {
        ReminderDate = reminderDate;
    }

    public String getReminderId() {
        return ReminderId;
    }

    public void setReminderId(String reminderId) {
        ReminderId = reminderId;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
