
package com.parkeee.android.model.params;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;


public class StationDetailResponse implements Serializable{

    @SerializedName("GSId")
    @Expose
    private Integer gSId;
    @SerializedName("GSIdEnc")
    @Expose
    private String gSIdEnc;
    @SerializedName("BusinessName")
    @Expose
    private String businessName;
    @SerializedName("IsDefaultDeal")
    @Expose
    private Integer isDefaultDeal;
    @SerializedName("DefaultDealLikes")
    @Expose
    private Integer defaultDealLikes;
    @SerializedName("DefaultDealDisLikes")
    @Expose
    private Integer defaultDealDisLikes;
    @SerializedName("IsLikeDefaultDeal")
    @Expose
    private Integer isLikeDefaultDeal;
    @SerializedName("CoverImage")
    @Expose
    private String coverImage;
    @SerializedName("LogoPath")
    @Expose
    private String logoPath;
    @SerializedName("BrandName")
    @Expose
    private String brandName;
    @SerializedName("BusinessPhone")
    @Expose
    private String businessPhone;
    @SerializedName("AvgRating")
    @Expose
    private String avgRating;
    @SerializedName("Zip")
    @Expose
    private String zip;
    @SerializedName("Latitude")
    @Expose
    private String latitude;
    @SerializedName("Longitude")
    @Expose
    private String longitude;
    @SerializedName("Street")
    @Expose
    private String street;
    @SerializedName("PricePerGallonInDoller")
    @Expose
    private Double pricePerGallonInDoller;

    @SerializedName("LastUpdateOn")
    @Expose
    private String lastUpdateOn;
    @SerializedName("Timing")
    @Expose
    private String timing;
    @SerializedName("IsOpen")
    @Expose
    private String isOpen;
    @SerializedName("DayNo")
    @Expose
    private String dayNo;
    @SerializedName("City")
    @Expose
    private String city;
    @SerializedName("Amenities")
    @Expose
    private String amenities;
    @SerializedName("PackageName")
    @Expose
    private String packageName;
    @SerializedName("Stars")
    @Expose
    private String stars;
    @SerializedName("Review")
    @Expose
    private String review;
    @SerializedName("TotalReviews")
    @Expose
    private String totalReviews;
    @SerializedName("Deals")
    @Expose
    public List<Deal> deals = new ArrayList<Deal>();
    @SerializedName("Reviews")
    @Expose
    public List<Reviews> reviews = new ArrayList<Reviews>();
    @SerializedName("Images")
    @Expose
    public List<Object> images = new ArrayList<Object>();
    @SerializedName("Prices")
    @Expose
    public Price prices = new Price();


    public Double getPricePerGallonInDoller() {
        return pricePerGallonInDoller;
    }

    public void setPricePerGallonInDoller(Double pricePerGallonInDoller) {
        this.pricePerGallonInDoller = pricePerGallonInDoller;
    }


    /**
     *
     * @return
     *     The gSId
     */
    public Integer getGSId() {
        return gSId;
    }

    /**
     *
     * @param gSId
     *     The GSId
     */
    public void setGSId(Integer gSId) {
        this.gSId = gSId;
    }

    /**
     *
     * @return
     *     The gSIdEnc
     */
    public String getGSIdEnc() {
        return gSIdEnc;
    }

    /**
     *
     * @param gSIdEnc
     *     The GSIdEnc
     */
    public void setGSIdEnc(String gSIdEnc) {
        this.gSIdEnc = gSIdEnc;
    }

    /**
     *
     * @return
     *     The businessName
     */
    public String getBusinessName() {
        return businessName;
    }

    /**
     *
     * @param businessName
     *     The BusinessName
     */
    public void setBusinessName(String businessName) {
        this.businessName = businessName;
    }

    /**
     *
     * @return
     *     The isDefaultDeal
     */
    public Integer getIsDefaultDeal() {
        return isDefaultDeal;
    }

    /**
     *
     * @param isDefaultDeal
     *     The IsDefaultDeal
     */
    public void setIsDefaultDeal(Integer isDefaultDeal) {
        this.isDefaultDeal = isDefaultDeal;
    }

    /**
     *
     * @return
     *     The defaultDealLikes
     */
    public Integer getDefaultDealLikes() {
        return defaultDealLikes;
    }

    /**
     *
     * @param defaultDealLikes
     *     The DefaultDealLikes
     */
    public void setDefaultDealLikes(Integer defaultDealLikes) {
        this.defaultDealLikes = defaultDealLikes;
    }

    /**
     *
     * @return
     *     The defaultDealDisLikes
     */
    public Integer getDefaultDealDisLikes() {
        return defaultDealDisLikes;
    }

    /**
     *
     * @param defaultDealDisLikes
     *     The DefaultDealDisLikes
     */
    public void setDefaultDealDisLikes(Integer defaultDealDisLikes) {
        this.defaultDealDisLikes = defaultDealDisLikes;
    }

    /**
     *
     * @return
     *     The isLikeDefaultDeal
     */
    public Integer getIsLikeDefaultDeal() {
        return isLikeDefaultDeal;
    }

    /**
     *
     * @param isLikeDefaultDeal
     *     The IsLikeDefaultDeal
     */
    public void setIsLikeDefaultDeal(Integer isLikeDefaultDeal) {
        this.isLikeDefaultDeal = isLikeDefaultDeal;
    }

    /**
     *
     * @return
     *     The coverImage
     */
    public String getCoverImage() {
        return coverImage;
    }

    /**
     *
     * @param coverImage
     *     The CoverImage
     */
    public void setCoverImage(String coverImage) {
        this.coverImage = coverImage;
    }

    /**
     *
     * @return
     *     The logoPath
     */
    public String getLogoPath() {
        return logoPath;
    }

    /**
     *
     * @param logoPath
     *     The LogoPath
     */
    public void setLogoPath(String logoPath) {
        this.logoPath = logoPath;
    }

    /**
     *
     * @return
     *     The brandName
     */
    public String getBrandName() {
        return brandName;
    }

    /**
     *
     * @param brandName
     *     The BrandName
     */
    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    /**
     *
     * @return
     *     The businessPhone
     */
    public String getBusinessPhone() {
        return businessPhone;
    }

    /**
     *
     * @param businessPhone
     *     The BusinessPhone
     */
    public void setBusinessPhone(String businessPhone) {
        this.businessPhone = businessPhone;
    }

    /**
     *
     * @return
     *     The avgRating
     */
    public String getAvgRating() {
        return avgRating;
    }

    /**
     *
     * @param avgRating
     *     The AvgRating
     */
    public void setAvgRating(String avgRating) {
        this.avgRating = avgRating;
    }

    /**
     *
     * @return
     *     The zip
     */
    public String getZip() {
        return zip;
    }

    /**
     *
     * @param zip
     *     The Zip
     */
    public void setZip(String zip) {
        this.zip = zip;
    }

    /**
     *
     * @return
     *     The latitude
     */
    public String getLatitude() {
        return latitude;
    }

    /**
     *
     * @param latitude
     *     The Latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;

    }

    /**
     *
     * @return
     *     The longitude
     */
    public String getLongitude() {
        return longitude;
    }


    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     *     The street
     */
    public String getStreet() {
        return street;
    }

    /**
     *
     * @param street
     *     The Street
     */
    public void setStreet(String street) {
        this.street = street;
    }



    /**
     *
     * @return
     *     The lastUpdateOn
     */
    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    /**
     *
     * @param lastUpdateOn
     *     The LastUpdateOn
     */
    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    /**
     *
     * @return
     *     The timing
     */
    public String getTiming() {
        return timing;
    }

    /**
     *
     * @param timing
     *     The Timing
     */
    public void setTiming(String timing) {
        this.timing = timing;
    }

    /**
     *
     * @return
     *     The isOpen
     */
    public String getIsOpen() {
        return isOpen;
    }

    /**
     *
     * @param isOpen
     *     The IsOpen
     */
    public void setIsOpen(String isOpen) {
        this.isOpen = isOpen;
    }

    /**
     *
     * @return
     *     The dayNo
     */
    public String getDayNo() {
        return dayNo;
    }

    /**
     *
     * @param dayNo
     *     The DayNo
     */
    public void setDayNo(String dayNo) {
        this.dayNo = dayNo;
    }

    /**
     *
     * @return
     *     The city
     */
    public String getCity() {
        return city;
    }

    /**
     *
     * @param city
     *     The City
     */
    public void setCity(String city) {
        this.city = city;
    }

    /**
     *
     * @return
     *     The amenities
     */
    public String getAmenities() {
        return amenities;
    }

    /**
     *
     * @param amenities
     *     The Amenities
     */
    public void setAmenities(String amenities) {
        this.amenities = amenities;
    }

    /**
     *
     * @return
     *     The packageName
     */
    public String getPackageName() {
        return packageName;
    }

    /**
     *
     * @param packageName
     *     The PackageName
     */
    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    /**
     *
     * @return
     *     The stars
     */
    public String getStars() {
        return stars;
    }

    /**
     *
     * @param stars
     *     The Stars
     */
    public void setStars(String stars) {
        this.stars = stars;
    }

    /**
     *
     * @return
     *     The review
     */
    public String getReview() {
        return review;
    }

    /**
     *
     * @param review
     *     The Review
     */
    public void setReview(String review) {
        this.review = review;
    }

    /**
     *
     * @return
     *     The totalReviews
     */
    public String getTotalReviews() {
        return totalReviews;
    }

    /**
     *
     * @param totalReviews
     *     The TotalReviews
     */
    public void setTotalReviews(String totalReviews) {
        this.totalReviews = totalReviews;
    }

    /**
     *
     * @return
     *     The deals
     */
    public List<Deal> getDeals() {
        return deals;
    }

    /**
     *
     * @param deals
     *     The Deals
     */
    public void setDeals(List<Deal> deals) {
        this.deals = deals;
    }

    /**
     *
     * @return
     *     The reviews
     */
    public List<Reviews> getReviews() {
        return reviews;
    }

    /**
     *
     * @param reviews
     *     The Reviews
     */
    public void setReviews(List<Reviews> reviews) {
        this.reviews = reviews;
    }

    /**
     *
     * @return
     *     The images
     */
    public List<Object> getImages() {
        return images;
    }

    /**
     *
     * @param images
     *     The Images
     */
    public void setImages(List<Object> images) {
        this.images = images;
    }

    /**
     *
     * @return
     *     The prices
     */
    public Price getPrices() {
        return prices;
    }

    /**
     *
     * @param prices
     *     The Prices
     */
    public void setPrices(Price prices) {
        this.prices = prices;
    }

}
