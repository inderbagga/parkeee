package com.parkeee.android.model.params;

/**
 * Created by myoffice on 12/25/2016.
 */

public class GSFavourite {

    private String gsid;
    private String puid;

    public String getGsid() {
        return gsid;
    }

    public void setGsid(String gsid) {
        this.gsid = gsid;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
