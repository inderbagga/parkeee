package com.parkeee.android.model.params;

/**
 * Created by myoffice on 12/25/2016.
 */

public class SaveDeal {

    private String dealId;
    private String isLike;
    private String isDislike;
    private String isDefault;
    private String puid;

    public String getDealId() {
        return dealId;
    }

    public void setDealId(String dealId) {
        this.dealId = dealId;
    }

    public String getIsLike() {
        return isLike;
    }

    public void setIsLike(String isLike) {
        this.isLike = isLike;
    }

    public String getIsDislike() {
        return isDislike;
    }

    public void setIsDislike(String isDislike) {
        this.isDislike = isDislike;
    }

    public String getIsDefault() {
        return isDefault;
    }

    public void setIsDefault(String isDefault) {
        this.isDefault = isDefault;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
