
package com.parkeee.android.model.params;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;


public class Price implements Serializable{

    @SerializedName("RegularGasPrice")
    @Expose
    public Double regularGasPrice;
    @SerializedName("IsRegularGasAvailable")
    @Expose
    public Boolean isRegularGasAvailable;
    @SerializedName("IsDieselFuelAvailable")
    @Expose
    public Boolean isDieselFuelAvailable;
    @SerializedName("IsPremiumAvailable")
    @Expose
    public Boolean isPremiumAvailable;
    @SerializedName("IsMidgradeAvailable")
    @Expose
    public Boolean isMidgradeAvailable;
    @SerializedName("DieselFuelPrice")
    @Expose
    public Double dieselFuelPrice;
    @SerializedName("PremiumPrice")
    @Expose
    public Double premiumPrice;
    @SerializedName("MidgradePrice")
    @Expose
    public Double midgradePrice;

    /**
     * 
     * @return
     *     The regularGasPrice
     */
    public Double getRegularGasPrice() {
        return regularGasPrice;
    }

    /**
     * 
     * @param regularGasPrice
     *     The RegularGasPrice
     */
    public void setRegularGasPrice(Double regularGasPrice) {
        this.regularGasPrice = regularGasPrice;
    }

    /**
     * 
     * @return
     *     The isRegularGasAvailable
     */
    public Boolean getIsRegularGasAvailable() {
        return isRegularGasAvailable;
    }

    /**
     * 
     * @param isRegularGasAvailable
     *     The IsRegularGasAvailable
     */
    public void setIsRegularGasAvailable(Boolean isRegularGasAvailable) {
        this.isRegularGasAvailable = isRegularGasAvailable;
    }

    /**
     * 
     * @return
     *     The isDieselFuelAvailable
     */
    public Boolean getIsDieselFuelAvailable() {
        return isDieselFuelAvailable;
    }

    /**
     * 
     * @param isDieselFuelAvailable
     *     The IsDieselFuelAvailable
     */
    public void setIsDieselFuelAvailable(Boolean isDieselFuelAvailable) {
        this.isDieselFuelAvailable = isDieselFuelAvailable;
    }

    /**
     * 
     * @return
     *     The isPremiumAvailable
     */
    public Boolean getIsPremiumAvailable() {
        return isPremiumAvailable;
    }

    /**
     * 
     * @param isPremiumAvailable
     *     The IsPremiumAvailable
     */
    public void setIsPremiumAvailable(Boolean isPremiumAvailable) {
        this.isPremiumAvailable = isPremiumAvailable;
    }

    /**
     * 
     * @return
     *     The isMidgradeAvailable
     */
    public Boolean getIsMidgradeAvailable() {
        return isMidgradeAvailable;
    }

    /**
     * 
     * @param isMidgradeAvailable
     *     The IsMidgradeAvailable
     */
    public void setIsMidgradeAvailable(Boolean isMidgradeAvailable) {
        this.isMidgradeAvailable = isMidgradeAvailable;
    }

    /**
     * 
     * @return
     *     The dieselFuelPrice
     */
    public Double getDieselFuelPrice() {
        return dieselFuelPrice;
    }

    /**
     * 
     * @param dieselFuelPrice
     *     The DieselFuelPrice
     */
    public void setDieselFuelPrice(Double dieselFuelPrice) {
        this.dieselFuelPrice = dieselFuelPrice;
    }

    /**
     * 
     * @return
     *     The premiumPrice
     */
    public Double getPremiumPrice() {
        return premiumPrice;
    }

    /**
     * 
     * @param premiumPrice
     *     The PremiumPrice
     */
    public void setPremiumPrice(Double premiumPrice) {
        this.premiumPrice = premiumPrice;
    }

    /**
     * 
     * @return
     *     The midgradePrice
     */
    public Double getMidgradePrice() {
        return midgradePrice;
    }

    /**
     * 
     * @param midgradePrice
     *     The MidgradePrice
     */
    public void setMidgradePrice(Double midgradePrice) {
        this.midgradePrice = midgradePrice;
    }

}
