package com.parkeee.android.model.tables;

import io.realm.RealmObject;

/**
 * Created by inderbagga on 10/12/16.
 */
public class UserBillingInsertUpdateCache extends RealmObject {

    private String isRepeat;
    private String ReminderBefore;
    private String Bill_due_day_of_week;
    private String Bill_due_month;
    private String Bill_due_day;
    private String BillType;
    private String BillAmount;
    private String BillName;
    private String CategoryName;
    private String BillId;
    private String isNotiEnabled;
    private String puid;

    private String isINSDeleted;
    private String isINSUpdated;
    private String isINSUploaded;
    private long timeStamp;

    public String getIsINSDeleted() {
        return isINSDeleted;
    }

    public void setIsINSDeleted(String isINSDeleted) {
        this.isINSDeleted = isINSDeleted;
    }

    public String getIsINSUpdated() {
        return isINSUpdated;
    }

    public void setIsINSUpdated(String isINSUpdated) {
        this.isINSUpdated = isINSUpdated;
    }

    public String getIsINSUploaded() {
        return isINSUploaded;
    }

    public void setIsINSUploaded(String isINSUploaded) {
        this.isINSUploaded = isINSUploaded;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getIsRepeat() {
        return isRepeat;
    }

    public void setIsRepeat(String isRepeat) {
        this.isRepeat = isRepeat;
    }

    public String getReminderBefore() {
        return ReminderBefore;
    }

    public void setReminderBefore(String reminderBefore) {
        ReminderBefore = reminderBefore;
    }

    public String getBill_due_day_of_week() {
        return Bill_due_day_of_week;
    }

    public void setBill_due_day_of_week(String bill_due_day_of_week) {Bill_due_day_of_week = bill_due_day_of_week;}

    public String getBill_due_month() {
        return Bill_due_month;
    }

    public void setBill_due_month(String bill_due_month) {
        Bill_due_month = bill_due_month;
    }

    public String getBill_due_day() {
        return Bill_due_day;
    }

    public void setBill_due_day(String bill_due_day) {
        Bill_due_day = bill_due_day;
    }

    public String getBillType() {
        return BillType;
    }

    public void setBillType(String billType) {
        BillType = billType;
    }

    public String getBillAmount() {
        return BillAmount;
    }

    public void setBillAmount(String billAmount) {
        BillAmount = billAmount;
    }

    public String getBillName() {
        return BillName;
    }

    public void setBillName(String billName) {
        BillName = billName;
    }

    public String getCategoryName() {
        return CategoryName;
    }

    public void setCategoryName(String categoryName) {
        CategoryName = categoryName;
    }

    public String getBillId() {
        return BillId;
    }

    public void setBillId(String billId) {
        BillId = billId;
    }

    public String getIsNotiEnabled() {
        return isNotiEnabled;
    }

    public void setIsNotiEnabled(String isNotiEnabled) {
        this.isNotiEnabled = isNotiEnabled;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
