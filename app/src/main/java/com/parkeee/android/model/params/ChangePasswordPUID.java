package com.parkeee.android.model.params;

/**
 * Created by user on 15-12-2016.
 */

public class ChangePasswordPUID {
    private String password;
    private String puid;

    public String getPassword() {
        return password;
    }
    public void setPassword(String password) {
        this.password = password;
    }
    public String getPuid() {
        return puid;
    }
    public void setPuid(String puid) {
        this.puid = puid;
    }
}
