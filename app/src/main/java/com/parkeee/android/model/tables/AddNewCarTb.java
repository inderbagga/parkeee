package com.parkeee.android.model.tables;

import io.realm.RealmObject;

/**
 * Created by user on 16-12-2016.
 */

public class AddNewCarTb extends RealmObject {

    private String brand;
    private String color;
    private String id;
    private String inspectionDate;
    private String inspectionReminderBefore;
    private String isCarDeleted;
    private String isUploaded;
    private String plateNumber;
    private String registrationDate;
    private String registrationReminderBefore;
    private String regNumber;
    private String type;

    public String getBrand() {
        return brand;
    }

    public void setBrand(String brand) {
        this.brand = brand;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getRegNumber() {
        return regNumber;
    }

    public void setRegNumber(String regNumber) {
        this.regNumber = regNumber;
    }

    public String getRegistrationReminderBefore() {
        return registrationReminderBefore;
    }

    public void setRegistrationReminderBefore(String registrationReminderBefore) {
        this.registrationReminderBefore = registrationReminderBefore;
    }

    public String getRegistrationDate() {
        return registrationDate;
    }

    public void setRegistrationDate(String registrationDate) {
        this.registrationDate = registrationDate;
    }

    public String getPlateNumber() {
        return plateNumber;
    }

    public void setPlateNumber(String plateNumber) {
        this.plateNumber = plateNumber;
    }

    public String getIsUploaded() {
        return isUploaded;
    }

    public void setIsUploaded(String isUploaded) {
        this.isUploaded = isUploaded;
    }

    public String getIsCarDeleted() {
        return isCarDeleted;
    }

    public void setIsCarDeleted(String isCarDeleted) {
        this.isCarDeleted = isCarDeleted;
    }

    public String getInspectionReminderBefore() {
        return inspectionReminderBefore;
    }

    public void setInspectionReminderBefore(String inspectionReminderBefore) {
        this.inspectionReminderBefore = inspectionReminderBefore;
    }

    public String getInspectionDate() {
        return inspectionDate;
    }

    public void setInspectionDate(String inspectionDate) {
        this.inspectionDate = inspectionDate;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
