package com.parkeee.android.model.params;

/**
 * Created by user on 29-12-2016.
 */

public class ShareReferralCode {

    private String body;
    private String msisdn;

    public String getBody() {
        return body;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public String getMsisdn() {
        return msisdn;
    }

    public void setMsisdn(String msisdn) {
        this.msisdn = msisdn;
    }
}
