package com.parkeee.android.model.tables;

import io.realm.RealmObject;

/**
 * Created by inderbagga on 10/12/16.
 */

public class ViolationInsertUpdateCache extends RealmObject {

    private String  violation_id;
    private String  violation_type;
    private String violation_number;
    private String violation_state;
    private String violation_plate;
    private String violation_amount;
    private String violation_date;
    private String due_date;
    private String remind_before_time;
    private String remind_before_type;
    private String puid;
    private String vl_front_base64string;
    private String vl_back_base64string;
    private String isNotiEnabled;

    private String isINSDeleted;
    private String isINSUpdated;
    private String isINSUploaded;
    private long timeStamp;

    public String getIsINSDeleted() {
        return isINSDeleted;
    }

    public void setIsINSDeleted(String isINSDeleted) {
        this.isINSDeleted = isINSDeleted;
    }

    public String getIsINSUpdated() {
        return isINSUpdated;
    }

    public void setIsINSUpdated(String isINSUpdated) {
        this.isINSUpdated = isINSUpdated;
    }

    public String getIsINSUploaded() {
        return isINSUploaded;
    }

    public void setIsINSUploaded(String isINSUploaded) {
        this.isINSUploaded = isINSUploaded;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public void setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
    }

    public String getViolation_id() {
        return violation_id;
    }

    public void setViolation_id(String violation_id) {
        this.violation_id = violation_id;
    }

    public String getViolation_type() {
        return violation_type;
    }

    public void setViolation_type(String violation_type) {
        this.violation_type = violation_type;
    }

    public String getViolation_number() {
        return violation_number;
    }

    public void setViolation_number(String violation_number) {
        this.violation_number = violation_number;
    }

    public String getViolation_state() {
        return violation_state;
    }

    public void setViolation_state(String violation_state) {
        this.violation_state = violation_state;
    }

    public String getViolation_plate() {
        return violation_plate;
    }

    public void setViolation_plate(String violation_plate) {
        this.violation_plate = violation_plate;
    }

    public String getViolation_amount() {
        return violation_amount;
    }

    public void setViolation_amount(String violation_amount) {
        this.violation_amount = violation_amount;
    }

    public String getViolation_date() {
        return violation_date;
    }

    public void setViolation_date(String violation_date) {
        this.violation_date = violation_date;
    }

    public String getDue_date() {
        return due_date;
    }

    public void setDue_date(String due_date) {
        this.due_date = due_date;
    }

    public String getRemind_before_time() {
        return remind_before_time;
    }

    public void setRemind_before_time(String remind_before_time) {
        this.remind_before_time = remind_before_time;
    }

    public String getRemind_before_type() {
        return remind_before_type;
    }

    public void setRemind_before_type(String remind_before_type) {
        this.remind_before_type = remind_before_type;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }

    public String getVl_front_base64string() {
        return vl_front_base64string;
    }

    public void setVl_front_base64string(String vl_front_base64string) {
        this.vl_front_base64string = vl_front_base64string;
    }

    public String getVl_back_base64string() {
        return vl_back_base64string;
    }

    public void setVl_back_base64string(String vl_back_base64string) {
        this.vl_back_base64string = vl_back_base64string;
    }

    public String getIsNotiEnabled() {
        return isNotiEnabled;
    }

    public void setIsNotiEnabled(String isNotiEnabled) {
        this.isNotiEnabled = isNotiEnabled;
    }
}
