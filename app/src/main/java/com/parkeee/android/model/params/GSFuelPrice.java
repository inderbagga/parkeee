package com.parkeee.android.model.params;

/**
 * Created by user on 17-12-2016.
 */

public class GSFuelPrice {

    private String GSId;
    private String IsRegularGasAvailable;
    private String IsMidgradeAvailable;
    private String IsPremiumAvailable;
    private String IsDieselFuelAvailable;
    private String RegularGasPrice;
    private String MidgradePrice;
    private String PremiumPrice;
    private String DieselFuelPrice;

    public String getGSId() {
        return GSId;
    }

    public void setGSId(String GSId) {
        this.GSId = GSId;
    }

    public String getIsRegularGasAvailable() {
        return IsRegularGasAvailable;
    }

    public void setIsRegularGasAvailable(String isRegularGasAvailable) {
        IsRegularGasAvailable = isRegularGasAvailable;
    }

    public String getIsMidgradeAvailable() {
        return IsMidgradeAvailable;
    }

    public void setIsMidgradeAvailable(String isMidgradeAvailable) {
        IsMidgradeAvailable = isMidgradeAvailable;
    }

    public String getIsPremiumAvailable() {
        return IsPremiumAvailable;
    }

    public void setIsPremiumAvailable(String isPremiumAvailable) {
        IsPremiumAvailable = isPremiumAvailable;
    }

    public String getIsDieselFuelAvailable() {
        return IsDieselFuelAvailable;
    }

    public void setIsDieselFuelAvailable(String isDieselFuelAvailable) {
        IsDieselFuelAvailable = isDieselFuelAvailable;
    }

    public String getRegularGasPrice() {
        return RegularGasPrice;
    }

    public void setRegularGasPrice(String regularGasPrice) {
        RegularGasPrice = regularGasPrice;
    }

    public String getMidgradePrice() {
        return MidgradePrice;
    }

    public void setMidgradePrice(String midgradePrice) {
        MidgradePrice = midgradePrice;
    }

    public String getPremiumPrice() {
        return PremiumPrice;
    }

    public void setPremiumPrice(String premiumPrice) {
        PremiumPrice = premiumPrice;
    }

    public String getDieselFuelPrice() {
        return DieselFuelPrice;
    }

    public void setDieselFuelPrice(String dieselFuelPrice) {
        DieselFuelPrice = dieselFuelPrice;
    }
}
