package com.parkeee.android.model.params;

/**
 * Created by user on 23-12-2016.
 */

public class SubmitReview {

    private String GSId;
    private String Review;
    private String Stars;
    private String puid;

    public String getGSId() {
        return GSId;
    }

    public void setGSId(String GSId) {
        this.GSId = GSId;
    }

    public String getReview() {
        return Review;
    }

    public void setReview(String review) {
        Review = review;
    }

    public String getStars() {
        return Stars;
    }

    public void setStars(String stars) {
        Stars = stars;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
