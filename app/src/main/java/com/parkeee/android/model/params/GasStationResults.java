package com.parkeee.android.model.params;

/**
 * Created by user on 20-12-2016.
 */

public class GasStationResults {

    private String GSId;
    private String BrandName;
    private String Latitude;
    private String Longitude;
    private String Street;
    private Double PricePerGallonInDoller;
    private String PackageName;
    private String LastUpdateOn;
    private String CoffeeDeal;
    private String Zip;
    private String City;
    private Double Distance;

    public String getGSId() {
        return GSId;
    }

    public void setGSId(String GSId) {
        this.GSId = GSId;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public String getLatitude() {
        return Latitude;
    }

    public void setLatitude(String latitude) {
        Latitude = latitude;
    }

    public String getLongitude() {
        return Longitude;
    }

    public void setLongitude(String longitude) {
        Longitude = longitude;
    }

    public String getStreet() {
        return Street;
    }

    public void setStreet(String street) {
        Street = street;
    }

    public Double getPricePerGallonInDoller() {
        return PricePerGallonInDoller;
    }

    public void setPricePerGallonInDoller(Double pricePerGallonInDoller) {
        PricePerGallonInDoller = pricePerGallonInDoller;
    }

    public String getPackageName() {
        return PackageName;
    }

    public void setPackageName(String packageName) {
        PackageName = packageName;
    }

    public String getLastUpdateOn() {
        return LastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        LastUpdateOn = lastUpdateOn;
    }

    public String getCoffeeDeal() {
        return CoffeeDeal;
    }

    public void setCoffeeDeal(String coffeeDeal) {
        CoffeeDeal = coffeeDeal;
    }

    public String getZip() {
        return Zip;
    }

    public void setZip(String zip) {
        Zip = zip;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public Double getDistance() {
        return Distance;
    }

    public void setDistance(Double distance) {
        Distance = distance;
    }
}
