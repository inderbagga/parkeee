package com.parkeee.android.model.params;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by user on 23-12-2016.
 */

public class Reviews{

    private String puid;
    private String Stars;
    private String Review;
    private String puFirstName;
    private String puLastName;
    private String profile_pic;
    private String reviewId;
    private String submit_date;
    private String owner_response;
    private String response_date;
    private String replies_count;
    private String Minutes;
    private String Hours;
    private String Days;
    private String ResMinutes;
    private String ResHours;
    private String ResDays;
    public List<Reply> Replies = new ArrayList<Reply>();



    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }

    public String getStars() {
        return Stars;
    }

    public void setStars(String stars) {
        Stars = stars;
    }

    public String getReview() {
        return Review;
    }

    public void setReview(String review) {
        Review = review;
    }

    public String getPuFirstName() {
        return puFirstName;
    }

    public void setPuFirstName(String puFirstName) {
        this.puFirstName = puFirstName;
    }

    public String getPuLastName() {
        return puLastName;
    }

    public void setPuLastName(String puLastName) {
        this.puLastName = puLastName;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getSubmit_date() {
        return submit_date;
    }

    public void setSubmit_date(String submit_date) {
        this.submit_date = submit_date;
    }

    public String getOwner_response() {
        return owner_response;
    }

    public void setOwner_response(String owner_response) {
        this.owner_response = owner_response;
    }

    public String getResponse_date() {
        return response_date;
    }

    public void setResponse_date(String response_date) {
        this.response_date = response_date;
    }

    public String getReplies_count() {
        return replies_count;
    }

    public void setReplies_count(String replies_count) {
        this.replies_count = replies_count;
    }

    public String getMinutes() {
        return Minutes;
    }

    public void setMinutes(String minutes) {
        Minutes = minutes;
    }

    public String getHours() {
        return Hours;
    }

    public void setHours(String hours) {
        Hours = hours;
    }

    public String getDays() {
        return Days;
    }

    public void setDays(String days) {
        Days = days;
    }

    public String getResMinutes() {
        return ResMinutes;
    }

    public void setResMinutes(String resMinutes) {
        ResMinutes = resMinutes;
    }

    public String getResHours() {
        return ResHours;
    }

    public void setResHours(String resHours) {
        ResHours = resHours;
    }

    public String getResDays() {
        return ResDays;
    }

    public void setResDays(String resDays) {
        ResDays = resDays;
    }

    public List<Reply> getReplies() {
        return Replies;
    }

    public void setReplies(List<Reply> replies) {
        Replies = replies;
    }
}
