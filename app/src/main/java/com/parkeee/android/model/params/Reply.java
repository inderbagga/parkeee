package com.parkeee.android.model.params;

/**
 * Created by user on 23-12-2016.
 */

public class Reply {

    private String puid;
    private String ReplyId;
    private String ReviewReply;
    private String puFirstName;
    private String puLastName;
    private String profile_pic;
    private String reviewId;
    private String submit_date;
    private String Minutes;
    private String Hours;
    private String Days;

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }

    public String getReplyId() {
        return ReplyId;
    }

    public void setReplyId(String replyId) {
        ReplyId = replyId;
    }

    public String getReviewReply() {
        return ReviewReply;
    }

    public void setReviewReply(String reviewReply) {
        ReviewReply = reviewReply;
    }

    public String getPuFirstName() {
        return puFirstName;
    }

    public void setPuFirstName(String puFirstName) {
        this.puFirstName = puFirstName;
    }

    public String getPuLastName() {
        return puLastName;
    }

    public void setPuLastName(String puLastName) {
        this.puLastName = puLastName;
    }

    public String getProfile_pic() {
        return profile_pic;
    }

    public void setProfile_pic(String profile_pic) {
        this.profile_pic = profile_pic;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getSubmit_date() {
        return submit_date;
    }

    public void setSubmit_date(String submit_date) {
        this.submit_date = submit_date;
    }

    public String getMinutes() {
        return Minutes;
    }

    public void setMinutes(String minutes) {
        Minutes = minutes;
    }

    public String getHours() {
        return Hours;
    }

    public void setHours(String hours) {
        Hours = hours;
    }

    public String getDays() {
        return Days;
    }

    public void setDays(String days) {
        Days = days;
    }
}
