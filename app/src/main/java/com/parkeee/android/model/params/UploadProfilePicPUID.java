package com.parkeee.android.model.params;

/**
 * Created by user on 15-12-2016.
 */

public class UploadProfilePicPUID {
    private String puid;
    private String ImageBase64String;

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }

    public String getImageBase64String() {
        return ImageBase64String;
    }

    public void setImageBase64String(String imageBase64String) {
        ImageBase64String = imageBase64String;
    }


}
