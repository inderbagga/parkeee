package com.parkeee.android.model.params;

/**
 * Created by user on 17-12-2016.
 */

public class GSManagerLogin {

    private String GSId;
    private String PIN;

    private String ftid;
    private String FuelType;
    private String PricePerGallonInDoller;
    private String IsGasAvailable;

    public String getFtid() {
        return ftid;
    }

    public void setFtid(String ftid) {
        this.ftid = ftid;
    }

    public String getFuelType() {
        return FuelType;
    }

    public void setFuelType(String fuelType) {
        FuelType = fuelType;
    }

    public String getPricePerGallonInDoller() {
        return PricePerGallonInDoller;
    }

    public void setPricePerGallonInDoller(String pricePerGallonInDoller) {
        PricePerGallonInDoller = pricePerGallonInDoller;
    }

    public String getIsGasAvailable() {
        return IsGasAvailable;
    }

    public void setIsGasAvailable(String isGasAvailable) {
        IsGasAvailable = isGasAvailable;
    }

    public String getGSId() {
        return GSId;
    }

    public void setGSId(String GSId) {
        this.GSId = GSId;
    }

    public String getPIN() {
        return PIN;
    }

    public void setPIN(String PIN) {
        this.PIN = PIN;
    }
}
