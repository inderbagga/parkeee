
package com.parkeee.android.model.params;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;


public class Review {

    @SerializedName("puid")
    @Expose
    private String puid;
    @SerializedName("Stars")
    @Expose
    private String stars;
    @SerializedName("Review")
    @Expose
    private String review;
    @SerializedName("puFirstName")
    @Expose
    private String puFirstName;
    @SerializedName("puLastName")
    @Expose
    private String puLastName;

    /**
     * 
     * @return
     *     The puid
     */
    public String getPuid() {
        return puid;
    }

    /**
     * 
     * @param puid
     *     The puid
     */
    public void setPuid(String puid) {
        this.puid = puid;
    }

    /**
     * 
     * @return
     *     The stars
     */
    public String getStars() {
        return stars;
    }

    /**
     * 
     * @param stars
     *     The Stars
     */
    public void setStars(String stars) {
        this.stars = stars;
    }

    /**
     * 
     * @return
     *     The review
     */
    public String getReview() {
        return review;
    }

    /**
     * 
     * @param review
     *     The Review
     */
    public void setReview(String review) {
        this.review = review;
    }

    /**
     * 
     * @return
     *     The puFirstName
     */
    public String getPuFirstName() {
        return puFirstName;
    }

    /**
     * 
     * @param puFirstName
     *     The puFirstName
     */
    public void setPuFirstName(String puFirstName) {
        this.puFirstName = puFirstName;
    }

    /**
     * 
     * @return
     *     The puLastName
     */
    public String getPuLastName() {
        return puLastName;
    }

    /**
     * 
     * @param puLastName
     *     The puLastName
     */
    public void setPuLastName(String puLastName) {
        this.puLastName = puLastName;
    }

}
