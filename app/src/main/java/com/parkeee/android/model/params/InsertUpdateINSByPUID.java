package com.parkeee.android.model.params;

/**
 * Created by inderbagga on 10/12/16.
 */

public class InsertUpdateINSByPUID {

/*    {
        "car_plate": "BMW7894",
            "due_date_month": "5",
            "insurance_id": "6699FF211",
            "due_date_year": "0",
            "remind_before_type": "0",
            "expire_date": "07-24-2017",
            "InsServerId": "0",
            "company_name": "Auto-OwnerInsurance",
            "premium": "50",
            "payment_type": "2",
            "isNotiEnabled": "1",
            "due_date_day": "20",
            "remind_before": "3",
            "primary_driver": "Manik",
            "puid": "8396865563230"
    }*/

    private String car_plate;
    private String due_date_month;
    private String insurance_id;
    private String due_date_year;
    private String remind_before_type;
    private String expire_date;
    private String InsServerId;
    private String company_name;
    private String premium;
    private String payment_type;
    private String isNotiEnabled;
    private String due_date_day;
    private String remind_before;
    private String primary_driver;
    private String puid;

    public String getCar_plate() {
        return car_plate;
    }

    public void setCar_plate(String car_plate) {
        this.car_plate = car_plate;
    }

    public String getDue_date_month() {
        return due_date_month;
    }

    public void setDue_date_month(String due_date_month) {
        this.due_date_month = due_date_month;
    }

    public String getInsurance_id() {
        return insurance_id;
    }

    public void setInsurance_id(String insurance_id) {
        this.insurance_id = insurance_id;
    }

    public String getDue_date_year() {
        return due_date_year;
    }

    public void setDue_date_year(String due_date_year) {
        this.due_date_year = due_date_year;
    }

    public String getRemind_before_type() {
        return remind_before_type;
    }

    public void setRemind_before_type(String remind_before_type) {
        this.remind_before_type = remind_before_type;
    }

    public String getExpire_date() {
        return expire_date;
    }

    public void setExpire_date(String expire_date) {
        this.expire_date = expire_date;
    }

    public String getInsServerId() {
        return InsServerId;
    }

    public void setInsServerId(String insServerId) {
        InsServerId = insServerId;
    }

    public String getCompany_name() {
        return company_name;
    }

    public void setCompany_name(String company_name) {
        this.company_name = company_name;
    }

    public String getPremium() {
        return premium;
    }

    public void setPremium(String premium) {
        this.premium = premium;
    }

    public String getPayment_type() {
        return payment_type;
    }

    public void setPayment_type(String payment_type) {
        this.payment_type = payment_type;
    }

    public String getIsNotiEnabled() {
        return isNotiEnabled;
    }

    public void setIsNotiEnabled(String isNotiEnabled) {
        this.isNotiEnabled = isNotiEnabled;
    }

    public String getDue_date_day() {
        return due_date_day;
    }

    public void setDue_date_day(String due_date_day) {
        this.due_date_day = due_date_day;
    }

    public String getRemind_before() {
        return remind_before;
    }

    public void setRemind_before(String remind_before) {
        this.remind_before = remind_before;
    }

    public String getPrimary_driver() {
        return primary_driver;
    }

    public void setPrimary_driver(String primary_driver) {
        this.primary_driver = primary_driver;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
