package com.parkeee.android.model.params;

/**
 * Created by user on 23-12-2016.
 */

public class SubmitReviewReply {

    private String ReplyId;
    private String ReviewReply;
    private String reviewId;
    private String puid;

    public String getReplyId() {
        return ReplyId;
    }

    public void setReplyId(String replyId) {
        ReplyId = replyId;
    }

    public String getReviewReply() {
        return ReviewReply;
    }

    public void setReviewReply(String reviewReply) {
        ReviewReply = reviewReply;
    }

    public String getReviewId() {
        return reviewId;
    }

    public void setReviewId(String reviewId) {
        this.reviewId = reviewId;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }
}
