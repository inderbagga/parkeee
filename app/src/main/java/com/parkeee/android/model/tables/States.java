package com.parkeee.android.model.tables;

import io.realm.RealmObject;

/**
 * Created by user on 29-12-2016.
 */

public class States  extends RealmObject {

    private String StateName;

    public String getStateName() {
        return StateName;
    }

    public void setStateName(String stateName) {
        StateName = stateName;
    }
}
