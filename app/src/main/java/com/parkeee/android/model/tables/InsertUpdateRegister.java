package com.parkeee.android.model.tables;

import io.realm.RealmObject;

/**
 * Created by inderbagga on 10/12/16.
 */

public class InsertUpdateRegister extends RealmObject {

    private String firstname;
    private String lastname;
    private String mobile;
    private String postal_code;
    private String email;
    private String password;
    private String device_id;
    private String device_type;
    private String dob;
    private String address;
    private String address1;
    private String puCity;
    private String puCountry;
    private String puid;
    private String referal_code;
    private String reg_date;
    private String reward_points;
    private String nick_name;
    private String state;
    private String primary_card;
    private String isMobileVerified;
    private String isEmailVerified;
    private String imagePath;
    private String Referral_date;
    private String Keep_me_signed_in;
    private String LogOut;

    public String getLogOut() {
        return LogOut;
    }
    public void setLogOut(String logOut) {
        LogOut = logOut;
    }

    public String getKeep_me_signed_in() {
        return Keep_me_signed_in;
    }

    public void setKeep_me_signed_in(String keep_me_signed_in) {
        Keep_me_signed_in = keep_me_signed_in;
    }

    public String getReferral_date() {
        return Referral_date;
    }
    public void setReferral_date(String referral_date) {
        Referral_date = referral_date;
    }

    public String getIsMobileVerified() {
        return isMobileVerified;
    }

    public void setIsMobileVerified(String isMobileVerified) {
        this.isMobileVerified = isMobileVerified;
    }

    public String getIsEmailVerified() {
        return isEmailVerified;
    }

    public void setIsEmailVerified(String isEmailVerified) {
        this.isEmailVerified = isEmailVerified;
    }

    public String getImagePath() {
        return imagePath;
    }

    public void setImagePath(String imagePath) {
        this.imagePath = imagePath;
    }

    public String getPrimary_card() {
        return primary_card;
    }
    public void setPrimary_card(String primary_card) {
        this.primary_card = primary_card;
    }
    public String getNick_name() {
        return nick_name;
    }
    public void setNick_name(String nick_name) {
        this.nick_name = nick_name;
    }
    public String getState() {
        return state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getFirstname() {
        return firstname;
    }

    public void setFirstname(String firstname) {
        this.firstname = firstname;
    }

    public String getLastname() {
        return lastname;
    }

    public void setLastname(String lastname) {
        this.lastname = lastname;
    }

    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }

    public String getPostal_code() {
        return postal_code;
    }

    public void setPostal_code(String postal_code) {
        this.postal_code = postal_code;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDevice_type() {
        return device_type;
    }

    public void setDevice_type(String device_type) {
        this.device_type = device_type;
    }

    public String getDevice_id() {
        return device_id;
    }

    public void setDevice_id(String device_id) {
        this.device_id = device_id;
    }

    public String getDob() {
        return dob;
    }

    public void setDob(String dob) {
        this.dob = dob;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getAddress1() {
        return address1;
    }

    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    public String getPuCity() {
        return puCity;
    }

    public void setPuCity(String puCity) {
        this.puCity = puCity;
    }

    public String getPuCountry() {
        return puCountry;
    }

    public void setPuCountry(String puCountry) {
        this.puCountry = puCountry;
    }

    public String getPuid() {
        return puid;
    }

    public void setPuid(String puid) {
        this.puid = puid;
    }

    public String getReferal_code() {
        return referal_code;
    }

    public void setReferal_code(String referal_code) {
        this.referal_code = referal_code;
    }

    public String getReg_date() {
        return reg_date;
    }

    public void setReg_date(String reg_date) {
        this.reg_date = reg_date;
    }

    public String getReward_points() {
        return reward_points;
    }

    public void setReward_points(String reward_points) {
        this.reward_points = reward_points;
    }
}
