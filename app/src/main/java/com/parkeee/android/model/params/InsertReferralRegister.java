package com.parkeee.android.model.params;

/**
 * Created by user on 13-12-2016.
 */

public class InsertReferralRegister {
    private String ref_code;
    private String ref_type;
    private String puid;

    public String getPuid() {
        return puid;
    }
    public void setPuid(String puid) {
        this.puid = puid;
    }
    public String getRef_type() {
        return ref_type;
    }
    public void setRef_type(String ref_type) {
        this.ref_type = ref_type;
    }
    public String getRef_code() {
        return ref_code;
    }
    public void setRef_code(String ref_code) {
        this.ref_code = ref_code;
    }
}
