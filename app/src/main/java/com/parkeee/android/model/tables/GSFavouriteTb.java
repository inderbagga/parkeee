package com.parkeee.android.model.tables;

import io.realm.Realm;
import io.realm.RealmObject;

/**
 * Created by user on 26-12-2016.
 */

public class GSFavouriteTb extends RealmObject {

    private String gsID;
    private String brandName;
    private String lat;
    private String lng;
    private String street;
    private String city;
    private String zip;
    private String pricePer;
    private String coffeeDeal;
    private String lastUpdateOn;
    private String pkg;
    private String ftid;

    public String getFtid() {
        return ftid;
    }

    public void setFtid(String ftid) {
        this.ftid = ftid;
    }

    public String getGsID() {
        return gsID;
    }

    public void setGsID(String gsID) {
        this.gsID = gsID;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getLat() {
        return lat;
    }

    public void setLat(String lat) {
        this.lat = lat;
    }

    public String getLng() {
        return lng;
    }

    public void setLng(String lng) {
        this.lng = lng;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getZip() {
        return zip;
    }

    public void setZip(String zip) {
        this.zip = zip;
    }

    public String getPricePer() {
        return pricePer;
    }

    public void setPricePer(String pricePer) {
        this.pricePer = pricePer;
    }

    public String getCoffeeDeal() {
        return coffeeDeal;
    }

    public void setCoffeeDeal(String coffeeDeal) {
        this.coffeeDeal = coffeeDeal;
    }

    public String getLastUpdateOn() {
        return lastUpdateOn;
    }

    public void setLastUpdateOn(String lastUpdateOn) {
        this.lastUpdateOn = lastUpdateOn;
    }

    public String getPkg() {
        return pkg;
    }

    public void setPkg(String pkg) {
        this.pkg = pkg;
    }
}
