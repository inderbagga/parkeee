package com.parkeee.android;

import android.app.AlertDialog;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.support.design.widget.TextInputLayout;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.view.ContextThemeWrapper;
import android.text.Editable;
import android.text.Html;
import android.text.SpannableString;
import android.text.Spanned;
import android.text.TextWatcher;
import android.text.method.LinkMovementMethod;
import android.text.style.ClickableSpan;

import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.WindowManager;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;

import com.google.gson.Gson;
import com.parkeee.android.backend.API;
import com.parkeee.android.backend.OnResultReceived;
import com.parkeee.android.backend.RealmController;
import com.parkeee.android.backend.RequestSource;
import com.parkeee.android.model.params.InsertUpdateRegisterPUID;
import com.parkeee.android.model.params.SaveInbox;
import com.parkeee.android.model.tables.InsertUpdateRegister;
import com.parkeee.android.processor.PostApiClient;
import com.parkeee.android.utils.AppMessages;
import com.parkeee.android.utils.Utils;

import cn.pedant.SweetAlert.SweetAlertDialog;
import io.realm.Realm;

import android.provider.Settings.Secure;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by user on 25-11-2016.
 */

public class ActivityRegister extends AppCompatActivity implements View.OnClickListener, OnResultReceived {
    TextView text_tc,txt_custom_dialog;
    ImageView img_termsandcond, img_back;
    int termsandcond_value = 0;
    EditText et_PhoneNumber, et_FirstName, et_LastName, etzipcode, et_email_id, et_password, et_confirm_password;
    String a;
    int keyDel;
    Button btnRegister,btn_clear_all_register;
    private TextInputLayout inputLayoutName;
    Context mContext;
    OnResultReceived mOnResultReceived;
    Gson oGson;
    ProgressDialog mProgressDialog;
    String puid, referral_code, card_no, ServerResult;
    String formattedDate;
    private Realm realm;
    Dialog mdialog;
    Button btn_positive_custom_dailog;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
        setContentView(R.layout.activity_register);

        Initailize();
    }

    private void Initailize() {
        mdialog = new  Dialog(ActivityRegister.this,R.style.mStyleDialog);
        mdialog.getWindow().getAttributes().windowAnimations = R.style.DialogAnimation;
        mdialog.setContentView(R.layout.custom_dailog);
        mdialog.setCancelable(true);
        txt_custom_dialog=(TextView)mdialog.findViewById(R.id.txt_custom_dialog);
        btn_positive_custom_dailog=(Button) mdialog.findViewById(R.id.btn_positive_custom_dailog);
        btn_positive_custom_dailog.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mdialog.cancel();
            }
        });


        Calendar c = Calendar.getInstance();
        System.out.println("Current time => " + c.getTime());
        SimpleDateFormat df = new SimpleDateFormat("MM/dd/yyyy hh:mm:ss aa");
        formattedDate = df.format(c.getTime());

        if (formattedDate.contains("a.m.")) {
            formattedDate = formattedDate.replace("a.m.", "AM");
        } else if (formattedDate.contains("p.m.")) {
            formattedDate = formattedDate.replace("p.m.", "PM");
        }

        //get realm instance
        this.realm = RealmController.with(this).getRealm();

        oGson = new Gson();
        mOnResultReceived = this;
        mContext = ActivityRegister.this;
        inputLayoutName = (TextInputLayout) findViewById(R.id.input_layout_name);
        getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_STATE_HIDDEN);
        mProgressDialog = new ProgressDialog(this);
        mProgressDialog.setIndeterminate(true);
        mProgressDialog.setMessage("Please wait ...");

        btnRegister = (Button) findViewById(R.id.btnRegister);
        btnRegister.setOnClickListener(this);

        img_back = (ImageView) findViewById(R.id.img_back_reg);
        img_back.setOnClickListener(this);

        btn_clear_all_register=(Button)findViewById(R.id.btn_clear_all_register);
        btn_clear_all_register.setOnClickListener(this);

        text_tc = (TextView) findViewById(R.id.text_tc);

        img_termsandcond = (ImageView) findViewById(R.id.img_termsandcond);
        img_termsandcond.setOnClickListener(this);

        et_FirstName = (EditText) findViewById(R.id.etFirstName);
        et_LastName = (EditText) findViewById(R.id.etLastName);
        etzipcode = (EditText) findViewById(R.id.etzipcode);
        et_email_id = (EditText) findViewById(R.id.et_email_id);
        et_password = (EditText) findViewById(R.id.et_password);
        et_confirm_password = (EditText) findViewById(R.id.et_confirm_password);


        et_PhoneNumber = (EditText) findViewById(R.id.et_PhoneNumber);
        et_PhoneNumber.addTextChangedListener(new TextWatcher() {

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                boolean flag = true;
                String eachBlock[] = et_PhoneNumber.getText().toString().split("-");
                for (int i = 0; i < eachBlock.length; i++) {
                    if (eachBlock[i].length() > 3) {
                        Log.v("11111111111111111111", "cc" + flag + eachBlock[i].length());
                    }
                }
                if (flag) {
                    et_PhoneNumber.setOnKeyListener(new View.OnKeyListener() {

                        public boolean onKey(View v, int keyCode, KeyEvent event) {

                            if (keyCode == KeyEvent.KEYCODE_DEL)
                                keyDel = 1;
                            return false;
                        }
                    });

                    if (keyDel == 0) {
                        if (((et_PhoneNumber.getText().length() + 1) % 4) == 0) {
                            if (et_PhoneNumber.getText().toString().split("-").length <= 2) {
                                et_PhoneNumber.setText(et_PhoneNumber.getText() + "-");
                                et_PhoneNumber.setSelection(et_PhoneNumber.getText().length());
                            }
                        }
                        a = et_PhoneNumber.getText().toString();
                    } else {
                        a = et_PhoneNumber.getText().toString();
                        keyDel = 0;
                    }
                } else {
                    et_PhoneNumber.setText(a);
                }
            }

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {
            }

            public void afterTextChanged(Editable s) {
            }
        });

        SpannableString ss = new SpannableString(Html.fromHtml("I agree to Privacy Policy, EULA &amp; Terms &amp; Conditions"));
        Log.e("Length", "-----" + ss.length());
        ClickableSpan span1 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                newActivity(Utils.URL_PP, "Privacy Policy");
            }
        };
        ClickableSpan span2 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                newActivity(Utils.URL_EULA, "EULA");
            }
        };
        ClickableSpan span3 = new ClickableSpan() {
            @Override
            public void onClick(View textView) {
                newActivity(Utils.URL_TC, "Terms & Conditions");
            }
        };
        ss.setSpan(span1, 11, 25, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span2, 27, 31, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);
        ss.setSpan(span3, 34, 52, Spanned.SPAN_EXCLUSIVE_EXCLUSIVE);

        text_tc.setText(ss);
        text_tc.setMovementMethod(LinkMovementMethod.getInstance());
    }


    void newActivity(String url, String title) {
        Intent IWV = new Intent(this, WebViewActivity.class);
        IWV.putExtra("url", url);
        IWV.putExtra("title", title);
        startActivity(IWV);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()) {

            case R.id.img_termsandcond:

                if (termsandcond_value == 0) {
                    img_termsandcond.setImageResource(R.drawable.selected_checkbox);
                    termsandcond_value = 1;
                } else {
                    img_termsandcond.setImageResource(R.drawable.default_checkbox);
                    termsandcond_value = 0;
                }

                break;

            case R.id.btn_clear_all_register:
                et_confirm_password.setText("");
                et_password.setText("");
                et_email_id.setText("");
                etzipcode.setText("");
                et_FirstName.setText("");
                et_LastName.setText("");
                et_PhoneNumber.setText("");
                break;

            case R.id.img_back_reg:
                 this.finish();
                break;

            case R.id.btnRegister:
                int phone_number_length = et_PhoneNumber.length();
                phone_number_length = phone_number_length - 2;
                int zip_code_length = etzipcode.length();
                boolean is_email_valid = isValidEmail(et_email_id.getText().toString().trim());
                int password_length = et_password.length();
                if (et_FirstName.getText().toString().trim().equals("")) {
                    ErrorMessage("First name field can't be blank.");
                } else if (et_LastName.getText().toString().trim().equals("")) {
                    ErrorMessage("Last name field can't be blank.");
                } else if (et_PhoneNumber.getText().toString().trim().equals("")) {
                    ErrorMessage("Mobile field can't be blank.");
                } else if (phone_number_length < 10) {
                    ErrorMessage("Mobile number must be between 10-13 character.");
                } else if (etzipcode.getText().toString().trim().equals("")) {
                    ErrorMessage("Zip code field can't be blank.");
                } else if (zip_code_length < 5) {
                    ErrorMessage("Zip code must be between 5-6 character.");
                } else if (et_email_id.getText().toString().trim().equals("")) {
                    ErrorMessage("Email field can't be blank.");
                } else if (!is_email_valid) {
                    ErrorMessage("Please enter the valid email.");
                } else if (et_password.getText().toString().trim().equals("")) {
                    ErrorMessage("Password field can't be blank.");
                } else if (password_length < 8) {
                    ErrorMessage("Password must be between 8-20 character.");
                } else if (et_confirm_password.getText().toString().trim().equals("")) {
                    ErrorMessage("Confirm Password field can't be blank.");
                } else if (!et_password.getText().toString().trim().equals(et_confirm_password.getText().toString().trim())) {
                    ErrorMessage("Password and confirm password not matched.");
                } else if (termsandcond_value == 0) {
                    ErrorMessage(AppMessages.ACCEPT_TERM);
                } else {
                    String PhoneNumber = et_PhoneNumber.getText().toString().trim();
                    PhoneNumber = PhoneNumber.replace("-", "");
                    InsertUpdateRegisterPUID oInsertUpdateINSByPUID = new InsertUpdateRegisterPUID();
                    oInsertUpdateINSByPUID.setFirstname(et_FirstName.getText().toString().trim());
                    oInsertUpdateINSByPUID.setLastname(et_LastName.getText().toString().trim());
                    oInsertUpdateINSByPUID.setMobile("+1-" + PhoneNumber);
                    oInsertUpdateINSByPUID.setPostal_code(etzipcode.getText().toString().trim());
                    oInsertUpdateINSByPUID.setEmail(et_email_id.getText().toString().trim());
                    oInsertUpdateINSByPUID.setPassword(et_password.getText().toString().trim());
                    oInsertUpdateINSByPUID.setDevice_id(Secure.getString(mContext.getContentResolver(),
                            Secure.ANDROID_ID));
                    oInsertUpdateINSByPUID.setDevice_type("Android");
                    oInsertUpdateINSByPUID.setDob("08-26-1988");
                    oInsertUpdateINSByPUID.setAddress("");
                    oInsertUpdateINSByPUID.setAddress1("");
                    oInsertUpdateINSByPUID.setPuCity("");
                    oInsertUpdateINSByPUID.setPuCountry("");
                    oInsertUpdateINSByPUID.setPuid("0");
                    oInsertUpdateINSByPUID.setReferal_code("");
                    oInsertUpdateINSByPUID.setReg_date("");
                    oInsertUpdateINSByPUID.setReward_points("1");
                    String jsonString = oGson.toJson(oInsertUpdateINSByPUID, InsertUpdateRegisterPUID.class).toString();
                    mProgressDialog.show();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.InsertUpdateRegisterPUID);
                    oInsertUpdateApi.executePostRequest(API.fInsertUpdateRegisterPUID(), jsonString);
                }
                break;
            default:
                break;
        }
    }

    private void ErrorMessage(String content) {
        /*new SweetAlertDialog(this, SweetAlertDialog.WARNING_TYPE)
                .setTitleText("Alert")
                .setContentText(content)
                .setConfirmText("Ok")
                .show();*/
        txt_custom_dialog.setText(content);
        mdialog.show();
    }


    public final static boolean isValidEmail(CharSequence target) {
        if (target == null) {
            return false;
        } else {
            return android.util.Patterns.EMAIL_ADDRESS.matcher(target).matches();
        }
    }

    @Override
    public void dispatchString(RequestSource from, String what) {

        if (from.toString().equalsIgnoreCase("InsertUpdateRegisterPUID")) {
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    mProgressDialog.dismiss();
                    displayAlert("app error", false);
                    break;
                case "0":
                    mProgressDialog.dismiss();
                    displayAlert("Email id already registered.", false);
                    break;
                case "-1":
                    mProgressDialog.dismiss();
                    displayAlert("Sorry! The registration process failed due to some technical error. Please try after some time.", false);
                    break;
                default:
                    String res[] = ServerResult.split(";");
                    puid = res[0].trim();
                    referral_code = res[1].trim();
                    card_no = res[2].trim();
                    SharedPreferences.Editor editor1 = getSharedPreferences(
                            "RegisterResponse", MODE_PRIVATE).edit();
                    editor1.putString("puid", puid);
                    editor1.putString("referral_code", referral_code);
                    editor1.putString("card_no", card_no);

                    String Mobile_Number = et_PhoneNumber.getText().toString().trim();
                    Mobile_Number = Mobile_Number.replace("-", "");
                    Mobile_Number = "1-" + Mobile_Number;
                    editor1.putString("display_mobile_no", et_PhoneNumber.getText().toString().trim());
                    editor1.putString("mobile_no", Mobile_Number);
                    editor1.commit();

                    SaveInbox oSaveInbox = new SaveInbox();
                    oSaveInbox.setMsgID("0");
                    oSaveInbox.setTitle("Welcome");
                    oSaveInbox.setMessage("Thank you for joining us.");
                    oSaveInbox.setMsgDate(formattedDate);
                    oSaveInbox.setMsgType("WC");
                    oSaveInbox.setReminderDate("");
                    oSaveInbox.setReminderId("");
                    oSaveInbox.setPuid(puid);
                    String jsonString = oGson.toJson(oSaveInbox, SaveInbox.class).toString();

                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.SaveInboxMessage);
                    oInsertUpdateApi.executePostRequest(API.fInsertUpdateSaveInboxMessage(), jsonString);


                    break;
            }
        } else if (from.toString().equalsIgnoreCase("SaveInboxMessage")) {
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    mProgressDialog.dismiss();
                    displayAlert("app error", false);
                    break;
                case "-1":
                    mProgressDialog.dismiss();
                    displayAlert("Sorry! The registration process failed due to some technical error. Please try after some time.", false);
                    break;
                default:
                    SaveInbox oSaveInbox = new SaveInbox();
                    oSaveInbox.setMsgID("0");
                    oSaveInbox.setTitle("Referral");
                    oSaveInbox.setMessage(referral_code);
                    oSaveInbox.setMsgDate(formattedDate);
                    oSaveInbox.setMsgType("RC");
                    oSaveInbox.setReminderDate("");
                    oSaveInbox.setReminderId("");
                    oSaveInbox.setPuid(puid);
                    String jsonString = oGson.toJson(oSaveInbox, SaveInbox.class).toString();
                    //Post oInsertUpdateApi Api
                    PostApiClient oInsertUpdateApi = new PostApiClient(mOnResultReceived);
                    oInsertUpdateApi.setRequestSource(RequestSource.SaveInboxMessageRef);
                    oInsertUpdateApi.executePostRequest(API.fInsertUpdateSaveInboxMessage(), jsonString);

                    break;
            }
        } else {
            mProgressDialog.dismiss();
            ServerResult = what.replace("\"", "");
            switch (ServerResult) {
                case "-2":
                case "-3":
                    displayAlert("app error", false);
                    break;
                case "-1":
                    displayAlert("Sorry! The registration process failed due to some technical error. Please try after some time.", false);
                    break;
                default:
                    runOnUiThread(new Runnable() {
                        @Override
                        public void run() {
                            Realm realm1 = Realm.getDefaultInstance();
                            realm1.beginTransaction();
                            realm1.deleteAll();
                            realm1.commitTransaction();
                            realm1.close();


                            String PhoneNumber = et_PhoneNumber.getText().toString().trim();
                            PhoneNumber = PhoneNumber.replace("-", "");
                            InsertUpdateRegister oInsertUpdateRegister = new InsertUpdateRegister();
                            oInsertUpdateRegister.setFirstname(et_FirstName.getText().toString().trim());
                            oInsertUpdateRegister.setLastname(et_LastName.getText().toString().trim());
                            oInsertUpdateRegister.setMobile(PhoneNumber);
                            oInsertUpdateRegister.setPostal_code(etzipcode.getText().toString().trim());
                            oInsertUpdateRegister.setEmail(et_email_id.getText().toString().trim());
                            oInsertUpdateRegister.setPassword(et_password.getText().toString().trim());
                            oInsertUpdateRegister.setDevice_id(Secure.getString(mContext.getContentResolver(),
                                    Secure.ANDROID_ID));
                            oInsertUpdateRegister.setDevice_type("Android");
                            oInsertUpdateRegister.setDob("08-26-1988");
                            oInsertUpdateRegister.setAddress("");
                            oInsertUpdateRegister.setAddress1("");
                            oInsertUpdateRegister.setPuCity("");
                            oInsertUpdateRegister.setPuCountry("");
                            oInsertUpdateRegister.setPuid(puid);
                            oInsertUpdateRegister.setReferal_code(referral_code);
                            oInsertUpdateRegister.setReg_date("");
                            oInsertUpdateRegister.setPrimary_card(card_no);
                            oInsertUpdateRegister.setReward_points("0");
                            oInsertUpdateRegister.setNick_name("");
                            oInsertUpdateRegister.setImagePath("");
                            oInsertUpdateRegister.setIsEmailVerified("No");
                            oInsertUpdateRegister.setIsMobileVerified("No");
                            oInsertUpdateRegister.setReferral_date(formattedDate);
                            oInsertUpdateRegister.setKeep_me_signed_in("1");
                            oInsertUpdateRegister.setLogOut("0");
                            // Persist your data easily
                            realm.beginTransaction();
                            realm.copyToRealm(oInsertUpdateRegister);
                            realm.commitTransaction();
                            displayAlert("Registered Successfully", true);
                        }
                    });

                    break;
            }
        }
    }


    private void displayAlert(final String strMessage, final boolean is_successfully) {
        ActivityRegister.this.runOnUiThread(new Runnable() {
            @Override
            public void run() {
                AlertDialog.Builder exitDialog = new AlertDialog.Builder(mContext);
                exitDialog.setCancelable(true)
                        .setTitle(getString(R.string.app_name) + " says,")
                        .setCancelable(false)
                        .setMessage(strMessage)
                        .setNegativeButton(getString(R.string.action_ok), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                                if (is_successfully) {
                                    startActivity(new Intent(ActivityRegister.this, ActivityRegisterReferral.class));
                                }
                            }
                        })
                        .setOnCancelListener(new DialogInterface.OnCancelListener() {
                            @Override
                            public void onCancel(DialogInterface dialogInterface) {
                                dialogInterface.dismiss();
                            }
                        })
                        .setOnKeyListener(new DialogInterface.OnKeyListener() {
                            @Override
                            public boolean onKey(DialogInterface dialogInterface, int i, KeyEvent keyEvent) {

                                if (keyEvent.getKeyCode() == KeyEvent.KEYCODE_BACK) {
                                    dialogInterface.dismiss();
                                }
                                return true;
                            }
                        })
                        .show();
            }
        });
    }

    private void showToast(final String strMessage) {
        runOnUiThread(new Runnable() {
            public void run() {
                Toast.makeText(mContext, strMessage, Toast.LENGTH_LONG).show();
            }
        });
    }
}
