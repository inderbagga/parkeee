package com.parkeee.android;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

/**
 * Created by user on 08-12-2016.
 */

public class ActivityNewDeviceLogin extends Activity implements View.OnClickListener {

    Button btnCancel,btnContinue;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_device_login);
        InitializeInterface();
    }
    private void InitializeInterface() {
        btnCancel=(Button)findViewById(R.id.btnCancel_new_device_login);
        btnCancel.setOnClickListener(this);

        btnContinue=(Button)findViewById(R.id.btnContinue_new_device_login);
        btnContinue.setOnClickListener(this);
    }
    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnCancel_new_device_login:
                this.finish();
            case R.id.btnContinue_new_device_login:
                startActivity(new Intent(ActivityNewDeviceLogin.this,ActivitySync.class));
                break;
            default:
                break;
        }
    }
}
